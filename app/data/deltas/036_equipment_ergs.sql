--
-- Table structure for table `equipment_ergs`
--

CREATE TABLE IF NOT EXISTS `equipment_ergs` (
                                                `erg_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `whitelabel_id` int(11) NOT NULL,
                                                `identifier` varchar(100) NOT NULL,
                                                `make` varchar(100) NOT NULL,
                                                `model` varchar(100) NOT NULL,
                                                `year_purchased` int(11) NOT NULL,
                                                `location` varchar(100) NOT NULL,
                                                `insured` varchar(3) NOT NULL,
                                                `insured_value` varchar(10) NOT NULL,
                                                `status` varchar(20) NOT NULL,
                                                `created_at` int(11) NOT NULL,
                                                PRIMARY KEY (`erg_id`),
                                                KEY `fk_whitelabel_id_ergs` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_ergs`
--
ALTER TABLE `equipment_ergs`
    ADD CONSTRAINT `fk_whitelabel_id_ergs` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
