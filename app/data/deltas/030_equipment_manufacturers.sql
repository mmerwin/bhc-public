-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application
--
-- Table structure for table `equipment_manufacturers`
--

CREATE TABLE IF NOT EXISTS `equipment_manufacturers` (
                                                         `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
                                                         `type` varchar(50) NOT NULL,
                                                         `name` varchar(100) NOT NULL,
                                                         `store_url` text NOT NULL,
                                                         PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `equipment_manufacturers`
--

TRUNCATE TABLE `equipment_manufacturers`;
--
-- Dumping data for table `equipment_manufacturers`
--

INSERT IGNORE INTO `equipment_manufacturers` (`manufacturer_id`, `type`, `name`, `store_url`) VALUES
(1, 'Boat', 'Hudson', 'https://us.hudsonboatworks.com/'),
(2, 'Boat', 'Kaschper', 'https://www.kaschper.com/parts/'),
(3, 'Boat', 'Wintech', 'https://store.wintechracing.com/'),
(4, 'Boat', 'Vespoli', 'https://store.vespoli.com/'),
(5, 'Boat', 'King', 'https://store.wintechracing.com/'),
(6, 'Boat', 'Fluid', 'https://www.rowfluidesign.com/shop/'),
(7, 'Boat', 'Van Dusen', 'http://composite-eng.com/rowing-shells-van-dusen/shell-equipment-choices/shell-parts/'),
(8, 'Boat', 'Filippi', 'https://www.filippiboats.com/eng'),
(9, 'Boat', 'Alden', 'https://www.adirondackrowing.com/parts/'),
(10, 'Boat', 'Empacher', 'https://www.empachernorthamerica.com/links-and-information.html'),
(11, 'Boat', 'Sykes', 'https://www.sykesusa.com/collections/parts-store'),
(12, 'Boat', 'Stampfli', 'https://www.durhamboat.com/carbon-parts/'),
(13, 'Boat', 'Pocock', 'https://pocockparts.com/'),
(14, 'Boat', 'Resolute', 'https://www.resoluteracing.com/store'),
(15, 'Boat', 'Maas', 'https://www.maasboats.com/parts/'),
(16, 'Boat', 'Peinert', 'http://www.peinert.com/ordering.html'),
(17, 'Boat', 'Swift Racing', 'https://swiftracing.us/shop/'),
(18, 'Boat', 'Cucchietti', 'http://www.cucchiettiboats.net/'),
(19, 'Boat', 'Liangjin Boat', 'http://en.ljboat.com/'),
(20, 'Boat', 'Kanghua', 'https://www.kanghuaboats.com/');
COMMIT;
