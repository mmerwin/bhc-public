<?php


class DB_Equipment_Ergs extends DataModel
{
    protected $schema = "equipment_ergs";

    public function addErg($whitelabel_id, $identifier, $make, $model, $yearPurchased, $location, $insured, $insured_value)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, identifier, make, model, year_purchased, location, insured, insured_value, status, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'Available', ?)", array($whitelabel_id, $identifier, $make, $model, $yearPurchased, $location, $insured, $insured_value, time()));
    }

    public function getAllErgs($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getErg($whitelabel_id, $ergId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND erg_id = ? LIMIT 1", array($whitelabel_id, $ergId))->fetchArray();
    }

    public function deleteErg($whitelabel_id, $ergId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND erg_id = ? LIMIT 1", array($whitelabel_id, $ergId));
    }

    public function updateIdentifier($whitelabel_id, $ergId, $newIdentifier)
    {
        $this->DB->query("UPDATE {$this->schema} SET identifier = ? WHERE whitelabel_id = ? AND erg_id = ?", array($newIdentifier, $whitelabel_id, $ergId));
    }

    public function updateLocation($whitelabel_id, $ergId, $location)
    {
        $this->DB->query("UPDATE {$this->schema} SET location = ? WHERE whitelabel_id = ? AND erg_id = ?", array($location, $whitelabel_id, $ergId));
    }

    public function updateInsured($whitelabel_id, $ergId, $insured)
    {
        $this->DB->query("UPDATE {$this->schema} SET insured = ? WHERE whitelabel_id = ? AND erg_id = ?", array($insured, $whitelabel_id, $ergId));
    }

    public function updateInsuredValue($whitelabel_id, $ergId, $newValue)
    {
        $this->DB->query("UPDATE {$this->schema} SET insured_value = ? WHERE whitelabel_id = ? AND erg_id = ?", array($newValue, $whitelabel_id, $ergId));
    }

    public function updateStatus($whitelabel_id, $ergId, $status)
    {
        $this->DB->query("UPDATE {$this->schema} SET status = ? WHERE whitelabel_id = ? AND erg_id = ?", array($status, $whitelabel_id, $ergId));
    }
}