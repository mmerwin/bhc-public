--
-- Table structure for table `emergency_contact`
--

CREATE TABLE IF NOT EXISTS `emergency_contact` (
                                                   `emergency_contact_id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `custid` int(11) NOT NULL,
                                                   `fname` varchar(100) NOT NULL,
                                                   `lname` varchar(100) NOT NULL,
                                                   `phone` varchar(20) NOT NULL,
                                                   `relationship` varchar(100) NOT NULL,
                                                   `created_at` int(11) NOT NULL,
                                                   `last_updated` int(11) NOT NULL,
                                                   PRIMARY KEY (`emergency_contact_id`),
                                                   KEY `fk_custid_econtact` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emergency_contact`
--
ALTER TABLE `emergency_contact`
    ADD CONSTRAINT `fk_custid_econtact` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
