<?php


class Service_Lineups
{
    public function addToLineup($whitelabel_id, $practice_id, $custid, $boatId, $position)
    {
        //get boatDetails
        $boat = (new EquipmentModel($whitelabel_id))->getBoatById($boatId);
        $seat = self::validatePosition($position, $boat["boat_type"]);

        (new DB_Practice_Lineups())->addAthleteToLineup($whitelabel_id, $practice_id, $custid, $boatId, $seat);

    }

    public function removeFromLineup($whitelabel_id, $practice_id, $boatId, $position = null)
    {
        if (!is_null($position)) {
            (new DB_Practice_Lineups())->removePositionFromLineup($whitelabel_id, $practice_id, $boatId, $position);
        } else {
            (new DB_Practice_Lineups())->removeAllFromBoat($whitelabel_id, $practice_id, $boatId);
        }
    }

    public function removeUserFromLineup($whitelabel_id, $practice_id, $custid)
    {
        (new DB_Practice_Lineups())->removeAthleteFromLineup($whitelabel_id, $practice_id, $custid);
    }

    private function validatePosition($position, $hullType)
    {
        $returnError = json_encode(array("Error" => "Invalid position"));
        switch (strtolower($position)) {
            case "cox":
            case "coxswain":
            case "coxwain":
                return "coxswain";
                break;
            case "bow":
            case "1":
                return "1";
                break;
            case "stroke":
            case "stern":
                return $hullType;
                break;
            case "8":
                return (intval($hullType) == 8) ? "8" : die($returnError);
                break;
            case "7":
                return (intval($hullType) == 8) ? "7" : die($returnError);
                break;
            case "6":
                return (intval($hullType) == 8) ? "6" : die($returnError);
                break;
            case "5":
                return (intval($hullType) == 8) ? "5" : die($returnError);
                break;
            case "4":
                return (intval($hullType) == 8 || intval($hullType) == 4) ? "4" : die($returnError);
                break;
            case "3":
                return (intval($hullType) == 8 || intval($hullType) == 4) ? "3" : die($returnError);
                break;
            case "2":
                return (intval($hullType) == 8 || intval($hullType) == 4 || intval($hullType) == 2) ? "2" : die($returnError);
                break;
            default:
                die($returnError);
        }
    }

    public function markPublished($whitelabel_id, $practiceId)
    {
        (new DB_Practice_Schedule_Meta())->markLineupsSet($whitelabel_id, $practiceId);
    }

    public function submitPracticeSurvey($whitelabel_id, $practiceId, $custid, $lineup, $sessionplan, $equipment, $comments)
    {
        if ($equipment != "") {
            Service_Notification_Send::equipmentIssueFromSurvey($custid, $whitelabel_id, $equipment);
        }
        (new DB_Practice_Surveys())->addSurveyResponse($whitelabel_id, $practiceId, $custid, $lineup, $sessionplan, $equipment, $comments);
        (new DB_Practice_Lineups())->markSurveyCompleted($whitelabel_id, $practiceId, $custid);
    }

    public function getReportedIssuesByWhitelabel($whitelabel_id)
    {
        $issues =  (new DB_Practice_Surveys())->getSurveyResults($whitelabel_id);
        $returnable = [];
        foreach ($issues as $row) {
            if ($row["equipment_issues"] != '') {
                $returnable[] = array(
                    "whitelabel_id" => $row["whitelabel_id"],
                    "practice_id"   => $row["practice_id"],
                    "custid"        => $row["custid"],
                    "fname"         => $row["fname"],
                    "lname"         => $row["lname"],
                    "practice_name" => $row["practice_name"],
                    "practice_start"=> $row["practice_start_time"],
                    "practice_end"  => $row["practice_end_time"],
                    "reported_at"   => $row["timestamp"],
                    "issue"         => $row["equipment_issues"]
                );
            }
        }
        return $returnable;
    }

    public function getPracticeSurveyResults($whitelabel_id, $practiceId)
    {
        return (new DB_Practice_Surveys())->getSurveyResults($whitelabel_id, $practiceId);
    }


}