--
-- Table structure for table `usrowing_raw`
--

CREATE TABLE IF NOT EXISTS `usrowing_raw` (
                                              `member_id` int(11) NOT NULL,
                                              `fname` varchar(100) NOT NULL,
                                              `lname` varchar(100) NOT NULL,
                                              `email` varchar(360) NOT NULL,
                                              `gender` varchar(10) NOT NULL,
                                              `ltwt_status` varchar(100) NOT NULL,
                                              `birth_year` int(11) NOT NULL,
                                              `birth_month` int(11) NOT NULL,
                                              `birth_day` int(11) NOT NULL,
                                              `waiver_signed` varchar(10) NOT NULL,
                                              `waiver_expires` int(11) NOT NULL,
                                              `membership_expires` varchar(50) NOT NULL,
                                              `membership_title` varchar(100) NOT NULL,
                                              `rc_orgid` int(11) NOT NULL,
                                              `rc_participant_id` int(11) NOT NULL,
                                              `last_updated` int(11) NOT NULL,
                                              PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;