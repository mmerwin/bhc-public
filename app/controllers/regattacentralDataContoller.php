<?php
ini_set("memory_limit", "-1");
set_time_limit(0);

class regattacentralDataContoller extends Controller
{
    public function rowstatsGetRegattaById()
    {
        $regattaId      = $this->param("regatta_id");
        $regattaCentral = new Integrations_RegattaCentral();
        $data           = json_decode($regattaCentral->getBulkRegattaDataById($regattaId), true);

        (new Service_Rowstats_Data_Collection())->addNewRegatta($data["data"]);
        //$this->view("json", $data);
    }

    public function rowstatsScrapeFromTable()
    {
        $regattaCentral = new Integrations_RegattaCentral();
        $scraperTable   = (new DB_Regattacentral_regatta_scraper());
        $unscraped      = $scraperTable->getUnscraped(15);

        $lastRefresh    = time();
        foreach ($unscraped as $row) {
            // verify token is still valid and refresh it if more than 15 minutes old
            if ($lastRefresh < (time() - (60 * 15))) {
                $regattaCentral->refreshApiToken();
                $lastRefresh = time();
                echo "refreshed API token, new token is {$regattaCentral->token} \n";
            }
            echo "starting: {$row["regatta_id"]} \n";
            $data = json_decode($regattaCentral->getBulkRegattaDataById($row["regatta_id"]), true);
            if (isset($data["data"])) {
                (new Service_Rowstats_Data_Collection())->addNewRegatta($data["data"]);
            }
            $scraperTable->updateAsScraped($row["regatta_id"]);
        }
    }

    public function massAddScraper()
    {
        $scraperTable   = (new DB_Regattacentral_regatta_scraper());
        $counter        = 5878;
        $max            = 6378;
        while ($counter < $max) {
            $scraperTable->addRegattaId($counter);
            $counter++;
        }
    }

}