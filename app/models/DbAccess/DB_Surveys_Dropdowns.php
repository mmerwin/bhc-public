<?php


class DB_Surveys_Dropdowns extends DataModel
{
    protected $schema = "surveys_dropdowns";

    public function addDropdownData($surveyId, $questionId, array $optionsArray)
    {
        $this->DB->query("INSERT INTO {$this->schema} (survey_id, question_id, 
                   selection_1, selection_2, selection_3, selection_4, selection_5, 
                   selection_6, selection_7, selection_8, selection_9, selection_10,
                   selection_11, selection_12, selection_13, selection_14, selection_15,         
                   selection_16, selection_17, selection_18, selection_19, selection_20)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($surveyId, $questionId,
                $optionsArray["selection_1"], $optionsArray["selection_2"], $optionsArray["selection_3"], $optionsArray["selection_4"], $optionsArray["selection_5"],
                $optionsArray["selection_6"], $optionsArray["selection_7"], $optionsArray["selection_8"], $optionsArray["selection_9"], $optionsArray["selection_10"],
                $optionsArray["selection_11"], $optionsArray["selection_12"], $optionsArray["selection_13"], $optionsArray["selection_14"], $optionsArray["selection_15"],
                $optionsArray["selection_16"], $optionsArray["selection_17"], $optionsArray["selection_18"], $optionsArray["selection_19"], $optionsArray["selection_20"]));
    }

    public function getByQuestionId($surveyId, $questionId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE survey_id = ? AND question_id = ? LIMIT 1", array($surveyId, $questionId))->fetchArray();
    }

    public function deleteAllBySurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

}