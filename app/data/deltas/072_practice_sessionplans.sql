--
-- Table structure for table `practice_sessionplans`
--

CREATE TABLE IF NOT EXISTS `practice_sessionplans` (
                                                       `practice_id` int(11) NOT NULL,
                                                       `whitelabel_id` int(11) NOT NULL,
                                                       `session_plan` longtext NOT NULL,
                                                       `last_updated` int(11) NOT NULL,
                                                       PRIMARY KEY (`practice_id`),
                                                       KEY `fk_whitelabel_id_practice_sessionplans` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `practice_sessionplans`
--
ALTER TABLE `practice_sessionplans`
    ADD CONSTRAINT `fk_practice_id_practice_sessionplans` FOREIGN KEY (`practice_id`) REFERENCES `practice_schedule_meta` (`practice_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_practice_sessionplans` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
