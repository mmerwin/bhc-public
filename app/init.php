<?php
spl_autoload_register(function ($class) {
    $path = '../app/models/DbAccess/' . ucfirst($class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
    $path2 = '../app/models/' . ucfirst($class) . '.php';
    if (file_exists($path2)) {
        require_once $path2;
    }
    $path3 = '../app/models/Service/' . ucfirst($class) . '.php';
    if (file_exists($path3)) {
        require_once $path3;
    }

    $path4 = '../app/models/Integrations/' . ucfirst($class) . '.php';
    if (file_exists($path4)) {
        require_once $path4;
    }

});
require_once '../app/core/App.php';
require_once '../app/core/Controller.php';
require_once '../app/core/View.php';
require_once (dirname(__FILE__).'/vendor/autoload.php');

?>
