<?php


class DB_Stripe_Cards extends DataModel
{
    protected $schema = "stripe_cards";

    public function addCard($id, $object, $addressCity, $addressCountry, $addressLine1,
                            $addressLine1Check, $addressLine2, $addressState, $addressZip, $addressZipCheck,
                            $brand, $country, $customer, $cvcCheck, $dynamicLast4,
                            $expMonth, $expYear, $fingerprint, $funding, $last4,
                            $name, $tokenizationMethod, $metadata)
    {
        $this->DB->query("INSERT INTO stripe_cards (id, object, address_city, address_country, address_line1, 
                                                        address_line1_check, address_line2, address_state, address_zip, address_zip_check, 
                                                        brand, country, customer, cvc_check, dynamic_last4, 
                                                        exp_month, exp_year, fingerprint, funding, last4, 
                                                        name, tokenization_method, metadata) 
                          VALUES (?, ?, ?, ?, ?, 
                                  ?, ? ,?, ?, ?, 
                                  ?, ?, ?, ?, ?, 
                                  ?, ?, ?, ?, ?, 
                                  ?, ?, ?)
                                ON DUPLICATE KEY UPDATE
                                object = VALUES(`object`),  address_city= VALUES(`address_city`),  address_country= VALUES(`address_country`),  address_line1= VALUES(`address_line1`), address_line1_check= VALUES(`address_line1_check`),
                                address_line2= VALUES(`address_line2`),  address_state= VALUES(`address_state`), address_zip= VALUES(`address_zip`),  address_zip_check= VALUES(`address_zip_check`),  brand= VALUES(`brand`),  
                                country= VALUES(`country`), customer= VALUES(`customer`),  cvc_check= VALUES(`cvc_check`),  dynamic_last4= VALUES(`dynamic_last4`),  exp_month= VALUES(`exp_month`),  
                                exp_year= VALUES(`exp_year`),  fingerprint= VALUES(`fingerprint`),  funding= VALUES(`funding`),  last4= VALUES(`last4`),  name= VALUES(`name`),  
                                tokenization_method= VALUES(`tokenization_method`),  metadata= VALUES(`metadata`)",
            array($id, $object, $addressCity, $addressCountry, $addressLine1,
                $addressLine1Check, $addressLine2, $addressState, $addressZip, $addressZipCheck,
                $brand, $country, $customer, $cvcCheck, $dynamicLast4,
                $expMonth, $expYear, $fingerprint, $funding, $last4,
                $name, $tokenizationMethod, $metadata));
        }

        public function getCard($id)
        {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE id = ? LIMIT 1")->fetchArray($id);
        }

}