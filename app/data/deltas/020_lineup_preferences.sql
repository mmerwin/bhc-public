
-- Table structure for table `lineup_preferences`
--

CREATE TABLE IF NOT EXISTS `lineup_preferences` (
                                                    `custid` int(11) NOT NULL,
                                                    `port` int(11) NOT NULL,
                                                    `starboard` int(11) NOT NULL,
                                                    `sculling` int(11) NOT NULL,
                                                    `coxwain` int(11) NOT NULL,
                                                    `last_updated` int(11) NOT NULL,
                                                    PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lineup_preferences`
--
ALTER TABLE `lineup_preferences`
    ADD CONSTRAINT `fk_custid_lineup_preferences` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
