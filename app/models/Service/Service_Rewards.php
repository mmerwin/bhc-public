<?php


class Service_Rewards
{
    private $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function getRewardData()
    {
        $history        = self::getRewardHistory("whitelabel");
        $redemptions    = self::getRedemptions();
        $prizes         = self::getRewardPrizes();
        $pointBalance   = self::calculatePointBalance($history, $redemptions);
        return [
            "balance"       => $pointBalance,
            'next_reward'   => self::findNextPrize($pointBalance, $prizes),
            "prizes"        => self::getRewardPrizes(),
            "triggers"      => self::getTriggers(),
            "redemptions"   => $redemptions,
            "history"       => $history
        ];
    }

    public function getRewardPrizes()
    {
        return (new DB_Reward_Prizes())->getPrizes();
    }

    public function getRewardHistory($entity, $custid=0)
    {
        if ($entity == "user") {
            return (new DB_Reward_History())->getCustidHistory($custid, $this->whitelabel_id);
        } else if ($entity == 'whitelabel') {
            return (new DB_Reward_History())->getWhitelabelHistory($this->whitelabel_id);
        }

    }

    public function getRedemptions()
    {
        return (new DB_Reward_Redemptions())->getWhitelabelRedemptions($this->whitelabel_id);
    }

    public function calculatePointBalance($history, $redemptions)
    {
        $pointsEarned = 0;
        foreach ($history as $row) {
            $pointsEarned+= $row['points_earned'];
        }
        $pointsSpent = 0;
        foreach ($redemptions as $row) {
            $pointsSpent+= $row['points'];
        }
        return ($pointsEarned - $pointsSpent);
    }

    public function getTriggers()
    {
        return (new DB_Reward_Triggers())->getActiveTriggers();
    }

    public function findNextPrize($points, $rewards)
    {
        foreach ($rewards as $prize) {
            if ($points <= $prize['points']) {
                $prize['points_left'] = ($prize['points'] - $points);
                return $prize;
            }
        }
    }

    public function checkPointEligibility($triggerId, $custid, $whitelabel_id)
    {
        $trigger = (new DB_Reward_Triggers())->getTrigger($triggerId);
        if ($trigger['status'] != "Active") {
            return false;
        } else {
            // get last occurrence of trigger
            if ($trigger['entity'] == 'user') {
                $lastOccurrence = (new DB_Reward_History())->getLastTriggerTimeByCustid($whitelabel_id, $custid, $triggerId);
            } else {
                $lastOccurrence = (new DB_Reward_History())->getLastTriggerTimeByWhitelabel($whitelabel_id, $triggerId);
            }

            // check if eligible
            if ($trigger['frequency'] == 0 && isset($lastOccurrence['timestamp'])) {
                return false;
            } else if (!isset($lastOccurrence['timestamp'])) {
                return true;
            } else if ($trigger['frequency'] != 0 && isset($lastOccurrence['timestamp']) && time()> ($lastOccurrence['timestamp'] + $trigger['frequency'])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function addRewardPoints($triggerId, $custid)
    {
        if ($triggerId == 4 || $triggerId == 5 || $triggerId == 6 || $triggerId == 13) {
            // check for each whitelabel the user is affiliated with
            $affiliations = Service_Affiliations::findAllUserAffiliations($custid);
            foreach ($affiliations as $wl) {
                self::addPoints($triggerId, $custid, $wl['whitelabel_id']);
            }
        } else {
            self::addPoints($triggerId, $custid, $this->whitelabel_id);
        }
    }

    private function addPoints($triggerId, $custid, $whitelabel_id)
    {
        // check point eligibility
        if (self::checkPointEligibility($triggerId, $custid, $whitelabel_id)) {
            // get point value
            $trigger = (new DB_Reward_Triggers())->getTrigger($triggerId);
            $points  = $trigger['points'];

            // add points to reward history
            (new DB_Reward_History())->add($custid, $whitelabel_id, $triggerId, $points);
        }
    }

    public function getPointsEarnedByAllWhitelabels()
    {
        $earned   = (new DB_Reward_History())->sumAllPointsInTable();
        $redeemed = (new DB_Reward_Redemptions())->sumAllPointsInTable();

        return [
            "earned"    => ($earned['earned']+400),
            "redeemed"  => $redeemed['redeemed']
        ];
    }

}