<?php


class Service_ExternalMessages
{

    public function getNewMessageId($whitelabel_id, $custid)
    {
        return (new DB_External_Messages())->newMessage($whitelabel_id, $custid);
    }

    public function updateScheduledSendTime($messageId, $whitelabel_id, $scheduledTime)
    {
        // verify that the message belongs to the whitelabel
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages())->updateScheduledTime($messageId, $scheduledTime);
        }

    }

    public function getGroupsInMessage($messageId)
    {
        return (new DB_External_Messages_Groups())->getGroupsById($messageId);
    }

    public function addGroupToMessage($messageId, $whitelabel_id, $groupId)
    {
        // verify that the message belongs to the whitelabel
        $message     = self::getMessageById($messageId, $whitelabel_id);
        $groupExists = (new Groups($whitelabel_id))->checkIfGroupExists($groupId);
        if ($message['whitelabel_id'] == $whitelabel_id && $groupExists) {
            (new DB_External_Messages_Groups())->AddGroupWithoutStakeholder($messageId, $groupId, $whitelabel_id);
            self::setApprovalRequired($messageId, $message['custid'], $whitelabel_id);
        }
    }

    public function addStakeholdersToMessage($messageId, $whitelabel_id, $groupId)
    {
        // verify that the message belongs to the whitelabel
        $message     = self::getMessageById($messageId, $whitelabel_id);
        $groupExists = (new Groups($whitelabel_id))->checkIfGroupExists($groupId);
        if ($message['whitelabel_id'] == $whitelabel_id && $groupExists) {
            (new DB_External_Messages_Groups())->AddGroupWithStakeholder($messageId, $groupId, $whitelabel_id);
            self::setApprovalRequired($messageId, $message['custid'], $whitelabel_id);
        }
    }

    public function removeGroupFromMessage($messageId, $whitelabel_id, $groupId)
    {
        // verify that the message belongs to the whitelabel
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages_Groups())->removeGroup($messageId, $groupId, $whitelabel_id);
            self::setApprovalRequired($messageId, $message['custid'], $whitelabel_id);
        }
    }

    public function removeStakeholderGroupFromMessage($messageId, $whitelabel_id, $groupId)
    {
        // verify that the message belongs to the whitelabel
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages_Groups())->removeStakeholderGroup($messageId, $groupId, $whitelabel_id);
            self::setApprovalRequired($messageId, $message['custid'], $whitelabel_id);
        }
    }

    public function getMessageById($messageId, $whitelabel_id)
    {
        $message =  (new DB_External_Messages())->getDetailsByMessageId($messageId);
        if ($message && $message['whitelabel_id'] == $whitelabel_id) {
            // get groups
            $groups = self::getGroupsInMessage($messageId);
            // get recipients list
            if ($message["sent_time"] > 0)  {
                $sentStatus = true;
            } else {
                $sentStatus = false;
            }
            $recipients = self::getActiveSenderList($messageId, $message['type'], $sentStatus);

            $message['groups']      = $groups;
            $message['sender_list'] = $recipients;

            return $message;
        } else {
            return array();
        }
    }

    public function changeMessageToEmail($messageId, $whitelabel_id)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages())->changeMessageType($messageId, "Email");
        }
    }
    public function changeMessageToText($messageId, $whitelabel_id)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages())->changeMessageType($messageId, "Text");
        }
    }

    private function setApprovalRequired($messageId, $custid, $whitelabel_id)
    {
        // check if message needs approval
        if (self::checkIfApprovalRequired($messageId, $custid, $whitelabel_id)) {
            (new DB_External_Messages())->updateApprovalRequired($messageId, "Yes");
            return true;
        } else {
            (new DB_External_Messages())->updateApprovalRequired($messageId, "No");
            return false;
        }
    }

    public function updateSubjectLine($messageId, $whitelabel_id, $subject)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            (new DB_External_Messages())->updateSubject($messageId, $subject);
        }
    }

    public function updateMessageBody($messageId, $whitelabel_id, $body)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            $externalMessages = new DB_External_Messages();
            $externalMessages->updateBody($messageId, $body);

        }
    }

    private function checkIfApprovalRequired($messageId, $custid, $whitelabel_id)
    {
        // get all permissions for user
        $permissions        = Service_Permissions::getAllPermissionsForUser($custid,$whitelabel_id);
        $requiresApproval   = true;
        $hasPermission13    = false;
        $inAllGroups        = true;
        foreach ($permissions as $row) {
            if ($row['permission_id'] == 12 || $row['permission_id'] == 14) {
                $requiresApproval = false;
            }
            if ($row['permission_id'] == 13) {
                $hasPermission13 = true;
            }
        }

        if ($requiresApproval && $hasPermission13) {
            // check if user has permission 13 AND is only sending to member groups
            $groupsInMessage = self::getGroupsInMessage($messageId);
            foreach ($groupsInMessage as $groups) {
                if (is_null($groups['sender_is_member'])) {
                    $inAllGroups = false;
                }
            }
        }
        if (!$requiresApproval || ($inAllGroups && $hasPermission13)) {
            return false;
        } else {
            return true;
        }
    }

    public function setReadyToSend($messageId, $whitelabel_id)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            // check if approval is required
            $requiresApproval = self::setApprovalRequired($messageId, $message['custid'], $whitelabel_id);
            if ($requiresApproval) {
                Service_Notification_Send::newMessageRequiringApproval($message['custid'],$whitelabel_id, $message['type']);
            }

            // mark ready for sending
            $externalMessages = new DB_External_Messages();
            $externalMessages->setReadyToSend($messageId);
        }
    }

    public function getPendingMessages($whitelabel_id, $type, $approvalRequired)
    {
        $DbExternalMessages       = new DB_External_Messages();
        $DbExternalMessagesGroups = new DB_External_Messages_Groups();

        // Get all messages matching approval and type
        $allMessagesRaw      = $DbExternalMessages->getAllMessages($whitelabel_id);
        $allMessageGroupsRaw = $DbExternalMessagesGroups->getAllByWhitelabel($whitelabel_id);

        $data = [];
        // add messages that match search terms
        foreach ($allMessagesRaw as $messages) {
            if ($messages['sent_time'] == 0 && ($messages["type"] == $type || $type == 'All') && ($messages['approval_required'] == $approvalRequired || $approvalRequired == 'All') && $messages['ready_to_send'] == 'Yes' && $messages['approved_by'] == 0)  {
                //add message to $data array
                $data[] = $messages;
            }
        }

        // verify permissions required to send message
        foreach ($data as $key=>$messages) {
            if ($messages['approval_required'] == '') {
                // check approval required
                $approvalRequired = self::checkIfApprovalRequired($messages['message_id'], $messages['custid'], $messages['whitelabel_id']);
                if ($approvalRequired) {
                    (new DB_External_Messages())->updateApprovalRequired($messages['message_id'], "Yes");
                    $data[$key]['approval_required'] = "Yes";
                } else {
                    (new DB_External_Messages())->updateApprovalRequired($messages['message_id'], "No");
                    $data[$key]['approval_required'] = "No";
                }
            }
        }

        // add group data and senders list to messages found in first search
        foreach ($data as $key => $messages) {

            $data[$key]['groups']       = [];
            $data[$key]['sender_list']  = self::getActiveSenderList($messages['message_id'], $messages['type']);
            // find groups and add to array
            foreach ($allMessageGroupsRaw as $groupKey=>$group) {
                // set nulls for "sender_is_member" to "No"
                if (is_null($group['sender_is_member'])) {
                    $allMessageGroupsRaw[$groupKey]["sender_is_member"] = "No";
                }
                if ($group['message_id'] == $messages['message_id']) {
                    $data[$key]['groups'][] = $group;
                }
            }
        }

        return $data;
    }

    public function getActiveSenderList($messageId, $type, $sent = false)
    {
        if ($type == 'Text') {
            if ($sent) {
                $results = (new DB_External_Messages_Recipients())->getSentRecipients($messageId, 'phone_number');
                foreach ($results as $key=> $row) {
                    $results[$key]['phone_number'] = preg_replace("/[^0-9]/", "", $row['phone_number'] );
                }
            } else {
                $results =  (new DB_External_Messages_Groups())->getTextMessageList($messageId);
                foreach ($results as $key=> $row) {
                    $results[$key]['phone_number'] = preg_replace("/[^0-9]/", "", $row['phone_number'] );
                }
            }
            return $results;

        } else if ($type == 'Email') {
            if ($sent) {
                return (new DB_External_Messages_Recipients())->getSentRecipients($messageId, "email");
            } else {
                return (new DB_External_Messages_Groups())->getEmailList($messageId);
            }
        }
    }

    public function setApprovals($messageId, $whitelabel_id, $status, $custid)
    {
        $message = self::getMessageById($messageId, $whitelabel_id);
        if ($message['whitelabel_id'] == $whitelabel_id) {
            $externalMessages = new DB_External_Messages();
            if ($status == 'Yes') {
                $externalMessages->setApproval($messageId, $custid);
            } else if($status == 'No') {
                $externalMessages->setFailedReview($messageId);
            }

        }
    }

    public function getSentMessages($whitelabel_id, $type = "All", $limit=10, $since = 0, $sort ="ASC")
    {
        $DbExternalMessages = new DB_External_Messages();
        $allMessagesRaw     = $DbExternalMessages->getAllMessages($whitelabel_id);
        $webhooks           = new DB_External_Messages_Webhooks();
        $count              = 0;
        $data               = [];
        if ($sort == 'DESC') {
            //reverse order of the array
            $allMessagesRaw = array_reverse($allMessagesRaw, true);
            $since          = 0;
        }
        foreach ($allMessagesRaw as $message) {
            if(($type == "All" || $type == $message['type']) && $count < $limit && $message['message_id'] > $since && $message['sent_time'] > 0) {
                $data[] = [
                    "message_id"        => $message['message_id'],
                    "whitelabel_id"     => $message['whitelabel_id'],
                    "custid"            => $message['custid'],
                    "sender_name"       => $message['sender_name'],
                    "sent_time"         => $message['sent_time'],
                    "type"              => $message['type'],
                    "subject"           => $message['subject'],
                    "body"              => $message['body'],
                    "approved_by"       => $message['approved_by'],
                    "approved_by_name"  => $message['approved_by_name'],
                    "unique_opens"      => $webhooks->getOpenCount($message['message_id']),
                    "unique_clicks"     => $webhooks->getUniqueClickCount($message['message_id']),
                    "recipients"        => self::getActiveSenderList($message['message_id'], $message['type'], true),
                    "activity"          => $webhooks->getAllActivityByMessageId($message['message_id'])
                ];
                $count++;
            }
        }
        return $data;
    }

    public function getFirstOpens($messageId, $whitelabel_id)
    {
        // verify that message belongs to the whitelabel
        $message = self::getMessageById($messageId, $whitelabel_id);
        if (count($message) == 0) {
            return [];
        }
        if ($message['whitelabel_id'] == $whitelabel_id) {
            return (new DB_External_Messages_Webhooks())->getFirstOpen($messageId);
        }
        return [];
    }

    public function getFirstClicks($messageId, $whitelabel_id)
    {
        // verify that message belongs to the whitelabel
        $message = self::getMessageById($messageId, $whitelabel_id);
        if (count($message) == 0) {
            return [];
        }
        if ($message['whitelabel_id'] == $whitelabel_id) {
            return (new DB_External_Messages_Webhooks())->getFirstClick($messageId);
        }
        return [];
    }

    public function getMyMessages($custid, $whitelabel_id)
    {
        // get messages sent and scheduled by the user
        $DB_External_Messages            = new DB_External_Messages();
        $DB_External_Messages_Recipients = new DB_External_Messages_Recipients();
        $whitelabelMessages              = $DB_External_Messages->getAllMessages($whitelabel_id);
        $myDraftMessages                 = [];
        foreach ($whitelabelMessages as $message) {
            if ($message['ready_to_send'] == 'Yes' AND $message['custid'] == $custid) {
                $myDraftMessages[] = $message;
            }
        }

        // add recipient details to the draft messages
        foreach($myDraftMessages as $key=>$message) {
            $myDraftMessages[$key]["recipients"] = self::getActiveSenderList($message['message_id'], $message['type']);
        }

        return [
            "my_scheduled_messages" =>  array_reverse($myDraftMessages),
            "my_received_messages"  =>  $DB_External_Messages_Recipients->getMessagesSentToUser($custid, $whitelabel_id)
        ];

    }

    public function deleteMessage($messageId, $whitelabel_id)
    {
        (new DB_External_Messages())->removeMessage($messageId, $whitelabel_id);
    }

}