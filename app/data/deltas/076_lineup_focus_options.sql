-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2020 at 12:22 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `bhconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `lineup_focus_options`
--

CREATE TABLE `lineup_focus_options` (
                                        `id` int(11) NOT NULL,
                                        `focus` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lineup_focus_options`
--

INSERT IGNORE INTO `lineup_focus_options` (`id`, `focus`) VALUES
(1, 'Similar Experience'),
(2, 'Similar Age'),
(3, 'Upcoming Racing Lineups'),
(4, 'Similar Weight'),
(5, 'Experimentation'),
(6, 'Maximum Number of Boats'),
(7, 'Minimum Number of Boats'),
(8, 'Similar Boat Speeds'),
(9, 'Fastest Boat Speeds'),
(10, 'Other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lineup_focus_options`
--
ALTER TABLE `lineup_focus_options`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lineup_focus_options`
--
ALTER TABLE `lineup_focus_options`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;
