<?php
session_name("bhc-49fj489gj348gj39gfj39"); //random unguessable string -- this is different than the string in core/App.php
session_start();
require_once (dirname(__FILE__).'/../vendor/autoload.php');
use \Rollbar\Rollbar;
use \Rollbar\Payload\Level;


//example cli request:
//
//php mcli.php -c "navbar" -m "getNavbar" --whitelabel_id="127" --custid="564" --hasview="true"
//

(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('cli only'); //verify that this is only being run on the cli

spl_autoload_register(function ($class) {
    $path = '../models/DbAccess/' . ucfirst($class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
    $path2 = '../models/' . ucfirst($class) . '.php';
    if (file_exists($path2)) {
        require_once $path2;
    }
    $path3 = '../models/Service/' . ucfirst($class) . '.php';
    if (file_exists($path3)) {
        require_once $path3;
    }

    $path4 = '../models/Integrations/' . ucfirst($class) . '.php';
    if (file_exists($path4)) {
        require_once $path4;
    }

});

//rollbar error reporting
Rollbar::init(
    array(
        'access_token' => '[redacted]',
        'environment' => Environment::getEnvCode(),
        'custom' => array(
            "controller"    => "mcli"
        )
    )
);

$shortopts  = "";
$shortopts .= "c:";     // Required value - controller
$shortopts .= "m:";     // Required value - method in controller
$shortopts .= "whitelabel_id::";    // Optional value - whitelabel_id
$shortopts .= "custid::";    // Optional value - custid
$shortopts .= "hasview::";    // Optional value - hasview (true or false)

$longopts  = array(
    "required:",
    "whitelabel_id::",
    "custid::",
    "hasview::",
    "opt",
);
$options = getopt($shortopts, $longopts);
//var_dump($options);
//echo " My controller is: ".$options["c"];
//echo "; My Method is: ".$options["m"];


if(isset($options['whitelabel_id']))
{
    //echo "; setting whitelabel_id to ".$options['whitelabel_id'];
    $_SESSION['whitelabel_id'] = $options['whitelabel_id'];
} else{
    //echo "; unsetting whitelabel_id";
    unset($_SESSION['whitelabel_id']);
}

if(isset($options['custid']))
{
    $_SESSION['custid'] = $options['custid'];
} else{
    unset($_SESSION['custid']);
}

//check that custid (if set) belongs to the whitelabel_id specified


//run the requested action

require_once '../core/Controller.php';
require_once '../controllers/'.$options["c"].'.php';
$controllerName = strval($options["c"]);
$methodName = strval($options["m"]);
$controller = new $controllerName;
$controller->$methodName();
if(isset($options['hasview']) && $options['hasview'])
{
    require_once '../views/json.phtml';
}



