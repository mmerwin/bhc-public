--
-- Table structure for table `practice_attendance`
--

CREATE TABLE IF NOT EXISTS `practice_attendance` (
                                                     `practice_id` int(11) NOT NULL,
                                                     `whitelabel_id` int(11) NOT NULL,
                                                     `custid` int(11) NOT NULL,
                                                     `status` varchar(20) NOT NULL,
                                                     `timestamp` int(11) NOT NULL,
                                                     PRIMARY KEY (`practice_id`,`whitelabel_id`,`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
