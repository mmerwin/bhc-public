<?php


class Service_Logs
{
    public static function api_log($controller, $method, $ip, $custid, $type = "user")
    {
        if ($type == "admin") {
            self::adminApiLog($controller, $method, $ip, $custid);
        } else {
            if (strtolower($controller) !== "notifications" && strtolower($method) !== "mynotifications") {
                // keeps from logging notification checks every 30 seconds
                if (strtolower($controller) == "zapier") {
                    self::zapierApiLog($method, $ip, $custid);
                } else {
                    self::standardApiLog($controller, $method, $ip, $custid);
                }
            }
        }
    }

    private static function standardApiLog($controller, $method, $ip, $custid)
    {
        (new DB_Endpoint_Log())->addToLog($controller,$method, $ip, $custid);
    }

    private static function zapierApiLog($method, $ip, $custid)
    {
        (new DB_Endpoint_Log_Zapier())->addToLog("zapier",$method, $ip, $custid);
    }

    private static function adminApiLog($controller, $method, $ip, $adminid)
    {
        (new DB_Admin_Endpoint_Log())->addToLog($controller,$method, $ip, $adminid);
    }

}