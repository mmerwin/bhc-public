--
-- Table structure for table `surveys_dropdowns`
--

CREATE TABLE IF NOT EXISTS `surveys_dropdowns` (
                                                   `dropdown_id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `survey_id` int(11) NOT NULL,
                                                   `question_id` int(11) NOT NULL,
                                                   `selection_1` text DEFAULT NULL,
                                                   `selection_2` text DEFAULT NULL,
                                                   `selection_3` text DEFAULT NULL,
                                                   `selection_4` text DEFAULT NULL,
                                                   `selection_5` text DEFAULT NULL,
                                                   `selection_6` text DEFAULT NULL,
                                                   `selection_7` text DEFAULT NULL,
                                                   `selection_8` text DEFAULT NULL,
                                                   `selection_9` text DEFAULT NULL,
                                                   `selection_10` text DEFAULT NULL,
                                                   `selection_11` text DEFAULT NULL,
                                                   `selection_12` text DEFAULT NULL,
                                                   `selection_13` text DEFAULT NULL,
                                                   `selection_14` text DEFAULT NULL,
                                                   `selection_15` text DEFAULT NULL,
                                                   `selection_16` text DEFAULT NULL,
                                                   `selection_17` text DEFAULT NULL,
                                                   `selection_18` text DEFAULT NULL,
                                                   `selection_19` text DEFAULT NULL,
                                                   `selection_20` text DEFAULT NULL,
                                                   PRIMARY KEY (`dropdown_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;