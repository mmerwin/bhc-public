<?php


class DB_Surveys_Questions extends DataModel
{
    protected $schema = "surveys_questions";

    public function addElement($surveyId, $descr,$elementType)
    {
        $nextPosition = (self::getLastPosition($surveyId) + 1);
        $this->DB->query("INSERT INTO {$this->schema} (survey_id, position, descr, element_type ) VALUES (?, ?, ?, ?)", array($surveyId, $nextPosition, $descr, $elementType));

        //get question_id
        $questionId = $this->DB->query("SELECT question_id FROM {$this->schema} WHERE survey_id = ? AND position = ? ORDER BY question_id DESC LIMIT 1", array($surveyId, $nextPosition))->fetchArray();

        return $questionId['question_id'];
    }

    public function getAllElements($surveyId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE survey_id = ? ORDER BY position ASC", array($surveyId))->fetchAll();
    }

    public function deleteByQuestionId($surveyId, $questionId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ? AND question_id = ? LIMIT 1", array($surveyId, $questionId));
    }

    public function deleteSurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

    private function getLastPosition($surveyId)
    {
        $lastPosition = $this->DB->query("SELECT position FROM {$this->schema} WHERE survey_id = ? ORDER BY position DESC LIMIT 1", array($surveyId));
        if($lastPosition->numRows() == 0)
        {
            return 1;
        } else{
            $positions = $lastPosition->fetchArray();
            return $positions['position'];
        }
    }

    public function swapPositions($surveyId, $questionId1, $questionId2)
    {
        //find position numbers of  of q1 & q2
        $q1Position = $this->DB->query("SELECT position FROM {$this->schema} WHERE survey_id = ? AND question_id = ? LIMIT 1", array($surveyId, $questionId1))->fetchArray();
        $q2Position = $this->DB->query("SELECT position FROM {$this->schema} WHERE survey_id = ? AND question_id = ? LIMIT 1", array($surveyId, $questionId2))->fetchArray();

        //update positions
        $this->DB->query("UPDATE {$this->schema} SET position = ? WHERE survey_id = ? AND question_id = ? LIMIT 1", array($q2Position['position'], $surveyId, $questionId1));
        $this->DB->query("UPDATE {$this->schema} SET position = ? WHERE survey_id = ? AND question_id = ? LIMIT 1", array($q1Position['position'], $surveyId, $questionId2));
    }

    public function deleteAllBySurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

}