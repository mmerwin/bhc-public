--
-- Table structure for table `user_stripe_accounts`
--

CREATE TABLE IF NOT EXISTS `user_stripe_accounts` (
                                                      `custid` int(11) NOT NULL,
                                                      `env` varchar(10) NOT NULL,
                                                      `stripe_customer_id` varchar(100) NOT NULL,
                                                      `preferred_payment_method` varchar(100) NOT NULL,
                                                      `delinquent` varchar(5) NOT NULL DEFAULT 'No',
                                                      PRIMARY KEY (`custid`,`env`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
