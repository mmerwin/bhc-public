<?php


class DB_Socialmedia_Accounts extends DataModel
{
    protected $schema = "socialmedia_accounts";

    public function getAllByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getUrlsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND type LIKE '%--url%'", array($whitelabel_id))->fetchAll();
    }

    public function updateUrl($whitelabel_id, $platform, $newUrl)
    {
        $type = "{$platform}--url";
        //remove old entry
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND type = ? LIMIT 1", array($whitelabel_id, $type));

        //add entry
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, type, data, last_updated) VALUES (?, ?, ?, ?)", array($whitelabel_id, $type, $newUrl, time()));
    }

}