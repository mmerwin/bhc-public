--
-- Table structure for table `reward_prizes`
--

CREATE TABLE IF NOT EXISTS `reward_prizes` (
                                               `reward_prize_id` int(11) NOT NULL AUTO_INCREMENT,
                                               `points` int(11) NOT NULL,
                                               `item` text NOT NULL,
                                               `descr` text NOT NULL,
                                               `status` varchar(20) NOT NULL DEFAULT 'Active',
                                               PRIMARY KEY (`reward_prize_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reward_prizes`
--

INSERT IGNORE INTO `reward_prizes` (`reward_prize_id`, `points`, `item`, `descr`, `status`) VALUES
(1, 250, '10 cam-buckle boat straps (9ft or 12ft)', 'If your boats are stored outside, or your club travels frequently, new boat straps are always needed. For 250 points, your club will receive 10 new Boathouse Connect boat straps. We will reach out to get a proper mailing address as well as what size straps you want.', 'Active'),
(2, 600, '4-pack of 100 nuts & washers (400 nuts and 400 washers)', 'Boat hardware is always in short supply. Redeeming this reward for 600 points, you will get 200 10mm stainless-steel nuts & washers AND 200 7/16\" stainless-steel nuts & washers. If all of your boats are the same size, you can request to receive 400 of one size instead of 200 each.', 'Active'),
(3, 1100, 'Concept 2 oar carrier', 'The Concept2 Oar Carrier helps you carry four sweep oars or two pairs of sculls so that they are balanced for carrying and protected on the way to and from the dock. Made of 1/2\" Baltic Birch plywood, we send these unfinished so you can paint them in your team colors for easy identification.', 'Active'),
(4, 1300, 'Replacement rowing shoes', 'Get a brand new set of rowing shoes for one of your seats. When redeeming this reward, specify which boat will receive the shoes and we will make sure that they will be a match.', 'Active'),
(5, 2000, 'Bow number set', 'In-person regattas will happen again soon enough! Be prepared with a set of bow numbers.', 'Active'),
(6, 2500, 'Free month of Boathouse Connect', 'Want to skip a monthly payment? Redeem this reward for 2500 points to get the next month of Boathouse Connect for free!', 'Active'),
(7, 2500, 'NK gear bag', 'The NK Gear Bag features multiple compartments with adjustable dividers that can easily swallow up a Cox-Box, mic, charger, SpeedCoach, Maintenance Kit, tools and more.', 'Active'),
(8, 2900, '8 Concept 2 oar grips (sweep or sculling)', 'Get new grips for an 8+ or for a 4x worth of oars! Choose between 8 sweep grips or 8 sculling grips.', 'Active'),
(9, 4000, 'NK Cox Box spare battery', 'Replacement/Spare battery pack for NK Cox Box.', 'Active'),
(10, 6100, 'Vespoli large boat-slings', 'Pair of Large/Tall boat slings for eights and coxed fours. Wide spread feet for stability. Redeem this reward to receive a set of 2 slings. Not for long term storage or \"unattended\" outside storage. Base of slings is approximately 18.6\" x 26.25\". Height is 35\".', 'Active');
COMMIT;
