<?php


class Service_Rowstats_Data_Collection
{
    private $requiredBuffer = (86400*14); // only store regattas 14 days after they have started
    private $DB_Regattacentral_Regattas;
    private $DB_Regattacentral_Venues;
    private $DB_Regattacentral_Regatta_Entries;
    private $DB_Regattacentral_Regatta_Events;
    private $DB_Regattacentral_Regatta_Entries_Participants;
    private $DB_Regattacentral_Participant_Organizations;
    private $DB_Regattacentral_Regatta_Races;
    private $DB_Regattacentral_Regatta_Races_Results;
    private $RegattaCentralAPI;

    public function __construct()
    {
        $this->DB_Regattacentral_Regattas                       = (new DB_Regattacentral_Regattas());
        $this->DB_Regattacentral_Venues                         = (new DB_Regattacentral_Venues());
        $this->DB_Regattacentral_Regatta_Entries                = (new DB_Regattacentral_Regatta_Entries());
        $this->DB_Regattacentral_Regatta_Events                 = (new DB_Regattacentral_Regatta_Events());
        $this->DB_Regattacentral_Regatta_Entries_Participants   = (new DB_Regattacentral_Regatta_Entries_Participants());
        $this->DB_Regattacentral_Participant_Organizations      = (new DB_Regattacentral_Participant_Organizations());
        $this->DB_Regattacentral_Regatta_Races                  = (new DB_Regattacentral_Regatta_Races());
        $this->DB_Regattacentral_Regatta_Races_Results          = (new DB_Regattacentral_Regatta_Races_Results());
        $this->RegattaCentralAPI                                = (new Integrations_RegattaCentral());
    }

    public function addNewRegatta($bulkData)
    {
        $regattaId      = $bulkData["jobId"];
        $parentId       = $bulkData["parent"];
        $name           = $bulkData["name"];
        $abbreviation   = $bulkData["abbreviation"];
        $startTime      = ($bulkData["regattaDates"][0])/1000;
        $timezone       = $bulkData["timezone"];
        $checkPayableTo = $bulkData["checkPayableTo"];
        $billingAddress = $bulkData["billingAddress"];
        $contactName    = $bulkData["contactName"];
        $contactEmail   = $bulkData["contactEmail"];
        $primaryHost    = $bulkData["primaryHost"];
        $regattaUrl     = $bulkData["regattaUrl"];
        $resultsUrl     = isset($bulkData["offlineResults"][0]["url"]) ? $bulkData["offlineResults"][0]["url"] : "";
        $raceType       = strtolower($bulkData["raceType"]);
        $venueId        = $bulkData["venue"]["venueId"];

        // find host
        $hostOrdId = 0;
        foreach ($bulkData["hosts"] as $host) {
            if (strtolower($host["role"]) == "host") {
                $hostOrdId = $host["orgId"];
                break;
            }
        }

        // only continue if the regatta started more than 14 days ago
        if ($startTime < (time() - $this->requiredBuffer)) {
            // save the venue data
            $this->addVenue($bulkData["venue"]);

            // create new regatta
            $this->DB_Regattacentral_Regattas->addRegatta($regattaId, $parentId, $name, $abbreviation,
                $startTime, $timezone, $checkPayableTo, $billingAddress, $contactName, $contactEmail, $primaryHost,
                $regattaUrl, $hostOrdId, $raceType, $venueId, $resultsUrl);

            // save the events data
            $this->addEvents($regattaId, $bulkData["events"]);

            // save participant affiliations
            $this->addAllParticipants($bulkData["participants"]);
        }
    }

    public function addVenue($venue)
    {
        $venueId        = $venue["venueId"];
        $name           = $venue["name"];
        $abbreviation   = $venue["abbreviation"];
        $address1       = $venue["address1"];
        $address2       = $venue["address2"];
        $city           = $venue["city"];
        $region         = $venue["region"];
        $country        = $venue["country"];
        $postalCode     = $venue["postalCode"];
        $latitude       = $venue["latitude"];
        $longitude      = $venue["longitude"];
        $venueUrl       = $venue["venueUrl"];
        $type           = strtolower($venue["type"]);
        $subType        = strtolower($venue["subType"]);
        $courses        = ($venue["courses"]) ? json_encode($venue["courses"]) : null;

        $this->DB_Regattacentral_Venues->addVenue($venueId, $name, $abbreviation, $address1, $address2,
            $city, $region, $country, $postalCode, $latitude, $longitude, $venueUrl, $type, $subType, $courses);
    }

    public function addEvents($regattaId, $events)
    {
        foreach ($events as $event) {
            $eventId                    = $event["eventId"];
            $sequence                   = $event["sequence"];
            $label                      = $event["label"];
            $code                       = $event["code"];
            $title                      = $event["title"];
            $minAthleteAge              = $event["minAthleteAge"];
            $maxAthleteAge              = $event["maxAthleteAge"];
            $minAvgAge                  = $event["minAvgAge"];
            $maxAvgAge                  = $event["maxAvgAge"];
            $athleteCountExcludingCox   = $event["athleteCountExcludingCox"];
            $sweep                      = $event["sweep"] ? 'true' : 'false';
            $coxed                      = $event["coxed"] ? 'true' : 'false';
            $cost                       = $event["cost"];
            $deadlineModel              = $event["deadlineModel"];
            $defaultRaceFormat          = strtolower($event["defaultRaceFormat"]);
            $finalRaceTime              = ($event["finalRaceTime"] / 1000);
            $defaultRaceUnits           = strtolower($event["defaultRaceUnits"]);
            $defaultHandicapAlgorithm   = ($event["defaultHandicapAlgorithm"]) ? json_encode($event["defaultHandicapAlgorithm"]) : null;
            $defaultHandicapMultiplier  = ($event["defaultHandicapMultiplier"]) ? json_encode($event["defaultHandicapMultiplier"]) : null;
            $defaultDuration            = $event["defaultDuration"];
            $maxEntries                 = $event["maxEntries"];
            $maxEntriesPerClub          = $event["maxEntriesPerClub"];
            $maxAlternates              = $event["maxAlternates"];
            $requireCoxswain            = $event["reqCoxswain"] ? 'true' : 'false';
            $status                     = strtolower($event["status"]);
            $weight                     = strtolower($event["weight"]);
            $gender                     = strtolower($event["gender"]);

            $athleteClass = $event["athleteClass"];
            $this->DB_Regattacentral_Regatta_Events->addEvent($regattaId, $eventId, $sequence, $label,
                    $code, $title, $minAthleteAge, $maxAthleteAge, $minAvgAge, $maxAvgAge, $athleteClass,
                    $athleteCountExcludingCox, $sweep, $coxed, $cost, $deadlineModel, $defaultRaceFormat,
                    $finalRaceTime, $defaultRaceUnits, $defaultHandicapAlgorithm, $defaultHandicapMultiplier,
                    $defaultDuration, $maxEntries, $maxEntriesPerClub, $maxAlternates, $requireCoxswain, $status,
                    $weight, $gender);

            // add event entries
            $this->addEventEntries($regattaId, $eventId, $event["entries"]);

            // add event results
            $this->getEventResults($regattaId, $eventId);
        }
    }

    public function addEventEntries($regattaId, $eventId, $entries)
    {
        if (is_array($entries) && count($entries) > 0) {
            foreach ($entries as $entry) {
                $entryId            = $entry["entryId"];
                $organizationId     = $entry["organizationId"];
                $contactId          = $entry["contactId"];
                $entryLabel         = $entry["entryLabel"];
                $alternateTitle     = $entry["alternateTitle"];
                $averageAge         = $entry["averageAge"];
                $handicap           = $entry["handicap"];
                $compositeLabel     = $entry["compositeLabel"];
                $seed               = $entry["seed"];
                $ergScore           = $entry["ergScore"];
                $division           = ($entry["division"])         ? json_encode($entry["division"])         : null;
                $bow                = ($entry["bow"])              ? json_encode($entry["bow"])              : null;
                $waitlistPriority   = ($entry["waitListPriority"]) ? json_encode($entry["waitListPriority"]) : null;
                $createdOn          = $entry["createdOn"]/1000;
                $modifiedOn         = $entry["modifiedOn"]/1000;
                $composite          = $entry["composite"] ? 'true' : 'false';
                $international      = $entry["international"] ? 'true' : 'false';

                $this->DB_Regattacentral_Regatta_Entries->addEntry($regattaId, $eventId, $entryId, $organizationId,
                    $contactId, $entryLabel, $alternateTitle, $averageAge, $handicap, $compositeLabel, $seed, $ergScore,
                    $division, $bow, $waitlistPriority, $createdOn, $modifiedOn, $composite, $international);

                // add entry participants
                if (is_array($entry["entryParticipants"]) && count($entry["entryParticipants"]) > 0) {
                    foreach ($entry["entryParticipants"] as $participant) {
                        $this->addEntryParticipants($regattaId, $eventId, $entryId, $participant["participantId"],
                            $participant["organizationId"], $participant["sequence"], $participant["type"]);
                    }
                }
            }
        }
    }

    public function addEntryParticipants($regattaId, $eventId, $entryId, $participantId, $organizationId, $sequence, $type)
    {
        $this->DB_Regattacentral_Regatta_Entries_Participants->addParticipant($regattaId, $eventId, $entryId,
            $participantId, $organizationId, $sequence, strtolower($type));
    }

    public function addAllParticipants($participants)
    {
        foreach ($participants as $row) {
            foreach ($row["memberId"] as $member) {
                $participantId  = $row["participantId"];
                $organizationId = $member["organizationId"];
                $memberId       = $member["memberId"];
                $fname          = $row["firstName"];
                $lname          = $row["lastName"];
                $gender         = strtolower($row["gender"]);

                $this->DB_Regattacentral_Participant_Organizations->addParticipantId($participantId, $organizationId,
                    $fname, $lname, $gender, $memberId);
            }
        }
    }

    public function getEventResults($regattaId, $eventId)
    {
        // make API call to get event results
        $eventResults = json_decode($this->RegattaCentralAPI->getResultsByEvent($regattaId, $eventId), true);
        $races        = $eventResults["data"];

        // iterate through all races
        foreach ($races as $race) {
            $raceId         = $race["raceId"];
            $uuid           = $race["uuid"];
            $displayNumber  = $race["displayNumber"];
            $displayType    = strtolower($race["displayType"]);
            $displayOrder   = $race["displayOrder"];
            $status         = strtolower($race["status"]);
            $scheduledStart = ($race["scheduledStart"] / 1000);
            $raceType       = strtolower($race["displayType"]);

            $this->DB_Regattacentral_Regatta_Races->addRace($raceId, $regattaId, $eventId, $uuid, $displayNumber,
                $displayType, $displayOrder, $status, $scheduledStart, $raceType);

            // iterate through results of each race
            $allResults = $race["lanes"];
            foreach ($allResults as $laneData) {
                $entryId        = $laneData["entryId"];
                $lane           = $laneData["lane"];
                $displayNumber  = $laneData["displayNumber"];
                $distance       = 0;
                $resultData     = null;

                // find final split / time
                if (is_array($laneData["results"])) {
                    // multiple split / times were provided, find the final distance and insert that one
                    foreach ($laneData["results"]as $results) {
                        if (isset ($results["splitLocation"]["distance"]) && $results["splitLocation"]["distance"] >= $distance) {
                            $resultData = $results;
                        }
                    }
                    if (!is_null($resultData)) {
                        $this->insertResults($raceId, $regattaId, $eventId, $entryId, $lane, $displayNumber, $resultData);
                    }
                }
            }
        }
    }

    public function insertResults($raceId, $regattaId, $eventId, $entryId, $lane, $displayNumber, $results)
    {
        $distance                   = $results["splitLocation"]["distance"];
        $elapsedTime                = $results["elapsedTime"];
        $splitTime                  = $results["splitTime"];
        $marginToFirst              = $results["marginToFirst"];
        $marginToPrevious           = $results["marginToPrevious"];
        $adjustedTime               = $results["adjustedTime"];
        $adjustedMarginToFirst      = $results["adjustedMarginToFirst"];
        $adjustedMarginToPrevious   = $results["adjustedMarginToPrevious"];
        $penaltyTime                = $results["penaltyTime"];
        $penaltyCode                = ($results["penaltyCode"]) ? $results["penaltyCode"] : "";
        $comment                    = ($results["comment"]) ? $results["comment"] : "";

        $this->DB_Regattacentral_Regatta_Races_Results->addResult($raceId, $regattaId, $eventId, $entryId, $lane,
            $displayNumber, $distance, $elapsedTime, $splitTime, $marginToFirst, $marginToPrevious, $adjustedTime,
            $adjustedMarginToFirst, $adjustedMarginToPrevious, $penaltyTime, $penaltyCode, $comment);
    }

}