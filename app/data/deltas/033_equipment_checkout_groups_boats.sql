--
-- Table structure for table `equipment_checkout_groups_boats`
--

CREATE TABLE IF NOT EXISTS `equipment_checkout_groups_boats` (
                                                                 `whitelabel_id` int(11) NOT NULL,
                                                                 `boat_id` int(11) NOT NULL,
                                                                 `group_id` int(11) NOT NULL,
                                                                 PRIMARY KEY (`whitelabel_id`,`boat_id`,`group_id`),
                                                                 KEY `fk_boatid_equipment_boats_checkout` (`boat_id`),
                                                                 KEY `fk_groupid_equipment_boats_checkout` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_checkout_groups_boats`
--
ALTER TABLE `equipment_checkout_groups_boats`
    ADD CONSTRAINT `fk_boatid_equipment_boats_checkout` FOREIGN KEY (`boat_id`) REFERENCES `equipment_boats` (`boat_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_groupid_equipment_boats_checkout` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_equipment_boats_checkout` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
