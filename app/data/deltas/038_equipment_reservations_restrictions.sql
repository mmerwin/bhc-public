--
-- Table structure for table `equipment_reservations_restrictions`
--

CREATE TABLE IF NOT EXISTS `equipment_reservations_restrictions` (
                                                                     `whitelabel_id` int(11) NOT NULL,
                                                                     `time_limit` int(11) NOT NULL DEFAULT 0,
                                                                     `min_lead_time` int(11) NOT NULL DEFAULT 0,
                                                                     `max_lead_time` int(11) NOT NULL DEFAULT 0,
                                                                     `seven_day_limit` int(11) NOT NULL DEFAULT 0,
                                                                     `fourteen_day_limit` int(11) NOT NULL DEFAULT 0,
                                                                     PRIMARY KEY (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_reservations_restrictions`
--
ALTER TABLE `equipment_reservations_restrictions`
    ADD CONSTRAINT `fk_whitelabel_id_reservation_restrictions` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
