--
-- Table structure for table `user_affiliations`
--

CREATE TABLE IF NOT EXISTS `user_affiliations` (
                                                   `custid` int(11) NOT NULL,
                                                   `whitelabel_id` int(11) NOT NULL,
                                                   `created_at` int(11) NOT NULL,
                                                   `last_accessed` int(11) NOT NULL,
                                                   `skin` int(11) NOT NULL DEFAULT 1,
                                                   PRIMARY KEY (`custid`,`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_affiliations`
--
ALTER TABLE `user_affiliations`
    ADD CONSTRAINT `fk_user_affils` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabels` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
