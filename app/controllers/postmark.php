<?php
require_once (dirname(__FILE__).'/../vendor/autoload.php');
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
class postmark extends Controller
{
    public function index()
    {
        echo strtotime("2010-11-26T12:01:05.1794748-05:00");
        $this->view("json",'');
    }
    public function deliveryWebhook()
    {
        $data = (file_get_contents('php://input'));
    }

    public function prepareExternalMessages()
    {
        $startTime = time();
        $prepareId = rand(0,999999)."-".$startTime;

        //set and get preparable messages for this run
        $DB_External_Messages_Receipts  = new DB_External_Messages_Recipients();
        $DB_External_Messages           = new DB_External_Messages();
        $messages                       = (new DB_External_Messages())->setPreparableMessages($prepareId);
        foreach ($messages as $row) {
            //set recipients list
            if($row['type'] == 'Email') {
                $DB_External_Messages_Receipts->populateEmailByMessageId($row['message_id']);
            } else if($row['type'] == 'Text') {
                $DB_External_Messages_Receipts->populateTextByMessageId($row['message_id']);
            }
            //set message as prepared
            $DB_External_Messages->setMessageAsPrepared($row['message_id']);
        }
    }

    public function sendExternalMessages()
    {
        //disable SSL check so we can get attachments from the CDN
        $context = stream_context_create(array(
            "ssl" => array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            )
        ));
        $postmarkService = new Service_Postmark();
        $DB_External_Messages_Recipients = new DB_External_Messages_Recipients();
        $DB_External_Messages = new DB_External_Messages();

        $startTime = time();
        $prepareId = rand(0,999999)."-".$startTime;
        $apiCredentials = Environment::getExternalKeys("postmark");

        $externalMessageTemplateUsers = file_get_contents(dirname(__FILE__)."/../data/postmark_templates/whitelabel-external-template/users.txt");
        $externalMessageTemplateStakeholders = file_get_contents(dirname(__FILE__)."/../data/postmark_templates/whitelabel-external-template/stakeholders.txt");
        $defaultSenderEmail = "Messaging@BoathouseConnect.com";

        $messages = $DB_External_Messages->setSendableEmailMessages($prepareId);
        foreach($messages as $row) {
            //get sending domain name
            $wlSignatureSettings = $postmarkService->getDefaultSignatures($row['whitelabel_id']);
            if (isset($wlSignatureSettings["other_emails"]) && $wlSignatureSettings["other_emails"]['confirmed'] == 'true') {
                $from = $wlSignatureSettings["other_emails"]['email_address'];
            } else {
                $from = $defaultSenderEmail;
            }

            //get reply-to email.
            $senderContact = (new DB_Logins())->getEmailByCustid($row["custid"]);
            $replyToEmail  = $senderContact["email"];

            //get message details
            $emailBodyUsers        = $externalMessageTemplateUsers;
            $emailBodyStakeholders = $externalMessageTemplateStakeholders;

            //add message details
            $emailBodyUsers        = str_replace("{{{ @content }}}",$row['body'], $emailBodyUsers);
            $emailBodyStakeholders = str_replace("{{{ @content }}}",$row['body'], $emailBodyStakeholders);

            //add From name
            $fromName              = $senderContact["fname"]." ".$senderContact["lname"];
            $emailBodyUsers        = str_replace("{{FromName}}", $fromName, $emailBodyUsers);
            $emailBodyStakeholders = str_replace("{{FromName}}", $fromName, $emailBodyStakeholders);

            $subject = $row['subject'];
            $stakeholderUnsubscribeBase = Environment::retrieveAppUrl(Environment::getEnvCode())."home/stakeholderUnsubscribe/{$row['message_id']}/{$row['whitelabel_id']}/";

            //assemble message array
            $bulkMessages  = [];
            $allRecipients = $DB_External_Messages_Recipients->assembleList($row['message_id']);
            if ($row["attachment"]) {
                //get attachment
                $encodedFile = base64_encode(file_get_contents("https://bhccdn.boathouseconnect.com/{$row['attachment']}", FALSE, $context));
                $fileName    = $row["attachment_name"];
                $attachment  = '[{"Name": "'.$fileName.'",
                            "Content": "'.$encodedFile.'",
                            "ContentType": "application/octet-stream"}]';
                foreach ($allRecipients as $recipient) {
                    if ($recipient["user_type"] == "User") {
                        $emailBody = $emailBodyUsers;
                    } else {
                        // add Unsubscribe link for stakeholders
                        $emailBody = str_replace("{{stakeholder_unsubscribe}}",$stakeholderUnsubscribeBase.urlencode($recipient["contact"]),$emailBodyStakeholders);
                    }

                    $bulkMessages[] = array(
                        'To'          => $recipient["contact"],
                        'From'        => $from,
                        'Subject'     => $subject,
                        'HtmlBody'    => str_replace("{{name}}",$recipient["name"],$emailBody),
                        'Tag'         => $row['message_id'],
                        "ReplyTo"     => $replyToEmail,
                        "Attachments" => json_decode($attachment, true)
                    );
                }
            } else {
                foreach ($allRecipients as $recipient) {
                    if ($recipient["user_type"] == "User") {
                        $emailBody = $emailBodyUsers;
                    } else {
                        // add Unsubscribe link for stakeholders
                        $emailBody = str_replace("{{stakeholder_unsubscribe}}",$stakeholderUnsubscribeBase.urlencode($recipient["contact"]),$emailBodyStakeholders);
                    }
                    $bulkMessages[] = array(
                        'To'       => $recipient["contact"],
                        'From'     => $from,
                        'Subject'  => $subject,
                        'HtmlBody' => str_replace("{{name}}",$recipient["name"],$emailBody),
                        'Tag'      => $row['message_id'],
                        "ReplyTo"  => $replyToEmail
                    );
                }
            }
            

            //send bulk messages
            $client = new PostmarkClient($apiCredentials['token']);
            $sendResult = $client->sendEmailBatch($bulkMessages);

            //update the external message recipients with the send and external_id data
            for ($count = 0; $count < count($allRecipients); $count++) {
                if (!isset($sendResult[$count]["messageid"])) {
                    $newExternalid = "bounce-{$row['message_id']}-".md5(time()."-".$row['message_id'].rand(1,999999)."-".rand(1,999999));
                    (new DB_External_Messages_Webhooks())->addEvent($newExternalid, "Invalid Email", $row['message_id'], "Bounced");
                } else {
                    $newExternalid = $sendResult[$count]["messageid"];
                }
                $DB_External_Messages_Recipients->updateAsSent($row['message_id'], $allRecipients[$count]['name'], $allRecipients[$count]['contact'], $newExternalid, time());
            }

            //mark message as sent
            $DB_External_Messages->setMessageAsSent($row['message_id']);
        }
    }

    public function sendExternalTexts()
    {
        $DB_External_Messages_Recipients = new DB_External_Messages_Recipients();
        $DB_External_Messages = new DB_External_Messages();

        $startTime = time();
        $prepareId = rand(0,999999)."-".$startTime;

        $messages = $DB_External_Messages->setSendableTextMessages($prepareId);

        $apiCredentials = Environment::getExternalKeys("twilio");
        $sid = $apiCredentials['client_id']; // Your Account SID from www.twilio.com/console
        $token = $apiCredentials['token']; // Your Auth Token from www.twilio.com/console
        $senderPhone = "+14695072121";

        $client = new Twilio\Rest\Client($sid, $token);

        foreach($messages as $row) {
            $allRecipients = $DB_External_Messages_Recipients->assembleList($row['message_id']);

            foreach ($allRecipients as $recipient) {
                //twilio requires each of the individual message be sent with a different API call
                try {
                    $message = $client->messages->create(
                        $recipient['contact'], // Text this number
                        [
                            'from' => $senderPhone, // From a valid Twilio number
                            'body' => "bhconnect alert - ".$row['body']
                        ]
                    );
                    $DB_External_Messages_Recipients->updateAsSent($recipient['message_id'], $recipient['name'], $recipient['contact'], $message->sid, time());
                } catch (exception $e) {
                    $DB_External_Messages_Recipients->updateAsSent($recipient['message_id'], $recipient['name'], $recipient['contact'], "exception-thrown-error:".time(), time());
                }

            }

            //mark message as sent
            $DB_External_Messages->setMessageAsSent($row['message_id']);
        }
    }

    public function sendScheduledSurveys()
    {
        $processIdentifier = "Z".substr(md5(rand(0,9999999)),0,8);

        //generate base url
        $baseUrl = (Environment::retrieveAppUrl(Environment::getEnvFromHostname()));

        //set surveys where open_time is now or passed to sent_status = $processIdentifier and return the surveys
        $rawSurveys = (new DB_Surveys())->retrieveSurveysToSend($processIdentifier);

        //setup postmark
        $defaultSenderEmail = "OrgSurveys@BoathouseConnect.com";
        $client             = new PostmarkClient(Service_External_Keys::getToken("postmark-platform"));

        foreach ($rawSurveys as $survey) {
            //set timezone
            $timezone = (new DB_Whitelabels())->getTimezone($survey['whitelabel_id']);
            $dt = new DateTime("now", new DateTimeZone($timezone)); //first argument "must" be a string
            $dt->setTimestamp($survey["close_time"]); //adjust the object to correct timestamp
            $closeTimeString = $dt->format('D M j \a\t g:i a T');


            //get sender signature for "other"
            $allSignatures = (new Service_Postmark())->getDefaultSignatures($survey["whitelabel_id"]);
            if (isset($allSignatures["other_emails"]) && $allSignatures["other_emails"]['confirmed'] == 'true') {
                $from = $allSignatures["other_emails"]['email_address'];
            } else {
                $from = $defaultSenderEmail;
            }

            //send survey email to add recipients
            $recipients = (new DB_Surveys_Recipients())->getAllRecipients($survey['whitelabel_id'], $survey["survey_id"]);
            foreach ($recipients as $recipient) {
                $url = $baseUrl."surveys/takesurvey/".$survey['whitelabel_id']."/".$survey["survey_id"]."/".$recipient['recipient_id']."/".$recipient["unique_id"];
                try {
                    $client->sendEmailWithTemplate(
                        $from,
                        $recipient["email"],
                        21025389,
                        [   "recipient_name"      => $recipient["name"],
                            "survey_title"        => $survey["title"],
                            "survey_description"  => $survey['description'],
                            "survey_close_date"   => $closeTimeString,
                            "action_url"          => $url
                        ]
                    );
                } catch (exception $e) {

                }

            }
            //TODO - send notification to all recipients that have a custid

            //mark survey as sent
            (new DB_Surveys())->markSurveySent($survey["survey_id"]);
        }
    }

    public function sendPracticeLineups()
    {
        $whitelabelSenderSignatures = array();
        $postmarkService = new Service_Postmark();
        $maxTimeBefore   = (time()+(3600*12)); //practice start must be within 12 hours
        $unsentLineups   = (new DB_Practice_Lineups())->getUnsentLineupsForCron();
        $apiCredentials  = Environment::getExternalKeys("postmark");
        $client          = new PostmarkClient($apiCredentials["token"]);
        $baseUrl         = (Environment::retrieveAppUrl(Environment::getEnvFromHostname()));
        $url             = $baseUrl."practices/AllPractices";
        foreach ($unsentLineups as $lineup) {
            if ($lineup["practice_start"] < $maxTimeBefore) {
                //check if already in array
                if (isset($whitelabelSenderSignatures[$lineup["whitelabel_id"]])) {
                    $from = $whitelabelSenderSignatures[$lineup["whitelabel_id"]];
                } else {
                    // not in array, so getting sending signature info
                    $wlSignatureSettings = $postmarkService->getDefaultSignatures($lineup['whitelabel_id']);
                    if (isset($wlSignatureSettings["automated_reminders"]) && $wlSignatureSettings["automated_reminders"]['confirmed'] == 'true') {
                        $from = $wlSignatureSettings["automated_reminders"]['email_address'];
                    } else {
                        $from = "Practices@BoathouseConnect.com";
                    }
                    $whitelabelSenderSignatures[$lineup["whitelabel_id"]] = $from;
                }
                date_default_timezone_set($lineup["timezone"]);
                try {
                    $client->sendEmailWithTemplate(
                        $from,
                        $lineup["email"],
                        "practice-lineups",
                        [   "name"              => $lineup["fname"]." ".$lineup["lname"],
                            "practice_name"     => $lineup["practice_name"],
                            "practice_time"     => date("D M j, g:i a",$lineup["practice_start"]),
                            "practice_location" => $lineup["location_name"],
                            "lineup_boat"       => $lineup["boat_name"],
                            "lineup_seat"       => ucfirst($lineup["seat"])."-seat",
                            "action_url"        => $url
                        ]
                    );
                    //mark lineup as sent
                    (new DB_Practice_Lineups())->markLineupSent($lineup["whitelabel_id"], $lineup["practice_id"], $lineup["custid"]);
                } catch (exception $e) {
                    //mark lineup as sent
                    (new DB_Practice_Lineups())->markLineupSent($lineup["whitelabel_id"], $lineup["practice_id"], $lineup["custid"]);
                }
            }
        }
    }

    function sendPostPracticeSurveys()
    {
        $whitelabelSenderSignatures = array();
        $postmarkService = new Service_Postmark();
        $unsentSurveys   = (new DB_Practice_Lineups())->getUnsentPracticeSurveys();
        $apiCredentials  = Environment::getExternalKeys("postmark");
        $client          = new PostmarkClient($apiCredentials["token"]);
        $baseUrl         = (Environment::retrieveAppUrl(Environment::getEnvFromHostname()));
        $baseUrl         = $baseUrl."practices/survey";
        foreach ($unsentSurveys as $lineup) {
            //check if already in array
            if (isset($whitelabelSenderSignatures[$lineup["whitelabel_id"]])) {
                $from = $whitelabelSenderSignatures[$lineup["whitelabel_id"]];
            } else {
                // not in array, so getting sending signature info
                $wlSignatureSettings = $postmarkService->getDefaultSignatures($lineup['whitelabel_id']);
                if (isset($wlSignatureSettings["automated_reminders"]) && $wlSignatureSettings["automated_reminders"]['confirmed'] == 'true') {
                    $from = $wlSignatureSettings["automated_reminders"]['email_address'];
                } else {
                    $from = "Practices@BoathouseConnect.com";
                }
                $whitelabelSenderSignatures[$lineup["whitelabel_id"]] = $from;
            }
            $urlHash = md5($lineup['whitelabel_id']."-postpractice-".$lineup['practice_id']."-survey-".$lineup['custid']."-bhconnect-587fh38ggh48gh398ghj");
            try {
                $client->sendEmailWithTemplate(
                    $from,
                    $lineup["email"],
                    "post-practice-survey",
                    [   "recipient_name"    => $lineup["fname"]." ".$lineup["lname"],
                        "action_url"        => $baseUrl."/{$lineup['whitelabel_id']}/{$lineup['practice_id']}/{$lineup['custid']}/{$urlHash}/Start"
                    ]
                );
                //mark survey as sent
                (new DB_Practice_Lineups())->markSurveySent($lineup["whitelabel_id"], $lineup["practice_id"], $lineup["custid"]);

                //check if rewards points need to be added to the user/whitelabel for attending practice once/year
                (new Service_Rewards($lineup["whitelabel_id"]))->addRewardPoints(12, $lineup["custid"]);
            } catch (exception $e) {
                (new DB_Practice_Lineups())->markSurveySent($lineup["whitelabel_id"], $lineup["practice_id"], $lineup["custid"]);
            }
        }
    }

    public function refreshAllDomainNames()
    {
        $postmark = new Service_Postmark();
        $postmark->refreshAllDomainNamesWithPostmark();
    }

    public function sendOverdueAccountsEmails()
    {
        //get all whitelabels
        $whitelabels = (new Service_Whitelabel())->getAllWhitelabels();
        $delinquentAccountsRaw = [];
        foreach ($whitelabels as $whitelabel) {
            $balances = (new Service_User_Payments())->getWhitelabelAccountBalances($whitelabel["whitelabel_id"]);
            foreach ($balances as $account) {
                if ($account["delinquent"] == "Yes" && $account["outstanding"] > 0) {
                    $delinquentAccountsRaw[] = [
                        'whitelabel_id'         => $account['whitelabel_id'],
                        'custid'                => $account["custid"],
                        'whitelabel_account'    => $account["whitelabel_account"],
                        'user_customer_id'      => $account["user_customer_id"],
                        'fname'                 => $account["fname"],
                        'lname'                 => $account["lname"],
                        'email'                 => $account["email"],
                        'whitelabel_name'       => $account["whitelabel_name"]
                    ];
                }
            }
        }

        $apiCredentials  = Environment::getExternalKeys("postmark");
        $client          = new PostmarkClient($apiCredentials["token"]);
        //send emails to each delinquent account
        foreach ($delinquentAccountsRaw as $account) {
            $client->sendEmailWithTemplate(
                "Billing@BoathouseConnect.com",
                $account["email"],
                "pastdue-account",
                [   "fname"             => $account["fname"],
                    "whitelabel_name"   => $account["whitelabel_name"]
                ]
            );
        }

        $this->view("json", $delinquentAccountsRaw);
    }

    public function monthlyWhitelabelDigest()
    {
        $serverToken = Service_External_Keys::getToken("postmark-platform");
        $client      = new PostmarkClient($serverToken);

        //generate report for all whitelabels
        (new Service_Whitelabel_Digest())->createReportsForAllWhitelabels();

        //get generated reports
        $month            = date("Ym", time());
        $generatedReports = (new DB_Whitelabel_Digest_History())->getAllReportsByMonth($month);
        foreach ($generatedReports as $report) {
            //only send digests to whitelabels with more than 5 users
            if ($report["total_users"] > 5) {
                //get all users with permission id 17
                $allowedUsers = Service_Permissions::findUsersWithPermission(17, $report["whitelabel_id"]);
                foreach ($allowedUsers as $receiver) {
                    //send to allowed users in whitelabel
                    $client->sendEmailWithTemplate(
                        "digests@boathouseconnect.com",
                        $receiver["email"],
                        "monthly-whitelabel-digest",
                        [   "fname"                         => $receiver["fname"],
                            "whitelabel_name"               => $report["whitelabel_name"],
                            "total_users"                   => $report["total_users"],
                            "athletes_attended_practice"    => $report["athletes_attended_practice"],
                            "messages_sent"                 => $report["messages_sent"],
                            "rewards_points_earned"         => $report["rewards_points_earned"],
                            "total_payments_received"       => $report["total_payments_received"],
                            "nextmonth"                     => date('F',strtotime('first day of +1 month')),
                            "anticipated_revenue"           => "$".number_format(round($report["anticipated_revenue"]/100,2)),
                            "delinquent_users"              => $report["delinquent_users"],
                            "monthyear"                     => date('F',time())
                        ]
                    );
                }
            }
        }
    }
}