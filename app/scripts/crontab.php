<?php

$cron_file = dirname(__FILE__)."/crontab-actual";
exec("rm ".$cron_file);
// Create the file
touch($cron_file);
// Make it writable
chmod($cron_file, 0777);
// Save the cron

$crontabLog = " >> /var/log/crontab/error.log 2>&1";

//add of the cron jobs that need to run here
file_put_contents($cron_file, '* * * * * php /var/www/GitlabRepoUpdate.php'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '* * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "prepareExternalMessages"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '* * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendExternalMessages"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '* * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendExternalTexts"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '*/2 * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendScheduledSurveys"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '0 */3 * * * cd '.dirname(__FILE__).'; php mcli.php -c "practices" -m "normalizePastPracticesFromSeries"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '*/10 * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendPracticeLineups"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '55 * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendPostPracticeSurveys"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '45 * * * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "refreshAllDomainNames"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '0 12 * * * cd '.dirname(__FILE__).'; php mcli.php -c "stripePaymentWebhooks" -m "chargeOrgSubscriptions"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '15 */6 * * * cd '.dirname(__FILE__).'; php mcli.php -c "stripePaymentWebhooks" -m "updateAllWhitelabelConnectedAccounts"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '20 */6 * * * cd '.dirname(__FILE__).'; php mcli.php -c "stripePaymentWebhooks" -m "processUserWhitelabelPayments"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '0 0 * * 0 cd '.dirname(__FILE__).'; php mcli.php -c "waivers" -m "updateUsrowingWaivers"'.$crontabLog.PHP_EOL,FILE_APPEND );
file_put_contents($cron_file, '0 0 * * 4 cd '.dirname(__FILE__).'; php mcli.php -c "waivers" -m "updateUsrowingWaivers"'.$crontabLog.PHP_EOL,FILE_APPEND );

// set environment-specific crontabs
$environment = Environment::getEnvCode();
if ($environment == 'PROD') {
    file_put_contents($cron_file, '32 0 * * * cd '.dirname(__FILE__).'; php mcli.php -c "appdata" -m "getAllRcOrgs"'.$crontabLog.PHP_EOL,FILE_APPEND );
    file_put_contents($cron_file, '32 12 * * * cd '.dirname(__FILE__).'; php mcli.php -c "appdata" -m "getAllRcOrgs"'.$crontabLog.PHP_EOL,FILE_APPEND );
    file_put_contents($cron_file, '0 12 * * 1 cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "sendOverdueAccountsEmails"'.$crontabLog.PHP_EOL,FILE_APPEND );
    file_put_contents($cron_file, '0 16 30 * * cd '.dirname(__FILE__).'; php mcli.php -c "postmark" -m "monthlyWhitelabelDigest"'.$crontabLog.PHP_EOL,FILE_APPEND );
}
if ($environment == 'UAT') {
    file_put_contents($cron_file, '32 0 * * * cd '.dirname(__FILE__).'; php mcli.php -c "appdata" -m "getAllRcOrgs"'.$crontabLog.PHP_EOL,FILE_APPEND );
}
// Install the cron
exec('crontab '.$cron_file);

?>

