<?php
require_once (dirname(__FILE__).'/../../vendor/autoload.php');

use Stripe\StripeClient;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;

class Service_Stripe_Connected_Accounts
{
    public static function checkForWhitelabelConnectedAccount($whitelabel_id)
    {
        $accountId = (new DB_Whitelabel_Connect_Accounts())->getAccountNumber($whitelabel_id, Environment::getEnvCode());
        if (is_null($accountId)) {
            return  self::createWhitelabelConnectedAccount($whitelabel_id);
        } else {
            return $accountId;
        }
    }

    private static function createWhitelabelConnectedAccount($whitelabel_id)
    {
        $stripe = new StripeClient(
            Service_External_Keys::getToken("stripe-secret")
        );
        $whitelabel = Service_Whitelabel::findNameById($whitelabel_id);
        $whitelabel_name = $whitelabel[0]["name"];


        try {
            $newAccount = $stripe->accounts->create([
                'type'         => 'custom',
                'capabilities' => [
                    'card_payments' => ['requested' => true],
                    'transfers'     => ['requested' => true]
                ],
                'metadata'     => ["whitelabel_id" => $whitelabel_id, "env" => Environment::getEnvCode(), "whitelabel_name" => $whitelabel_name, "slug" => Environment::getEnvCode()."-".$whitelabel_id],
                'settings'     => [
                    'payouts' => [
                        'schedule' => [
                            'interval'   => 'manual'
                        ],
                        'statement_descriptor' => "Boathouse Connect"
                    ]
                ]
            ]);

            (new DB_Whitelabel_Connect_Accounts())->setAccountId($whitelabel_id, Environment::getEnvCode(), $newAccount["id"]);
            return $newAccount["id"];
        }
        catch (exception $e) {
        }
    }

    public function getWhitelabelUrl($whitelabel_id, $refreshUrl, $returnUrl, $linkType = "account_onboarding")
    {
        switch ($linkType) {
            case "account_update":
                $type = "account_update";
                break;
            case "account_onboarding":
            default:
                $type = "account_onboarding";
                break;
        }
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);
        $baseUrl   = Environment::retrieveAppUrl(Environment::getEnvCode());

        $stripe = new StripeClient( Service_External_Keys::getToken("stripe-secret"));

        try {
            $url = $stripe->accountLinks->create([
                'account'     => $accountId,
                'refresh_url' => $baseUrl.''.$refreshUrl,
                'return_url'  => $baseUrl.''.$returnUrl,
                'type'        => $type,
            ]);

            return $url;

        } catch (exception $e) {
            return array("Exception" => $e);
        }
    }

    /*
     *  ONLY USED FOR DEV, TEST, AND UAT.
     *  DISABLED FOR PRODUCTION
     */
    public function deleteConnectedAccount($accountId)
    {
        if (Environment::getEnvCode() != "PROD") {
            $stripe = new StripeClient( Service_External_Keys::getToken("stripe-secret"));

            try {
                $stripe->accounts->delete(
                    $accountId,
                    []
                );
                (new DB_Whitelabel_Connect_Accounts())->deleteAccount($accountId);
            } catch (exception $e) {

            }
        } else {
            die("DISABLED IN PRODUCTION, ONLY AVAILABLE IN LOWER ENVIRONMENTS");
        }

    }

    public function retrieveWhitelabelAccount($whitelabel_id)
    {
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);
        $stripe    = new StripeClient( Service_External_Keys::getToken("stripe-secret"));

        try {
           $data = $stripe->accounts->retrieve(
               $accountId,
               []
           );
           return $data;

        } catch (exception $e) {
            die("exception: ".$e->getMessage());
        }
    }

    public function checkIfWhitelabelConnectedAccountSetup($whitelabel_id)
    {
        $accountInformation = $this->retrieveWhitelabelAccount($whitelabel_id);
        // update information
        (new DB_Whitelabel_Connect_Accounts())->updateStatus($accountInformation["id"], json_encode($accountInformation["charges_enabled"]),
            json_encode($accountInformation["details_submitted"]), json_encode($accountInformation["payouts_enabled"]));

        // update whitelabel settings with current values
        WhitelabelSettings::updateValue($whitelabel_id, "stripe.connectedaccount.charges_enabled", json_encode($accountInformation["charges_enabled"]));
        WhitelabelSettings::updateValue($whitelabel_id, "stripe.connectedaccount.details_submitted", json_encode($accountInformation["details_submitted"]));
        WhitelabelSettings::updateValue($whitelabel_id, "stripe.connectedaccount.payouts_enabled", json_encode($accountInformation["payouts_enabled"]));
    }

    public function updateAllWhitelabelConnectedAccounts()
    {
        $AllWhitelabels = Service_Whitelabel::getAllWhitelabels();

        foreach ($AllWhitelabels as $whitelabel) {
            $this->checkIfWhitelabelConnectedAccountSetup($whitelabel["whitelabel_id"]);
        }
    }

    public function addWhitelabelPayoutMethod($whitelabel_id, $payoutToken, $payoutType)
    {
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);
        $stripe    = new StripeClient( Service_External_Keys::getToken("stripe-secret") );
        try {
            if ($payoutType == "instant") {
                // add debit card from tokenized card info
                $cardData = $stripe->accounts->createExternalAccount(
                    $accountId,
                    ['external_account' => $payoutToken, "default_for_currency" => true]
                );
                return array("Status" => "Success", "Message" =>"Debit card added");
            } else if ($payoutType == "bank") {
                // add bank account from tokenized banking info
                $stripe->accounts->createExternalAccount(
                    $accountId,
                    [
                        'external_account' => $payoutToken,
                    ]
                );
                return array("Status" => "Success", "Message" =>"Bank account added");
            } else {
                return array("Status" => "Error", "Message" =>"Invalid payout_type");
            }
        } catch (exception $e) {
            return array("Status" => "Error", "Message" =>"Exception thrown from Stripe", "Exception"=> $e->getMessage());
        }


    }

    public function getWhitelabelBalance($whitelabel_id)
    {
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);

        \Stripe\Stripe::setApiKey(Service_External_Keys::getToken("stripe-secret"));
        $balance = \Stripe\Balance::retrieve(
            ['stripe_account' => $accountId]
        );
        return $balance;
    }

    /**
     * @param $whitelabel_id int whitelabel_id of the organization to grab payment methods for
     * @param null $type options are "card" or "bank_account". This will define which payout methods get selected. null values will select cards and banks
     * @return \Stripe\Collection
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function getWhitelabelPayoutAccounts($whitelabel_id, $type = null)
    {
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);
        $stripe    = new StripeClient( Service_External_Keys::getToken("stripe-secret") );
        if (is_null($type)) {
            return $stripe->accounts->allExternalAccounts(
                $accountId,
                []
            );
        } else {
            return $stripe->accounts->allExternalAccounts(
                $accountId,
                ['object' => $type]
            );
        }
    }

    /**
     * @param $whitelabel_id int whitelabel_id of the organization requesting a payout
     * @param $amount int payout amount in cents (ie. $10 should be passes as 1000)
     * @param null $method payout method to use. Default is bank account unless "instant" is provided, then a debit card is used.
     * @return string[] Status (Success or Error) and Message
     */
    public function triggerWhitelabelPayout($whitelabel_id, $amount, $method = null)
    {
        $accountId = self::checkForWhitelabelConnectedAccount($whitelabel_id);
        // check the max balance allowed
        $balances = $this->getWhitelabelBalance($whitelabel_id);
        if ($balances["instant_available"][0]["amount"] >= $amount) {
            try {
                \Stripe\Stripe::setApiKey(Service_External_Keys::getToken("stripe-secret"));
                if ($method == "instant") {
                    // create instant payout
                    \Stripe\Payout::create([
                        'amount' => $amount,
                        'currency' => 'usd',
                        'method' => 'instant',
                    ], [
                        'stripe_account' => $accountId,
                    ]);
                } else {
                    // standard bank account payout
                     \Stripe\Payout::create([
                        'amount' => $amount,
                        'currency' => 'usd',
                    ], [
                        'stripe_account' => $accountId,
                    ]);
                }
                return array("Status" =>"Success", "Message"=>"Payout request sent");

            } catch (exception $e) {
                return array("Status"=>"Error", "Message"=>"Stripe exception thrown. ".$e->getMessage());
            }
        } else {
            return array("Status"=>"Error", "Message" =>"Insufficient funds, available balance is ".$balances["instant_available"][0]["amount"]);
        }

    }

}