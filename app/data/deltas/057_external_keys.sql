-- Table truncated on each run.
-- Table structure for table `external_keys`
--

CREATE TABLE IF NOT EXISTS `external_keys` (
                                               `key_id` int(11) NOT NULL AUTO_INCREMENT,
                                               `service` varchar(100) NOT NULL,
                                               `environment` varchar(50) NOT NULL,
                                               `token` text NOT NULL,
                                               `client_id` varchar(500) NOT NULL,
                                               `client_secret` varchar(500) NOT NULL,
                                               `username` varchar(500) NOT NULL,
                                               `password` varchar(500) NOT NULL,
                                               `grant_type` varchar(500) NOT NULL,
                                               `expires` int(11) NOT NULL,
                                               PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `external_keys`
--

TRUNCATE TABLE `external_keys`;
--
-- Dumping data for table `external_keys`
--

-- THIS DATA HAS BEEN REDACTED FOR PRIVACY PURPOSES
