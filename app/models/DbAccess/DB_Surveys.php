<?php


class DB_Surveys extends DataModel
{
    protected $schema = "surveys";

    public function newSurvey($whitelabel_id, $custid, $title, $description)
    {
        $currentTime = time();
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, custid_creator, title, description, timestamp_created, open_time, close_time) VALUES (?, ?, ?, ?, ?, 0, 0)", array($whitelabel_id, $custid, $title, $description, $currentTime));

        //fetch new survey_id
        $survey = $this->DB->query("SELECT survey_id FROM {$this->schema} WHERE custid_creator = ? AND whitelabel_id = ? AND timestamp_created = ? ORDER BY survey_id DESC LIMIT 1", array($custid, $whitelabel_id, $currentTime))->fetchArray();

        return $survey['survey_id'];
    }

    public function getSurveyById($surveyId, $whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($surveyId, $whitelabel_id))->fetchArray();
    }

    public function getSurveysByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function updateTitle($whitelabel_id, $surveyId, $newTitle)
    {
        $this->DB->query("UPDATE {$this->schema} SET title = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($newTitle, $surveyId, $whitelabel_id));
    }

    public function updateDescription($whitelabel_id, $surveyId, $newDescription)
    {
        $this->DB->query("UPDATE {$this->schema} SET description = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($newDescription, $surveyId, $whitelabel_id));
    }

    public function updateOpenTime($whitelabel_id, $surveyId, $newOpenTime)
    {
        $this->DB->query("UPDATE {$this->schema} SET open_time = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($newOpenTime, $surveyId, $whitelabel_id));
    }

    public function updateCloseTime($whitelabel_id, $surveyId, $newCloseTime)
    {
        $this->DB->query("UPDATE {$this->schema} SET close_time = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($newCloseTime, $surveyId, $whitelabel_id));
    }

    public function updateAnonymous($whitelabel_id, $surveyId, $anonymous)
    {
        $this->DB->query("UPDATE {$this->schema} SET anonymous = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($anonymous, $surveyId, $whitelabel_id));
    }

    public function updatePublicStatus($whitelabel_id, $surveyId, $public)
    {
        $this->DB->query("UPDATE {$this->schema} SET public = ? WHERE survey_id = ? and whitelabel_id = ? LIMIT 1", array($public, $surveyId, $whitelabel_id));
    }

    public function setReadyToSend($whitelabel_id, $surveyId)
    {
        $this->DB->query("UPDATE {$this->schema} SET ready_to_send = 'Yes' WHERE survey_id = ? AND whitelabel_id = ? LIMIT 1", array($surveyId, $whitelabel_id));
    }

    public function retrieveSurveysToSend($processIdentifier)
    {
        //set sent_status to the processIdentifier
        $this->DB->query("UPDATE {$this->schema} SET sent_status = ? WHERE sent_status = 'Unsent' AND open_time <= ? AND ready_to_send = 'Yes'", array($processIdentifier, time()));

        //return surveys sitting in the processIdentifier
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE sent_status = ?", array($processIdentifier))->fetchAll();
    }

    public function markSurveySent($surveyId)
    {
        $this->DB->query("UPDATE {$this->schema} SET sent_status = 'Sent' WHERE survey_id = ? LIMIT 1", array($surveyId));
    }

    public function deleteAllBySurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

}