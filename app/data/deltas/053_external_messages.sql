--
-- Table structure for table `external_messages`
--

CREATE TABLE IF NOT EXISTS `external_messages` (
                                                   `message_id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `whitelabel_id` int(11) NOT NULL,
                                                   `custid` int(11) NOT NULL,
                                                   `approval_required` varchar(10) NOT NULL DEFAULT 'Yes',
                                                   `approved_by` int(11) NOT NULL,
                                                   `ready_to_send` varchar(10) NOT NULL DEFAULT 'No',
                                                   `scheduled_time` int(11) NOT NULL,
                                                   `sent_time` int(11) NOT NULL,
                                                   `type` varchar(20) NOT NULL DEFAULT 'Email',
                                                   `subject` varchar(200) NOT NULL DEFAULT '',
                                                   `body` longtext NOT NULL,
                                                   `preparing` varchar(20) NOT NULL DEFAULT 'No',
                                                   `created_at` int(11) NOT NULL,
                                                   PRIMARY KEY (`message_id`),
                                                   KEY `fk_whitelabel_id_external_messages` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `external_messages`
--
ALTER TABLE `external_messages`
    ADD CONSTRAINT `fk_whitelabel_id_external_messages` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
