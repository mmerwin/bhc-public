--
-- Table structure for table `user_payments_schedule`
--

CREATE TABLE IF NOT EXISTS `user_payment_schedule` (
                                                       `payment_schedule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                                                       `whitelabel_id` int(11) NOT NULL,
                                                       `custid` int(11) NOT NULL,
                                                       `created_at` int(11) NOT NULL,
                                                       `due_by` int(11) NOT NULL,
                                                       `created_by` varchar(150) NOT NULL,
                                                       `amount` int(11) NOT NULL,
                                                       `description` text NOT NULL,
                                                       `charge_type` varchar(100) NOT NULL,
                                                       `foreign_id` int(11) DEFAULT NULL,
                                                       PRIMARY KEY (`payment_schedule_id`),
                                                       KEY `whitelabel_id` (`whitelabel_id`),
                                                       KEY `custid` (`custid`),
                                                       KEY `charge_type` (`charge_type`),
                                                       KEY `due_by` (`due_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
