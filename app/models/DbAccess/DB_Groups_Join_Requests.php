<?php


class DB_Groups_Join_Requests extends DataModel
{
    protected $schema = "groups_join_requests";

    public function addRequest($groupId, $whitelabel_id, $custid)
    {
        $this->DB->query("INSERT INTO {$this->schema} (group_id, whitelabel_id, custid, created_at) VALUES (?, ?, ?, ?)", array($groupId, $whitelabel_id, $custid, time()));
    }

    public function removeRequest($groupId, $whitelabel_id, $custid)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE group_id = ? AND whitelabel_id = ? AND custid = ? LIMIT 1", array($groupId, $whitelabel_id, $custid));
    }

    public function getAllRequestsByGroupId($groupId, $whitelabel_id)
    {
        return $this->DB->query("SELECT jr.group_id, jr.whitelabel_id, jr.custid, jr.created_at, u.fname, u.lname, (SELECT count(*) FROM groups_members gm WHERE gm.group_id = jr.group_id AND gm.whitelabel_id = jr.whitelabel_id) AS member_count, g.max as max_members
                                        FROM groups_join_requests jr
                                        LEFT JOIN users u
                                        ON u.custid = jr.custid
                                        LEFT JOIN groups g
                                        ON g.group_id = jr.group_id
                                        WHERE jr.group_id = ? AND jr.whitelabel_id = ?", array($groupId, $whitelabel_id))->fetchAll();
    }

    public function getAllRequestsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT jr.group_id, g.title, jr.whitelabel_id, jr.custid, jr.created_at, u.fname, u.lname, (SELECT count(*) FROM groups_members gm WHERE gm.group_id = jr.group_id AND gm.whitelabel_id = jr.whitelabel_id) AS member_count, g.max as max_members
                                        FROM groups_join_requests jr
                                        LEFT JOIN users u
                                        ON u.custid = jr.custid
                                        LEFT JOIN groups g
                                        ON g.group_id = jr.group_id
                                        WHERE jr.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getRequestsByUser($custid, $whitelabel_id)
    {
        return $this->DB->query("SELECT jr.group_id, g.title, jr.whitelabel_id, jr.custid, jr.created_at, u.fname, u.lname, (SELECT count(*) FROM groups_members gm WHERE gm.group_id = jr.group_id AND gm.whitelabel_id = jr.whitelabel_id) AS member_count, g.max as max_members
                                        FROM groups_join_requests jr
                                        LEFT JOIN users u
                                        ON u.custid = jr.custid
                                        LEFT JOIN groups g
                                        ON g.group_id = jr.group_id
                                        WHERE jr.whitelabel_id = ? AND jr.custid = ?", array($whitelabel_id, $custid))->fetchAll();
    }
}