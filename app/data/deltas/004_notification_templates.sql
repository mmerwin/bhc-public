-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application
-- Table structure for table `notification_templates`
--
CREATE TABLE IF NOT EXISTS `notification_templates` (
                                                        `template_id` int(11) NOT NULL AUTO_INCREMENT,
                                                        `title` varchar(50) NOT NULL,
                                                        `descr` varchar(50) NOT NULL,
                                                        `message` text NOT NULL,
                                                        `tags` text NOT NULL,
                                                        `icon` varchar(30) NOT NULL,
                                                        `btn` varchar(10) NOT NULL,
                                                        `endpoint` varchar(100) NOT NULL,
                                                        `endpoint_title` varchar(100) NOT NULL,
                                                        PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification_templates`
--

INSERT IGNORE INTO `notification_templates` (`template_id`, `title`, `descr`, `message`, `tags`, `icon`, `btn`, `endpoint`, `endpoint_title`) VALUES
(1, 'Welcome!', 'Welcome to Boathouse Connect!', 'Hey [*fname*],<br><br>Welcome to Boathouse Connect, We\'re glad your here! Boathouse Connect is the premier rowing organization management tool. Whether you are a rower, coach, coxswain, parent, board member, or otherwise involved with rowing, this is the tool for you. Visit our <a href=\"/home/gettingstarted\" target=\"_blank\">Getting Started</a> guide to learn how Boathouse Connect can work for you. Happy rowing!', 'fname', 'far fa-star', 'success', '/home/gettingstarted', 'View Getting Started Guide'),
(2, 'Join Request', 'New pending join request!', 'You have a new join request from [*fname*] [*lname*] to join [*whitelabel_name*]! Go to your dashboard to either approve or decline the join request.', 'fname, lname, whitelabel_name', 'fas fa-plus', 'info', '/dashboard/index', 'Go To Dashboard'),
(3, 'New Org Added', 'Join request has been approved', 'Hey [*fnameRequester*], Congrats! [*fnameApprover*] [*lnameApprover*] has approved you to join [*whitelabel_name*]! You can now have access to the organization next time you login. Happy Rowing!', 'fnameRequester, fnameApprover, lnameApprover, whitelabel_name', 'fas fa-plus', 'info', '/home/logout', 'Activate Org'),
(4, 'User Added', 'New user has been approved to ', '[*fnameRequester*] [*lnameRequester*] has been approved to join [*whitelabel_name*] by [*fnameApprover*] [*lnameApprover*]. ', 'fnameRequester, lnameRequester, fnameApprover, lnameApprover, whitelabel_name', 'fas fa-plus', 'info', '/dashboard/index', 'Go To Dashboard'),
(5, 'Join Request Declined', 'Join request has been declined', 'Uh-oh, [*fnameApprover*] [*lnameApprover*] declined your request to join [*whitelabel_name*]. If you feel that was a mistake, please contact [*whitelabel_name*] outside of Boathouse Connect to get clarification on why your request was decline. Once the issue is resolved, [*fnameApprover*] [*lnameApprover*] can approve the request.', 'fnameApprover, lnameApprover, whitelabel_name', 'fas fa-frown', 'danger', '/profile/myprofile', 'Go To My Profile'),
(6, 'New Org Added', 'Pre-approved to join org', 'Hey [*fnameRequester*], Congrats! You have been pre-approved to join [*whitelabel_name*]! You can now have access to the organization. Happy Rowing!', 'fnameRequester, whitelabel_name', 'fas fa-plus', 'info', '/dashboard', 'Go To Dashboard'),
(7, 'New Group', 'You are a member of a new group', 'You have been added to the group \"[*groupName*]\" by [*fname*] [*lname*]! Go to your My Groups page to get more information. ', 'groupName, fname, lname', 'mr-2 mdi mdi-account-star', 'primary', '/groups/mygroups', 'My Groups'),
(8, 'Removed From Group', 'You have been removed from a group', 'You have been removed from the group \"[*groupName*]\" by [*fname*] [*lname*] If you feel this was in error, please contact [*fname*] [*lname*] for clarification.\r\n', 'groupName, fname, lname', 'mr-2 mdi mdi-account-off', 'danger', 'groups/mygroups', 'View My Groups'),
(9, 'Group Join Request', 'A new request to join a group', '[*fname*] [*lname*] has requested to join the group [*groupName*]. Go to Groups->Join Requests to approve or decline the request.', 'fname, lname, groupName', '', 'info', '/groups/joinrequests', 'See Join Request'),
(10, 'Group Join Request Declined', 'Your request to join a group was decline', 'Your request to join the group [*groupName*] has been declined by [*fname*] [*lname*]. If you believe this is an error, please contact [*fname*] to find out why your request was declined.', 'groupName, fname, lname', '', 'danger', '/groups/mygroups', 'View My Goups'),
(11, 'Equipment Issue Reported', 'A user has reported an equipment issue', '[*name_sender*] has reported equipment issues while submitting a post-practice survey:<br><br>[*survey_feedback*]', 'name_sender, survey_feedback', 'mr-2 mdi mdi-wrench', 'danger', '/equipment/reportedIssues', 'View Equipment issues'),
(12, 'New Message Requires Approval', 'There is a new message that must be approved', '[*senderName*] has requested to send a [*messageType*] but does not have proper permissions to send messages. Please review the message and approve or decline it.', 'senderName, messageType', 'mr-2 mdi mdi-email-outline', 'info', '/communications/pending', 'View Pending Messages'),
(13, 'Payment Error', 'Payment to Boathouse Connect has been declined', 'Your most recent payment to Boathouse Connect has been declined. Please update your organization\'s payment method. If the account remains unpaid after 20 days, the organization account may be suspended.', '', 'fas fa-dollar-sign', 'danger', '/whitelabel/plan', 'Subscription Settings');
COMMIT;
