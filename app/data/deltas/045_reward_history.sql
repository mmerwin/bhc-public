--
-- Table structure for table `reward_history`
--

CREATE TABLE IF NOT EXISTS `reward_history` (
                                                `reward_hist_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `custid` int(11) NOT NULL,
                                                `whitelabel_id` int(11) NOT NULL,
                                                `trigger_id` int(11) NOT NULL,
                                                `points_earned` int(11) NOT NULL,
                                                `timestamp` int(11) NOT NULL,
                                                PRIMARY KEY (`reward_hist_id`),
                                                KEY `fk_whitelabel_id_rewards_earned` (`whitelabel_id`),
                                                KEY `fk_trigger_id_rewards_history` (`trigger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reward_history`
--
ALTER TABLE `reward_history`
    ADD CONSTRAINT `fk_trigger_id_rewards_history` FOREIGN KEY (`trigger_id`) REFERENCES `reward_triggers` (`reward_trigger_id`),
    ADD CONSTRAINT `fk_whitelabel_id_rewards_earned` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
