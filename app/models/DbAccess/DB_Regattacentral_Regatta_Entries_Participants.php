<?php


class DB_Regattacentral_Regatta_Entries_Participants extends DataModel
{
    protected $schema = "regattacentral_regatta_entries_participants";

    public function addParticipant($regattaId, $eventId, $entryId, $participantId, $organizationId, $sequence, $type)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (regatta_id, event_id, entry_id, participant_id, 
                          organization_id, sequence, type) VALUES (?, ?, ?, ?, ?, ?, ?)",
            array($regattaId, $eventId, $entryId, $participantId, $organizationId, $sequence, $type));
    }

    public function getParticipantsInEntry($regattaId, $eventId, $entryId)
    {
        return $this->DB->query("SELECT rrep.regatta_id, rrep.event_id, rrep.entry_id, rrep.participant_id, 
       rpo.fname, rpo.lname, rrep.organization_id, ro.name as org_name, ro.abbreviation as org_abbreviation, 
       rrep.sequence, rrep.type FROM regattacentral_regatta_entries_participants rrep
        LEFT JOIN regattacentral_participant_organizations rpo
        ON rpo.participant_id = rrep.participant_id AND rpo.organization_id = rrep.organization_id
        LEFT JOIN regattacentral_orgs ro
        ON ro.organizationId = rrep.organization_id
        WHERE rrep.regatta_id = ? AND rrep.event_id = ? AND rrep.entry_id = ? AND rrep.type !='coach'
        ORDER BY sequence DESC
        ", array($regattaId, $eventId, $entryId))->fetchAll();
    }

    public function getEntryByEventAndCustid($regattaId, $eventId, $custid)
    {
        return $this->DB->query("SELECT * FROM regattacentral_regatta_entries_participants WHERE participant_id IN ( 
                                    SELECT participant_id FROM regattacentral_participant_custid_map WHERE custid = ? ) 
                                      AND regatta_id = ? AND event_id = ?",
            array($custid, $regattaId, $eventId))->fetchArray();
    }

}