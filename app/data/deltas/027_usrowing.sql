--
-- Table structure for table `usrowing`
--

CREATE TABLE IF NOT EXISTS `usrowing` (
                                          `usrowingID` int(11) NOT NULL,
                                          `custid` int(11) NOT NULL,
                                          `expires` int(11) NOT NULL,
                                          `last_updated` int(11) NOT NULL,
                                          PRIMARY KEY (`usrowingID`),
                                          KEY `fk_custid_usrowing` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `usrowing`
--
ALTER TABLE `usrowing`
    ADD CONSTRAINT `fk_custid_usrowing` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
