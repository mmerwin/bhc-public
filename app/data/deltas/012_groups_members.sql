--
-- Table structure for table `groups_members`
--

CREATE TABLE IF NOT EXISTS `groups_members` (
                                                `group_member_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `whitelabel_id` int(11) NOT NULL,
                                                `group_id` int(11) NOT NULL,
                                                `custid` int(11) NOT NULL,
                                                `created_at` int(11) NOT NULL,
                                                PRIMARY KEY (`group_member_id`),
                                                KEY `fk_whitelabel_group_members` (`whitelabel_id`),
                                                KEY `fk_groups_membership` (`group_id`),
                                                KEY `fk_custid_group_membership` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups_members`
--
ALTER TABLE `groups_members`
    ADD CONSTRAINT `fk_custid_group_membership` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_groups_membership` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_group_members` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
