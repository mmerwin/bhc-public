<?php


class DB_Regattacentral_Participant_Organizations extends DataModel
{
    protected $schema = "regattacentral_participant_organizations";

    public function addParticipantId($participantId, $organizationId, $fname, $lname, $gender, $memberId, $custid = null)
    {
        if (!is_null($custid)) {
            $this->DB->query("INSERT IGNORE INTO {$this->schema} (participant_id, organization_id, fname, lname, 
                          gender, member_id, custid) VALUES (?, ?, ?, ?, ?, ?, ?)",
                array($participantId, $organizationId, $fname, $lname, $gender, $memberId, $custid));
        } else {
            $this->DB->query("INSERT IGNORE INTO {$this->schema} (participant_id, organization_id, fname, lname, 
                          gender, member_id) VALUES (?, ?, ?, ?, ?, ?)",
                array($participantId, $organizationId, $fname, $lname, $gender, $memberId));
        }
    }

    public function setCustid($participantId, $custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET custid = ? WHERE participant_id = ?",
            array($custid, $participantId));
    }

}