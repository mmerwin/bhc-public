<?php


class DB_Postmark_Domains extends DataModel
{
    protected $schema = "postmark_domains";

    public function getDomainsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getDomain($whitelabel_id, $domain)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND name = ?", array($whitelabel_id, $domain))->fetchArray();
    }

    public function addDomain($whitelabel_id,$data)
    {
        $this->DB->query("INSERT INTO `postmark_domains` (`whitelabel_id`, `name`, `spf_verified`, `spf_host`, `spf_text_value`, `dkim_verified`, `weak_dkim`, `dkim_host`, `dkim_text_value`, `dkim_pending_host`, `dkim_pending_text_value`, `dkim_revoked_host`, `dkim_revoked_text_value`, `safe_to_remove_revoked_key_from_dns`, `dkim_update_status`, `return_path_domain`, `return_path_domain_verified`, `return_path_domain_cname_value`, `id`) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
            array($whitelabel_id, $data["Name"], $this->booleanToString($data["SPFVerified"]), $data["SPFHost"], $data["SPFTextValue"], $this->booleanToString($data["DKIMVerified"]), $this->booleanToString($data["WeakDKIM"]), $data["DKIMHost"], $data["DKIMTextValue"], $data["DKIMPendingHost"], $data["DKIMPendingTextValue"], $data["DKIMRevokedHost"], $data["DKIMRevokedTextValue"], $this->booleanToString($data["SafeToRemoveRevokedKeyFromDNS"]), $data["DKIMUpdateStatus"], $data["ReturnPathDomain"], $this->booleanToString($data["ReturnPathDomainVerified"]), $data["ReturnPathDomainCNAMEValue"], $data["ID"]));
    }

    public function replaceDomain($whitelabel_id, $data)
    {
        //delete existing domain record
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND name = ? LIMIT 1", array($whitelabel_id, $data['Name']));
        //add domain record with updated data
        $this->addDomain($whitelabel_id, $data);
        return $this->getDomainById($data['ID']);
    }

    public function getDomainById($domainId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE id = ?", array($domainId))->fetchArray();
    }

    public function getAllDomains()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

}