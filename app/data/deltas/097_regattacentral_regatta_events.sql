--
-- Table structure for table `regattacentral_regatta_events`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regatta_events` (
                                                               `regatta_id` int(11) NOT NULL,
                                                               `event_id` int(11) NOT NULL,
                                                               `sequence` int(11) NOT NULL,
                                                               `label` varchar(50) NOT NULL,
                                                               `code` varchar(200) NOT NULL,
                                                               `title` varchar(200) NOT NULL,
                                                               `min_athlete_age` int(11) NOT NULL,
                                                               `max_athlete_age` int(11) NOT NULL,
                                                               `min_avg_age` int(11) NOT NULL,
                                                               `max_avg_age` int(11) NOT NULL,
                                                               `athlete_class` varchar(20) NOT NULL,
                                                               `athlete_count_excluding_cox` int(11) NOT NULL,
                                                               `sweep` varchar(10) NOT NULL,
                                                               `coxed` varchar(10) NOT NULL,
                                                               `cost` varchar(10) NOT NULL,
                                                               `deadline_model` int(11) NOT NULL,
                                                               `default_race_format` varchar(20) NOT NULL,
                                                               `final_race_time` int(11) NOT NULL,
                                                               `default_race_units` varchar(20) NOT NULL,
                                                               `default_handicap_algorithm` text NOT NULL,
                                                               `default_handicap_multiplier` text NOT NULL,
                                                               `default_duration` int(11) NOT NULL,
                                                               `max_entries` int(11) NOT NULL,
                                                               `max_entries_per_club` int(11) NOT NULL,
                                                               `max_alternates` int(11) NOT NULL,
                                                               `require_coxswain` varchar(10) NOT NULL,
                                                               `status` varchar(50) NOT NULL,
                                                               `weight` varchar(50) NOT NULL,
                                                               `gender` varchar(20) NOT NULL,
                                                               PRIMARY KEY (`regatta_id`,`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
