# Boathouse Connect API Environment Setup
This Readme contains both the setup for Boathouse Connect and Merwin-MVC. The guides for the Merwin-MVC default setup are at the 
end of this document.

-- What is required:
* PHP version 7.4.6 or later. Earlier versions of PHP are not guaranteed. Please update this guide if new versions require different instructions
* All configurations as outlined in Merwin-MVC
* Composer (for php dependencies in app/vendor)
* cURL php extension
* php-mbstring extension
* SSL certificates from LetsEncrypt Certbot for the boathouse Connect api subdomain
* MySQL database locally. ** As currently designed, the db must be local
* .htaccess with mod_rewrite enabled (see Merwin-MVC)
* change session_name() in app/core/App.php to an unrecognizable random 27-character long string of letters and numbers. This helps keep the session data unique and unguessable.

-- Composer Setup <br>
Make sure composer (the package) is installed on the server. This can be done on the command line by following the DigitalOcean guide: 
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04 (** check the version of Ubuntu and that
the guide is not outdated)
<br>Step 1: $ sudo apt update
<br>Step 2: $ sudo apt install curl php-cli php-mbstring git unzip //this will install curl and php-mbstring
<br>Step 3: $ cd ~ && curl -sS https://getcomposer.org/installer -o composer-setup.php
<br>Step 4: $ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer //this installs Composer globally, it will not need to be done for each project.
<br>Step 5: test the installation by $ composer
<br><br>With Composer setup on the server, download the dependencies for this project
<br>Step 1: $ cd [*absolute-path-to-this-project*]/app && git pull && composer install
<br>Step 2: Check all required dependencies have been installed. $ cd vendor && ls
<br><br>As of January. 28, 2021 the subdirectories in the vendor directory should include (at a minimum):
* apimatic
* bandwidth
* guzzlehttp
* jwhennessey
* mailchimp
* mashape
* paragonie
* php-ai
* psr
* ralouphie
* stripe
* symfony
* wildbit
* rollbar

--MySQL Requirements<br>
Boathouse Connect databases in all environments should use the same credentials as follows:
* database name: [redacted]
* user: [redacted]
* password: [redacted]
* privileges - ALL PRIVILEGES (read/write/update/delete)
* table-specific-privileges: NO

-- Setup VirtualHosts for Boathouse Connect API<br>
In order to house multiple services and applications on the same dev, test, stage, UAT, and production servers, Apache 
Virtual Hosts is utilized. This guide assumes that the location of this README.md file is /var/www/boathouseconnectapi/README.md<br>
This means that the repository is contained in /var/www/boathouseconnectapi. To set up the virtual host follow these steps:
<br>Step 1: Enable mod_rewrite for Apache using $ sudo a2enmod rewrite
<br>Step 2: Go to the site-enabled directory $ cde /etc/apache2/sites-enabled
<br>Step 3: Create a new config file with the name being the landing url that the server should recognize. For example, UAT environment would be "uat-api.boathouseconnect.com.conf"
<br>Step 4: In your newly created config file, add the following code snippet. We are assuming that you are setting it up for UAT:
<br><br>
```
<VirtualHost *:80>
        ServerAdmin ServerSupport@BoathouseConnect.com
        DocumentRoot /var/www/boathouseconnectapi/public

        <Directory /var/www/boathouseconnectapi/>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
        </Directory>
        ServerAdmin ServerSupport@BoathouseConnect.com
        ServerName uat-api.boathouseconnect.com
        ServerAlias user-acceptance-test-api.boathouseconnect.com
ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        <IfModule mod_dir.c>
            DirectoryIndex index.php index.pl index.cgi index.html index.xhtml index.htm
        </IfModule>

RewriteEngine on
RewriteCond %{SERVER_NAME} =uat-api.boathouseconnect.com [OR]
RewriteCond %{SERVER_NAME} =user-acceptance-test-api.boathouseconnect.com
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

```
Add ServerAlias lines for each sub-domain that will exist on the server that you want to point to the API.
In the example above, the urls "uat-api.boathouseconnect.com" and "user-acceptance-test-api.boathouseconnect.com" exist
on the server and will point to the directory /var/www/boathouseconnectapi/
<br><br>Step 5: Enable the new virtual hosts using $ sudo a2ensite uat-api.boathouseconnect.com.conf //note the file name that we are enabling.
This only needs to be done for each new virtual host file, not for each ServerAlias.
<br>Step 6: Disable the default conf file. This is usually "000-default.conf". Check what the file it is by looking in the /vet/apache2/sites-enabled directory
and performing this step for each of the pre-existing conf files in the directory: $ sudo a2dissite 000-default.conf<br>
If something has gone horribly wrong, you can re-enable the conf files that you just disabled by following step 5 except changing the file to any one that you want to re-enable.
<br>Step 7: Restart Apache by using one of the two following commands: $ <b>sudo systemctl restart apache2</b>  //or// $ <b>sudo service apache2 restart </b>


# merwin-MVC default setup.
(You can ignore these setup instructions if you are following the Boathouse Connect specific setup above)

Installation and setup:

A) Make sure to active the .htaccess files using the instructions found on http://merwincode.com/viewElements.php?ID=6 under "How to Activate an .htaccess file"

How to activate an .htaccess file on DigitalOcean Ubuntu Server

This is derived from the following post: https://www.digitalocean.com/community/questions/how-to-activate-an-htaccess-file

1) enable mod_rewrite using the command: sudo a2enmod rewrite

2) go to the /etc/apache2/sites-available/000-default.conf file

3) add the following to the bottom of the page

```
 <Directory /var/www/html/public>

 Options Indexes FollowSymLinks MultiViews
 AllowOverride All
 Order allow,deny
 allow from all
 </Directory>
```
In that same file, change the line 
```
DocumentRoot /var/www/html
```
to
```
DocumentRoot /var/www/html/public
```
This will change apache's default root directory for port 80. 

restart apache server using: sudo service apache2 restart


B) update the .htaccess file located in merwin-mvc/.htaccess to change " RewriteBase /merwin-mvc " to the directory you want the framework to be hosted inside.

C) change session_name() in app/core/App.php to an unrecognizable random string of letters and numbers. This helps keep the session data unique and unguessable.