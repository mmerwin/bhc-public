--
-- Table structure for table `equipment_reservations`
--

CREATE TABLE IF NOT EXISTS `equipment_reservations` (
                                                        `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
                                                        `whitelabel_id` int(11) NOT NULL,
                                                        `custid` int(11) NOT NULL,
                                                        `created_at` int(11) NOT NULL,
                                                        `start_time` int(11) NOT NULL,
                                                        `end_time` int(11) NOT NULL,
                                                        `type` varchar(50) NOT NULL,
                                                        PRIMARY KEY (`reservation_id`),
                                                        KEY `fk_whitelabel_id_reservations_all` (`whitelabel_id`),
                                                        KEY `fk_custid_reservations_all` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_reservations`
--
ALTER TABLE `equipment_reservations`
    ADD CONSTRAINT `fk_custid_reservations_all` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_reservations_all` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
