<?php


class Service_Practice_Series
{
    public function getAllSeries($whitelabel_id)
    {
        $rawSeries = (new DB_Practice_Series_Meta())->getAllWhitelabelSeries($whitelabel_id);
        $formatted = [];
        $location  = (new PracticeLocations($whitelabel_id));
        foreach ($rawSeries as $series) {
            $formatted[] = [
                "whitelabel_id"             => $series["whitelabel_id"],
                "series_id"                 => $series['series_id'],
                "name"                      => $series["name"],
                "location"                  => $location->getLocationById($series["location_id"]),
                "max_attendees"             => $series["max_attendees"],
                "start_date"                => $series["start_date"],
                "end_date"                  => $series["end_date"],
                "visiting_rowers_allowed"   => $series["visiting_rowers"],
                "finalized"                 => $series["finalized"],
                "created_series_at"         => $series["created_at"]
            ];
        }

        return $formatted;
    }

    public function createNewSeries($whitelabel_id, $name, $locationId, $maxAttendees, $startDate, $endDate, $visitingRowers, $startTime, $endTime, $custid, array $daysArray)
    {
        // check that location belongs to whitelabel
        $locationMeta = (new DB_Practice_Locations())->getByLocationId($whitelabel_id, $locationId);
        if (isset($locationMeta["location_id"]) && $locationMeta["location_id"] == $locationId) {
            $locationId = $locationMeta["location_id"];
        } else {
            $locationId = 0;
        }
        $newSeriesId = (new DB_Practice_Series_Meta())->createSeries($whitelabel_id, $name, $locationId, $maxAttendees, $startDate, $endDate, $visitingRowers);

        //create practices for days in $daysArray for duration of the series
        foreach ($daysArray as $day) {
            $this->buildSeriesPractices($whitelabel_id, $day, $startDate, $endDate, $startTime, $endTime, $newSeriesId, $custid);
        }

        return $newSeriesId;
    }

    private function buildSeriesPractices($whitelabel_id, $day, $startDate, $endDate, $startTime, $endTime, $seriesId, $custid)
    {
        $currentDate     = ($startDate-86400); //this is to make sure we are not starting on the "first date"
        $practiceService = new Service_Practices($whitelabel_id);
        do {
            $currentDate    = strtotime("next {$day} ", $currentDate);
            if ($currentDate >= ($endDate+86400)) {
                break;
            }
            $practiceStart  = strtotime($startTime,$currentDate);
            $practiceEnd    = strtotime($endTime, $currentDate);

            // create practice
            $practiceService->createPractice($whitelabel_id, $seriesId, null, null, $practiceStart, $practiceEnd, null, "No", null, $custid);
        } while ($currentDate <= $endDate);

    }

    public function getSeriesById($whitelabel_id, $seriesId)
    {
        $rawSeriesMeta = (new DB_Practice_Series_Meta())->getSeriesById($whitelabel_id, $seriesId);
        if (isset($rawSeriesMeta["whitelabel_id"])) {
            return array(
                "whitelabel_id"             => $rawSeriesMeta["whitelabel_id"],
                "series_id"                 => $rawSeriesMeta["series_id"],
                "name"                      => $rawSeriesMeta["name"],
                "location"                  => (new PracticeLocations($rawSeriesMeta["whitelabel_id"]))->getLocationById($rawSeriesMeta["location_id"]),
                "max_attendees"             => $rawSeriesMeta["max_attendees"],
                "start_date"                => $rawSeriesMeta["start_date"],
                "end_date"                  => $rawSeriesMeta["end_date"],
                "visiting_rowers_allowed"   => $rawSeriesMeta["visiting_rowers"],
                "finalized"                 => $rawSeriesMeta["finalized"],
                "series_created_at"         => $rawSeriesMeta["created_at"],
                "practices_scheduled"       => (new DB_Practice_Schedule_Meta())->getPracticesInSeries($whitelabel_id, $rawSeriesMeta["series_id"])
            );
        } else {
            return array("Status" => "Error", "Message"=>"Series not found");
        }

    }
}