<?php


class DB_Whitelabel_Calendar_Events extends DataModel
{
    protected $schema = "whitelabel_calendar_events";

    public function addEvent($whitelabel_id, $startTime, $endTime, $public, $title, $descr, $linkColor)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, start_time, end_time, public, title, descr, link_color) VALUES (?, ?, ?, ?, ?, ?, ?)",
            array($whitelabel_id, $startTime, $endTime, $public, $title, $descr, $linkColor));
    }

    public function removeEvent($whitelabel_id, $eventId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND event_id = ? LIMIT 1", array($whitelabel_id, $eventId));
    }

    public function getUpcomingEvents($whitelabel_id = null)
    {
        if ($whitelabel_id) {
            $whitelabelQuery = "AND whitelabel_id = '".intval($whitelabel_id)."'";
        } else {
            $whitelabelQuery = "";
        }
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE end_time >= ? {$whitelabelQuery} ORDER BY start_time ASC", array(time()))->fetchAll();
    }

    public function getEventsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? ORDER BY start_time ASC", array($whitelabel_id))->fetchAll();
    }

    public function getEventById($eventId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE event_id = ? LIMIT 1", array($eventId))->fetchArray();
    }

}