<?php


class DB_Equipment_Oars extends DataModel
{
    protected $schema = "equipment_oars";
    protected $subschemaA = "equipment_oars_sets";

    public function addOar($setId, $side, $desc, $status = "Available")
    {
        $this->DB->query("INSERT INTO {$this->schema} (oar_set_id, side, descriptor, status, created_at) VALUES (?, ?, ?, ?, ?)",array($setId, $side, $desc, $status, time()));
    }

    public function deleteOar($setId, $oarId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE oar_id = ? AND oar_set_id = ? LIMIT 1", array($oarId, $setId));
    }

    public function updateDesc($oarId, $newDesc)
    {
        $this->DB->query("UPDATE {$this->schema} SET descriptor = ? WHERE oar_id = ? LIMIT 1", array($newDesc, $oarId));
    }

    public function setAsAvailable($oarId)
    {
        $this->DB->query("UPDATE {$this->schema} SET status = 'Available' WHERE oar_id = ? LIMIT 1", array($oarId));
    }

    public function setAsOutOfService($oarId)
    {
        $this->DB->query("UPDATE {$this->schema} SET status = 'Out Of Service' WHERE oar_id = ? LIMIT 1", array($oarId));
    }

    public function getAllOarsForWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT eo.oar_set_id, eos.name as set_name, eos.type as set_type, eos.descr as set_descr, eos.manufacturer, eos.model, eo.oar_id, eo.side, eo.descriptor, eo.status, eo.created_at
                                FROM {$this->subschemaA} eos
                                LEFT JOIN {$this->schema} eo
                                ON eo.oar_set_id = eos.oar_set_id
                                WHERE eos.whitelabel_id = ? AND eo.oar_id IS NOT NULL", array($whitelabel_id))->fetchAll();
    }

    public function getAllOarsInSet($setId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE oar_set_id = ?", array($setId))->fetchAll();
    }

    public function getOutOfServiceOars($whitelabel_id)
    {
        return $this->DB->query("SELECT eo.oar_set_id, eos.name as set_name, eos.type as set_type, eos.descr as set_descr, eos.manufacturer, eos.model, eo.oar_id, eo.side, eo.descriptor, eo.status, eo.created_at
                                        FROM {$this->subschemaA} eos
                                        LEFT JOIN {$this->schema} eo
                                        ON eo.oar_set_id = eos.oar_set_id
                                        WHERE eos.whitelabel_id = 127 AND eo.oar_id IS NOT NULL AND status = 'Out Of Service'")->fetchAll();
    }


}