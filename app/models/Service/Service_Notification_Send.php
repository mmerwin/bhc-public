<?php


class Service_Notification_Send
{

    public static function addNotificationToAllAllowedUsers($permissionId, $templateId, $whitelabel_id, $mergeTags, $endpointParams = [])
    {
        // get users with permission provided
        if (is_array($permissionId)) {
            $allowedUsers = Service_Permissions::findUsersWithPermissions($permissionId, $whitelabel_id);
        } else {
            $allowedUsers = Service_Permissions::findUsersWithPermission($permissionId, $whitelabel_id);
        }
        // add notification to all with permission
        foreach ($allowedUsers as $receiver) {
            $notification = new Notification($receiver['custid'], $whitelabel_id);
            $notification->newNotification($templateId, $mergeTags,$endpointParams);
        }
    }

    private static function addNotificationToSingleUser($custid, $templateId, $whitelabel_id, $mergeTags, $endpointParams = [])
    {
        $notification = new Notification($custid, $whitelabel_id);
        $notification->newNotification($templateId, $mergeTags,$endpointParams);
    }

    private static function getWhitelabelName($whitelabel_id)
    {
        // get whitelabel name
        $whitelabel = Service_Whitelabel::findNameById($whitelabel_id);
        return $whitelabel[0]['name'];
    }
    public static function newWhitelabelJoinRequest($custid_requester, $whitelabel_id)
    {
        // find name of custid holder
        $user = new User($custid_requester);

        $whitelabel_name = self::getWhitelabelName($whitelabel_id);

        $mergeTags = array("fname"=>$user->fname, "lname"=>$user->lname, "whitelabel_name"=>$whitelabel_name);
        self::addNotificationToAllAllowedUsers(2, 2, $whitelabel_id, $mergeTags);

    }

    public static function newapprovedjoinrequest($custid_requester, $custid_approver, $whitelabel_id, $preapproved = false)
    {
        // custid is of the newly approved user
        $user_requester = new User($custid_requester);
        $user_approver  = new User($custid_approver);

        $whitelabel_name = self::getWhitelabelName($whitelabel_id);
        if ($preapproved) {
            $templateId = 6;
            $mergeTags = array("fnameRequester"=>$user_requester->fname, "lnameRequester"=>$user_requester->lname, "whitelabel_name"=>$whitelabel_name,
                "fnameApprover"=>"a", "lnameApprover"=>"pre-authorization email");
        } else {
            $templateId = 3;
            $mergeTags = array("fnameRequester"=>$user_requester->fname, "lnameRequester"=>$user_requester->lname, "whitelabel_name"=>$whitelabel_name,
                "fnameApprover"=>$user_approver->fname, "lnameApprover"=>$user_approver->lname);
        }

        //add notifications
        self::addNotificationToSingleUser($custid_requester, $templateId, 0, $mergeTags);
        self::addNotificationToAllAllowedUsers(2, 4, $whitelabel_id, $mergeTags);

    }

    public static function newdeclinedjoinrequest($custid_requester, $custid_decliner, $whitelabel_id)
    {
        // custid is of the newly approved user
        $user_approver   = new User($custid_decliner);

        $whitelabel_name = self::getWhitelabelName($whitelabel_id);
        $mergeTags       = array("whitelabel_name"=>$whitelabel_name,
            "fnameApprover"=>$user_approver->fname, "lnameApprover"=>$user_approver->lname);

        // add notifications
        self::addNotificationToSingleUser($custid_requester, 5, 0, $mergeTags);
    }

    public static function addedToGroup($custidMember, $groupId)
    {
        $groups        = new Groups($_SESSION['whitelabel_id']);
        $custidAdder   = $_SESSION['custid'];
        $groupMetaData = $groups->getGroupMetaData($groupId);
        $users         = new User($custidAdder);

        $data = [
            "groupName"=>$groupMetaData['title'],
            "fname" =>$users->fname,
            "lname"=>$users->lname
        ];
        self::addNotificationToSingleUser($custidMember, 7, $_SESSION['whitelabel_id'], $data);
    }

    public static function removeFromGroup($custidMember, $groupId)
    {
        $groups        = new Groups($_SESSION['whitelabel_id']);
        $custidAdder   = $_SESSION['custid'];
        $groupMetaData = $groups->getGroupMetaData($groupId);
        $users         = new User($custidAdder);

        $data = [
            "groupName"=>$groupMetaData['title'],
            "fname" =>$users->fname,
            "lname"=>$users->lname
        ];

        self::addNotificationToSingleUser($custidMember, 8, $_SESSION['whitelabel_id'], $data);

    }

    public static function requestToJoinGroup($custidMember, $groupId, $whitelabel_id)
    {
        $groups        = new Groups($_SESSION['whitelabel_id']);
        $groupMetaData = $groups->getGroupMetaData($groupId);
        $users         = new User($custidMember);
        $data = [
            "groupName"=>$groupMetaData['title'],
            "fname" =>$users->fname,
            "lname"=>$users->lname
        ];

        self::addNotificationToAllAllowedUsers(4, 9, $whitelabel_id, $data);
    }

    public static function declinedToJoinGroup($custidDecliner, $custidRequester, $groupId, $whitelabel_id)
    {
        $groups        = new Groups($_SESSION['whitelabel_id']);
        $groupMetaData = $groups->getGroupMetaData($groupId);
        $users         = new User($custidDecliner);
        $data = [
            "groupName"=>$groupMetaData['title'],
            "fname" =>$users->fname,
            "lname"=>$users->lname
        ];

        self::addNotificationToSingleUser($custidRequester, 10, $whitelabel_id, $data);
    }


    public static function newMessageRequiringApproval($custidMessageCreator,$whitelabel_id, $messageType)
    {
        // custid is of the message creator
        $user_creator = new User($custidMessageCreator);
        $templateId   = 12;
        $mergeTags    = array("senderName"=>($user_creator->fname. ' '. $user_creator->lname), "messageType"=>$messageType);

        // add notifications
        self::addNotificationToAllAllowedUsers(array(12, 14), $templateId, $whitelabel_id, $mergeTags);
    }

    public static function equipmentIssueFromSurvey($custidMessageCreator, $whitelabel_id, $feedback)
    {
        // custid is of the message creator
        $user_creator = new User($custidMessageCreator);
        $templateId   = 11;
        $mergeTags    = array("name_sender"=>($user_creator->fname. ' '. $user_creator->lname), "survey_feedback"=>$feedback);

        // add notifications
        self::addNotificationToAllAllowedUsers(array(7,8,9,10), $templateId, $whitelabel_id, $mergeTags);
    }

}