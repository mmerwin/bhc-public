<?php


class DB_Equipment_Issues extends DataModel
{
    protected $schema = "equipment_issues";

    public function addIssue($whitelabel_id, $custid, $descr)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, custid, timestamp, descr) VALUES (?, ?, ?, ?)", array($whitelabel_id, $custid, time(), $descr));
    }

    public function removeIssue($whitelabel_id, $issueId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND id = ? LIMIT 1", array($whitelabel_id, $issueId));
    }

    public function getWhitelabelIssues($whitelabel_id)
    {
        return $this->DB->query("SELECT ei.id, ei.whitelabel_id, ei.custid, u.fname, u.lname, ei.timestamp, ei.descr FROM equipment_issues ei
                                        LEFT JOIN users u 
                                        ON u.custid = ei.custid 
                                        WHERE whitelabel_id = ? 
                                        ORDER BY timestamp DESC", array($whitelabel_id))->fetchAll();
    }

    public function updateIssue($whitelabel_id, $issueId, $descr)
    {
        $this->DB->query("UPDATE {$this->schema} SET descr = ?, timestamp = ? WHERE whitelabel_id = ? AND id = ? LIMIT 1",array($descr, time(), $whitelabel_id, $issueId));
    }

}