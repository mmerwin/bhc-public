<?php


class DB_User_Payment_Schedule extends DataModel
{
    protected $schema = "user_payment_schedule";

    public function createCharge($whitelabel_id, $custid, $dueBy, $createdBy, $amount, $description, $chargeType, $foreignId = null)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, custid, created_at, due_by, created_by, amount, description, charge_type, foreign_id) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                array($whitelabel_id, $custid, time(), $dueBy, $createdBy, $amount, $description, $chargeType, $foreignId));
    }

    public function removeSpecificCharge($whitelabel_id, $custid, $paymentScheduleId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND payment_schedule_id = ? LIMIT 1", array($whitelabel_id, $custid, $paymentScheduleId));
    }

    public function removeByFutureChargeType($whitelabel_id, $custid, $chargeType, $foreignId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND charge_type = ? AND foreign_id = ? AND due_by >= ?",
            array($whitelabel_id, $custid, $chargeType, $foreignId, time()));
    }

    public function getDistinctAccounts($whitelabel_id = null)
    {
        if ($whitelabel_id) {
            return $this->DB->query("SELECT ups.whitelabel_id, ups.custid, u.fname, u.lname, l.email
                                            FROM user_payment_schedule ups
                                            LEFT JOIN users u
                                            ON u.custid = ups.custid
                                            LEFT JOIN logins l
                                            ON l.custid = ups.custid
                                            WHERE ups.whitelabel_id = ?
                                            GROUP BY whitelabel_id, custid
                                            ORDER BY lname ASC", array($whitelabel_id))->fetchAll();
        } else {
            return $this->DB->query("SELECT ups.whitelabel_id, ups.custid, u.fname, u.lname, l.email
                                            FROM user_payment_schedule ups
                                            LEFT JOIN users u
                                            ON u.custid = ups.custid
                                            LEFT JOIN logins l
                                            ON l.custid = ups.custid
                                            GROUP BY whitelabel_id, custid
                                            ORDER BY lname ASC")->fetchAll();
        }
    }

    public function getPastDueAccounts($whitelabel_id = null)
    {
        $env = Environment::getEnvCode();
        if ($whitelabel_id) {
            return $this->DB->query("SELECT tbl.whitelabel_id, tbl.custid, tbl.outstanding, wca.account_id as whitelabel_account, usa.stripe_customer_id as user_customer_id, 
                                            usa.preferred_payment_method, usa.delinquent, u.fname, u.lname, l.email, wl.name as whitelabel_name  FROM (SELECT ups.whitelabel_id, ups.custid, 
                                            ((SELECT sum(amount) FROM user_payment_schedule WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid AND due_by < ?) - 
                                            IFNULL((select sum(amount_charged) FROM user_payment_credits WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid),0)) as outstanding
                                            FROM user_payment_schedule ups 
                                            GROUP BY whitelabel_id, custid) tbl
                                            LEFT JOIN whitelabel_connect_accounts wca
                                            ON wca.whitelabel_id = tbl.whitelabel_id and wca.env = '{$env}'
                                            LEFT JOIN user_stripe_accounts usa
                                            ON usa.custid = tbl.custid and usa.env = '{$env}'
                                            LEFT JOIN users u
                                            ON u.custid = tbl.custid
                                            LEFT JOIN logins l
                                            ON l.custid = tbl.custid
                                            LEFT JOIN whitelabels wl
                                            ON wl.whitelabel_id = tbl.whitelabel_id
                                            WHERE outstanding > 0 AND tbl.whitelabel_id = ? ORDER BY lname ASC", array(time(),$whitelabel_id))->fetchAll();
        } else {
            return $this->DB->query("SELECT tbl.whitelabel_id, tbl.custid, tbl.outstanding, wca.account_id as whitelabel_account, usa.stripe_customer_id as user_customer_id, 
                                            usa.preferred_payment_method, usa.delinquent, u.fname, u.lname, l.email, wl.name as whitelabel_name  FROM (SELECT ups.whitelabel_id, ups.custid, 
                                            ((SELECT sum(amount) FROM user_payment_schedule WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid AND due_by < ?) - 
                                            IFNULL((select sum(amount_charged) FROM user_payment_credits WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid),0)) as outstanding
                                            FROM user_payment_schedule ups 
                                            GROUP BY whitelabel_id, custid) tbl
                                            LEFT JOIN whitelabel_connect_accounts wca
                                            ON wca.whitelabel_id = tbl.whitelabel_id and wca.env = '{$env}'
                                            LEFT JOIN user_stripe_accounts usa
                                            ON usa.custid = tbl.custid and usa.env = '{$env}'
                                            LEFT JOIN users u
                                            ON u.custid = tbl.custid
                                            LEFT JOIN logins l
                                            ON l.custid = tbl.custid
                                            LEFT JOIN whitelabels wl
                                            ON wl.whitelabel_id = tbl.whitelabel_id
                                            WHERE outstanding > 0 ORDER BY RAND()", array(time()))->fetchAll();
        }
    }

    public function getAllAccounts($whitelabel_id = null)
    {
        $env = Environment::getEnvCode();
        if ($whitelabel_id) {
            return $this->DB->query("SELECT tbl.whitelabel_id, tbl.custid, tbl.outstanding, wca.account_id as whitelabel_account, usa.stripe_customer_id as user_customer_id, 
                                            usa.preferred_payment_method, usa.delinquent, u.fname, u.lname, l.email, wl.name as whitelabel_name  FROM (SELECT ups.whitelabel_id, ups.custid, 
                                            ((SELECT sum(amount) FROM user_payment_schedule WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid AND due_by < ?) - 
                                            IFNULL((select sum(amount_charged) FROM user_payment_credits WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid),0)) as outstanding
                                            FROM user_payment_schedule ups 
                                            GROUP BY whitelabel_id, custid) tbl
                                            LEFT JOIN whitelabel_connect_accounts wca
                                            ON wca.whitelabel_id = tbl.whitelabel_id and wca.env = '{$env}'
                                            LEFT JOIN user_stripe_accounts usa
                                            ON usa.custid = tbl.custid and usa.env = '{$env}'
                                            LEFT JOIN users u
                                            ON u.custid = tbl.custid
                                            LEFT JOIN logins l
                                            ON l.custid = tbl.custid
                                            LEFT JOIN whitelabels wl
                                            ON wl.whitelabel_id = tbl.whitelabel_id
                                            WHERE tbl.whitelabel_id = ?", array(time(),$whitelabel_id))->fetchAll();
        } else {
            return $this->DB->query("SELECT tbl.whitelabel_id, tbl.custid, tbl.outstanding, wca.account_id as whitelabel_account, usa.stripe_customer_id as user_customer_id, 
                                            usa.preferred_payment_method, usa.delinquent, u.fname, u.lname, l.email, wl.name as whitelabel_name  FROM (SELECT ups.whitelabel_id, ups.custid, 
                                            ((SELECT sum(amount) FROM user_payment_schedule WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid AND due_by < ?) - 
                                            IFNULL((select sum(amount_charged) FROM user_payment_credits WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid),0)) as outstanding
                                            FROM user_payment_schedule ups 
                                            GROUP BY whitelabel_id, custid) tbl
                                            LEFT JOIN whitelabel_connect_accounts wca
                                            ON wca.whitelabel_id = tbl.whitelabel_id and wca.env = '{$env}'
                                            LEFT JOIN user_stripe_accounts usa
                                            ON usa.custid = tbl.custid and usa.env = '{$env}'
                                            LEFT JOIN users u
                                            ON u.custid = tbl.custid
                                            LEFT JOIN logins l
                                            ON l.custid = tbl.custid
                                            LEFT JOIN whitelabels wl
                                            ON wl.whitelabel_id = tbl.whitelabel_id", array(time()))->fetchAll();
        }
    }

    public function getBalanceByCustid($custid)
    {
        $env = Environment::getEnvCode();
        return $this->DB->query("SELECT tbl.whitelabel_id, tbl.custid, tbl.outstanding, wca.account_id as whitelabel_account, usa.stripe_customer_id as user_customer_id, 
                                            usa.preferred_payment_method, usa.delinquent, u.fname, u.lname, l.email, wl.name as whitelabel_name  FROM (SELECT ups.whitelabel_id, ups.custid, 
                                            ((SELECT sum(amount) FROM user_payment_schedule WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid AND due_by < ?) - 
                                            IFNULL((select sum(amount_charged) FROM user_payment_credits WHERE whitelabel_id = ups.whitelabel_id AND custid = ups.custid),0)) as outstanding
                                            FROM user_payment_schedule ups 
                                            WHERE ups.custid = ?
                                            GROUP BY whitelabel_id, custid) tbl
                                            LEFT JOIN whitelabel_connect_accounts wca
                                            ON wca.whitelabel_id = tbl.whitelabel_id and wca.env = '{$env}'
                                            LEFT JOIN user_stripe_accounts usa
                                            ON usa.custid = tbl.custid and usa.env = '{$env}'
                                            LEFT JOIN users u
                                            ON u.custid = tbl.custid
                                            LEFT JOIN logins l
                                            ON l.custid = tbl.custid
                                            LEFT JOIN whitelabels wl
                                            ON wl.whitelabel_id = tbl.whitelabel_id
                                            WHERE tbl.custid = ?", array(time(),$custid, $custid))->fetchAll();
    }


    public function getChargesByCustid($custid, $whitelabel_id = null)
    {
        if ($whitelabel_id) {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? AND whitelabel_id = ?", array($custid, $whitelabel_id))->fetchAll();
        } else {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? ", array($custid))->fetchAll();
        }
    }

    public function upcomingChargesByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ups.payment_schedule_id, ups.whitelabel_id, ups.custid, u.fname, u.lname, ups.created_at, ups.due_by, 
                                        ups.created_by, ups.amount, ups.description, ups.charge_type
                                        FROM user_payment_schedule ups
                                        LEFT JOIN users u
                                        ON u.custid = ups.custid
                                        LEFT JOIN logins l
                                        ON l.custid = ups.custid
                                        WHERE ups.due_by > ? AND whitelabel_id = ?", array(time(), $whitelabel_id))->fetchAll();
    }

    public function upcomingChargesByDateRange($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND due_by >= ? AND due_by <= ?", array($whitelabel_id, $startTime, $endTime))->fetchAll();
    }



}