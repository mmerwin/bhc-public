--
-- Table structure for table `postmark_domains`
--

CREATE TABLE IF NOT EXISTS `postmark_domains` (
                                                  `whitelabel_id` int(11) NOT NULL,
                                                  `name` varchar(500) NOT NULL,
                                                  `spf_verified` text NOT NULL,
                                                  `spf_host` text NOT NULL,
                                                  `spf_text_value` text NOT NULL,
                                                  `dkim_verified` text NOT NULL,
                                                  `weak_dkim` text NOT NULL,
                                                  `dkim_host` text NOT NULL,
                                                  `dkim_text_value` text NOT NULL,
                                                  `dkim_pending_host` text NOT NULL,
                                                  `dkim_pending_text_value` text NOT NULL,
                                                  `dkim_revoked_host` text NOT NULL,
                                                  `dkim_revoked_text_value` text NOT NULL,
                                                  `safe_to_remove_revoked_key_from_dns` text NOT NULL,
                                                  `dkim_update_status` text NOT NULL,
                                                  `return_path_domain` text NOT NULL,
                                                  `return_path_domain_verified` text NOT NULL,
                                                  `return_path_domain_cname_value` text NOT NULL,
                                                  `id` int(11) NOT NULL,
                                                  PRIMARY KEY (`whitelabel_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
