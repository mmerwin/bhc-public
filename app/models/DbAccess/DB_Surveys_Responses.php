<?php


class DB_Surveys_Responses extends DataModel
{
    protected $schema = "surveys_responses";

    public function saveResponse($surveyId, $uniqueId, $questionId, $response)
    {
        $this->DB->query("INSERT INTO {$this->schema} (survey_id, unique_id, question_id, submission_time, response) VALUES (?, ?, ?, ?, ?)", array($surveyId, $uniqueId, $questionId, time(), $response));
    }

    public function deleteAllBySurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

    public function getAllResponses($surveyId)
    {
        return $this->DB->query("SELECT sres.survey_id, sres.unique_id, srec.whitelabel_id, srec.recipient_id, srec.custid, srec.name, srec.email, sres.question_id, sq.element_type, sres.submission_time, sres.response FROM surveys_responses sres
                                        LEFT JOIN surveys_recipients srec
                                        ON srec.unique_id = sres.unique_id
                                        LEFT JOIN surveys_questions sq
                                        ON sq.question_id = sres.question_id
                                        WHERE sres.survey_id = ? ORDER BY sres.unique_id ASC, sres.question_id ASC", array($surveyId))->fetchAll();
    }

}