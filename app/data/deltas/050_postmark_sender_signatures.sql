--
-- Table structure for table `postmark_sender_signatures`
--

CREATE TABLE IF NOT EXISTS `postmark_sender_signatures` (
                                                            `id` int(11) NOT NULL,
                                                            `whitelabel_id` int(11) NOT NULL,
                                                            `domain` varchar(500) NOT NULL,
                                                            `email_address` varchar(250) NOT NULL,
                                                            `reply_to_email_address` varchar(250) NOT NULL,
                                                            `email_name` varchar(250) NOT NULL,
                                                            `confirmed` varchar(10) NOT NULL,
                                                            PRIMARY KEY (`id`) USING BTREE,
                                                            KEY `fk_whitelabel_postmark_signatures` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `postmark_sender_signatures`
--
ALTER TABLE `postmark_sender_signatures`
    ADD CONSTRAINT `fk_whitelabel_postmark_signatures` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
