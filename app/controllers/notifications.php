<?php


class notifications extends Controller
{
    /**
     * @api
     */
    public function myNotifications()
    {
        $custid         = $_SESSION['custid'];
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $since          = $this->param("since");
        $max            = $this->param("max");
        $read           = $this->param("read");

        $notifications  = new Notification($custid, $whitelabel_id);
        $messages       = $notifications->getNotifications($since,$max,$read);
        $this->view("json",$messages);
    }

    /**
     * @api
     */
    public function markRead()
    {
        $custid         = $_SESSION['custid'];
        $whitelabel_id  = $this->param("whitelabel_id");
        $notificationId = $this->param("notification_id");

        $notifications  = new Notification($custid, $whitelabel_id);
        $notifications->readNotification($notificationId);
    }

    /**
     * @api
     */
    public function getNotificationById()
    {
        $custid         = $_SESSION['custid'];
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $receiptId      = $this->param("notification_id");
        $notifications  = new Notification($custid, $whitelabel_id);
        $messages       = $notifications->getNotificationById($receiptId, $custid, $whitelabel_id);
        $this->view("json",$messages);
    }

}