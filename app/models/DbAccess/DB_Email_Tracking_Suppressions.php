<?php


class DB_Email_Tracking_Suppressions extends DataModel
{
    protected $schema = "email_tracking_suppressions";

    public function addSuppression($custid)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (custid, last_updated) VALUES (?, ?)", array($custid, time()));
    }

    public function removeSuppression($custid)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE custid = ? LIMIT 1", array($custid));
    }

    public function checkForSuppression($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ?", array($custid))->fetchAll();
    }

}