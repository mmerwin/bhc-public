<?php


class webhooks extends Controller
{
    private $postmarkKey = "48fh37hb28h47vhfd7843hju";

    private function checkKey()
    {
        $authenticated = false;
        if (!is_null($this->param("postmark_key"))) {
            if($this->param("postmark_key") == $this->postmarkKey) {
                $authenticated = true;
            }
        }
        if (!$authenticated) {
            die(array("Error"=>"Not Webhook Authenticated"));
        }
    }

    public function postmarkExternalMessage()
    {
        self::checkKey();
        $data = json_decode(file_get_contents('php://input'), true);
        //check if bounce
        if ($data["RecordType"] == "Bounce") {
            (new DB_External_Messages_Webhooks())->addEvent($data["MessageID"], $data["Email"], $data["Tag"], $data["RecordType"]);
        } else {
            //check if user is on email_tracking_suppression list
            $recipient  = (new DB_External_Messages_Recipients())->getRecipientByExternalId($data["MessageID"]);
            $custid     = $recipient["custid"];
            if (!is_null($custid) && $custid != 0) {
                //check for suppression
                $suppressed = Service_Privacy::checkIfEmailTrackingSuppressed($custid);
                if (!$suppressed) {
                    (new DB_External_Messages_Webhooks())->addEvent($data["MessageID"], $data["Recipient"], $data["Tag"], $data["RecordType"]);
                }
            }
        }
    }

}