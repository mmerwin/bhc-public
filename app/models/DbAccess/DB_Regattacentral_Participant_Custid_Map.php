<?php


class DB_Regattacentral_Participant_Custid_Map extends DataModel
{
    protected $schema = "regattacentral_participant_custid_map";

    public function claimParticipantId($custid, $participantId, $uuid, $fname, $lname, $byear, $bmonth, $bday)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (participant_id, custid, timestamp_updated, uuid, 
                          fname, lname, byear, bmonth, bday) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($participantId, $custid, time(), $uuid, $fname, $lname, $byear, $bmonth, $bday));
    }

    public function unlinkParticipantId($participantId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE participant_id = ? LIMIT 1", array($participantId));
    }

    public function getParticipantIdsByCustid($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ?", array($custid))->fetchAll();
    }

    public function getParticipantId($participantId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE participant_id = ? LIMIT 1",
            array($participantId))->fetchArray();
    }

    public function getBhcName($participantId)
    {
        return $this->DB->query("SELECT rc.participant_id, rc.custid, u.fname, u.lname FROM regattacentral_participant_custid_map rc 
                                        LEFT JOIN users u 
                                        ON u.custid = rc.custid
                                        WHERE rc.participant_id = ? LIMIT 1", array($participantId))->fetchArray();
    }

}