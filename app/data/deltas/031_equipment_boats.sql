--
-- Table structure for table `equipment_boats`
--

CREATE TABLE IF NOT EXISTS `equipment_boats` (
                                                 `boat_id` int(11) NOT NULL AUTO_INCREMENT,
                                                 `whitelabel_id` int(11) DEFAULT NULL,
                                                 `boat_name` varchar(50) DEFAULT NULL,
                                                 `manufacturer` varchar(100) DEFAULT NULL,
                                                 `manufacturer_id` int(11) DEFAULT NULL,
                                                 `model` varchar(100) DEFAULT NULL,
                                                 `year_built` int(11) DEFAULT NULL,
                                                 `year_acquired` int(11) DEFAULT NULL,
                                                 `purchase_price` varchar(20) DEFAULT NULL,
                                                 `insured` varchar(5) DEFAULT NULL,
                                                 `insured_value` varchar(100) DEFAULT NULL,
                                                 `hull_type` varchar(5) DEFAULT NULL,
                                                 `min_weight` int(11) DEFAULT NULL,
                                                 `max_weight` int(11) DEFAULT NULL,
                                                 `owner` int(11) DEFAULT NULL,
                                                 `owner_custid` int(11) DEFAULT NULL,
                                                 `color` varchar(50) DEFAULT NULL,
                                                 `serial_number` varchar(50) DEFAULT NULL,
                                                 `mode` varchar(15) DEFAULT NULL,
                                                 `rigging` text DEFAULT NULL,
                                                 `coxed` varchar(5) DEFAULT NULL,
                                                 `status` varchar(20) NOT NULL DEFAULT 'Available',
                                                 PRIMARY KEY (`boat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
