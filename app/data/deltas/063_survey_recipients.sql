--
-- Table structure for table `surveys_recipients`
--

CREATE TABLE IF NOT EXISTS `surveys_recipients` (
                                                    `recipient_id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `survey_id` int(11) NOT NULL,
                                                    `whitelabel_id` int(11) NOT NULL,
                                                    `custid` int(11) NOT NULL,
                                                    `unique_id` varchar(100) NOT NULL,
                                                    `name` varchar(200) NOT NULL,
                                                    `email` varchar(360) NOT NULL,
                                                    `first_submission` int(11) NOT NULL,
                                                    PRIMARY KEY (`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
