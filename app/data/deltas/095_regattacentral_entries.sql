--
-- Table structure for table `regattacentral_regatta_entries`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regatta_entries` (
                                                                `regatta_id` int(11) NOT NULL,
                                                                `event_id` int(11) NOT NULL,
                                                                `entry_id` int(11) NOT NULL,
                                                                `organization_id` int(11) NOT NULL,
                                                                `contact_id` int(11) NOT NULL,
                                                                `entry_label` varchar(200) NOT NULL,
                                                                `alternate_title` varchar(200) NOT NULL,
                                                                `average_age` int(11) NOT NULL,
                                                                `handicap` int(11) NOT NULL,
                                                                `composite_label` varchar(200) NOT NULL,
                                                                `seed` varchar(50) NOT NULL,
                                                                `erg_score` varchar(200) NOT NULL,
                                                                `division` text NOT NULL,
                                                                `bow` text NOT NULL,
                                                                `waitlist_priority` text NOT NULL,
                                                                `created_on` int(11) NOT NULL,
                                                                `modified_on` int(11) NOT NULL,
                                                                `composite` varchar(10) NOT NULL,
                                                                `international` varchar(10) NOT NULL,
                                                                PRIMARY KEY (`regatta_id`,`event_id`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
