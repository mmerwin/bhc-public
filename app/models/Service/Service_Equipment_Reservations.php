<?php


class Service_Equipment_Reservations
{
    private $whitelabel_id;
    private $custid;
    private $timeLimit;
    private $minLeadTime;
    private $maxLeadTime;
    private $sevenDayLimit;
    private $fourteenDayLimit;

    public function __construct($whitelabel_id, $custid)
    {
        $this->whitelabel_id = $whitelabel_id;
        $this->setRestrictions();
        $this->custid = $custid;
    }

    private function setRestrictions()
    {
        $restrictions = (new DB_Equipment_Reservations_Restrictions())->getWhitelabelRestrictions($this->whitelabel_id);
        if (count($restrictions) == 0 ) {
            (new DB_Equipment_Reservations_Restrictions())->newWhitelabelId($this->whitelabel_id);
            $this->timeLimit        = 0;
            $this->minLeadTime      = 0;
            $this->maxLeadTime      = 0;
            $this->sevenDayLimit    = 0;
            $this->fourteenDayLimit = 0;
        } else {
            $this->timeLimit        = $restrictions['time_limit'];
            $this->minLeadTime      = $restrictions['min_lead_time'];
            $this->maxLeadTime      = $restrictions['max_lead_time'];
            $this->sevenDayLimit    = $restrictions['seven_day_limit'];
            $this->fourteenDayLimit = $restrictions['fourteen_day_limit'];
        }
    }

    public function getReservationsForUser($type = "private")
    {
        return (new DB_Equipment_Reservations())->reservationsByCustid($this->whitelabel_id, $this->custid, $type);
    }

    public function getReservationsForWhitelabel()
    {
        $rawReservations    = (new DB_Equipment_Reservations())->getAllReservationsByWhitelabel($this->whitelabel_id);
        $boatReservations   = (new DB_Equipment_Reservations_Boats())->getReservationsByWhitelabel($this->whitelabel_id);
        $oarReservations    = (new DB_Equipment_Reservations_Oars())->getReservationsByWhitelabel($this->whitelabel_id);
        $ergReservations    = (new DB_Equipment_Reservations_Ergs())->getReservationsByWhitelabel($this->whitelabel_id);

        $data = [];
        foreach ($rawReservations as $reservation) {
            $boatsInReservation = [];
            foreach ($boatReservations as $boatR) {
                if ($boatR['reservation_id'] == $reservation['reservation_id']) {
                    $boatsInReservation[] = $boatR;
                }
            }

            $oarsInReservation = [];
            foreach ($oarReservations as $oarR) {
                if ($oarR['reservation_id'] == $reservation['reservation_id']) {
                    $oarsInReservation[] = $oarR;
                }
            }
            $ergsInReservation = [];
            foreach ($ergReservations as $ergR) {
                if ($ergR['reservation_id'] == $reservation['reservation_id']) {
                    $ergsInReservation[] = $ergR;
                }
            }

            $timeHuman = date("D M j g:ia", $reservation['start_time'])." - ".date("g:ia", $reservation['end_time']);
            $data[]    = array(
                "reservation_id"=> $reservation['reservation_id'],
                "whitelabel_id" => $reservation['whitelabel_id'],
                "start_time"    => $reservation['start_time'],
                "end_time"      => $reservation['end_time'],
                "time_human"    => $timeHuman,
                "type"          => $reservation['type'],
                "created_at"    => $reservation['created_at'],
                "custid"        => $reservation['custid'],
                "fname"         => $reservation['fname'],
                "lname"         => $reservation['lname'],
                "phone_number"  => $reservation['phone_number'],
                "email"         => $reservation['email'],
                "boats"         => $boatsInReservation,
                "oars"          => $oarsInReservation,
                "ergs"          => $ergsInReservation
            );
        }
        return $data;
    }

    public function getReservationById($reservationId)
    {
        $reservation      = (new DB_Equipment_Reservations())->getReservationById($this->whitelabel_id, $reservationId);
        $boatReservations = (new DB_Equipment_Reservations_Boats())->getReservationsByWhitelabel($this->whitelabel_id);
        $oarReservations  = (new DB_Equipment_Reservations_Oars())->getReservationsByWhitelabel($this->whitelabel_id);
        $ergReservations  = (new DB_Equipment_Reservations_Ergs())->getReservationsByWhitelabel($this->whitelabel_id);

        $data = [];

        $boatsInReservation = [];
        foreach ($boatReservations as $boatR) {
            if ($boatR['reservation_id'] == $reservation['reservation_id']) {
                $boatsInReservation[] = $boatR;
            }
        }

        $oarsInReservation = [];
        foreach ($oarReservations as $oarR) {
            if ($oarR['reservation_id'] == $reservation['reservation_id']) {
                $oarsInReservation[] = $oarR;
            }
        }
        $ergsInReservation = [];
        foreach ($ergReservations as $ergR) {
            if ($ergR['reservation_id'] == $reservation['reservation_id']) {
                $ergsInReservation[] = $ergR;
            }
        }

        $timeHuman = date("D M j g:ia", $reservation['start_time'])." - ".date("g:ia", $reservation['end_time']);
        $data[]    = array(
            "reservation_id"=> $reservation['reservation_id'],
            "whitelabel_id" => $reservation['whitelabel_id'],
            "start_time"    => $reservation['start_time'],
            "end_time"      => $reservation['end_time'],
            "time_human"    => $timeHuman,
            "type"          => $reservation['type'],
            "created_at"    => $reservation['created_at'],
            "custid"        => $reservation['custid'],
            "fname"         => $reservation['fname'],
            "lname"         => $reservation['lname'],
            "phone_number"  => $reservation['phone_number'],
            "email"         => $reservation['email'],
            "boats"         => $boatsInReservation,
            "oars"          => $oarsInReservation,
            "ergs"          => $ergsInReservation
        );
        return $data;
    }

    public function getAvailableAssets($startTime, $endTime)
    {
        return [
            "boats" => self::getAvailableBoats($startTime, $endTime),
            "oars"  => self::getAvailableOars($startTime, $endTime),
            "ergs"  => self::getAvailableErgs($startTime, $endTime)
        ];
    }

    public function getAvailableBoats($startTime, $endTime, $fullData = true)
    {
        $returnArray     = [];
        $availableBoats  = (new DB_Equipment_Reservations_Boats())->getAvailableBoats($this->whitelabel_id, $startTime, $endTime);
        $rawRigging      = (new DB_Equipment_Boats_Rigging())->getAllRigging($this->whitelabel_id);

        // get group-permissible boats
        $boatPermissions = (new EquipmentModel($this->whitelabel_id))->getBoatCheckoutPermissionsByCustid($this->custid);
        foreach ($boatPermissions as $allowed) {
            $isAllowed = false;

            foreach ($availableBoats as $boats) {
                if ($allowed['boat_id'] == $boats['boat_id'] && $allowed['status'] == "Available") {
                    $isAllowed = true;
                }
            }

            if ($isAllowed) {
                $returnArray[] = [

                    "boat_id"       => $allowed['boat_id'],
                    "whitelabel_id" => $allowed['whitelabel_id'],
                    "boat_name"     => $allowed['boat_name'],
                    "manufacturer"  => $allowed['manufacturer'],
                    "model"         => $allowed['model'],
                    "year_built"    => $allowed['year_built'],
                    "year_acquired" => $allowed['year_acquired'],
                    "purchase_price"=> $allowed['purchase_price'],
                    "insured"       => $allowed['insured'],
                    "insured_value" => $allowed['insured_value'],
                    "hull_type"     => $allowed['hull_type'],
                    "min_weight"    => $allowed['min_weight'],
                    "max_weight"    => $allowed['max_weight'],
                    "color"         => $allowed['color'],
                    "serial_number" => $allowed['serial_number'],
                    "coxed"         => $allowed['coxed'],
                    "status"        => $allowed['status'],
                    "created_at"    => $allowed['created_at'],
                    "rigging"       => ($fullData)? self::getBoatRigging($allowed['boat_id'], $rawRigging) : []
                ];
            }
        }
        return $returnArray;
    }

    private function getBoatRigging($boatId, $rawRigging)
    {
        $boatRigging = [];
        foreach ($rawRigging as $rigging) {
            if ($rigging['boat_id'] == $boatId) {
                $boatRigging[] = [
                    "boat_id"                       => $rigging['boat_id'],
                    "seat"                          => $rigging['seat'],
                    "seat_side"                     => $rigging['seat_side'],
                    "total_spread"                  => (isset($rigging['total_spread']))                ? $rigging['total_spread']: null,
                    "port_lateral_pitch_min"        => (isset($rigging['port_lateral_pitch_min']))      ? $rigging['port_lateral_pitch_min']: null,
                    "port_lateral_pitch_max"        => (isset($rigging['port_lateral_pitch_max']))      ? $rigging['port_lateral_pitch_max']: null,
                    "starboard_lateral_pitch_min"   => (isset($rigging['starboard_lateral_pitch_min'])) ? $rigging['starboard_lateral_pitch_min']: null,
                    "starboard_lateral_pitch_max"   => (isset($rigging['starboard_lateral_pitch_max'])) ? $rigging['starboard_lateral_pitch_max']: null,
                    "port_front_pitch_min"          => (isset($rigging['port_front_pitch_min']))        ? $rigging['port_front_pitch_min']: null,
                    "port_front_pitch_max"          => (isset($rigging['port_front_pitch_max']))        ? $rigging['port_front_pitch_max']: null,
                    "starboard_front_pitch_min"     => (isset($rigging['starboard_front_pitch_min']))   ? $rigging['starboard_front_pitch_min']: null,
                    "starboard_front_pitch_max"     => (isset($rigging['starboard_front_pitch_max']))   ? $rigging['starboard_front_pitch_max']: null,
                    "port_swivel_height_min"        => (isset($rigging['port_swivel_height_min']))      ? $rigging['port_swivel_height_min']: null,
                    "port_swivel_height_max"        => (isset($rigging['port_swivel_height_max']))      ? $rigging['port_swivel_height_max']: null,
                    "starboard_swivel_height_min"   => (isset($rigging['starboard_swivel_height_min'])) ? $rigging['starboard_swivel_height_min']: null,
                    "starboard_swivel_height_max"   => (isset($rigging['starboard_swivel_height_max'])) ? $rigging['starboard_swivel_height_max']: null,
                    "track_length"                  => (isset($rigging['track_length']))                ? $rigging['track_length']: null,
                    "foot_stretcher_angle_min"      => (isset($rigging['foot_stretcher_angle_min']))    ? $rigging['foot_stretcher_angle_min']: null,
                    "foot_stretcher_angle_max"      => (isset($rigging['foot_stretcher_angle_max']))    ? $rigging['foot_stretcher_angle_max']: null,
                    "foot_stretcher_placement"      => (isset($rigging['foot_stretcher_placement']))    ? $rigging['foot_stretcher_placement']: null
                ];
            }
        }
        return $boatRigging;
    }

    public function getAvailableOars($startTime, $endTime)
    {
        return (new DB_Equipment_Reservations_Oars())->getAvailableOars($this->whitelabel_id, $startTime, $endTime);
    }

    public function getAvailableErgs($startTime, $endTime)
    {
        return (new DB_Equipment_Reservations_Ergs())->getAvailableErgs($this->whitelabel_id, $startTime, $endTime);
    }

    public function newReservation($startTime, $endTime, $type)
    {
        // check start/end times are vald
        if ($startTime >= $endTime){return array("Status"=>"Error", "Message"=>"End time must be after start time.");}
        if ($startTime <= time()){return array("Status"=>"Error", "Message"=>"Start time must be sometime in the future");}
        if ($startTime <= (time() + $this->minLeadTime) && $this->minLeadTime !=0 && strtolower($type) != "practice"){return array("Status"=>"Error", "Message"=>"Start time is too soon. Check Minimum lead-time for reservations requirement");}
        if ($startTime >= (time() + $this->maxLeadTime) && $this->maxLeadTime !=0 && strtolower($type) != "practice"){return array("Status"=>"Error", "Message"=>"Start time is too far away. Check Maximum lead-time for reservations requirement");}
        if (strtolower($type) == "private") {
            // check that the user is eligible to create a reservation
            if ($this->sevenDayLimit != 0 || $this->fourteenDayLimit != 0) {
                $sevenDayCount    = (new DB_Equipment_Reservations())->sevenDayCount($this->whitelabel_id, $this->custid);
                $fourteenDayCount = (new DB_Equipment_Reservations())->fourteenDayCount($this->whitelabel_id, $this->custid);
                if ($sevenDayCount >= $this->sevenDayLimit) {
                    return array("Status"=>"Error", "Message"=>"Too many reservations in a 7-day period. Limit is set at {$this->sevenDayLimit}, You currently have {$sevenDayCount}");
                }
                if ($fourteenDayCount >= $this->fourteenDayLimit) {
                    return array("Status"=>"Error", "Message"=>"Too many reservations in a 14-day period. Limit is set at {$this->fourteenDayLimit}, You currently have {$fourteenDayCount}");
                }

                // below reservation limit:
                $newReservation = (new DB_Equipment_Reservations())->newReservation($this->whitelabel_id, $this->custid, $startTime, $endTime, "private");
                return array("Status"=>"Success","reservation_id"=>$newReservation);
            } else {
                $newReservation = (new DB_Equipment_Reservations())->newReservation($this->whitelabel_id, $this->custid, $startTime, $endTime, "private");
                return array("Status"=>"Success","reservation_id"=>$newReservation);
            }
        } else if (strtolower($type) == "practice") {
            $newReservation = (new DB_Equipment_Reservations())->newReservation($this->whitelabel_id, $this->custid, $startTime, $endTime, "private");
            return array("Status"=>"Success","reservation_id"=>$newReservation);
        }
        return array("Status"=>"Error", "Message"=>"Unknown Error");;
    }

    public function addBoatToReservation($reservationId, $boatId)
    {
        // check that reservation id belongs to whitelabel and to the user
        $allReservations = self::getReservationsForUser("all");
        foreach ($allReservations as $reservation) {
            if ($reservation['reservation_id'] == $reservationId) {
                //check that boat is available during the required time
                if (self::checkIfBoatIsAvailable($boatId, $reservation['start_time'], $reservation['end_time'], false)) {
                    //check that the reservation is in the future
                    if ($reservation['end_time'] > time()) {
                        (new DB_Equipment_Reservations_Boats())->addReservation($reservationId, $this->whitelabel_id, $boatId);
                        return array("Status"=>"Success", "Message"=>"Boat added to reservation", "reservation_id"=>$reservationId);
                    } else {
                        return array("Status"=>"Error", "Message"=>"Attempting to add boat to a reservation that has already ended.");
                    }
                } else {
                    return array("Status"=>"Error", "Message"=>"One or more boats are not available during the requested time");
                }
            }
        }
        return array("Status"=>"Error", "Message"=>"Cannot add boat, reservation does not belong to the user.");

    }

    public function addOarToReservation($reservationId, $oarId)
    {
        // check that reservation id belongs to whitelabel and to the user
        $allReservations = self::getReservationsForUser();
        foreach ($allReservations as $reservation) {
            if ($reservation['reservation_id'] == $reservationId) {
                // check that oar is available during the required time
                if (self::checkIfOarIsAvailable($oarId, $reservation['start_time'], $reservation['end_time'])) {
                    // check that the reservation is in the future
                    if ($reservation['end_time'] > time()) {
                        (new DB_Equipment_Reservations_Oars())->addReservation($reservationId, $this->whitelabel_id, $oarId);
                        return array("Status"=>"Success", "Message"=>"Oar added to reservation", "reservation_id"=>$reservationId);
                    } else {
                        return array("Status"=>"Error", "Message"=>"Attempting to add oar to a reservation that has already ended.");
                    }
                } else {
                    return array("Status"=>"Error", "Message"=>"One or more oars are not available during the requested time");
                }
            }
        }
        return array("Status"=>"Error", "Message"=>"Cannot add oar, reservation does not belong to the user.");
    }

    public function addErgToReservation($reservationId, $ergId)
    {
        // check that reservation id belongs to whitelabel and to the user
        $allReservations = self::getReservationsForUser();
        foreach ($allReservations as $reservation) {
            if ($reservation['reservation_id'] == $reservationId) {
                // check that erg is available during the required time
                if (self::checkIfErgIsAvailable($ergId, $reservation['start_time'], $reservation['end_time'])) {
                    // check that the reservation is in the future
                    if ($reservation['end_time'] > time()) {
                        (new DB_Equipment_Reservations_Ergs())->addReservation($reservationId, $this->whitelabel_id, $ergId);
                        return array("Status"=>"Success", "Message"=>"Erg added to reservation", "reservation_id"=>$reservationId);
                    } else {
                        return array("Status"=>"Error", "Message"=>"Attempting to add erg to a reservation that has already ended.");
                    }
                } else {
                    return array("Status"=>"Error", "Message"=>"One or more ergs are not available during the requested time");
                }
            }
        }
        return array("Status"=>"Error", "Message"=>"Cannot add erg, reservation does not belong to the user.");
    }

    public function checkIfBoatIsAvailable($boatId, $startTime, $endTime, $fullData = true)
    {
        $availableBoats = self::getAvailableBoats($startTime, $endTime, $fullData);
        foreach ($availableBoats as $boat) {
            if ($boat['boat_id'] == $boatId) {
                return true;
            }
        }
        return false;
    }

    public function checkIfOarIsAvailable($oarId, $startTime, $endTime)
    {
        $availableOars = self::getAvailableOars($startTime, $endTime);
        foreach ($availableOars as $oar) {
            if ($oar['oar_id'] == $oarId) {
                return true;
            }
        }
        return false;
    }

    public function checkIfErgIsAvailable($ergId, $startTime, $endTime)
    {
        $availableErgs = self::getAvailableErgs($startTime, $endTime);
        foreach ($availableErgs as $erg) {
            if ($erg['erg_id'] == $ergId) {
                return true;
            }
        }
        return false;
    }

    public function deleteReservation($reservationId)
    {
        // check that reservation id belongs to whitelabel and to the user
        $allReservations = self::getReservationsForUser();
        foreach ($allReservations as $reservation) {
            if ($reservation['reservation_id'] == $reservationId) {
                (new DB_Equipment_Reservations)->deleteReservation($reservationId, $this->custid, $this->whitelabel_id);
                return array("Status"=>"Success", "Message"=>"Reservation has been deleted");
            }
        }
        return array("Status"=>"Error", "Message"=>"Cannot delete reservation, reservation does not belong to the user.");
    }

    public function deleteBoatFromReservation($reservationId, $boatId)
    {
        // check that reservation id belongs to whitelabel and to the user
        $allReservations = self::getReservationsForUser("all");
        foreach ($allReservations as $reservation) {
            if ($reservation['reservation_id'] == $reservationId) {
                (new DB_Equipment_Reservations_Boats())->removeFromReservation($this->whitelabel_id, $reservationId, $boatId);
                return array("Status"=>"Success", "Message"=>"Boat has been removed from reservation");
            }
        }
        return array("Status"=>"Error", "Message"=>"Cannot delete from reservation, reservation does not belong to the user.");
    }

    public function getAllBoatReservations()
    {
        return (new DB_Equipment_Reservations_Boats())->getReservationsByWhitelabel($this->whitelabel_id);
    }


}