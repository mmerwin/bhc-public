<?php


class DB_equipment_checkout_groups_boats extends DataModel
{
    protected $schema = "equipment_checkout_groups_boats";
    protected $subschemaA = "equipment_boats";
    protected $subschemaB = "groups";
    protected $subschemaC = "groups_members";

    public function getBoatGroups($boatid, $whitelabel_id)
    {
        return $this->DB->query("SELECT ep.whitelabel_id, ep.boat_id, eb.boat_name, eb.manufacturer, eb.model, eb.serial_number, eb.hull_type, eb.status, eb.created_at, eb.min_weight, eb.max_weight, ep.group_id, g.title as group_title, g.descr as group_descr, g.self_join as group_self_join
                                        FROM {$this->schema} ep
                                        LEFT JOIN {$this->subschemaA} eb
                                        ON eb.boat_id = ep.boat_id
                                        LEFT JOIN {$this->subschemaB} g
                                        ON g.group_id = ep.group_id
                                        WHERE ep.boat_id = ? AND ep.whitelabel_id = ? ORDER BY boat_name ASC", array($boatid, $whitelabel_id))->fetchAll();
    }

    public function addBoatPermissions($boatid, $whitelabel_id, $groupId)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (whitelabel_id, boat_id, group_id) VALUES (?, ?, ?)", array($whitelabel_id, $boatid, $groupId));
    }

    public function deleteBoatPermission($boatid, $whitelabel_id, $groupId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE boat_id = ? AND whitelabel_id = ? AND group_id = ? LIMIT 1", array($boatid, $whitelabel_id, $groupId));
    }

    public function getGroupPermissions($whitelabel_id, $groupId)
    {
        return $this->DB->query("SELECT ep.whitelabel_id, ep.boat_id, eb.boat_name, eb.manufacturer, eb.model, eb.serial_number, eb.hull_type, eb.status, eb.created_at, eb.min_weight, eb.max_weight, ep.group_id, g.title as group_title, g.descr as group_descr, g.self_join as group_self_join
                                        FROM {$this->schema} ep
                                        LEFT JOIN {$this->subschemaA} eb
                                        ON eb.boat_id = ep.boat_id
                                        LEFT JOIN {$this->subschemaB} g
                                        ON g.group_id = ep.group_id
                                        WHERE ep.whitelabel_id = ? AND ep.group_id = ? ORDER BY boat_name ASC", array($whitelabel_id, $groupId))->fetchAll();
    }

    public function getBoatPermissionByCustid($whitelabel_id, $custid)
    {
        return $this->DB->query("SELECT eb.*
                                        FROM {$this->subschemaC} gm
                                        LEFT JOIN {$this->schema} ecgp
                                        ON ecgp.group_id = gm.group_id
                                        LEFT JOIN {$this->subschemaA} eb
                                        ON eb.boat_id = ecgp.boat_id
                                        WHERE gm.custid = ?  AND gm.whitelabel_id = ? GROUP BY boat_id ", array($custid, $whitelabel_id))->fetchAll();
    }

    public function getAllByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ep.whitelabel_id, ep.boat_id, eb.boat_name, eb.manufacturer, eb.model, eb.serial_number, eb.hull_type, eb.status, eb.created_at, eb.min_weight, eb.max_weight, ep.group_id, g.title as group_title, g.descr as group_descr, g.self_join as group_self_join
                                        FROM {$this->schema} ep
                                        LEFT JOIN {$this->subschemaA} eb
                                        ON eb.boat_id = ep.boat_id
                                        LEFT JOIN {$this->subschemaB} g
                                        ON g.group_id = ep.group_id
                                        WHERE ep.whitelabel_id = ? ORDER BY boat_name ASC", array($whitelabel_id))->fetchAll();
    }

}