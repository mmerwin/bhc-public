<?php


class DB_Equipment_Reservations_Restrictions extends DataModel
{
    protected $schema = "equipment_reservations_restrictions";

    public function updateRestrictions($whitelabel_id, $timeLimit, $minLeadTime, $maxLeadTime, $sevenDayLimit, $fourteenDayLimit)
    {
        $this->DB->query("UPDATE {$this->schema} SET time_limit = ?, min_lead_time = ?, max_lead_time =?, seven_day_limit = ?, fourteen_day_limit = ? WHERE whitelabel_id = ?", array($timeLimit, $minLeadTime, $maxLeadTime, $sevenDayLimit, $fourteenDayLimit, $whitelabel_id));
    }
    public function newWhitelabelId($whitelabel_id)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (whitelabel_id) VALUES (?)", array($whitelabel_id));
    }

    public function getWhitelabelRestrictions($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? LIMIT 1", array($whitelabel_id))->fetchArray();
    }
}