--
-- Table structure for table `equipment_issues`
--

CREATE TABLE IF NOT EXISTS `equipment_issues` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `whitelabel_id` int(11) NOT NULL,
                                                  `custid` int(11) NOT NULL,
                                                  `timestamp` int(11) NOT NULL,
                                                  `descr` text NOT NULL,
                                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
