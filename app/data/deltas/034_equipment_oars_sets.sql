--
-- Table structure for table `equipment_oars_sets`
--

CREATE TABLE IF NOT EXISTS `equipment_oars_sets` (
                                                     `oar_set_id` int(11) NOT NULL AUTO_INCREMENT,
                                                     `whitelabel_id` int(11) NOT NULL,
                                                     `name` varchar(100) NOT NULL,
                                                     `type` varchar(10) NOT NULL,
                                                     `descr` text NOT NULL,
                                                     `manufacturer` varchar(100) NOT NULL,
                                                     `model` varchar(100) NOT NULL,
                                                     `created_at` int(11) NOT NULL,
                                                     PRIMARY KEY (`oar_set_id`),
                                                     KEY `fk_whitelabel_id_oar_sets` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_oars_sets`
--
ALTER TABLE `equipment_oars_sets`
    ADD CONSTRAINT `fk_whitelabel_id_oar_sets` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
