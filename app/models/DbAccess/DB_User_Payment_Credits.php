<?php


class DB_User_Payment_Credits extends DataModel
{
    protected $schema = "user_payment_credits";

    public function addCredit($whitelabel_id, $custid, $externalId, $postedBy, $amountCharged, $settledAmount, $description, $receiptUrl = "")
    {
        $this->DB->query("INSERT INTO {$this->schema} (external_id, custid, whitelabel_id, created_at, posted_by, amount_charged, settled_amount, receipt_url, descr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($externalId, $custid, $whitelabel_id, time(), $postedBy, $amountCharged, $settledAmount, $receiptUrl, $description));
    }

    public function getCreditsByWhitelabel($whitelabel_id, $limit = null)
    {
        if ($limit) {
            $rowLimit = "LIMIT ".intval($limit);
        } else {
            $rowLimit = "";
        }
        return $this->DB->query("SELECT upc.payment_credit_id, upc.whitelabel_id, upc.custid, upc.created_at, upc.posted_by, upc.external_id, upc.amount_charged, 
                                        upc.settled_amount, u.fname, u.lname, l.email, upc.descr, upc.receipt_url
                                        FROM user_payment_credits upc
                                        LEFT JOIN users u
                                        ON u.custid = upc.custid
                                        LEFT JOIN logins l
                                        ON l.custid = upc.custid
                                        WHERE upc.whitelabel_id = ? {$rowLimit}", array($whitelabel_id))->fetchAll();
    }

    public function getCreditsByCustid($custid, $whitelabel_id = null)
    {
        if ($whitelabel_id) {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? AND whitelabel_id = ?", array($custid, $whitelabel_id))->fetchAll();
        } else {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? ", array($custid))->fetchAll();
        }
    }

    public function getYearToDateRevenue($whitelabel_id)
    {
        $startTime = strtotime(date('Y-01-01'));
        return $this->DB->query("SELECT sum(amount_charged) as year_to_date_charged, sum(settled_amount) as year_to_date_settled
                                        FROM {$this->schema} WHERE whitelabel_id = ? AND created_at >= ?", array($whitelabel_id, $startTime))->fetchArray();
    }

    public function paymentsInRange($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE created_at >= ? AND created_at <= ? AND whitelabel_id = ?", array($startTime, $endTime, $whitelabel_id))->fetchAll();
    }

}