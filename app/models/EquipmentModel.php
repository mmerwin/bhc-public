<?php


class EquipmentModel
{
    private $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function addBoat($data)
    {
        // format data
        if($data['manufacturer_id'] != "0") {
            $manufacturer           = Service_Equipment_Manufacturer::getManufacturerById($data['manufacturer_id']);
            $manufacturerName       = $manufacturer['name'];
            $data['manufacturer']   = $manufacturerName;
        }

        // add boat to inventory
        // rewards points for first-boat
        (new Service_Rewards($this->whitelabel_id))->addRewardPoints(9, $_SESSION['custid']);
        return (new DB_Equipment_boats())->addBoat($data, $this->whitelabel_id);

    }

    private function getKeyValue($array,$key, $default)
    {
        return (isset($array[$key])) ? $array[$key] : $default;
    }

    public function getAllBoats()
    {
        $rawBoats   =  (new DB_Equipment_boats())->getAllBoats($this->whitelabel_id);
        $rawRigging = (new DB_Equipment_Boats_Rigging())->getAllRigging($this->whitelabel_id);
        $rawGroups  = (new DB_equipment_checkout_groups_boats())->getAllByWhitelabel($this->whitelabel_id);
        $formatted  = [];
        foreach ($rawBoats as $boat) {
            // get rigging
            $rigging = [];
            foreach ($rawRigging as $rig) {
                if ($rig['boat_id'] == $boat['boat_id']) {
                    $rigging[] = [
                          "boat_id"                     => self::getKeyValue($rig,'boat_id', "0"),
                          "seat"                        => self::getKeyValue($rig,'seat', "0"),
                          "seat_side"                   => self::getKeyValue($rig,'seat_side', "0"),
                          "total_spread"                => self::getKeyValue($rig,'total_spread', "0"),
                          "port_lateral_pitch_min"      => self::getKeyValue($rig,'port_lateral_pitch_min', "0"),
                          "port_lateral_pitch_max"      => self::getKeyValue($rig,'port_lateral_pitch_max', "0"),
                          "starboard_lateral_pitch_min" => self::getKeyValue($rig,'starboard_lateral_pitch_min', "0"),
                          "starboard_lateral_pitch_max" => self::getKeyValue($rig,'starboard_lateral_pitch_max', "0"),
                          "port_front_pitch_min"        => self::getKeyValue($rig,'port_front_pitch_min', "0"),
                          "port_front_pitch_max"        => self::getKeyValue($rig,'port_front_pitch_max', "0"),
                          "starboard_front_pitch_min"   => self::getKeyValue($rig,'starboard_front_pitch_min', "0"),
                          "starboard_front_pitch_max"   => self::getKeyValue($rig,'starboard_front_pitch_max', "0"),
                          "port_swivel_height_min"      => self::getKeyValue($rig,'port_swivel_height_min', "0"),
                          "port_swivel_height_max"      => self::getKeyValue($rig,'port_swivel_height_max', "0"),
                          "starboard_swivel_height_min" => self::getKeyValue($rig,'starboard_swivel_height_min', "0"),
                          "starboard_swivel_height_max" => self::getKeyValue($rig,'starboard_swivel_height_max', "0"),
                          "track_length"                => self::getKeyValue($rig,'track_length', "0"),
                          "foot_stretcher_angle_min"    => self::getKeyValue($rig,'foot_stretcher_angle_min', "0"),
                          "foot_stretcher_angle_max"    => self::getKeyValue($rig,'foot_stretcher_angle_max', "0"),
                          "foot_stretcher_placement"    => self::getKeyValue($rig,'foot_stretcher_placement', "0")
                    ];
                }
            }

            // get checkout groups
            $groups = [];
            foreach ($rawGroups as $group) {
                if ($group['boat_id'] == $boat['boat_id']) {
                    $groups[] = [
                          "whitelabel_id"   => $group['whitelabel_id'],
                          "boat_id"         => $group['boat_id'],
                          "boat_name"       => $group['boat_name'],
                          "manufacturer"    => $group['manufacturer'],
                          "model"           => $group['model'],
                          "serial_number"   => $group['serial_number'],
                          "hull_type"       => $group['hull_type'],
                          "status"          => $group['status'],
                          "created_at"      => $group['created_at'],
                          "min_weight"      => $group['min_weight'],
                          "max_weight"      => $group['max_weight'],
                          "group_id"        => $group['group_id'],
                          "group_title"     => $group['group_title'],
                          "group_descr"     => $group['group_descr'],
                          "group_self_join" => $group['group_self_join']
                    ];
                }
            }

            $formatted[] = [
                "boat_id"           => $boat['boat_id'],
                "whitelabel_id"     => $boat['whitelabel_id'],
                "created_at"        => $boat['created_at'],
                "boat_name"         => $boat['boat_name'],
                "boat_type"         => $boat['hull_type'],
                "coxed"             => $boat['coxed'],
                "manufacturer"      => $boat['manufacturer'],
                "model"             => $boat['model'],
                "serial"            => $boat['serial_number'],
                "year_built"        => $boat['year_built'],
                "year_purchased"    => $boat['year_acquired'],
                "weight_min"        => $boat['min_weight'],
                "weight_max"        => $boat['max_weight'],
                "color"             => $boat['color'],
                "insured"           => $boat['insured'],
                "insured_value"     => $boat['insured_value'],
                "purchase_price"    => $boat['purchase_price'],
                "status"            => $boat['status'],
                "rigging"           => $rigging,
                "checkout_groups"   => $groups
            ];
        }

        return $formatted;
    }

    public function getBoatById($boatId)
    {
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
            return [
                "boat_id"        => $boat['boat_id'],
                "whitelabel_id"  => $boat['whitelabel_id'],
                "created_at"     => $boat['created_at'],
                "boat_name"      => $boat['boat_name'],
                "boat_type"      => $boat['hull_type'],
                "coxed"          => $boat['coxed'],
                "manufacturer"   => $boat['manufacturer'],
                "model"          => $boat['model'],
                "serial"         => $boat['serial_number'],
                "year_built"     => $boat['year_built'],
                "year_purchased" => $boat['year_acquired'],
                "weight_min"     => $boat['min_weight'],
                "weight_max"     => $boat['max_weight'],
                "color"          => $boat['color'],
                "insured"        => $boat['insured'],
                "insured_value"  => $boat['insured_value'],
                "purchase_price" => $boat['purchase_price'],
                "status"         => $boat['status'],
                "rigging"        => self::getRigging($boat['boat_id']),
                "checkout_groups"=> self::getBoatCheckoutGroups($boat['boat_id'])
            ];
    }

    public function updateBoatData($boatId, $data)
    {
        (new DB_Equipment_boats())->updateBoatAttributes($this->whitelabel_id, $boatId, $data);
        return self::getAllBoats();
    }

    public function getRigging($boatId)
    {
        return (new DB_Equipment_Boats_Rigging())->getRigging($boatId);
    }

    public function updateSeatConfig($boatId, $seat, $config)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_Equipment_Boats_Rigging())->updateSeatConfig($boatId, $seat, $config);
        }
    }

    public function updateSpread($boatId, $seat, $spread)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_Equipment_Boats_Rigging())->updateSpread($boatId, $seat, $spread);
        }
    }

    public function updatePitch($boatId, $seat, $field, $degrees)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_Equipment_Boats_Rigging())->updatePitch($boatId, $seat, $field, $degrees);
        }
    }

    public function updateFootPlacement($boatId, $seat, $placement)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_Equipment_Boats_Rigging())->updateFootPlacement($boatId, $seat, $placement);
        }
    }

    public function updateTrackLength($boatId, $seat, $distance)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_Equipment_Boats_Rigging())->updateTrackLength($boatId, $seat, $distance);
        }
    }

    public function getBoatCheckoutGroups($boatId)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            return (new DB_equipment_checkout_groups_boats())->getBoatGroups($boatId, $this->whitelabel_id);
        }
    }

    public function addBoatCheckoutPermissions($boatId, $groupId)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_equipment_checkout_groups_boats())->addBoatPermissions($boatId, $this->whitelabel_id, $groupId);
        }
    }

    public function removeBoatCheckoutPermissions($boatId, $groupId)
    {
        // verify the boatId belongs to the whitelabel
        $boat = (new DB_Equipment_boats())->getBoatById($boatId);
        if ($boat['whitelabel_id'] == $this->whitelabel_id) {
            (new DB_equipment_checkout_groups_boats())->deleteBoatPermission($boatId, $this->whitelabel_id, $groupId);
        }
    }

    public function getGroupPermissions($groupId)
    {
        return (new DB_equipment_checkout_groups_boats())->getGroupPermissions($this->whitelabel_id, $groupId);
    }

    public function getBoatCheckoutPermissionsByCustid($custid)
    {
        return (new DB_equipment_checkout_groups_boats())->getBoatPermissionByCustid($this->whitelabel_id, $custid);
    }

    public function getAllOars()
    {
        $sets = (new DB_Equipment_Oars_Sets())->getAllSets($this->whitelabel_id);
        $oars = (new DB_Equipment_Oars())->getAllOarsForWhitelabel($this->whitelabel_id);

        // match oars to sets
        $oarSets = [];
        foreach ($sets as $set) {
            //find oars in set
            $setOars = [];
            foreach ($oars as $oar) {
                if ($oar['oar_set_id'] == $set['oar_set_id']) {
                    $setOars[] = [
                        "oar_id"    => $oar['oar_id'],
                        "side"      => $oar['side'],
                        "descr"     => $oar['descriptor'],
                        "status"    => $oar['status'],
                        "created_at"=> $oar['created_at']
                    ];
                }
            }
            $oarSets[] = [
                "oar_set_id"    => $set['oar_set_id'],
                "whitelabel_id" => $set['whitelabel_id'],
                "name"          => $set['name'],
                "type"          => $set['type'],
                "descr"         => $set['descr'],
                "manufacturer"  => $set['manufacturer'],
                "model"         => $set['model'],
                "created_at"    => $set['created_at'],
                "oars"          => $setOars
            ];
        }
        return $oarSets;
    }

    public function addOarSet($name, $type, $descr, $manufacturer, $model)
    {
        $type = ucfirst(strtolower($type));

        if ($type == "Sweep" || $type == "Sculling") {
            // rewards points for first-oar-set
            (new Service_Rewards($this->whitelabel_id))->addRewardPoints(10, $_SESSION['custid']);
            (new DB_Equipment_Oars_Sets())->addSet($this->whitelabel_id, $type, $name, $descr, $manufacturer, $model);
        }
    }

    public function deleteOarSet($setId)
    {
        (new DB_Equipment_Oars_Sets())->deleteSet($setId, $this->whitelabel_id);
    }

    public function addOar($setId, $side, $descriptor)
    {
        (new DB_Equipment_Oars())->addOar($setId, $side, $descriptor);
    }

    public function deleteOar($setId, $oarId)
    {
        // verify oar belongs to whitelabel
        $allOars = (new DB_Equipment_Oars())->getAllOarsForWhitelabel($this->whitelabel_id);
        foreach ($allOars as $oar) {
            if ($oar['oar_id'] == $oarId) {
                return (new DB_Equipment_Oars())->deleteOar($setId, $oarId);
            }
        }
    }

    public function updateOarDescriptor($oarId, $newDescriptor)
    {
        // verify oar belongs to whitelabel
        $allOars = (new DB_Equipment_Oars())->getAllOarsForWhitelabel($this->whitelabel_id);
        foreach ($allOars as $oar) {
            if ($oar['oar_id'] == $oarId) {
                return (new DB_Equipment_Oars())->updateDesc($oarId, $newDescriptor);
            }
        }
    }

    public function updateOarStatus($oarId, $status)
    {
        // verify oar belongs to whitelabel
        $allOars = (new DB_Equipment_Oars())->getAllOarsForWhitelabel($this->whitelabel_id);
        foreach ($allOars as $oar) {
            if ($oar['oar_id'] == $oarId) {
                if ($status == "Available" || $status == "available") {
                    (new DB_Equipment_Oars())->setAsAvailable($oarId);
                } else if ($status == "Out Of Service" || $status == "out of service") {
                    (new DB_Equipment_Oars())->setAsOutOfService($oarId);
                }
            }
        }
    }

    public function updateOarSetName($setId, $newName)
    {
        (new DB_Equipment_Oars_Sets())->updateName($setId, $this->whitelabel_id, $newName);
    }

    public function updateOarSetDescr($setId, $newDescr)
    {
        (new DB_Equipment_Oars_Sets())->updateDescr($setId, $this->whitelabel_id, $newDescr);
    }

    public function addErg($identifier, $make, $model, $yearPurchased, $location, $insured, $insured_value)
    {
        $insured = ucfirst(strtolower($insured));
        if ($insured != "Yes") {
            $insured = "No";
        }

        (new DB_Equipment_Ergs())->addErg($this->whitelabel_id, $identifier, $make, $model, $yearPurchased, $location, $insured, $insured_value);
    }

    public function getAllErgs()
    {
        return (new DB_Equipment_Ergs())->getAllErgs($this->whitelabel_id);
    }

    public function getErg($ergId)
    {
        return (new DB_Equipment_Ergs())->getErg($this->whitelabel_id, $ergId);
    }

    public function deleteErg($ergId)
    {
        (new DB_Equipment_Ergs())->deleteErg($this->whitelabel_id, $ergId);
    }

    public function updateIdentifier($ergId, $newIdentifier)
    {
        (new DB_Equipment_Ergs())->updateIdentifier($this->whitelabel_id, $ergId, $newIdentifier);
    }

    public function updateLocation($ergId, $location)
    {
        (new DB_Equipment_Ergs())->updateLocation($this->whitelabel_id, $ergId, $location);
    }

    public function updateInsured($ergId, $insured)
    {
        $insured = ucfirst(strtolower($insured));
        if($insured != "Yes"){$insured = "No";}

        (new DB_Equipment_Ergs())->updateInsured($this->whitelabel_id, $ergId, $insured);
    }

    public function updateInsuredValue($ergId, $newValue)
    {
        (new DB_Equipment_Ergs())->updateInsuredValue($this->whitelabel_id, $ergId, $newValue);
    }

    public function updateStatus($ergId, $status)
    {
        $status = ucfirst(strtolower($status));
        if ($status != "Available") {
            $status = "Out Of Service";
        }
        (new DB_Equipment_Ergs())->updateStatus($this->whitelabel_id, $ergId, $status);
    }

    public function addIssue($custid, $descr)
    {
        (new DB_Equipment_Issues())->addIssue($this->whitelabel_id, $custid, $descr);
        Service_Notification_Send::equipmentIssueFromSurvey($custid, $this->whitelabel_id, $descr);
    }

    public function getEquipmentIssues()
    {
        return (new DB_Equipment_Issues())->getWhitelabelIssues($this->whitelabel_id);
    }

}