--
-- Table structure for table `biometrics`
--

CREATE TABLE IF NOT EXISTS `biometrics` (
                                            `custid` int(11) NOT NULL,
                                            `added_at` int(11) NOT NULL,
                                            `height` int(11) NOT NULL,
                                            `weight` VARCHAR(10) NOT NULL,
                                            PRIMARY KEY (`custid`,`added_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `biometrics`
--
ALTER TABLE `biometrics`
    ADD CONSTRAINT `fk_custid_biometrics` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
