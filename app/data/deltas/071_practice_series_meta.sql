--
-- Table structure for table `practice_series_meta`
--

CREATE TABLE IF NOT EXISTS `practice_series_meta` (
                                                      `series_id` int(11) NOT NULL AUTO_INCREMENT,
                                                      `whitelabel_id` int(11) NOT NULL,
                                                      `name` varchar(50) NOT NULL,
                                                      `location_id` int(11) NOT NULL,
                                                      `max_attendees` int(11) NOT NULL,
                                                      `start_date` int(11) NOT NULL,
                                                      `end_date` int(11) NOT NULL,
                                                      `visiting_rowers` varchar(5) NOT NULL DEFAULT 'No',
                                                      `finalized` varchar(5) NOT NULL DEFAULT 'No',
                                                      `created_at` int(11) NOT NULL,
                                                      PRIMARY KEY (`series_id`),
                                                      KEY `fk_whitelabel_id_practice_series_meta` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `practice_series_meta`
--
ALTER TABLE `practice_series_meta`
    ADD CONSTRAINT `fk_whitelabel_id_practice_series_meta` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
