<?php


class finances extends Controller
{
    public function getAccountUrl()
    {
        $urlType       = $this->param("url_type");
        $returnUrl     = $this->param("return_url");
        $refreshUrl    = $this->param("refresh_url");

        $url = (new Service_Stripe_Connected_Accounts())->getWhitelabelUrl($_SESSION['whitelabel_id'], $refreshUrl, $returnUrl, $urlType);
        $this->view("json", $url);
    }

    public function reloadWhitelabelAccount()
    {
        (new Service_Stripe_Connected_Accounts())->checkIfWhitelabelConnectedAccountSetup($_SESSION["whitelabel_id"]);
    }

    /**
     * DO NOT INCLUDE IN API DOCUMENTATION
     * payout_type options are 'instant' or 'bank'
     */
    public function addWhitelabelPayoutMethod()
    {
        $cardToken  = $this->param("card_token");
        $payoutType = $this->param("payout_type");
        $result     = (new Service_Stripe_Connected_Accounts())->addWhitelabelPayoutMethod($_SESSION["whitelabel_id"], $cardToken, $payoutType);
        $this->view("json", $result);
    }

    public function getWhitelabelBalance()
    {
        $balance = (new Service_Stripe_Connected_Accounts())->getWhitelabelBalance($_SESSION["whitelabel_id"]);
        $this->view("json", $balance);
    }

    public function getWhitelabelPayoutSources()
    {
        $externalAccounts = (new Service_Stripe_Connected_Accounts())->getWhitelabelPayoutAccounts($_SESSION["whitelabel_id"]);
        $this->view("json", $externalAccounts);
    }

    public function whitelabelTriggerPayout()
    {
        $method = $this->param("payout_method");
        $amount = $this->param("payout_amount");

        $result = (new Service_Stripe_Connected_Accounts())->triggerWhitelabelPayout($_SESSION["whitelabel_id"], $amount, $method);
        $this->view("json", $result);
    }

    public function getUserTimeline()
    {
        $custid          = (!is_null($this->param("custid"))) ? $this->param("custid") : $_SESSION["custid"];

        $allCharges      = (new Service_User_Payments())->getAllCharges($_SESSION["whitelabel_id"], $custid);
        $paymentsCredits = (new Service_User_Payments())->getPaymentsAndCredits($_SESSION["whitelabel_id"], $custid);

        $data = array(
            "charges" => $allCharges,
            "credits" => $paymentsCredits
        );

        $this->view("json", $data);
    }

    public function getAccountBalance()
    {
        $custid          = (!is_null($this->param("custid"))) ? $this->param("custid") : $_SESSION["custid"];
        $accountBalances = (new Service_User_Payments())->getAccountBalanceByCustid($custid);

        $this->view("json", $accountBalances);
    }

    public function getUserPaymentMethods()
    {
        $paymentMethods = (new Service_User_Payments())->getUserPaymentCards($_SESSION["custid"]);

        $this->view("json", $paymentMethods);
    }

    public function addPaymentMethod()
    {
        $stripeToken = $this->param("stripeToken");
        (new Service_User_Payments())->addPaymentMethod($_SESSION["custid"], $stripeToken);
    }

    public function removePaymentMethod()
    {
        $sourceId = $this->param("payment_source_id");
        (new Service_User_Payments())->removePaymentMethod($_SESSION["custid"], $sourceId);
    }

    /**
     * @uses $amount int amount to charge user (in cents). i.e. $10.23 should be passed as 1023, $5 should be passed as 500, etc.
     * @uses $preferredPaymentMethod string stripe token representing the payment method that should be charged. If the payment fails, all methods linked will be attempted.
     * @uses $whitelabel_id int whitelabel id of the organization that will be receiving the funds. The user does not have to be affiliated.
     * @returns void
     */
    public function makePaymentToWhitelabel()
    {
        $amount                 = $this->param("amount");
        $whitelabel_id          = $this->param("whitelabel_id");
        $preferredPaymentMethod = $this->param("preferred_payment_method");

        $whitelabelAccountId    = Service_Stripe_Connected_Accounts::checkForWhitelabelConnectedAccount($whitelabel_id);

        //get user details
        $user            = new User($_SESSION["custid"]);
        $customerAccount = $user->stripeCustomerAccount["stripe_customer_id"];

        (new Service_User_Payments())->processPayment($whitelabel_id, $_SESSION["custid"], $user->email, $amount, $customerAccount, $preferredPaymentMethod, $whitelabelAccountId);
    }

    public function getUserAccountBalances()
    {
        $balances = (new Service_User_Payments())->getWhitelabelAccountBalances($_SESSION["whitelabel_id"]);

        $this->view("json", $balances);
    }

    public function addChargeToUser()
    {
        $custid     = $this->param("custid");
        $amount     = $this->param("amount");
        $descr      = $this->param("descr");
        $type       = $this->param("charge_type");
        $foreignId  = $this->param("foreign_id");
        $dueDate    = $this->param("due_date");

        if (!$dueDate) {
            $dueDate = time()+(86400*2);
        }
        (new Service_User_Payments())->postCharge($_SESSION["whitelabel_id"], $custid, $dueDate, $_SESSION["custid"], $amount, $descr, $type, $foreignId);
    }

    public function addCreditToUser()
    {
        $custid = $this->param("custid");
        $amount = $this->param("amount");
        $descr  = $this->param("description");

        (new Service_User_Payments())->postCredit($_SESSION["whitelabel_id"], $custid, $descr,"WHITELABEL GRANTED CREDIT", $_SESSION["custid"], $amount);
    }

    public function addGroupFeeSchedule()
    {
        $groupId = $this->param("group_id");
        $amount  = $this->param("amount");
        $dueBy   = $this->param("due_by");

        (new Service_User_Payments())->addFeeScheduleToGroup($_SESSION["whitelabel_id"], $groupId, $amount, $dueBy, $_SESSION["custid"]);
    }

    public function removeFromGroupFeeSchedule()
    {
        $groupScheduleId = $this->param("schedule_id");

        (new Service_User_Payments())->removeFeeFromGroupSchedule($_SESSION["whitelabel_id"], $groupScheduleId);
    }

    public function updateGroupJoinFee()
    {
        $groupId = $this->param("group_id");
        $amount  = $this->param("amount");

        (new Service_User_Payments())->updateGroupJoinFee($_SESSION["whitelabel_id"], $groupId, $amount);
    }

    public function getGroupFeeSchedule()
    {
        $groupId = $this->param("group_id");
        $fees = (new Service_User_Payments())->getGroupFeeSchedule($_SESSION["whitelabel_id"], $groupId);

        $this->view("json", $fees);
    }
    
    public function updateFeeRecoverySetting()
    {
        $setting = $this->param("setting");
        
        switch (strtoupper($setting)) {
            case "YES":
            case "TRUE":
                WhitelabelSettings::updateValue($_SESSION["whitelabel_id"], "stripe.feerecovery","Yes");
                break;
            case "NO":
            case "FALSE":
            default:
                WhitelabelSettings::updateValue($_SESSION["whitelabel_id"], "stripe.feerecovery","No");
                break;
        }
    }

}