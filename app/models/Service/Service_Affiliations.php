<?php


class Service_Affiliations
{
    public static function addAffiliation($custid, $whitelabel_id, $email = null)
    {
        $affiliation = new DB_User_Affiliations();
        // verify that whitelabel_id exists
        $whitelabel     = Service_Whitelabel::findNameById($whitelabel_id);
        $addAffiliation = false;
        if (count($whitelabel) > 0) {
            // check the max allowed user in the whitelabel
            $planData = (new Service_Whitelabel_Subscriptions($whitelabel_id))->getPlan();
            if ($planData["plan_id"] == 1) {
                // max allowed users is 15
                $existingUsers = self::getAllAffiliatedUsers($whitelabel_id);
                if (count($existingUsers) < 15) {
                    $addAffiliation = true;
                }
            } else {
                $addAffiliation = true;
            }
        }

        if ($addAffiliation) {
            $affiliation->addAffiliation($custid, $whitelabel_id);
            // check if usrowing number is already added to user
            $user         = new User($custid);
            $usrowingInfo = $user->usrowing;
            if (!isset($usrowingInfo["usrowingID"]) || is_null($usrowingInfo["usrowingID"])) {
                // attempt to find usrowing number
                $foundUsrowingNumber = (new Service_USRowing())->searchForUser($whitelabel_id, $custid);
                if (!is_null($foundUsrowingNumber)) {
                    $user->addUSRowingID($foundUsrowingNumber, 0);
                }
            }

            // remove exiting pre-authorization if it exists
            if (!is_null($email)) {
                Service_Notification_Send::newapprovedjoinrequest($custid, $_SESSION['custid'], $whitelabel_id, true);
                (new DB_PreAuthorized_New_Users())->removePreauthByEmailWhitelabel($email, $whitelabel_id);
            }
        }
    }

    public static function checkIfAffiliated($custid, $whitelabel_id, $timezone = false)
    {
        return (new DB_User_Affiliations())->checkIfAffiliated($custid, $whitelabel_id, $timezone);

    }

    public static function findAllUserAffiliations($custid)
    {
        $affiliations = (new DB_User_Affiliations())->findAllAffiliationsByCustid($custid);
        $returnable = [];
        foreach ($affiliations as $row) {
            $returnable[] = array (
                "custid"              => $row["custid"],
                "whitelabel_id"       => $row["whitelabel_id"],
                "whitelabel_name"     => $row["whitelabel_name"],
                "created_at"          => $row["created_at"],
                "last_accessed"       => $row["last_accessed"],
                "skin"                => $row["skin"],
                "timezone"            => $row["timezone"],
                "user_count"          => $row["user_count"],
                "whitelabel_settings" => WhitelabelSettings::getAllSettings($row["whitelabel_id"]),
                "plans"               => (new DB_Whitelabel_Plans())->getWhitelabelPlan($row["whitelabel_id"], false)
            );
        }

        return $returnable;
    }

    public static function UpdateTimeAccessed($whitelabel_id, $custid)
    {
        (new DB_User_Affiliations())->updateTimeAccessed($whitelabel_id, $custid);
    }

    public static function addJoinRequest($whitelabel_id)
    {
        $dbRequest = new DB_Whitelabel_Join_Requests();
        // check if request already made
        $existingRequest = $dbRequest->getRequest($_SESSION['custid'], $whitelabel_id);
        if (!empty($existingRequest)) {
            if ($existingRequest['status'] == 'Declined') {
                $dbRequest->updateRequest($_SESSION['custid'],$whitelabel_id, "Pending");
                return array("Status" =>"Success", "Message"=>"Re-sent request to join.", "NewState"=>"Pending");
            } else {
                return array("Status" =>"Error", "Message"=>"Already sent join request. Pending admin approval.","NewState"=>"Pending");
            }
        } else {
            $dbRequest->addRequest($_SESSION['custid'],$whitelabel_id);
            return array("Status" =>"Success", "Message"=>"Request sent.","NewState"=>"Pending");
        }
    }

    public static function declineJoinRequest($custid,$whitelabel_id)
    {
        $dbRequest = new DB_Whitelabel_Join_Requests();
        $dbRequest->updateRequest($custid,$whitelabel_id, "Declined");
        return array("Status" =>"Success", "Message"=>"Join request declined.","NewState"=>"Declined");

    }

    public static function approveJoinRequest($custid, $whitelabel_id)
    {
        // add to affiliation and set request as approved
        self::addAffiliation($custid, $whitelabel_id);
        (new DB_Whitelabel_Join_Requests())->updateRequest($custid,$whitelabel_id, "Approved");
        return array("Status" =>"Success", "Message"=>"Added to whitelabel.","NewState"=>"Approved");
    }

    public static function getPendingRequests($whitelabel_id)
    {
        return (new DB_Whitelabel_Join_Requests())->getRequests($whitelabel_id,"Pending");
    }

    public static function getDeclinedRequests($whitelabel_id)
    {
        return (new DB_Whitelabel_Join_Requests())->getRequests($whitelabel_id,"Declined");
    }

    public static function getApprovedRequests($whitelabel_id)
    {
        return (new DB_Whitelabel_Join_Requests())->getRequests($whitelabel_id,"Approved");
    }

    public static function getAllAffiliatedUsers($whitelabel_id)
    {
        return (new DB_User_Affiliations())->findAllAffiliationsByWhitelabel($whitelabel_id);
    }

    public static function getPendingRequestsZapier($whitelabel_id)
    {
        return (new DB_Whitelabel_Join_Requests())->getRequestsZapier($whitelabel_id,"Pending");
    }

    public static function addPreauthorization($email, $whitelabel_id, $custid_creator)
    {
        // check if user is already affiliated with the whitelabel
        $existingUsers    = self::getAllAffiliatedUsers($whitelabel_id);
        $userIsAffiliated = false;
        foreach ($existingUsers as $user) {
            if ($user["email"] == $email) {
                $userIsAffiliated = true;
                break;
            }
        }

        if (!$userIsAffiliated) {
            // if user is not affiliated, proceed to see if we can pre-authorize them.
            $addPreauth = false;

            // check if email is already pre-authorized
            $existingPreauths = (new DB_PreAuthorized_New_Users())->checkForPreauth($email, $whitelabel_id);
            if(count($existingPreauths) == 0)
            {
                // check the max allowed user in the whitelabel
                $planData = (new Service_Whitelabel_Subscriptions($whitelabel_id))->getPlan();
                if ($planData["plan_id"] == 1) {
                    // max allowed users is 15
                    $existingUsers = self::getAllAffiliatedUsers($whitelabel_id);
                    if (count($existingUsers) < 15) {
                        $addPreauth = true;
                    }
                } else {
                    $addPreauth = true;
                }
            }

            if ($addPreauth) {
                WhitelabelSettings::updateValue($whitelabel_id, "wlchecklist.preauth", "true");
                return (new DB_PreAuthorized_New_Users())->addPreauth($email, $whitelabel_id, $custid_creator);
            }
        }
        return null;
    }

    public static function getpreauthorizedEmails($whitelabel_id)
    {
        return (new DB_PreAuthorized_New_Users())->getAllPreauths($whitelabel_id);
    }

    public static function removePreauthorizedEmail($whitelabel_id, $email)
    {
        (new DB_PreAuthorized_New_Users())->deletePreauth($whitelabel_id, $email);
    }

    public function removeAffiliation($whitelabel_id, $custid)
    {
        (new DB_User_Affiliations())->removeAffiliation($whitelabel_id, $custid);
    }

}