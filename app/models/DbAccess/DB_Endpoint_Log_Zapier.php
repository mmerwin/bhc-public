<?php


class DB_Endpoint_Log_Zapier extends DataModel
{
    protected $schema = 'endpoint_log_zapier';

    public function addToLog($controller, $method, $ip, $custid)
    {
        $this->DB->query("INSERT INTO {$this->schema} (controller, method, timestamp, ip, custid) VALUES (?, ?, ?, ?, ?)", array($controller, $method, time(), $ip, $custid));
    }

}