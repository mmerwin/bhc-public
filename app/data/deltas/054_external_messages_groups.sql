--
-- Table structure for table `external_messages_groups`
--

CREATE TABLE IF NOT EXISTS `external_messages_groups` (
                                                          `message_id` int(11) NOT NULL,
                                                          `group_id` int(11) NOT NULL,
                                                          `stakeholders` varchar(10) NOT NULL DEFAULT 'No',
                                                          `whitelabel_id` int(11) NOT NULL,
                                                          PRIMARY KEY (`message_id`,`group_id`,`stakeholders`,`whitelabel_id`),
                                                          KEY `fk_whitelabel_id_external_messages_groups` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `external_messages_groups`
--
ALTER TABLE `external_messages_groups`
    ADD CONSTRAINT `fk_message_id_external_message_groups` FOREIGN KEY (`message_id`) REFERENCES `external_messages` (`message_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_external_messages_groups` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
