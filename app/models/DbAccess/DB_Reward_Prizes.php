<?php


class DB_Reward_Prizes extends DataModel
{
    protected $schema = "reward_prizes";

    public function getPrizes()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} ORDER BY points ASC")->fetchAll();
    }

}