--
-- Table structure for table `socialmedia_accounts`
--

CREATE TABLE IF NOT EXISTS `socialmedia_accounts` (
                                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                                      `whitelabel_id` int(11) NOT NULL,
                                                      `type` varchar(50) NOT NULL,
                                                      `data` text NOT NULL,
                                                      `last_updated` int(11) NOT NULL,
                                                      PRIMARY KEY (`id`),
                                                      KEY `fk_whitelabel_id_socialmedia` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `socialmedia_accounts`
--
ALTER TABLE `socialmedia_accounts`
    ADD CONSTRAINT `fk_whitelabel_id_socialmedia` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
