<?php


class DB_Postmark_Log extends DataModel
{
    protected $schema = "postmark_log";
    public function addToLog($message_id, $receiver, $sender, $error_code, $status, $template_id, $custid)
    {
        $this->DB->query("INSERT INTO {$this->schema} (message_id, receiver, sender, error_code, status, template_id, custid, opens, clicks, timestamp, server_id) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, 0, 0, ?, ?)", array($message_id, $receiver, $sender, $error_code, $status, $template_id, $custid, time(), 0));
    }

}