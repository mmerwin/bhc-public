--
-- Table structure for table `whitelabels`
--

CREATE TABLE IF NOT EXISTS `whitelabels` (
                                             `whitelabel_id` int(11) NOT NULL AUTO_INCREMENT,
                                             `name` varchar(100) NOT NULL,
                                             `abbreviation` varchar(10) NOT NULL,
                                             `rc_orgid` int(11) NOT NULL,
                                             `created_at` int(11) NOT NULL,
                                             `custid_claimed` int(11) NOT NULL,
                                             `timezone` varchar(30) NOT NULL DEFAULT 'America/New_York',
                                             PRIMARY KEY (`whitelabel_id`),
                                             KEY `fk_rc_id_whitelabels` (`rc_orgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `whitelabels`
--
ALTER TABLE `whitelabels`
    ADD CONSTRAINT `fk_rc_id_whitelabels` FOREIGN KEY (`rc_orgid`) REFERENCES `regattacentral_orgs` (`organizationId`);
COMMIT;
