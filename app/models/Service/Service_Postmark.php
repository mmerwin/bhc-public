<?php

class Service_Postmark
{

    public function getWhitelabelDomains($whitelabel_id)
    {
        $rawDomains       = (new DB_Postmark_Domains())->getDomainsByWhitelabel($whitelabel_id);
        $rawSignatures    = $this->getWhitelabelSignatures($whitelabel_id);
        $formattedDomains = [];
        foreach ($rawDomains as $domain) {
            // get sender signatures for domain
            $signatures = [];
            foreach ($rawSignatures as $signature) {
                if ($signature['domain'] == $domain['name']) {
                    $signatures[] = $signature;
                }
            }
            $formattedDomains[] = array(
                "domain"     => $domain,
                "signatures" => $signatures
            );
        }
        return $formattedDomains;
    }

    public function addDomain($whitelabel_id, $domain)
    {
        // send to postmark
        $data = json_encode(array(
            "Name"            =>  $domain,
            "ReturnPathDomain"=> "bhconnect-bounces-{$whitelabel_id}.{$domain}"
        ));
        $accountToken = Service_External_Keys::getToken("postmark-account");
        $request      = $this->apiCallAccount("https://api.postmarkapp.com/domains", "POST", $data, $accountToken);
        $response     = json_decode($request, true);

        // update postmark_domains table
        (new DB_Postmark_Domains())->addDomain($whitelabel_id, $response);

        // add sender default sender signatures
        $clubcode = $this->getClubCode($whitelabel_id);
        $this->addSenderSignatures($whitelabel_id, $domain,"notifications","{$clubcode} Notifications");
        $this->addSenderSignatures($whitelabel_id, $domain,"updates","{$clubcode} Updates");

        // send help email to all members with permission #12
        $allowedUsers = Service_Permissions::findUsersWithPermission(12, $whitelabel_id);
        //add notification to all with permission
        foreach ($allowedUsers as $receiver) {
            $json = '{"TemplateId":20221618,"TemplateAlias":"string","TemplateModel":{"sender_name":"'.$receiver["fname"].' '.$receiver["lname"].'","domain_name":"'.$response['Name'].'", "dkim_hostname":"'.$response['DKIMPendingHost'].'", "dkim_value":"'.$response['DKIMPendingTextValue'].'","returnpath_hostname":"'.$response['ReturnPathDomain'].'","returnpath_value":"'.$response['ReturnPathDomainCNAMEValue'].'","domain_id":"'.$response['ID'].'","whitelabel_id":"'.$whitelabel_id.'"},"InlineCss":true,"From":"Support@BoathouseConnect.com","To":"'.$receiver["email"].'","Tag":"DNS Setting Instructions","ReplyTo":"Support@BoathouseConnect.com","TrackOpens":true,"TrackLinks":"HtmlOnly"}';
            $this->sendPlatformEmail($json, $_SESSION['custid'], 20221618);
        }

        // return all domain names
        return self::getWhitelabelDomains($whitelabel_id);
    }

    public function sendPlatformEmail($data, $custid, $templateId)
    {
        $serverToken = Service_External_Keys::getToken("postmark-platform");
        $request     = self::apiCallServer("https://api.postmarkapp.com/email/withTemplate", "POST", $data, $serverToken);
        $response    = json_decode($request, true);
        (new DB_Postmark_Log())->addToLog($response['MessageID'], $response['To'], "Support@BoathouseConnect.com", $response['ErrorCode'], $response['Message'], $templateId, $custid);
    }

    public function passwordResetEmail($fname, $lname, $resetToken, $resetTime, $email, $custid)
    {
        $json = '{"TemplateId":20111373,"TemplateAlias":"string","TemplateModel":{"name":"'.$fname.' '.$lname.'","action_url":"https://boathouseconnect.com/home/passwordreset/'.$resetToken.'/'.$resetTime.'"},"InlineCss":true,"From":"Support@BoathouseConnect.com","To":"'.$email.'","Tag":"Password Reset","ReplyTo":"Support@BoathouseConnect.com","TrackOpens":true,"TrackLinks":"HtmlOnly"}';

        $this->sendPlatformEmail($json, $custid, 20083418);
    }


    public function apiCallServer($url, $method, $data, $serverToken)
    {
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "X-Postmark-Server-Token: $serverToken",
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $return = curl_exec($ch);

        $curl_error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // do some checking to make sure it sent
        if ($http_code == 200) {
            return $return;
        }
    }

    public function apiCallAccount($url, $method, $data, $accountToken)
    {
        $headers = array(
            "Accept: application/json",
            "Content-Type: application/json",
            "X-Postmark-Account-Token: $accountToken",
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $return = curl_exec($ch);

        $curl_error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // do some checking to make sure it sent
        if ($http_code == 200) {
            return $return;
        }
    }

    public function verifyRecords($domain_id)
    {
        // check if domain exists
        $domainData = (new DB_Postmark_Domains())->getDomainById($domain_id);
        if ($domainData['id'] == $domain_id) {
            $data = json_encode(array(
            ));
            $accountToken = Service_External_Keys::getToken("postmark-account");
            // verify DKIM
            $requestA =$this->apiCallAccount("https://api.postmarkapp.com/domains/{$domain_id}/verifyDkim", "PUT", $data, $accountToken);
            $requestB =json_decode($this->apiCallAccount("https://api.postmarkapp.com/domains/{$domain_id}/verifyReturnPath", "PUT", $data, $accountToken), true);

            return (new DB_Postmark_Domains())->replaceDomain($domainData['whitelabel_id'], $requestB);
        }

    }

    public function refreshAllDomainNamesWithPostmark()
    {
        $allDomains = (new DB_Postmark_Domains())->getAllDomains();
        foreach ($allDomains as $domain) {
            self::verifyRecords($domain['id']);
        }
    }

    public function addSenderSignatures($whitelabel_id, $domain,$fromUsername,$name)
    {
        // send to postmark
        $data = json_encode(array(
            "FromEmail"     => $fromUsername."@".$domain,
            "Name"          => $name,
            "ReplyToEmail"  => $whitelabel_id."-inbound@wlinbound.boathouseconnect.com"
        ));
        $accountToken = Service_External_Keys::getToken("postmark-account");
        $request      = $this->apiCallAccount("https://api.postmarkapp.com/senders", "POST", $data, $accountToken);
        $response     = json_decode($request, true);
        (new DB_Postmark_Sender_Signatures())->addSignature($whitelabel_id, $domain, $response['ID'], $response['EmailAddress'], $response["Name"], $response["Confirmed"], $response["ReplyToEmailAddress"]);
    }

    private function getClubCode($whitelabel_id)
    {
        $whitelabel = Service_Whitelabel::findNameById($whitelabel_id);
        return $whitelabel[0]['abbreviation'];
    }

    public function getWhitelabelSignatures($whitelabel_id)
    {
        return (new DB_Postmark_Sender_Signatures())->getSignaturesByWhitelabel($whitelabel_id);
    }

    public function deleteSignature($whitelabel_id, $signatureId)
    {
        // check that signature belongs to whitelabel
        $allSignatures = $this->getWhitelabelSignatures($whitelabel_id);
        foreach ($allSignatures as $signature) {
            if ($signature['id'] == $signatureId && $signature['whitelabel_id'] == $whitelabel_id) {
                // send to postmark
                $data         = json_encode(array());
                $accountToken = Service_External_Keys::getToken("postmark-account");
                $request      = $this->apiCallAccount("https://api.postmarkapp.com/senders/{$signatureId}", "DELETE", $data, $accountToken);
                (new DB_Postmark_Sender_Signatures())->deleteSignature($whitelabel_id, $signatureId);
            }
        }

    }

    public function getDefaultSignatures($whitelabel_id)
    {
        $defaults  = (new DB_Postmark_Default_Signatures())->getDefaultSignaturesByWhitelabel($whitelabel_id);
        $formatted = [];
        foreach ($defaults as $row) {
            if ($row["return_path_domain_verified"] == "true" && $row["dkim_verified"] == "true") {
                $confirmed = "true";
            } else {
                $confirmed = "false";
            }
            $formatted[$row['type']] = [
                "signature_id"           => $row['signature_id'],
                "domain"                 => $row['domain'],
                "email_address"          => $row['email_address'],
                "reply_to_email_address" => $row['reply_to_email_address'],
                "email_name"             => $row['email_name'],
                "confirmed"              => $confirmed
            ];
        }
        return $formatted;
    }

    public function swapDefaultSignature($whitelabel_id, $type, $signature_id)
    {
        (new DB_Postmark_Default_Signatures())->changeSignatureId($whitelabel_id, $type, $signature_id);
    }

    public function deleteDefaultSignature($whitelabel_id, $type)
    {
        (new DB_Postmark_Default_Signatures())->deleteSignature($whitelabel_id, $type);
    }

}

