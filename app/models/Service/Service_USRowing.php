<?php


class Service_USRowing extends Security
{
    public function fetchUpdates($whitelabel_id, $custid, array $credentials = null, $archived = "F", $firstTimeRun = true)
    {
        if (is_null($credentials)) {
            // get credentials
            $credentials = self::getCredentials($whitelabel_id);
        }

        // make request (with archived set to "F" (false)
        $request = json_decode(self::makeRequst($credentials["username"], $credentials["password"], $credentials["rc_orgid"], $archived, "0"), true);

        if (isset($request["errorMessage"])) {
            // the request has failed
            (new DB_Usrowing_Credentials())->setAsUnconfirmed($whitelabel_id);
            return array("Status"=>"Error","Message"=>"Bad Credentials");
        } else if (isset($request["header"]) && $request["header"]["resultlimit"] == 0) {
            // the request was good, but not authorized for the rc_orgid
            return array("Status"=>"Error","Message"=>"You do not have USRowing Administrator access to this organization. Please contact us for additional support.");

        } else if (!isset($request["header"]["resultlimit"])) {
            // unknown error has occurred
            return array("Status"=>"Error","Message"=> "Unknown issue. Please contact us for additional support.");

        } else {
            // request must be good
            // update the waivers
            self::updateWaivers($request["roster"], $credentials["rc_orgid"]);


            if ($firstTimeRun) {
                // save credentials for use later
                self::setCredentials($whitelabel_id, $custid, $credentials["username"], $credentials["password"], "Yes");
                WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.usrowing.waiver.crendentials", "true");
                (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(7, $custid);
            }

            (new DB_Usrowing_Credentials())->updateLastUsed($whitelabel_id);

            return array("Status"=>"Success","Message"=>"Hurray! Everything looks good, USRowing waviers have been imported");
        }
    }


    private function getCredentials($whitelabel_id)
    {
        $credentials = (new DB_Usrowing_Credentials())->getCredentials($whitelabel_id);
        return [
            "whitelabel_id" => $whitelabel_id,
            "username"      => $this->decrypt($credentials["username"]),
            "password"      => $this->decrypt($credentials["password"]),
            "rc_orgid"      => $credentials["rc_orgid"],
            "confirmed"     => $credentials["confirmed"]
        ];
    }

    private function setCredentials($whitelabel_id, $custid, $usernameRaw, $passwordRaw, $confirmed)
    {
        $usernameEnc = $this->encrypt($usernameRaw);
        $passwordEnc = $this->encrypt($passwordRaw);
        (new DB_Usrowing_Credentials())->setCredentials($whitelabel_id, $custid, $usernameEnc, $passwordEnc,$confirmed);
    }

    private function makeRequst($username, $password, $rc_orgid, $archived, $offset)
    {
        // get Apify details
        $apifyKeys  = Environment::getExternalKeys("apify-usrowingwaiver");
        $url        = "https://api.apify.com/v2/acts/{$apifyKeys["client_id"]}/run-sync?token={$apifyKeys["token"]}";
        $data = [
            "username"  => $username,
            "password"  => $password,
            "rc_orgid"  => strval($rc_orgid),
            "archived"  => $archived,
            "offset"    => strval($offset),
            "limit"     => strval(rand(5000,50000)),
            "timestamp" => time()
        ];

        $ch = curl_init( $url );
        // Setup request to send json via POST.
        $payload = json_encode($data);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        // Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        // Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function updateWaivers($roster, $rc_orgid)
    {
        $db = new DB_Usrowing_Raw();
        foreach ($roster as $row) {
            $values = self::verifyValues($row["member"]);
            if ($values["usrowingId"] != 0) {
                $db->updateMember($values["usrowingId"],$values["firstName"], $values["lastName"], $values["email"], $values["gender"], $values["ltwtStatus"], $values["birth_year"], $values["birth_month"],
                    $values["birth_day"], $values["waiverSigned"], $values["waiverExp"], $values["membershipExpires"], $values["membershipTitle"], $rc_orgid, $values["rc_participantId"]);
            }
        }
    }

    private function verifyValues($row)
    {
        $returnable = array();

        if (!isset($row["athleteId"])) {$returnable["rc_participantId"] = 0;} else{$returnable["rc_participantId"] = $row["athleteId"];}
        if (!isset($row["memberId"])) {$returnable["usrowingId"] = 0;} else{$returnable["usrowingId"] = $row["memberId"];}
        if (!isset($row["firstName"])) {$returnable["firstName"] = "Unknown";} else{$returnable["firstName"] = $row["firstName"];}
        if (!isset($row["lastName"])) {$returnable["lastName"] = "Unknown";} else{$returnable["lastName"] = $row["lastName"];}
        if (!isset($row["email"])) {$returnable["email"] = "Unknown";} else{$returnable["email"] = $row["email"];}
        if (!isset($row["gender"])) {$returnable["email"] = "Unknown";} else{if($row["gender"] == "M"){$returnable["gender"] = "Male";} else if($row["gender"] == "F"){$returnable["gender"] = "Female";} else{$returnable["gender"] = $row["gender"];}}
        if (!isset($row["ltwtStatus"])) {$returnable["ltwtStatus"] = "Unknown";} else{$returnable["ltwtStatus"] = $row["ltwtStatus"];}

        if (isset($row["birthdate"])) {
            $birthday = strtotime($row["birthdate"]);
        } else {
            $birthday = 0;
        }

        if(!isset($row["birthdate"])) {$returnable["birth_year"] = 0;} else{$returnable["birth_year"] = date("Y", $birthday);}
        if(!isset($row["birthdate"])) {$returnable["birth_month"] = 0;} else{$returnable["birth_month"] = date("m", $birthday);}
        if(!isset($row["birthdate"])) {$returnable["birth_day"] = 0;} else{$returnable["birth_day"] = date("d", $birthday);}

        if(!isset($row["waiverSigned"])) {$returnable["waiverSigned"] = "No";} else{if($row["waiverSigned"] == "true") {$returnable["waiverSigned"] = "Yes";} else{$returnable["waiverSigned"] = "No";}}
        if(!isset($row["waiverExp"])) {$returnable["waiverExp"] = 0;} else{$returnable["waiverExp"] = strtotime($row["waiverExp"]);}
        if(!isset($row["expiresOn"])) {$returnable["membershipExpires"] = 0;} else{$returnable["membershipExpires"] = strtotime($row["expiresOn"]);}
        if(!isset($row["serviceTitle"])) {$returnable["membershipTitle"] = "Unknown";} else{$returnable["membershipTitle"] = $row["serviceTitle"];}

        return $returnable;
    }

    public function getRawByWhitelabel($whitelabel_id)
    {
        // get rc_orgid
        $credentials = self::getCredentials($whitelabel_id);
        $rc_orgid    = $credentials["rc_orgid"];

        return (new DB_Usrowing_Raw())->getByRcId($rc_orgid);
    }

    public function getWhitelabelWaivers($whitelabel_id)
    {
        return (new DB_Usrowing())->getByWhitelabel($whitelabel_id);
    }

    public function getLastSyncTime($whitelabel_id)
    {
        $credentials = (new DB_Usrowing_Credentials())->getCredentials($whitelabel_id);

        return [
            "whitelabel_id"    => $credentials["whitelabel_id"],
            "confirmed"        => $credentials['confirmed'],
            "last_used"        => $credentials["last_used"],
            "last_used_text"   => date("F j, Y", $credentials["last_used"]),
            "custid_set"       => $credentials["custid"],
            "rc_orgid"         => $credentials["rc_orgid"]
        ];
    }

    public function searchForUser($whitelabel_id, $custid)
    {
        $user       = new User($custid);
        $whitelabel = Service_Whitelabel::findNameById($whitelabel_id);
        $rcOrgid    = $whitelabel[0]["rc_orgid"];

        // check if USRowing can be found exclusively using RegattaCentral orgid
        $result = (new DB_Usrowing_Raw())->searchForUser($user->fname, $user->lname, $user->sex, $user->birthYear, $user->birthMonth, $user->birthDay, $rcOrgid);

        if ($result["member_id"] < 1) {
            // attempt to find USRowing information without specifying rcOrgid
            $newResult = (new DB_Usrowing_Raw())->searchForUser($user->fname, $user->lname, $user->sex, $user->birthYear, $user->birthMonth, $user->birthDay, null);
            if ($newResult["member_id"] < 1) {
                $usrowingId = null;
            }
            else {
                $usrowingId = $newResult["member_id"];
            }
        } else {
            $usrowingId = $result["member_id"];
        }
        return $usrowingId;
    }

    public function getAllConfirmedCredentials()
    {
        return (new DB_Usrowing_Credentials())->getAllConfirmedCredentials();
    }

}