--
-- Table structure for table `groups_join_requests`
--

CREATE TABLE IF NOT EXISTS `groups_join_requests` (
                                                      `group_id` int(11) NOT NULL,
                                                      `whitelabel_id` int(11) NOT NULL,
                                                      `custid` int(11) NOT NULL,
                                                      `created_at` int(11) NOT NULL,
                                                      PRIMARY KEY (`group_id`,`whitelabel_id`,`custid`),
                                                      KEY `fk_group_whitelabel_id_request` (`whitelabel_id`),
                                                      KEY `fk_custid_groups_request` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups_join_requests`
--
ALTER TABLE `groups_join_requests`
    ADD CONSTRAINT `fk_custid_groups_request` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_group_id_request` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_group_whitelabel_id_request` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`) ON DELETE CASCADE;
COMMIT;
