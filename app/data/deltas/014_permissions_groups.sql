--
-- Table structure for table `permissions_groups`
--

CREATE TABLE IF NOT EXISTS `permissions_groups` (
                                                    `group_id` int(11) NOT NULL,
                                                    `permission_id` int(11) NOT NULL,
                                                    `created_at` int(11) NOT NULL,
                                                    PRIMARY KEY (`group_id`,`permission_id`),
                                                    KEY `fk_permission_id_permission_groups` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permissions_groups`
--
ALTER TABLE `permissions_groups`
    ADD CONSTRAINT `fk_group_id_permission_groups` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_permission_id_permission_groups` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`);
COMMIT;
