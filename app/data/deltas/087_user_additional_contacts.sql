--
-- Table structure for table `user_additional_contacts`
--

CREATE TABLE IF NOT EXISTS`user_additional_contacts` (
                                                         `custid` int(11) NOT NULL,
                                                         `type` varchar(10) NOT NULL,
                                                         `contact` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_additional_contacts`
--
ALTER TABLE `user_additional_contacts`
    ADD PRIMARY KEY (`custid`,`type`,`contact`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_additional_contacts`
--
ALTER TABLE `user_additional_contacts`
    ADD CONSTRAINT `fk_custid_user_additional_contacts` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
