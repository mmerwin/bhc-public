<?php


class DB_Regattacentral_Orgs extends DataModel
{
    protected $schema = "regattacentral_orgs";

    public function getAllOrgs()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

    public function getUnclaimedOrgs($country = null)
    {
        if(!is_null($country)){
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE country = ? AND claimed = 'no' AND name NOT LIKE '%test%'",$country)->fetchAll();
        } else{
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE claimed = 'no'")->fetchAll();
        }
    }

    public function claimOrg($orgid)
    {
        return $this->DB->query("UPDATE {$this->schema} SET claimed = 'yes' WHERE organizationId = ? LIMIT 1", $orgid);
    }
  
    public function checkIfOrgClaimed($rc_orgid)
    {
      $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE organizationId = ? LIMIT 1", $rc_orgid)->fetchArray();
      if(is_null($result['claimed'])){
        return array("Status" => "Error","Message" => "Invalid rc_orgid");
      } else{
        if($result['claimed'] == 'yes'){
          return array("Status" => "Success","Message" => "Claimed");
        } else{
          if($result['claimed'] == 'no'){
            return array("Status" => "Successs","Message" => "Unclaimed");
          }
        }
      }
    }
  
    public function getRcOrgDetails($rc_orgid)
    {
      return $this->DB->query("SELECT * FROM {$this->schema} WHERE organizationId = ? LIMIT 1", $rc_orgid)->fetchArray();
    }

}