--
-- Table structure for table `whitelabel_connect_accounts`
--

CREATE TABLE IF NOT EXISTS `whitelabel_connect_accounts` (
                                                             `whitelabel_id` int(11) NOT NULL,
                                                             `env` varchar(10) NOT NULL,
                                                             `account_id` varchar(100) NOT NULL,
                                                             `charges_enabled` varchar(10) NOT NULL DEFAULT 'false',
                                                             `details_submitted` varchar(10) NOT NULL DEFAULT 'false',
                                                             `payouts_enabled` varchar(10) NOT NULL DEFAULT 'false',
                                                             PRIMARY KEY (`whitelabel_id`,`env`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
