<?php


class DB_Equipment_Reservations_Oars extends DataModel
{
    protected $schema = "equipment_reservations_oars";

    public function addReservation($reservationId, $whitelabel_id, $oarId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (reservation_id, whitelabel_id, oar_id) VALUES(?, ?, ?)", array($reservationId, $whitelabel_id, $oarId));
    }

    public function getReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ero.reservation_id, eo.oar_id, eos.whitelabel_id, eo.oar_set_id, eo.descriptor, eo.side, eo.status, eo.created_at, eos.name as set_name, eos.descr as set_descr, eos.type, eos.manufacturer, eos.model
                                        FROM equipment_reservations_oars ero
                                        LEFT JOIN equipment_oars eo
                                        ON eo.oar_id = ero.oar_id
                                        LEFT JOIN equipment_oars_sets eos
                                        ON eos.oar_set_id = eo.oar_set_id
                                        WHERE ero.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getOarsReservedByTime($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT eo.*, eos.* 
                                        FROM equipment_oars eo
                                        LEFT JOIN equipment_oars_sets eos
                                        ON eos.oar_set_id = eo.oar_set_id
                                        WHERE oar_id IN(
                                        SELECT ero.oar_id
                                        FROM equipment_reservations er
                                        LEFT JOIN equipment_reservations_oars ero
                                        ON ero.reservation_id = er.reservation_id
                                        WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ?)
                                        AND whitelabel_id = ?",
            array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id, $whitelabel_id))->fetchAll();
    }

    public function getAvailableOars($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT eo.*, eos.* 
                                        FROM equipment_oars eo
                                        LEFT JOIN equipment_oars_sets eos
                                        ON eos.oar_set_id = eo.oar_set_id
                                        WHERE oar_id NOT IN(
                                        SELECT ero.oar_id
                                        FROM equipment_reservations er
                                        LEFT JOIN equipment_reservations_oars ero
                                        ON ero.reservation_id = er.reservation_id
                                        WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ? AND oar_id IS NOT NULL)
                                        AND whitelabel_id = ?",
                                        array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id, $whitelabel_id))->fetchAll();
    }

}
