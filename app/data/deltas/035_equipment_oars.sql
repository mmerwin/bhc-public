--
-- Table structure for table `equipment_oars`
--

CREATE TABLE IF NOT EXISTS `equipment_oars` (
                                                `oar_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `oar_set_id` int(11) NOT NULL,
                                                `side` varchar(10) NOT NULL,
                                                `descriptor` varchar(100) NOT NULL,
                                                `status` varchar(20) NOT NULL DEFAULT 'Available',
                                                `created_at` int(11) NOT NULL,
                                                PRIMARY KEY (`oar_id`),
                                                KEY `fk_oar_set_id` (`oar_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_oars`
--
ALTER TABLE `equipment_oars`
    ADD CONSTRAINT `fk_oar_set_id` FOREIGN KEY (`oar_set_id`) REFERENCES `equipment_oars_sets` (`oar_set_id`) ON DELETE CASCADE;
COMMIT;
