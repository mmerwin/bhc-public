<?php


class DB_Integration_Keys extends DataModel
{
    protected $schema = "integration_keys";

    public function getData($service, $returnMethod)
    {
        $data = $this->DB->query("SELECT * FROM {$this->schema} WHERE service = ?", $service)->fetchArray();
        if($returnMethod == "array")
        {
            return json_decode($data['data'], true);
        } else if($returnMethod == 'json')
        {
            return $data['data'];
        }
    }
}