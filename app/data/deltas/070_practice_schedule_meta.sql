--
-- Table structure for table `practice_schedule_meta`
--

CREATE TABLE IF NOT EXISTS `practice_schedule_meta` (
                                                        `practice_id` int(11) NOT NULL AUTO_INCREMENT,
                                                        `whitelabel_id` int(11) NOT NULL,
                                                        `series_id` int(11) NOT NULL,
                                                        `name` varchar(50) DEFAULT NULL,
                                                        `location_id` int(11) DEFAULT NULL,
                                                        `start_time` int(11) NOT NULL,
                                                        `end_time` int(11) NOT NULL,
                                                        `max_attendees` int(11) DEFAULT NULL,
                                                        `reservation_id` int(11) NOT NULL,
                                                        `lineups_set` varchar(5) NOT NULL DEFAULT 'No',
                                                        `public` varchar(5) NOT NULL DEFAULT 'No',
                                                        `visiting_rowers` varchar(5) DEFAULT NULL,
                                                        `custid` int(11) NOT NULL,
                                                        `lineup_focus` INT NOT NULL DEFAULT '0',
                                                        PRIMARY KEY (`practice_id`),
                                                        KEY `fk_custid_practice_schedule_meta` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `practice_schedule_meta`
--
ALTER TABLE `practice_schedule_meta`
    ADD CONSTRAINT `fk_custid_practice_schedule_meta` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`);
