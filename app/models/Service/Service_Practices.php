<?php


class Service_Practices
{
    private $whitelabel_series;
    private $whitelabel_locations;
    private $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id        = $whitelabel_id;
        $this->whitelabel_series    = (new DB_Practice_Series_Meta())->getAllWhitelabelSeries($whitelabel_id);
        $this->whitelabel_locations = (new DB_Practice_Locations())->getWhitelabelLocations($whitelabel_id);
    }

    private function searchForPractices($whitelabel_id, $limit, $upcoming, $attending, $since, $custid, $eligible)
    {
        // get raw practices for the whitelabel matching the upcoming, since, custid, and eligible search fields
        if (!is_null($custid) && $eligible) {
            $rawPractices     = (new DB_Practice_Schedule_Meta())->getPracticeScheduleForUser($whitelabel_id, $custid, $upcoming, $since);
        } else {
            if ($upcoming) {
                $rawPractices = (new DB_Practice_Schedule_Meta())->getAllWhitelabelPractices($whitelabel_id, "ASC", $since, $upcoming);
            } else {
                $rawPractices = (new DB_Practice_Schedule_Meta())->getAllWhitelabelPractices($whitelabel_id, "DESC", $since, $upcoming);
            }
        }

        // remove only select practices the user is attending
        if ($attending) {
            $attendingPractices = (new DB_Practice_Attendance())->getAllByCustid($whitelabel_id, $custid);
            $attendanceArray = [];
            foreach ($attendingPractices as $row) {
                if ($row["status"] == "Attending")
                $attendanceArray[] = $row["practice_id"];
            }
            // add practices the user is attending to new array
            $practices = [];
            foreach ($rawPractices as $row) {
                if (in_array($row['practice_id'], $attendanceArray)) {
                    $practices[] = $row;
                }
            }
        } else {
            $practices = $rawPractices;
        }

        // trim length of practices down
        $practicesTrimmed = [];
        for ($i=0; $i< $limit; $i++) {
            if (!isset($practices[$i])) {
                break;
            }
            $practicesTrimmed[] = $practices[$i];
        }

        return $practicesTrimmed;
    }

    public function getPractices($whitelabel_id, $limit, $upcoming, $attending, $since, $custid, $eligible) {
        $rawPractices = $this->searchForPractices($whitelabel_id, $limit, $upcoming, $attending, $since, $custid, $eligible);

        $formattedPractices = [];
        foreach ($rawPractices as $practice) {
            $formattedPractices[] = $this->formatPractice($practice, $custid);
        }
        return $formattedPractices;
    }

    private function formatPractice($rawPractice, $custid = null)
    {
        if ($rawPractice) {
            $attendeeArray = (new DB_Practice_Attendance())->getAttendanceByPracticeId($rawPractice["whitelabel_id"],$rawPractice["practice_id"]);
            if (is_null($custid)) {
                $custid = $_SESSION["custid"];
            }

            return [
                "whitelabel_id"             => $rawPractice["whitelabel_id"],
                "practice_id"               => $rawPractice["practice_id"],
                "series_id"                 => $rawPractice["series_id"],
                "name"                      => $this->validateMetaField($rawPractice, "name"),
                "location"                  => $this->getLocationById($this->validateMetaField($rawPractice, "location_id")),
                "start_time"                => $rawPractice["start_time"],
                "end_time"                  => $rawPractice["end_time"],
                "formatted_times"           => $this->formatStartEnd($rawPractice["start_time"], $rawPractice["end_time"]),
                "max_attendees"             => $this->validateMetaField($rawPractice, "max_attendees"),
                "visiting_rowers_allowed"   => $this->validateMetaField($rawPractice, "visiting_rowers"),
                "custid"                    => $rawPractice["custid"],
                "reservation_id"            => $rawPractice["reservation_id"],
                "lineups_set"               => $rawPractice["lineups_set"],
                "session_plan"              => $this->getSessionplan($rawPractice["practice_id"]),
                "is_attending"              => $this->isAttending($attendeeArray, $custid),
                "attendee_count"            => $this->getAttendeeCount($attendeeArray),
                "attendance"                => $attendeeArray,
                "possible_attendees"        => $this->getPossibleAttendees($rawPractice["practice_id"]),
                "groups"                    => $this->getGroupsByPracticeId($rawPractice["whitelabel_id"], $rawPractice["practice_id"], $rawPractice["start_time"]),
                "equipment"                 => (new Service_Equipment_Reservations($rawPractice["whitelabel_id"], $rawPractice["custid"]))->getReservationById($rawPractice["reservation_id"]),
                "lineup_focus"              =>  $rawPractice["lineup_focus"]
            ];
        } else {
            return [];
        }
    }

    private function formatStartEnd($startTime, $endTime)
    {
        return [
            "start_unix"    => $startTime,
            "start_date"    => date("M j, Y", $startTime),
            "start_time"    => date("g:i A", $startTime),
            "start_24"      => date("H:i:s", $startTime),
            "end_unix"      => $endTime,
            "end_date"      => date("M j, Y", $endTime),
            "end_time"      => date("g:i A", $endTime),
            "end_24"        => date("H:i:s", $endTime),
            "time_range"    => date("D g:i - ",$startTime).date("g:i A", $endTime),
            "calendar_date" => date("Y-m-d", $startTime),
            "ios"           => date("D M j | g:i - ",$startTime).date("g:i A", $endTime)
        ];
    }

    private function formatBoats($rawEquipment)
    {
        $rawBoats       = (new DB_Equipment_boats())->getAllBoats($this->whitelabel_id);
        $finishedBoats  = [];
        foreach ($rawEquipment as $equip) {
            foreach ($rawBoats as $boat) {
                if ($boat["boat_id"] == $equip["equipment_id"] && $equip["type"] == "boat") {
                    $finishedBoats[] = [
                        "boat_id"       => $boat["boat_id"],
                        "boat_name"     => $boat["boat_name"],
                        "hull_type"     => intval($boat["hull_type"]),
                        "coxed"         => $boat["coxed"],
                        "manufacturer"  => $boat["manufacturer"],
                        "color"         => $boat["color"],
                        "status"        => $boat["status"],
                        "min_weight"    => $boat["min_weight"],
                        "max_weight"    => $boat["max_weight"]
                    ];
                }
            }
        }
        return $finishedBoats;
    }

    private function formatOars($rawEquipment)
    {
        $rawOars      = (new DB_Equipment_Oars_Sets())->getAllSets($this->whitelabel_id);
        $finishedOars = [];
        foreach ($rawEquipment as $equip) {
            foreach ($rawOars as $oars) {
                if ($oars["oar_set_id"] == $equip["equipment_id"] && $equip["type"] == "oars") {
                    $finishedOars[] = [
                        "oar_set_id"        => $oars["oar_set_id"],
                        "name"              => $oars["name"],
                        "type"              => $oars["type"],
                        "manufacturer"      => $oars["manufacturer"],
                        "model"             => $oars["model"],
                        "descr"             => $oars["descr"],
                        "port_count"        => $oars["port_count"],
                        "starboard_count"   => $oars["starboard_count"]
                    ];
                }
            }
        }
        return $finishedOars;
    }

    private function getGroupsByPracticeId($whitelabel_id, $practiceId, $practiceStartTime)
    {
        $rawGroups       = (new DB_Practice_Groups())->getGroupsByPractice($whitelabel_id, $practiceId);
        $formattedGroups = [];
        foreach ($rawGroups as $group) {
            $formattedGroups[] = [
                "group_id"                  => $group["group_id"],
                "title"                     => $group["title"],
                "descr"                     => $group["descr"],
                "group_max"                 => $group["group_max"],
                "group_self_join"           => $group["group_self_join"],
                "declare_attendance_start"  => ($practiceStartTime - $group["attendance_start"]),
                "declare_attendance_end"    => ($practiceStartTime - $group["attendance_end"]),
            ];
        }
        return $formattedGroups;
    }

    private function validateMetaField($practiceRow, $field)
    {
        if ($practiceRow["series_id"] == 0) {
            return $practiceRow[$field];
        } else {
            // check if we need the series
            if (is_null($practiceRow[$field])) {
                // find series
                foreach ($this->whitelabel_series as $series) {
                    if ($series['series_id'] == $practiceRow["series_id"]) {
                        return $series[$field];
                    }
                }
            } else {
                return $practiceRow[$field];
            }
        }
    }

    public function getAttendeeCount($attendeeArray)
    {
        $attendeeCount = 0;
        foreach ($attendeeArray as $user) {
            if ($user["attendance_plan"] == "Attending") {
                $attendeeCount++;
            }
        }

        return $attendeeCount;
    }

    public function isAttending($attendeeArray, $custid)
    {
        if (!$custid) {
            return false;
        }
        foreach ($attendeeArray as $user) {
            if ($user["custid"] == $custid && $user["attendance_plan"] == "Attending") {
                return true;
            }
        }
        return false;
    }

    public function getLocationById($locationId)
    {
        foreach ($this->whitelabel_locations as $location) {
            if ($location["location_id"] == $locationId) {
                return [
                    "whitelabel_id" => $location["whitelabel_id"],
                    "location_id"   => $location["location_id"],
                    "name"          => $location["name"],
                    "address"       => $location["address"],
                    "city"          => $location["city"],
                    "state"         => $location["state"],
                    "zipcode"       => $location["zipcode"],
                    "entry_fee"     => "$".$location["fee"],
                    "notes"         => $location["notes"],
                    "active"        => $location["active"]
                ];
            }
        }
    }

    public function getPossibleAttendees($practiceId)
    {
        return (new DB_Practice_Attendance())->getPossibleAttendees($this->whitelabel_id, $practiceId);
    }

    public function getPracticeById($whitelabel_id, $practiceId)
    {
        return $this->formatPractice((new DB_Practice_Schedule_Meta())->getPracticeById($whitelabel_id, $practiceId));
    }

    public function createPractice($whitelabel_id, $seriesId, $name, $locationId, $startTime, $endTime, $maxAttendees, $public, $visitingRowers, $custid, $return = false)
    {
        if (!is_null($seriesId) && $seriesId > 0 && $return != true) {
            $returnId = false;
        } else {
            $returnId = true;
            $seriesId = 0;
        }

        if (is_null($public) || $public == "Yes") {
            $public = "Yes";
        } else {
            $public = "No";
        }
        // create equipment reservation
        $reservation = (new Service_Equipment_Reservations($whitelabel_id, $custid))->newReservation($startTime, $endTime, "practice");
        if ($reservation["Status"] == "Error") {
            die(json_encode($reservation));
        } else {
            $reservationId = $reservation["reservation_id"];
        }

        return (new DB_Practice_Schedule_Meta())->createPractice($whitelabel_id, $seriesId, $custid, $name, $locationId, $startTime, $endTime, $maxAttendees, $public, $visitingRowers, $reservationId, $returnId);
    }

    public function updatePracticeMeta($practiceId, $name, $locationId, $startTime, $endTime, $maxAttendees, $lineupsSet, $visitingRowers)
    {
        $psm = new DB_Practice_Schedule_Meta();
        if (!is_null($name)) {
            $psm->updateName($this->whitelabel_id, $practiceId, $name);
        }

        if (!is_null($locationId)) {
            $psm->updateLocation($this->whitelabel_id, $practiceId, (new PracticeLocations($this->whitelabel_id))->checkIfAffiliated($locationId));
        }

        if (!is_null($startTime)) {
            $psm->updateStartTime($this->whitelabel_id, $practiceId, $startTime);
        }

        if (!is_null($endTime)) {
            $psm->updateEndTime($this->whitelabel_id, $practiceId, $endTime);
        }

        if (!is_null($maxAttendees)) {
            $psm->updateMaxAttendees($this->whitelabel_id, $practiceId, $maxAttendees);
        }

        if (!is_null($lineupsSet) && strtoupper($lineupsSet) != 'NO') {
            $psm->markLineupsSet($this->whitelabel_id, $practiceId);
        }

        if (!is_null($visitingRowers)) {
            $psm->updateVisitingRowers($this->whitelabel_id, $practiceId, $visitingRowers);
        }
    }

    public function updateSessionplan($practiceId, $text)
    {
        // check that practice_id belongs to whitelabel
        $practice = $this->getPracticeById($this->whitelabel_id, $practiceId);
        if ($practice["practice_id"] == $practiceId) {
            (new DB_Practice_Sessionplans())->updateSessionplan($this->whitelabel_id, $practiceId, $text);
        }
    }

    public function getSessionplan($practiceId)
    {
        return (new DB_Practice_Sessionplans())->getSessionplan($this->whitelabel_id, $practiceId);
    }

    public function addGroup($practiceId, $groupId, $attendanceStart, $attendanceEnd)
    {
        (new DB_Practice_Groups())->addGroupToPractice($this->whitelabel_id, $practiceId, $groupId, $attendanceStart, $attendanceEnd);
    }

    public function removeGroup($practiceId, $groupId)
    {
        (new DB_Practice_Groups())->removeGroup($this->whitelabel_id, $practiceId, $groupId);
    }

    public function updateGroup($practiceId, $groupId, $attendanceStart, $attendanceEnd)
    {
        (new DB_Practice_Groups())->updateGroup($this->whitelabel_id, $practiceId, $groupId, $attendanceStart, $attendanceEnd);
    }

    public function setAttendance($practiceId, $custid, $status)
    {
        // check if practice belongs to whitelabel
        $practice = $this->getPracticeById($this->whitelabel_id, $practiceId);
        if ($practice["practice_id"] == $practiceId) {
            switch (strtolower($status)) {
                case "attending":
                    $attendance = "Attending";
                    break;
                case "not attending":
                default:
                    $attendance = "Not Attending";
            }
            (new DB_Practice_Attendance())->updateAttendance($this->whitelabel_id, $practiceId, $custid, $attendance);

            //remove from lineup
            (new Service_Lineups())->removeUserFromLineup($this->whitelabel_id, $practiceId, $custid);
        }
    }

    public function updateLineupFocus($practiceId, $focusId)
    {
        (new DB_Practice_Schedule_Meta())->updateLineupFocus($this->whitelabel_id, $practiceId, $focusId);
    }
}