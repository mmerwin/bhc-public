<?php

class Service_ApiTokenValidation
{
    public static function validateToken($token_hash, $update = true)
    {
        $DB_Api_Tokens = new DB_Api_Tokens();
        $token         = $DB_Api_Tokens->checkToken($token_hash);
        if ($update) {
            self::updateLastUsed($token_hash);
        }

        return $token;
    }

    public static function validateAdminToken($token_hash, $update = true)
    {
        $DB_Admin_Api_Tokens = new DB_Admin_Api_Tokens();
        $token               = $DB_Admin_Api_Tokens->checkToken($token_hash);
        if ($update) {
            self::updateLastUsed($token_hash);
        }

        return $token;
    }

    public static function newToken($custid, $type='api', $descr='')
    {
        if ($type == "admin") {
            $DB_Admin_Api_Tokens = new DB_Admin_Api_Tokens();
            return $DB_Admin_Api_Tokens->createNewToken($custid, "admin", "app-admin token");
        }
        $DB_Api_Tokens = new DB_Api_Tokens();
        return $DB_Api_Tokens->createNewToken($custid, $type, $descr);
    }

    public static function getCustidFromToken($token_hash)
    {
        $DB_Api_Tokens = new DB_Api_Tokens();
        $details       = $DB_Api_Tokens->checkToken($token_hash);
        return $details['custid'];
    }

    public function timeToExpire($token_hash)
    {
        $DB_Api_Tokens = new DB_Api_Tokens();
        $details       = $DB_Api_Tokens->checkToken($token_hash);

        return array("time_remaining" => ($details['expires'] - time()), "expiration_date" => $details['expires']);
    }

    public static function updateLastUsed($token_hash)
    {
        (new DB_Api_Tokens())->setLastUsed($token_hash);
    }

    public static function deleteKey($token_hash)
    {
        return (new DB_Api_Tokens())->deleteToken($token_hash);
    }

    public static function getAllPublicTokens($custid)
    {
        return (new DB_Api_Tokens())->getAllPublicTokens($custid);
    }

    public static function refreshExpirationDate($token_hash)
    {
        return (new DB_Api_Tokens())->refreshExpirationDate($token_hash);
    }

}