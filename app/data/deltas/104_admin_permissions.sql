--
-- Table structure for table `admin_permissions`
--

CREATE TABLE IF NOT EXISTS `admin_permissions` (
                                                   `permission_id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `name` varchar(100) NOT NULL,
                                                   `description` varchar(500) NOT NULL,
                                                   PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
