<?php

class DB_Logins extends DataModel
{
    protected $schema = "logins";

    public function changePassword($loginid, $newPassword)
    {
        $this->DB->query("UPDATE {$this->schema} SET password = ? WHERE loginid = ? LIMIT 1", $this->hashPassword($newPassword), $loginid);
    }

    public function changeEmailByLoginid($loginid, $newEmail)
    {
        $this->DB->query("UPDATE {$this->schema} SET email = ? WHERE loginid = ? LIMIT 1", $newEmail, $loginid);
    }

    public function changeEmailByCustid($custid, $newEmail)
    {
        $this->DB->query("UPDATE {$this->schema} SET email = ? WHERE custid = ? LIMIT 1", $newEmail, $custid);
    }

    public function findLoginByCustid($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? LIMIT 1",$custid)->fetchArray();
    }

    public function findLoginByEmailPassword($email, $password)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE email = ? and password = ? LIMIT 1", array($email, $this->hashPassword($password)))->fetchArray();
    }

    private function hashPassword($password)
    {
        return md5($password);
    }

    public function checkIfEmailExists($email)
    {
        return $this->DB->query("SELECT loginid, custid, email FROM {$this->schema} WHERE email = ? LIMIT 1", array($email))->fetchArray();
    }

    public function getEmailByCustid($custid)
    {
        return $this->DB->query("SELECT l.loginid, l.custid, l.email, u.fname, u.lname FROM logins l 
                                        LEFT JOIN users u ON u.custid = ? 
                                        WHERE l.custid = ? LIMIT 1", array($custid, $custid))->fetchArray();
    }

    public function addLogin($custid, $email, $password)
    {
        $hashedPassword = $this->hashPassword($password);
        $this->DB->query("INSERT INTO {$this->schema} (custid, email, password) VALUES (?, ?, ?)", array($custid, $email, $hashedPassword));
    }

    public function addResetToken($email)
    {
        $token = $this->hashPassword(rand(0,10000)."-bhconnect".$email.time().rand(1,50000));
        $resetTime = time();
        $this->DB->query("UPDATE {$this->schema} SET reset_token = ?, reset_time = ? WHERE email = ? LIMIT 1 ", array($token, $resetTime, $email));

        //find verified email
        return $this->DB->query("SELECT l.loginid, l.custid, l.email, u.fname, u.lname, l.reset_token, l.reset_time
                                        FROM logins l
                                        LEFT JOIN users u
                                        ON u.custid = l.custid
                                        WHERE reset_token = ? AND reset_time = ? LIMIT 1", array($token, $resetTime))->fetchArray();

    }

    public function resetPasswordFromToken($resetToken, $resetTime, $newPassword)
    {
        $hashedPassword = $this->hashPassword($newPassword);
        $this->DB->query("UPDATE {$this->schema} SET password = ?, reset_token = '', reset_time = 0 WHERE reset_token = ? AND reset_time = ? AND reset_time != 0 LIMIT 1", array($hashedPassword, $resetToken, $resetTime));
    }

}