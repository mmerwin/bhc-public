
--
-- Table structure for table `whitelabel_digest_history`
--

CREATE TABLE IF NOT EXISTS `whitelabel_digest_history` (
                                                           `whitelabel_id` int(11) NOT NULL,
                                                           `month` int(11) NOT NULL,
                                                           `calc_timestamp` int(11) NOT NULL,
                                                           `total_users` int(11) NOT NULL,
                                                           `total_payments_received` int(11) NOT NULL,
                                                           `anticipated_revenue` int(11) NOT NULL,
                                                           `delinquent_users` int(11) NOT NULL,
                                                           `athletes_attended_practice` int(11) NOT NULL,
                                                           `messages_sent` int(11) NOT NULL,
                                                           `rewards_points_earned` int(11) NOT NULL,
                                                           PRIMARY KEY (`whitelabel_id`,`month`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `whitelabel_digest_history`
--
ALTER TABLE `whitelabel_digest_history`
    ADD CONSTRAINT `fk_whitelabel_id_whitelabel_digest_history` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
