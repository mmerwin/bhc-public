<?php


class DB_Reward_Redemptions extends DataModel
{
    protected $schema = "reward_redemptions";

    public function getWhitelabelRedemptions($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function sumAllPointsInTable()
    {
        return $this->DB->query("SELECT sum(points) as redeemed FROM {$this->schema}")->fetchArray();
    }
}