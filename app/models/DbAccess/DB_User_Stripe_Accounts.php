<?php


class DB_User_Stripe_Accounts extends DataModel
{
    protected $schema = "user_stripe_accounts";

    public function  getCustomer($custid, $env)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? AND env = ? LIMIT 1", array($custid, $env))->fetchArray();
    }

    public function createCustomer($custid, $env, $stripeCustomerId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (custid, env, stripe_customer_id, preferred_payment_method, delinquent) VALUES (?, ?, ?, '', 'No')", array($custid, $env, $stripeCustomerId));
    }

    public function updatePreferredPaymentMethod($custid, $env, $preferredPaymentMethod)
    {
        $this->DB->query("UPDATE {$this->schema} SET preferred_payment_method = ? WHERE custid = ? AND env = ? LIMIT 1", array($preferredPaymentMethod, $custid, $env));
    }

    public function setAsDelinquent($custid, $env)
    {
      $this->DB->query("UPDATE {$this->schema} SET delinquent = 'Yes' WHERE custid = ? AND env = ? LIMIT 1", array($custid, $env));
    }

    public function setAsNotDelinquent($custid, $env)
    {
        $this->DB->query("UPDATE {$this->schema} SET delinquent = 'No' WHERE custid = ? AND env = ? LIMIT 1", array($custid, $env));
    }

}