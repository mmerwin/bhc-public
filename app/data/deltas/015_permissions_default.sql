-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application
--
-- Table structure for table `permissions_default`
--

CREATE TABLE IF NOT EXISTS `permissions_default` (
                                                     `permission_default_id` int(11) NOT NULL AUTO_INCREMENT,
                                                     `default_group_id` int(11) NOT NULL,
                                                     `permission_id` int(11) NOT NULL,
                                                     PRIMARY KEY (`permission_default_id`),
                                                     KEY `fk_group_id_permission_default` (`default_group_id`),
                                                     KEY `fk_permission_id_default` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `permissions_default`
--

TRUNCATE TABLE `permissions_default`;
--
-- Dumping data for table `permissions_default`
--

INSERT IGNORE INTO `permissions_default` (`permission_default_id`, `default_group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 4, 1),
(8, 1, 4),
(9, 1, 5),
(10, 1, 6),
(11, 1, 7),
(12, 1, 8),
(13, 1, 9),
(14, 1, 10),
(15, 1, 11),
(16, 1, 12),
(17, 1, 13),
(18, 1, 14),
(19, 1, 15),
(21, 1, 17),
(22, 1, 18),
(23, 1, 19),
(24, 2, 19),
(25, 2, 5),
(26, 2, 6),
(27, 2, 7),
(28, 2, 18),
(29, 1, 20),
(30, 1, 21);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permissions_default`
--
ALTER TABLE `permissions_default`
    ADD CONSTRAINT `fk_group_id_permission_default` FOREIGN KEY (`default_group_id`) REFERENCES `groups_default` (`default_group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_permission_id_default` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;
