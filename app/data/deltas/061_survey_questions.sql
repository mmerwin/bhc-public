--
-- Table structure for table `surveys_questions`
--

CREATE TABLE IF NOT EXISTS `surveys_questions` (
                                                   `question_id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `survey_id` int(11) NOT NULL,
                                                   `position` int(11) NOT NULL,
                                                   `descr` longtext NOT NULL,
                                                   `element_type` varchar(50) NOT NULL,
                                                   PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
