--
-- Table structure for table `endpoint_log_zapier`
--

CREATE TABLE IF NOT EXISTS `endpoint_log_zapier` (
                                                     `logid` int(11) NOT NULL AUTO_INCREMENT,
                                                     `controller` varchar(100) NOT NULL,
                                                     `method` varchar(100) NOT NULL,
                                                     `timestamp` int(11) NOT NULL,
                                                     `ip` varchar(40) NOT NULL,
                                                     `custid` int(11) NOT NULL DEFAULT '0',
                                                     PRIMARY KEY (`logid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
