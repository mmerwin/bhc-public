--
-- Table structure for table `admin_logins`
--

CREATE TABLE IF NOT EXISTS `admin_logins` (
                                              `adminid` int(11) NOT NULL AUTO_INCREMENT,
                                              `fname` varchar(50) NOT NULL,
                                              `lname` varchar(50) NOT NULL,
                                              `email` varchar(250) NOT NULL,
                                              `username` varchar(100) NOT NULL,
                                              `password` varchar(100) NOT NULL,
                                              `account_expires` int(11) NOT NULL,
                                              `login_redirect` text NOT NULL,
                                              PRIMARY KEY (`adminid`),
                                              UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
