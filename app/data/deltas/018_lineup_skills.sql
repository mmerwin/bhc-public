-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application
--
-- Table structure for table `lineup_skills`
--

CREATE TABLE IF NOT EXISTS `lineup_skills` (
                                               `skill_id` int(11) NOT NULL AUTO_INCREMENT,
                                               `title` varchar(30) NOT NULL,
                                               PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `lineup_skills`
--

TRUNCATE TABLE `lineup_skills`;
--
-- Dumping data for table `lineup_skills`
--

INSERT IGNORE INTO `lineup_skills` (`skill_id`, `title`) VALUES
(1, 'Never tried it'),
(2, 'Done it a few times'),
(3, 'Somewhat decent'),
(4, 'I do this well'),
(5, 'I am a master');
COMMIT;
