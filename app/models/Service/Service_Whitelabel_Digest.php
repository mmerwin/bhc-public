<?php


class Service_Whitelabel_Digest
{

    public function generateReport($whitelabel_id, $month = null)
    {
        if (!$month) {
            $month = date("Ym", time());
        }

        $startOfMonthTime           = strtotime($month."01");
        $nextMonthStartTime         = strtotime(date('Y-m-d', strtotime('first day of next month', $startOfMonthTime)));
        $nextMonthEndTime           = strtotime(date('Y-m-d', strtotime('last day of next month', $startOfMonthTime)));
        $totalUsers                 = count(Service_Affiliations::getAllAffiliatedUsers($whitelabel_id));
        $totalPaymentsReceived      = count((new DB_User_Payment_Credits())->paymentsInRange($whitelabel_id, $startOfMonthTime, $nextMonthStartTime));
        $delinquentUsers            = count((new DB_User_Payment_Schedule())->getPastDueAccounts($whitelabel_id));
        $athletesAttendedPractice   = count((new DB_Practice_Attendance())->getWhitelabelAttendeesByDateRange($whitelabel_id, $startOfMonthTime, $nextMonthStartTime));
        $nextMonthCharges           = (new DB_User_Payment_Schedule())->upcomingChargesByDateRange($whitelabel_id, $nextMonthStartTime, $nextMonthEndTime);
        $anticipatedRevenue         = 0;
        $messagesSent               = count((new DB_External_Messages())->getSentMessageIds($whitelabel_id, $startOfMonthTime, $nextMonthStartTime));
        $allRewardsPointsHistory    = (new DB_Reward_History())->getPointsEarnedBetweenDates($whitelabel_id, $startOfMonthTime, $nextMonthStartTime);
        $totalRewardsPointsEarned   = 0;
        // calculate expected revenue based on charges
        foreach ($nextMonthCharges as $charge) {
            $anticipatedRevenue += $charge["amount"];
        }

        // calculate earned points over past month
        foreach ($allRewardsPointsHistory as $points) {
            $totalRewardsPointsEarned += $points["points_earned"];
        }
        (new DB_Whitelabel_Digest_History())->addReport($whitelabel_id, $month, $totalUsers, $totalPaymentsReceived, $anticipatedRevenue, $delinquentUsers, $athletesAttendedPractice, $messagesSent, $totalRewardsPointsEarned);
    }

    public function createReportsForAllWhitelabels()
    {
        // get all whitelabels
        $whitelabels = Service_Whitelabel::getAllWhitelabels();
        foreach ($whitelabels as $row) {
            $this->generateReport($row["whitelabel_id"]);
        }
    }

    public function backfillWhitelabelReport($whitelabel_id, $month)
    {
        $this->generateReport($whitelabel_id, $month);
    }

    public function backfillMonth($month)
    {
        // get all whitelabels
        $whitelabels = Service_Whitelabel::getAllWhitelabels();
        foreach ($whitelabels as $row) {
            $this->backfillWhitelabelReport($row["whitelabel_id"], $month);
        }
    }

}