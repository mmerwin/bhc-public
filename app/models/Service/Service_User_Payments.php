<?php
require_once (dirname(__FILE__).'/../../vendor/autoload.php');

use Stripe\StripeClient;

class Service_User_Payments
{
    private $whitelabelMap = null;

    private function setWhitelabelMap()
    {
        $this->whitelabelMap = Service_Whitelabel::getAllWhitelabels();
    }

    private function getWhitelabelName($whitelabel_id)
    {
        if (is_null($this->whitelabelMap)) {
            $this->setWhitelabelMap();
        }

        foreach ($this->whitelabelMap as $row) {
            if ($row["whitelabel_id"] == $whitelabel_id) {
                return $row["name"];
            }
        }
        return "Boathouse Connect";
    }

    private function createCustomer($custid)
    {
        if (!is_null($custid)) {
            // get user
            $user       = (new DB_Users())->getUser($custid);
            $fname      = $user["fname"];
            $lname      = $user["lname"];
            $stripe     = new StripeClient(Service_External_Keys::getToken("stripe-secret"));
            $response   = $stripe->customers->create([
                'description' => $custid." - Boathouse Connect User",
                'metadata'    => [
                    "custid"        => $custid,
                    "type"          => "user",
                    "env"           => Environment::getEnvCode(),
                    "slug"          => Environment::getEnvCode()."-user-".$custid,
                    "fname"         => $fname,
                    "lname"         => $lname
                ],
                "name"        =>  $fname." ".$lname
            ]);

            (new DB_User_Stripe_Accounts())->createCustomer($custid, Environment::getEnvCode(), $response["id"]);

            // create zero-dollar initialization charge
            $this->postCharge(0, $custid, 0, $custid, 0, "Initialization", "initialization", null);
            return (new DB_User_Stripe_Accounts())->getCustomer($custid, Environment::getEnvCode());
        } else {
            return "";
        }
    }

    public function getCustomerAccount($custid)
    {
        // check if custid is already a Stripe customer
        $customer = (new DB_User_Stripe_Accounts())->getCustomer($custid, Environment::getEnvCode());
        if (!isset($customer["stripe_customer_id"])) {
            $customer = $this->createCustomer($custid);
        }
        return $customer;
    }

    public function postCharge($whitelabel_id, $custid, $dueBy, $custidCreator, $amount, $description, $chargeType, $foreignId = null)
    {
        $chargeType = $this->verifyChargeType($chargeType);

        // check that both custidCreator and custid are affiliated with the whitelabel
        $creatorIsAffiliated = Service_Affiliations::checkIfAffiliated($custidCreator, $whitelabel_id);
        $custidIsAffiliated  =  Service_Affiliations::checkIfAffiliated($custid, $whitelabel_id);

        if ($creatorIsAffiliated && $custidIsAffiliated) {
            // check amount is allowed
            if ($amount >= 0) {
                $creatorUser = new User($custidCreator);
                $creator     = $creatorUser->fname." ".$creatorUser->lname;
                (new DB_User_Payment_Schedule())->createCharge($whitelabel_id, $custid, $dueBy, $creator, $amount, $description, $chargeType, $foreignId);
                return array("Status" => "Success", "Message" => "Charge added");
            } else {
                return array("Status" => "Error", "Message" => "Invalid 'amount'. amount must be an integer greater than zero");
            }

        } else {
            return array("Status" => "Error", "Message" => "Not affiliated with whitelabel");
        }
    }

    public function postCredit($whitelabel_id, $custid, $description, $externalId, $postedByCustid, $amountCharged, $settledAmount = 0)
    {
        $user     = new User($postedByCustid);
        $postedBy = $user->fname." ".$user->lname;

        (new DB_User_Payment_Credits())->addCredit($whitelabel_id, $custid, $externalId, $postedBy, $amountCharged, $settledAmount, $description);
    }

    public function getAllPastDueAccounts($whitelabel_id = null)
    {
        return (new DB_User_Payment_Schedule())->getPastDueAccounts($whitelabel_id);
    }

    public function getAccountBalanceByCustid($custid) {
        return (new DB_User_Payment_Schedule())->getBalanceByCustid($custid);
    }

    private function verifyChargeType($chargeType)
    {
        switch ($chargeType) {
            case "single_group":
                return "single_group";
            case "recurring_group":
                return "recurring_group";
            case "donation":
                return "donation";
            case "initialization":
                return "initialization";
            case "single":
            default:
                return "single";
        }
    }

    public function getAllCharges($whitelabel_id, $custid=null)
    {
        if ($custid) {
            return (new DB_User_Payment_Schedule())->getChargesByCustid($custid, $whitelabel_id);
        } else {
            return [];
        }
    }

    public function getPaymentsAndCredits($whitelabel_id, $custid=null)
    {
        if ($custid) {
            return (new DB_User_Payment_Credits())->getCreditsByCustid($custid, $whitelabel_id);
        } else {
            return [];
        }
    }

    public function getUserPaymentCards($custid)
    {
        $stripe                 = new StripeClient(Service_External_Keys::getToken("stripe-secret"));
        $customerAccount        = $this->getCustomerAccount($custid);
        $customerPaymentMethods =  $stripe->customers->allSources(
            $customerAccount["stripe_customer_id"],
            ['object' => 'card',]
        );
        if ($customerAccount["preferred_payment_method"] == '' && isset($customerPaymentMethods["data"][0]["id"])) {
            (new DB_User_Stripe_Accounts())->updatePreferredPaymentMethod($custid, Environment::getEnvCode(), $customerPaymentMethods["data"][0]["id"]);
        }
        return $customerPaymentMethods;
    }

    public function addPaymentMethod($custid, $stripeToken)
    {
        $customerAccount = $this->getCustomerAccount($custid);
        $stripe          = new StripeClient(Service_External_Keys::getToken("stripe-secret"));
        $stripe->customers->createSource(
            $customerAccount["stripe_customer_id"],
            ['source' => $stripeToken]
        );
            return $this->getUserPaymentCards($custid);
    }

    public function removePaymentMethod($custid, $sourceId)
    {
        $customerAccount = $this->getCustomerAccount($custid);
        $stripe          = new StripeClient(Service_External_Keys::getToken("stripe-secret"));
        $stripe->customers->deleteSource(
            $customerAccount["stripe_customer_id"],
            $sourceId,
            []
        );
        if ($customerAccount["preferred_payment_method"] == $sourceId) {
            (new DB_User_Stripe_Accounts())->updatePreferredPaymentMethod($custid, Environment::getEnvCode(), "");
            $this-> getUserPaymentCards($custid);
        }
    }

    public function processPayment($whitelabel_id, $custid, $emailAddress, $amount, $stripeCustomerId, $preferredPaymentMethod, $whitelabelAccountId, $stripeSecretKey = null, $sourceType = "card")
    {
        if (!$stripeSecretKey) {
            $stripeSecretKey = Service_External_Keys::getToken("stripe-secret");
        }
        
        // check if cost recovery is enabled
        if (WhitelabelSettings::getSetting($whitelabel_id, "stripe.feerecovery", "No") == "Yes") {
            $costRecovery = true;
        } else {
            $costRecovery = false;
        }
        
        // calculate application fee
        if ($sourceType == "bank") {
            $applicationFee = round(($amount*0.0125),0);
        } else {
            $fixedFee    = WhitelabelSettings::getSetting($whitelabel_id, "stripe.processingfee.fixed", 50);
            $variableFee = WhitelabelSettings::getSetting($whitelabel_id, "stripe.processingfee.variable", 0.0325);
            
            if ($costRecovery) {
                $chargeAmount = round(($amount+$fixedFee)/(1-$variableFee),0);
            } else {
                $chargeAmount = $amount;
            }
        }
        $applicationFee = round(($chargeAmount*$variableFee)+$fixedFee, 0);
        $settleAmount   = ($chargeAmount - $applicationFee);

        // attempt to collect payment from the provided payment method
        $payment = $this->attemptPayment($whitelabel_id, $custid, $emailAddress, $chargeAmount, $applicationFee, $stripeCustomerId, $preferredPaymentMethod, $stripeSecretKey, $whitelabelAccountId);
        if ($payment) {
            // payment was successful, add to payments and credits table
            (new DB_User_Stripe_Accounts())->setAsNotDelinquent($custid, Environment::getEnvCode());
            return (new DB_User_Payment_Credits())->addCredit($whitelabel_id, $custid, $payment["id"], "BC Payment Service", $amount, $settleAmount, "Payment By User", $payment["receipt_url"]);
        } else {
            // get all payment methods and attempt each one. If they all fail, mark account as delinquent
            $allPaymentOptions = $this->getUserPaymentCards($custid);
            foreach ($allPaymentOptions["data"] as $card) {
                $attemptPayment = $this->attemptPayment($whitelabel_id, $custid, $emailAddress, $chargeAmount, $applicationFee, $stripeCustomerId, $card["id"], $stripeSecretKey, $whitelabelAccountId);
                if ($attemptPayment) {
                    (new DB_User_Payment_Credits())->addCredit($whitelabel_id, $custid, $attemptPayment["id"], "BC Payment Service", $amount, $settleAmount, "Payment By User",$attemptPayment["receipt_url"]);
                    (new DB_User_Stripe_Accounts())->setAsNotDelinquent($custid, Environment::getEnvCode());
                    return (new DB_User_Stripe_Accounts())->updatePreferredPaymentMethod($custid, Environment::getEnvCode(), $card["id"]);
                }
            }
            //all payment options available have failed, mark the account delinquent so the user must input a valid new card
            return (new DB_User_Stripe_Accounts())->setAsDelinquent($custid, Environment::getEnvCode());
        }

    }

    private function attemptPayment($whitelabel_id, $custid, $emailAddress, $amount, $applicationFee, $stripeCustomerId, $paymentSource, $stripeSecretKey, $whitelabelAccountId)
    {
        $stripe = new StripeClient($stripeSecretKey);
        try {
            $charge = $stripe->charges->create([
                'amount'                 => $amount,
                'currency'               => 'usd',
                'application_fee_amount' => $applicationFee,
                'customer'               => $stripeCustomerId,
                'source'                 => $paymentSource,
                'description'            => 'Payment to '.$this->getWhitelabelName($whitelabel_id),
                'receipt_email'          => $emailAddress,
                'metadata'               => [
                    "custid"        => $custid,
                    "whitelabel_id" => $whitelabel_id,
                    "env"           => Environment::getEnvCode()
                ],
                'transfer_data' => [
                    'destination' => $whitelabelAccountId,
                ]
            ]);
            if ($charge["paid"]) {
                return array("status" => "paid", "id"=>$charge["id"], "receipt_url"=>$charge["receipt_url"]);
            }
        } catch (exception $e) {
            return false;
        }
        return [];
    }

    public function getWhitelabelAccountBalances($whitelabel_id)
    {
        $accountBalances =  (new DB_User_Payment_Schedule())->getAllAccounts($whitelabel_id);
        $whitelabelUsers = Service_Affiliations::getAllAffiliatedUsers($whitelabel_id);
        $requireReload = false;
        foreach ($whitelabelUsers as $users) {
            // check if contained in account balances array
            $hasAccount = false;
            foreach ($accountBalances as $accounts) {
                if ($accounts["custid"] == $users["custid"]) {
                    $hasAccount = true;
                }
            }
            if (!$hasAccount) {
                $requireReload = true;
                $this->postCharge($whitelabel_id, $users["custid"], time(), $users["custid"], 0, "Account Initialization", "initialization", null);
            }
        }
        if ($requireReload) {
            return (new DB_User_Payment_Schedule())->getAllAccounts($whitelabel_id);
        } else {
            return $accountBalances;
        }

    }

    public function addFeeScheduleToGroup($whitelabel_id, $groupId, $amount, $dueBy, $creatorCustid)
    {
        $group = new Groups($whitelabel_id);
        // get all users in group
        $groupUsers = $group->getGroupMembers($groupId);

        // get group meta
        $groupMeta = $group->getGroupMetaData($groupId);
        $groupName = $groupMeta["title"];

        // add to group schedule
        $scheduleId = (new DB_Groups_Payment_Schedules())->addToSchedule($whitelabel_id,$groupId, $amount, $dueBy);

        // add charge to all existing users
        foreach ($groupUsers as $user) {
            $this->postCharge($whitelabel_id, $user["custid"], $dueBy, $creatorCustid, $amount, "Group fee: {$groupName}", "recurring_group", $scheduleId);
        }
    }

    public function removeFeeFromGroupSchedule($whitelabel_id, $groupScheduleId)
    {
        (new DB_Groups_Payment_Schedules())->removeFromSchedule($whitelabel_id, $groupScheduleId);
    }

    public function removeUserFromFeeGroup($whitelabel_id, $groupId, $custid)
    {
        $allFutureGroupCharges = (new DB_Groups_Payment_Schedules())->getUpcomingSchedulesByGroup($whitelabel_id, $groupId);
        foreach ($allFutureGroupCharges as $futureCharge) {
            // remove future charges from user's payment schedule
            (new DB_User_Payment_Schedule())->removeByFutureChargeType($whitelabel_id, $custid, "recurring_group", $futureCharge["schedule_id"]);
        }
    }

    public function addUserToFeeGroup($whitelabel_id, $groupId, $custid)
    {
        $allFutureGroupCharges = (new DB_Groups_Payment_Schedules())->getUpcomingSchedulesByGroup($whitelabel_id, $groupId);
        // get group meta data
        $group = new Groups($whitelabel_id);
        $groupMeta = $group->getGroupMetaData($groupId);
        $groupName = $groupMeta["title"];

        foreach ($allFutureGroupCharges as $futureCharge) {
            // add future group charges to user's payment schedule
            $this->postCharge($whitelabel_id, $custid, $futureCharge["due_date"], $custid, $futureCharge["amount"], "Group fee: {$groupName}", "recurring_group", $futureCharge["schedule_id"]);
        }
    }

    public function updateGroupJoinFee($whitelabel_id, $groupId, $fee)
    {
        (new DB_groups())->updateJoinFee($whitelabel_id, $groupId, $fee);
    }

    public function getGroupJoinFee($groupId)
    {
        $metaData = (new DB_groups())->getGroupMetaData($groupId);
        if (is_null($metaData["join_fee"]) || $metaData["join_fee"] == '' || $metaData["join_fee"] == "0") {
            return 0;
        } else {
            return $metaData["join_fee"];
        }
    }

    public function addChargeForUserGroupJoin($whitelabel_id, $groupId, $custid)
    {
        $groupFee  = $this->getGroupJoinFee($groupId);
        // get group meta data
        $group     = new Groups($whitelabel_id);
        $groupMeta = $group->getGroupMetaData($groupId);
        $groupName = $groupMeta["title"];

        if ($groupFee > 0 && !(new DB_Groups_Payment_Schedules())->checkIfUserPaidJoinFee($whitelabel_id, $groupId, $custid)) {
            $this->postCharge($whitelabel_id, $custid, time(), $custid, $groupFee, "Group membership charge - {$groupName}", "single_group", $groupId);
        }
    }

    public function getGroupFeeSchedule($whitelabel_id, $groupId)
    {
        $upcomingFees = (new DB_Groups_Payment_Schedules())->getUpcomingSchedulesByGroup($whitelabel_id, $groupId);
        $pastFees     = (new DB_Groups_Payment_Schedules())->getPastSchedulesByGroup($whitelabel_id, $groupId);

        return [
            "whitelabel_id" => $whitelabel_id,
            "group_id"      => $groupId,
            "join_fee"      => $this->getGroupJoinFee($groupId),
            "upcoming_fees" => $upcomingFees,
            "past_fees"     => $pastFees,
        ];
    }

    public function getAllOverdueAccounts()
    {
        return (new DB_User_Payment_Schedule())->getPastDueAccounts();
    }

}