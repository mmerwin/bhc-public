-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2020 at 12:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `bhconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `postmark_log`
--

CREATE TABLE IF NOT EXISTS `postmark_log` (
                                              `message_id` varchar(50) NOT NULL,
                                              `receiver` varchar(250) NOT NULL,
                                              `sender` varchar(250) NOT NULL,
                                              `error_code` varchar(50) NOT NULL,
                                              `status` varchar(50) NOT NULL,
                                              `template_id` int(11) NOT NULL,
                                              `server_id` int(11) NOT NULL,
                                              `custid` int(11) NOT NULL,
                                              `opens` int(11) NOT NULL,
                                              `clicks` int(11) NOT NULL,
                                              `timestamp` int(11) NOT NULL,
                                              PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
