<?php


class DB_Permissions extends DataModel
{
    protected $schema = "permissions";
    protected $subschemaA = "permissions_default";
    protected $subschemaB = "permissions_groups";
    public function getAllPermissions()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

    public function getPermissionById($id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE permission_id = ?", $id)->fetchArray();
    }

    public function getPermissionByTitle($title)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE title = ?", $title)->fetchAll();
    }

    public function addGroupPermission($groupId, $permissionId)
    {
        $alreadyExists = $this->checkGroupPermission($groupId, $permissionId);
        if(!$alreadyExists){
            $this->DB->query("INSERT INTO {$this->subschemaB} (group_id, permission_id, created_at) VALUES (?, ?, ?)", array($groupId, $permissionId, time()));
        }

    }

    public function checkGroupPermission($groupId, $permissionId)
    {
        $result = $this->DB->query("SELECT created_at FROM {$this->subschemaB} WHERE group_id = ? AND permission_id = ? LIMIT 1", array($groupId, $permissionId))->numRows();
        if($result > 0){
            return true;
        } else{
            return false;
        }
    }

    public function getDefaultPermissions()
    {
        return $this->DB->query("SELECT * FROM {$this->subschemaA}")->fetchAll();
    }

    public function getDefaultPermissionByDefaultGroupId($defaultGroupId)
    {
        return $this->DB->query("SELECT * FROM {$this->subschemaA} WHERE default_group_id = ?", $defaultGroupId)->fetchAll();
    }

    public function getPermissionsByUser($custid, $whitelabel_id)
    {
        return $this->DB->query("SELECT distinct(pg.permission_id), p.title,p.descr
                                        FROM groups_members gm
                                        LEFT JOIN {$this->subschemaB} pg
                                        ON gm.group_id = pg.group_id AND gm.custid = ? AND gm.whitelabel_id = ?
                                        LEFT JOIN permissions p ON p.permission_id = pg.permission_id", array($custid, $whitelabel_id))->fetchAll();
    }

    public function findUsersWithPermission($permissionId, $whitelabel_id)
    {
        return $this->DB->query("SELECT distinct(gm.custid), u.fname, u.lname, u.phone_number, l.email
                                        FROM {$this->subschemaB} pg
                                        LEFT JOIN groups_members gm
                                        ON pg.group_id = gm.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = gm.custid
                                        WHERE pg.permission_id = ? AND gm.whitelabel_id = ?", array($permissionId, $whitelabel_id))->fetchAll();
    }

    public function findUsersWithPermissions($permissionIdArray, $whitelabel_id)
    {
        $permissions = '-99';
        foreach($permissionIdArray as $permission)
        {
            $permissions = $permissions.", ".$permission;
        }

        return $this->DB->query("SELECT distinct(gm.custid), u.fname, u.lname, u.phone_number, l.email
                                        FROM {$this->subschemaB} pg
                                        LEFT JOIN groups_members gm
                                        ON pg.group_id = gm.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = gm.custid
                                        WHERE pg.permission_id IN ({$permissions}) AND gm.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getGroupPermissions($groupId)
    {
        return $this->DB->query("SELECT distinct(pg.permission_id) as permission_id,  p.title, p.descr
                                        FROM {$this->subschemaB} pg
                                        LEFT JOIN {$this->schema} p
                                        ON p.permission_id = pg.permission_id
                                        WHERE pg.group_id = ?", array($groupId))->fetchAll();
    }

    public function removeGroupPermission($groupId, $permissionId)
    {
        return $this->DB->query("DELETE FROM {$this->subschemaB} WHERE group_id = ? AND permission_id = ?", array($groupId, $permissionId));
    }

    public function getNonAssignedPermissions($groupId)
    {
        return $this->DB->query("SELECT * FROM permissions WHERE permission_id NOT IN (SELECT permission_id FROM permissions_groups WHERE group_id= ?)", array($groupId))->fetchAll();
    }

    public function getAllPermissionsWithUsers($whitelabel_id)
    {
        return $this->DB->query("SELECT pg.permission_id,gm.custid, u.fname, u.lname, u.phone_number, l.email
                                        FROM permissions_groups pg
                                        LEFT JOIN groups_members gm
                                        ON pg.group_id = gm.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = gm.custid
                                        WHERE gm.whitelabel_id = ? GROUP BY permission_id, custid  ORDER BY permission_id ASC, lname ASC, fname ASC", array($whitelabel_id))->fetchAll();
    }
}