-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application
--
-- Table structure for table `navbar`
--

CREATE TABLE IF NOT EXISTS `navbar` (
                                        `pageid` int(11) NOT NULL AUTO_INCREMENT,
                                        `title` varchar(15) NOT NULL,
                                        `controller` varchar(50) NOT NULL,
                                        `method` varchar(50) NOT NULL,
                                        `weight` int(11) NOT NULL,
                                        `access` varchar(20) NOT NULL DEFAULT 'everyone',
                                        `permission_id` int(11) NOT NULL,
                                        `parent_id` int(11) NOT NULL,
                                        PRIMARY KEY (`pageid`),
                                        KEY `fk_navbar_parent` (`parent_id`),
                                        KEY `fk_permission_id_navbar` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `navbar` before inserting new data
--
TRUNCATE TABLE `navbar`;

INSERT IGNORE INTO `navbar` (`pageid`, `title`, `controller`, `method`, `weight`, `access`, `permission_id`, `parent_id`) VALUES
(1, 'My Profile', 'profile', 'myprofile', 1, 'everyone', 1, 1),
(3, 'Preferences', 'profile', 'preferences', 3, 'everyone', 1, 1),
(5, 'Dashboard', 'dashboard', 'index', 2, 'everyone', 1, 1),
(7, 'Manage Groups', 'groups', 'manage', 2, 'restricted', 4, 2),
(13, 'API Keys', 'profile', 'api', 100, 'everyone', 1, 1),
(14, 'Notifications', 'profile', 'notifications', 4, 'everyone', 1, 1),
(15, 'Stakeholders', 'groups', 'stakeholders', 5, 'restricted', 15, 2),
(16, 'My Groups', 'groups', 'mygroups', 1, 'everyone', 1, 2),
(17, 'Join Requests', 'groups', 'joinrequests', 7, 'restricted', 4, 2),
(18, 'All Equipment', 'equipment', 'all', 1, 'everyone', 1, 5),
(20, 'Reservations', 'equipment', 'reservations', 3, 'restricted', 11, 5),
(21, 'Reported Issues', 'equipment', 'reportedIssues', 4, 'everyone', 1, 5),
(22, 'Insurance', 'equipment', 'insurance', 20, 'restricted', 8, 5),
(25, 'Rosters', 'groups', 'rosters', 5, 'everyone', 1, 2),
(26, 'Communication', 'communications', 'settings', 2, 'restricted', 17, 8),
(27, 'Send Message', 'communications', 'draft', 1, 'everyone', 1, 3),
(28, 'Pending Review', 'communications', 'pending', 3, 'restricted', 14, 3),
(30, 'Analytics', 'communications', 'analytics', 5, 'restricted', 12, 3),
(31, 'Pending Review', 'communications', 'pending', 3, 'restricted', 12, 3),
(33, 'My Messages', 'communications', 'mymessages', 2, 'everyone', 1, 3),
(34, 'Analytics', 'communications', 'analytics', 5, 'restricted', 14, 3),
(35, 'Stakeholders', 'groups', 'stakeholders', 5, 'restricted', 4, 2),
(36, 'General', 'whitelabel', 'settings', 1, 'restricted', 17, 8),
(37, 'Surveys', 'surveys', 'index', 1, 'restricted', 19, 7),
(38, 'My Surveys', 'surveys', 'mysurveys', 2, 'everyone', 1, 7),
(39, 'USRowing', 'waivers', 'usrowing', 1, 'restricted', 18, 9),
(40, 'My Waivers', 'waivers', 'mywaivers', 2, 'everyone', 1, 9),
(41, 'Practices', 'practices', 'AllPractices', 1, 'everyone', 1, 4),
(42, 'Lineups', 'practices', 'lineups', 2, 'restricted', 7, 4),
(43, 'Attendance', 'practices', 'attendance', 3, 'everyone', 1, 4),
(45, 'Manage', 'practices', 'manageUpcoming', 5, 'restricted', 7, 4),
(48, 'Locations', 'practices', 'locations', 8, 'restricted', 7, 4),
(50, 'Permissions', 'groups', 'permissions', 10, 'restricted', 4, 2),
(51, 'Settings', 'communications', 'settings', 100, 'restricted', 12, 3),
(53, 'Subscription', 'whitelabel', 'plan', 10, 'restricted', 17, 8),
(54, 'Settings', 'finances', 'settings', 10, 'restricted', 20, 10),
(55, 'My Finances', 'finances', 'myFinances', 1, 'everyone', 1, 10),
(56, 'Group Fees', 'finances', 'groups', 2, 'restricted', 20, 10),
(57, 'Group Fees', 'finances', 'groups', 2, 'restricted', 21, 10),
(58, 'All Accounts', 'finances', 'allAccounts', 3, 'restricted', 20, 10),
(59, 'All Accounts', 'finances', 'allAccounts', 3, 'restricted', 21, 10),
(60, 'Calendar', 'whitelabel', 'calendarManage', 4, 'restricted', 17, 8),
(61, 'My Races', 'rowstats', 'myraces', 1, 'everyone', 1, 11);
