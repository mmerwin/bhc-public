<?php

require_once (dirname(__FILE__).'/../../vendor/autoload.php');

use Stripe\StripeClient;

class Service_Whitelabel_Subscriptions
{
    private $stripePublicKey;
    private $stripeSecretKey;
    private $whitelabel_id;
    private $whitelabelMeta;
    private $customerId;
    private $stripe;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id    = $whitelabel_id;
        $this->stripePublicKey  = Service_External_Keys::getToken("stripe-public");
        $this->stripeSecretKey  = Service_External_Keys::getToken("stripe-secret"); //if this changes, update syncWhitelabelPlansWithStripeIds() function
        $whitelabelMeta         = (Service_Whitelabel::findNameById($whitelabel_id));
        $this->whitelabelMeta   = $whitelabelMeta[0];
        $this->stripe           = new StripeClient($this->stripeSecretKey);
        $this->customerId       = $this->getStripeCustomerId();
    }

    public function createWhitelabelCustomer()
    {
        $response = $this->stripe->customers->create([
            'description' => $this->whitelabel_id." - whitelabel subscription client",
            'metadata'    => [
                "whitelabel_id" => $this->whitelabel_id,
                "type"          => "whitelabel",
                "env"           => Environment::getEnvCode(),
                "slug"          => Environment::getEnvCode()."-whitelabel-".$this->whitelabel_id
            ],
            "name"        =>  $this->whitelabelMeta["name"]
        ]);
        (new DB_Whitelabel_Plans())->updateStripeCustomerId($this->whitelabel_id, $response["id"]);
        return $response;
    }

    private function getStripeCustomerId()
    {
        $response = (new DB_Whitelabel_Plans())->getWhitelabelPlan($this->whitelabel_id);
        return $response["stripe_customer_id"];
    }

    public function addCreditCard($cardToken)
    {
        $newCard = $this->stripe->customers->createSource(
            $this->customerId,
            ['source' => $cardToken]
        );
        $this->addCardToTable($newCard);
    }

    private function addCardToTable($response)
    {
        (new DB_Stripe_Cards())->addCard($response["id"], $response["object"], $response["address_city"], $response["address_country"], $response["address_line1"],
            $response["address_line1_check"], $response["address_line2"], $response["address_state"], $response["address_zip"], $response["address_zip_check"],
            $response["brand"], $response["country"], $response["customer"], $response["cvc_check"], $response["dynamic_last4"],
            $response["exp_month"], $response["exp_year"], $response["fingerprint"], $response["funding"], $response["last4"],
            $response["name"], $response["tokenization_method"], json_encode($response["meta=data"]));
    }

    public function getWhitelabelCards()
    {
        return $this->stripe->customers->allSources(
            $this->customerId,
            ['object' => 'card',]
        );
    }

    public function removeCard($cardId)
    {
        return $this->stripe->customers->deleteSource(
            $this->customerId,
            $cardId,
            []
        );
    }

    public function changePlan($newPlan)
    {
        // get existing plan
        $currentPlanData = (new DB_Whitelabel_Plans())->getWhitelabelPlan($this->whitelabel_id, false);
        if ($currentPlanData["plan_id"] != $newPlan) {
            // check that new plan exists
            $newPlanData = (new DB_Platform_Plans())->getAllPlans();
            foreach ($newPlanData as $row) {
                if ($row["plan_id"] == $newPlan) {
                    $whitelabelPlans = new DB_Whitelabel_Plans();
                    $whitelabelPlans->changePlan($this->whitelabel_id, $newPlan);
                    return [];
                }
            }
            die(array("Status"=>"Error", "Message"=>"Invalid plan_id"));
        }
    }

    public function getPlan()
    {
        return (new DB_Whitelabel_Plans())->getWhitelabelPlan($this->whitelabel_id, false);
    }

}