<?php


class DB_External_Messages extends DataModel
{
    protected $schema = "external_messages";

    public function newMessage($whitelabel_id, $custid)
    {
        $currentTime = time();

        $this->DB->query("INSERT INTO {$this->schema} (`message_id`, `whitelabel_id`, `custid`, `approval_required`, `approved_by`, `ready_to_send`, `scheduled_time`, `sent_time`, `type`, `body`, `preparing`, `created_at`) 
                                VALUES (NULL, ?, ?, '', 0, 'No', 0, 0, 'Email', '', 'No', ?)", array($whitelabel_id, $custid, $currentTime));

        //get new message_id
        $result = $this->DB->query("SELECT message_id FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND created_at = ? AND ready_to_send = 'No' ORDER BY message_id DESC LIMIT 1", array($whitelabel_id, $custid, $currentTime))->fetchArray();
        return $result['message_id'];
    }

    public function getDetailsByMessageId($messageId)
    {
        return $this->DB->query("SELECT em.*, CONCAT_WS(' ', u.fname, u.lname) AS sender_name  FROM external_messages em
                                        LEFT JOIN users u
                                        on u.custid = em.custid WHERE message_id = ? LIMIT 1", array($messageId))->fetchArray();
    }

    public function updateBody($messageId, $body)
    {
        $this->DB->query("UPDATE {$this->schema} SET body = ? WHERE message_id = ? LIMIT 1", array($body, $messageId));
    }

    public function updateSubject($messageId, $subject)
    {
        $this->DB->query("UPDATE {$this->schema} SET subject = ? WHERE message_id = ? LIMIT 1", array($subject, $messageId));
    }

    public function updateApprovalRequired($messageId, $approvalRequired)
    {
        $this->DB->query("UPDATE {$this->schema} SET approval_required = ? WHERE message_id = ? LIMIT 1", array($approvalRequired, $messageId));
    }

    public function setApproval($messageId, $custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET approved_by = ?, ready_to_send = 'Yes', approval_required = 'No' WHERE message_id = ? LIMIT 1", array($custid, $messageId));
    }

    public function updateScheduledTime($messageId, $scheduledTime)
    {
        $this->DB->query("UPDATE {$this->schema} SET scheduled_time = ? WHERE message_id = ? LIMIT 1", array($scheduledTime, $messageId));
    }

    public function changeMessageType($messageId, $newType)
    {
        $this->DB->query("UPDATE {$this->schema} SET type = ? WHERE message_id = ? LIMIT 1", array($newType, $messageId));
    }

    public function setReadyToSend($messageId)
    {
        $this->DB->query("UPDATE {$this->schema} SET ready_to_send = 'Yes' WHERE message_id = ? LIMIT 1", array($messageId));
    }

    public function setFailedReview($messageId)
    {
        $this->DB->query("UPDATE {$this->schema} SET ready_to_send = 'No' WHERE message_id = ? LIMIT 1", array($messageId));
    }

    public function getAllMessages($whitelabel_id)
    {
        return $this->DB->query("SELECT em.*, CONCAT_WS(' ', u.fname, u.lname) AS sender_name, (SELECT CONCAT_WS(' ', fname, lname) FROM users WHERE custid = em.approved_by LIMIT 1) as approved_by_name  FROM {$this->schema} em
                                        LEFT JOIN users u
                                        on u.custid = em.custid
                                        WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function removeMessage($messageId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ? AND whitelabel_id = ? LIMIT 1", array($messageId, $whitelabel_id));
    }

    public function setPreparableMessages($prepareId)
    {
        $this->DB->query("update {$this->schema} set preparing = ? WHERE ready_to_send = 'Yes' AND ((approval_required = 'Yes' AND approved_by > 0) OR approval_required = 'No') AND preparing = 'No' AND scheduled_time < ?", array($prepareId, time()));
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE preparing = ? ORDER BY scheduled_time ASC", array($prepareId))->fetchAll();
    }

    public function setSendableEmailMessages($prepareId)
    {
        $this->DB->query("update {$this->schema} set preparing = ? WHERE preparing = 'Prepared' AND type='Email'", array($prepareId));
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE preparing = ? ORDER BY scheduled_time AND type='Email' ASC", array($prepareId))->fetchAll();
    }

    public function setSendableTextMessages($prepareId)
    {
        $this->DB->query("update {$this->schema} set preparing = ? WHERE preparing = 'Prepared' AND type='Text'", array($prepareId));
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE preparing = ? ORDER BY scheduled_time AND type='Text' ASC", array($prepareId))->fetchAll();
    }

    public function setMessageAsPrepared($messageId)
    {
        $this->DB->query("UPDATE {$this->schema} SET preparing = 'Prepared' WHERE message_id = ? LIMIT 1", array($messageId));
    }

    public function setMessageAsSent($messageId)
    {
        $this->DB->query("UPDATE {$this->schema} SET preparing = 'Sent', sent_time = ? WHERE message_id = ? LIMIT 1", array(time(), $messageId));
    }

    public function setPracticeMessage($whitelabel_id, $custid, $type, $subject, $body)
    {
        $currentTime = time();
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, custid, approval_required, approved_by, ready_to_send, scheduled_time, sent_time, type,
                                subject, body, preparing, created_at) VALUES (?, ?, 'No', '0', 'No', ?, '0', ?, ?, ?, 'No', ?)",
                                array($whitelabel_id, $custid, $currentTime, $type, $subject, $body, $currentTime));
        $result = $this->DB->query("SELECT message_id FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND type = ? AND created_at = ? 
                                            ORDER BY message_id DESC LIMIT 1", array($whitelabel_id, $custid, $type, $currentTime))->fetchArray();
        return $result["message_id"];
    }

    public function setPracticeMessageReady($messageId)
    {
        $this->DB->query("UPDATE {$this->schema} SET ready_to_send = 'Yes', preparing = 'Prepared'WHERE message_id = ? LIMIT 1", array($messageId));
    }
    
    public function addAttachmentUrl($whitelabel_id, $messageId, $url)
    {
        $this->DB->query("UPDATE {$this->schema} SET attachment = ? WHERE whitelabel_id = ? AND message_id = ? LIMIT 1", array($url, $whitelabel_id, $messageId));
    }
    
    public function addAttachmentName($whitelabel_id, $messageId, $name)
    {
        $this->DB->query("UPDATE {$this->schema} SET attachment_name = ? WHERE whitelabel_id = ? AND message_id = ? LIMIT 1", array($name, $whitelabel_id, $messageId));
    }

    public function getSentMessageIds($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("select distinct(emr.message_id) FROM external_messages_recipients emr
                                        LEFT JOIN external_messages em 
                                        ON em.message_id = emr.message_id
                                        WHERE emr.status = 'sent' AND em.whitelabel_id = ?
                                        AND emr.sent_time > ? AND emr.sent_time <= ?",
                                array($whitelabel_id, $startTime, $endTime))->fetchAll();
    }
}