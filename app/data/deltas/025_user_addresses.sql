--
-- Table structure for table `user_address`
--

CREATE TABLE IF NOT EXISTS `user_address` (
                                              `address_id` int(11) NOT NULL AUTO_INCREMENT,
                                              `custid` int(11) NOT NULL,
                                              `street` text NOT NULL,
                                              `street_2` text NOT NULL,
                                              `city` varchar(100) NOT NULL,
                                              `state` varchar(2) NOT NULL,
                                              `zip` int(11) NOT NULL,
                                              `created_at` int(11) NOT NULL,
                                              `last_updated` int(11) NOT NULL,
                                              PRIMARY KEY (`address_id`),
                                              KEY `fk_custid_address` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
    ADD CONSTRAINT `fk_custid_address` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
