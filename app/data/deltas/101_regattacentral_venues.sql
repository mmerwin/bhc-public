--
-- Table structure for table `regattacentral_venues`
--

CREATE TABLE IF NOT EXISTS `regattacentral_venues` (
                                                       `venue_id` int(11) NOT NULL,
                                                       `name` varchar(500) NOT NULL,
                                                       `abbreviation` varchar(50) NOT NULL,
                                                       `address1` varchar(200) NOT NULL,
                                                       `address2` varchar(200) NOT NULL,
                                                       `city` varchar(200) NOT NULL,
                                                       `region` varchar(100) NOT NULL,
                                                       `country` varchar(30) NOT NULL,
                                                       `postal_code` varchar(30) NOT NULL,
                                                       `latitude` varchar(100) NOT NULL,
                                                       `longitude` varchar(100) NOT NULL,
                                                       `venue_url` text NOT NULL,
                                                       `type` varchar(50) NOT NULL,
                                                       `subtype` varchar(50) NOT NULL,
                                                       `courses` text NOT NULL,
                                                       PRIMARY KEY (`venue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
