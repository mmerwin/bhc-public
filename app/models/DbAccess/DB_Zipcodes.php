<?php

class DB_Zipcodes extends DataModel
{
    public function validateZipcode($zip, $state_id){
        if($_SESSION['DB']->query("SELECT * FROM zipcodes WHERE zip = ? AND state_id = ? LIMIT 1",array($zip, $state_id))->numRows() == 1){
            return true;
        } else{
            return false;
        }
    }

    public function addZipcode($zip, $city, $state_id){
        if(is_null($zip) || is_null($city) || is_null($state_id) || (self::validateState($state_id) == false) || !self::zipexists($zip)){
            return false;
        }
        $state_name = self::getStateFromStateId($state_id);
        if($state_name == "Invalid"){
            return false;
        }
        $_SESSION['DB']->query("INSERT INTO zipcodes(zip, city, state_id, state_name) VALUES (?, ?, ?, ?)", array($zip, $city, $state_id, $state_name));

    }

    public function validateState($state_id){
        if(is_null($state_id)){
            return false;
        }
        else{
            if($_SESSION['DB']->query("SELECT DISTINCT state_id FROM zipcodes WHERE state_id = ? LIMIT 1", array($state_id))->numRows() == 1){
                return true;
            } else{
                return false;
            }
        }

    }

    public function zipexists($zip){
        if(is_null($zip)){
            return false;
        } else{
            if($_SESSION['DB']->query("SELECT DISTINCT zip FROM zipcodes WHERE zip = ? LIMIT 1", array($zip))->numRows() == 1){
                return true;
            } else{
                return false;
            }
        }
    }

    public function getStateInfo($state_id){
        $data = array();
        $result = $_SESSION['DB']->query("SELECT * FROM zipcodes WHERE state_id = ?", array($state_id));
        $data['zipcodeCount'] = $result->numRows();
        $resultData = $result->fetchAll();


        $data['state_name'] = $resultData[0]['state_name'];
        $data['AllData'] = $resultData;

        return $data;
    }

    public function getStateFromStateId($state_id){
        if(self:: validateState($state_id)){
            $result =$_SESSION['DB']->query("SELECT state_name FROM zipcodes WHERE state_id = ? LIMIT 1", arrray($state_id))->fetchAll();
            return $result['state_name'];
        } else{
            return "Invalid";
        }
    }
}