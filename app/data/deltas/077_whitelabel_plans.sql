--
-- Table structure for table `whitelabel_plans`
--

CREATE TABLE IF NOT EXISTS `whitelabel_plans` (
                                                  `whitelabel_id` int(11) NOT NULL,
                                                  `plan_id` int(11) NOT NULL,
                                                  `next_payment` int(11) NOT NULL,
                                                  `stripe_customer_id` varchar(100) NOT NULL,
                                                  PRIMARY KEY (`whitelabel_id`),
                                                  KEY `fk_plan_id_whitelabel_plans` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `whitelabel_plans`
--
ALTER TABLE `whitelabel_plans`
    ADD CONSTRAINT `fk_plan_id_whitelabel_plans` FOREIGN KEY (`plan_id`) REFERENCES `platform_plans` (`plan_id`),
    ADD CONSTRAINT `fk_whitelabel_id_whitelabel_plans` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
