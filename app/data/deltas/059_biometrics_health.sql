--
-- Table structure for table `biometrics_health`
--

CREATE TABLE IF NOT EXISTS `biometrics_health` (
                                                   `custid` int(11) NOT NULL AUTO_INCREMENT,
                                                   `dietary` text NOT NULL,
                                                   `allergies` text NOT NULL,
                                                   PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `biometrics_health`
--
ALTER TABLE `biometrics_health`
    ADD CONSTRAINT `fk_custid_biometrics_health` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
