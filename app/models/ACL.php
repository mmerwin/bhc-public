<?php


class ACL
{
    public function allowEndpoint($controller, $method)
    {

    }

    public function endpointPermissions()
    {
        //returns endpoints that are restricted by permissions
        return array(
            "appdata" => array(

            ),
            "authenticate" => array(

            ),
            "communications" => array(

            ),
            "equipment" => array(

            ),
            "navbar" => array(

            ),
            "notifications" => array(

            ),
            "practices" => array(

            ),
            "reservations" => array(

            ),
            "surveys" => array(

            ),
            "users" => array(

            ),
            "waivers" => array(

            ),
            "whitelabel" => array(

            ),
            "wlgroups"  => array(

            )
        );
    }

}