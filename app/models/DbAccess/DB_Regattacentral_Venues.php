<?php


class DB_Regattacentral_Venues extends DataModel
{
    protected $schema = "regattacentral_venues";

    public function addVenue($venueId, $name, $abbreviation, $address1, $address2, $city, $region, $country, $postalCode, $latitude, $longitude, $venueUrl, $type, $subType, $courses)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (venue_id, name, abbreviation, address1, address2, 
                          city, region, country, postal_code, latitude, longitude, venue_url, type, subtype, courses) 
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", array($venueId, $name, $abbreviation,
            $address1, $address2, $city, $region, $country, $postalCode, $latitude, $longitude, $venueUrl, $type,
            $subType, $courses));
    }

}