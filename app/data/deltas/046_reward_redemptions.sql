--
-- Table structure for table `reward_redemptions`
--

CREATE TABLE IF NOT EXISTS `reward_redemptions` (
                                                    `redemption_id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `whitelabel_id` int(11) NOT NULL,
                                                    `custid` int(11) NOT NULL,
                                                    `points` int(11) NOT NULL,
                                                    `reward_prize` int(11) NOT NULL,
                                                    `fufilled` varchar(50) NOT NULL DEFAULT 'pending',
                                                    PRIMARY KEY (`redemption_id`),
                                                    KEY `fk_whitelabel_reward_redemption` (`whitelabel_id`),
                                                    KEY `fk_reward_prize` (`reward_prize`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reward_redemptions`
--
ALTER TABLE `reward_redemptions`
    ADD CONSTRAINT `fk_reward_prize` FOREIGN KEY (`reward_prize`) REFERENCES `reward_prizes` (`reward_prize_id`),
    ADD CONSTRAINT `fk_whitelabel_reward_redemption` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
