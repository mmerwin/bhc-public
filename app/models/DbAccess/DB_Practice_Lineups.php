<?php


class DB_Practice_Lineups extends DataModel
{
    protected $schema = "practice_lineups";

    public function removePositionFromLineup($whitelabel_id, $practice_id, $boatId, $position)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? AND boat_id = ? AND seat = ? LIMIT 1", array($whitelabel_id, $practice_id, $boatId, $position));
    }

    public function removeAthleteFromLineup($whitelabel_id, $practice_id, $custid)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? AND custid = ? LIMIT 1", array($whitelabel_id, $practice_id, $custid));
    }

    public function addAthleteToLineup($whitelabel_id, $practice_id, $custid, $boatId, $position)
    {
        //make sure athlete is not already assigned
        $this->removeAthleteFromLineup($whitelabel_id, $practice_id, $custid);
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, practice_id, custid, boat_id, seat, last_updated, lineup_sent, survey) VALUES (?, ?, ?, ?, ?, ?, 'No', 'Unsent')",array($whitelabel_id, $practice_id, $custid, $boatId, $position, time()));
    }

    public function removeAllFromBoat($whitelabel_id, $practice_id, $boatId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? AND boat_id = ? LIMIT 9", array($whitelabel_id, $practice_id, $boatId));
    }

    public function getUnsentLineupsForCron()
    {
        return $this->DB->query("SELECT pl.whitelabel_id, wl.timezone, pl.practice_id, pl.custid, pl.boat_id, pl.seat, pl.last_updated, pl.lineup_sent, pl.survey,
                                        u.fname, u.lname, u.phone_number, 
                                        l.email,
                                        psm.name as practice_name, psm.start_time as practice_start, psm.lineups_set,
                                        eb.boat_name, eb.manufacturer as boat_manufacturer, eb.hull_type as hull_size, eb.color as boat_color,
                                        lc.name as location_name, lc.address as location_address, lc.city as location_city, lc.state as location_state, 
                                        lc.zipcode as location_zipcode, lc.fee as location_fee, lc.notes as location_notes,
                                        (SELECT value FROM whitelabel_settings WHERE name = 'sendPrepracticeEmails' AND whitelabel_id = pl.whitelabel_id) as whitelabel_send
                                        FROM practice_lineups pl
                                        LEFT JOIN users u
                                        ON u.custid = pl.custid
                                        LEFT JOIN logins l
                                        ON l.custid = pl.custid
                                        LEFT JOIN practice_schedule_meta psm
                                        ON psm.practice_id = pl.practice_id
                                        LEFT JOIN equipment_boats eb
                                        ON eb.boat_id = pl.boat_id
                                        LEFT JOIN practice_locations lc
                                        ON lc.location_id = psm.location_id
                                        LEFT JOIN whitelabels wl
                                        ON wl.whitelabel_id = pl.whitelabel_id
                                        WHERE pl.lineup_sent = 'No'
                                        HAVING (whitelabel_send = 'Yes' OR whitelabel_send IS NULL) 
                                        AND practice_start >= ?
", array(time()))->fetchAll();
    }

    public function markLineupSent($whitelabel_id, $practiceId, $custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET lineup_sent = 'Yes' WHERE whitelabel_id = ? AND practice_id = ? AND custid = ? LIMIT 1", array($whitelabel_id, $practiceId, $custid));
    }

    public function markSurveyCompleted($whitelabel_id, $practiceId, $custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET survey = 'Submitted' WHERE whitelabel_id = ? AND practice_id = ? AND custid = ? LIMIT 1", array($whitelabel_id, $practiceId, $custid));
    }
    public function markSurveySent($whitelabel_id, $practiceId, $custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET survey = 'Sent' WHERE whitelabel_id = ? AND practice_id = ? AND custid = ? LIMIT 1", array($whitelabel_id, $practiceId, $custid));
    }

    public function getUnsentPracticeSurveys()
    {
        return $this->DB->query("SELECT pl.whitelabel_id, pl.practice_id, pl.custid, pl.survey, u.fname, u.lname,  l.email,
                                        psm.name as practice_name, psm.start_time as practice_start, psm.lineups_set
                                      	FROM practice_lineups pl
                                        LEFT JOIN users u
                                        ON u.custid = pl.custid
                                        LEFT JOIN logins l
                                        ON l.custid = pl.custid
                                        LEFT JOIN practice_schedule_meta psm
                                        ON psm.practice_id = pl.practice_id
                                        WHERE pl.survey = 'Unsent' AND pl.lineup_sent = 'Yes' AND psm.end_time < UNIX_TIMESTAMP()")->fetchAll();
    }

}