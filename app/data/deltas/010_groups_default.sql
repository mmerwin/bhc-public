-- This table gets truncated before new inserts!!! This is done to ensure that the ids match all of the environments and the application

--
-- Table structure for table `groups_default`
--

CREATE TABLE IF NOT EXISTS `groups_default` (
                                                `default_group_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `title` varchar(50) NOT NULL,
                                                `descr` text NOT NULL,
                                                `max` int(11) NOT NULL,
                                                `self_join` varchar(5) NOT NULL DEFAULT 'No',
                                                PRIMARY KEY (`default_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `groups_default`
--

--
-- Dumping data for table `groups_default`
--

INSERT IGNORE INTO `groups_default` (`default_group_id`, `title`, `descr`, `max`, `self_join`) VALUES
(1, 'Super Admin', 'Access to everything. Able to grant/revoke permissions for all groups. Able to do everything. Warning: Only add users to this group that should have permission to manage the entire organization. Strongly recommend limiting to organization president and vice-president.', 5, 'No'),
(2, 'Coach', 'Allowed to create and manage team practices which includes setting lineups, viewing user contact/personal details (along with emergency contacts), planing regattas and send communications.', 30, 'No'),
(4, 'General Member', 'Limited access. Cannot make any changes that affect any other members or the entire organization. ', 0, 'Yes');
COMMIT;
