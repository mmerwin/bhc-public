--
-- Table structure for table `external_messages_webhooks`
--

CREATE TABLE IF NOT EXISTS `external_messages_webhooks` (
                                                            `webhook_id` int(11) NOT NULL AUTO_INCREMENT,
                                                            `external_id` varchar(100) NOT NULL,
                                                            `email` varchar(320) NOT NULL,
                                                            `tag` varchar(20) NOT NULL,
                                                            `activity` varchar(20) NOT NULL,
                                                            `timestamp` int(11) NOT NULL,
                                                            PRIMARY KEY (`webhook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
