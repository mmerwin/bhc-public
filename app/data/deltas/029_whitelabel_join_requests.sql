-- Table structure for table `whitelabel_join_requests`
--

CREATE TABLE IF NOT EXISTS `whitelabel_join_requests` (
                                                          `custid` int(11) NOT NULL,
                                                          `whitelabel_id` int(11) NOT NULL,
                                                          `status` varchar(10) NOT NULL DEFAULT 'Pending',
                                                          `timestamp` int(11) NOT NULL,
                                                          PRIMARY KEY (`custid`,`whitelabel_id`),
                                                          KEY `fk_custid_joinrequests` (`custid`),
                                                          KEY `fk_whitelabel_joinrequests` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `whitelabel_join_requests`
--
ALTER TABLE `whitelabel_join_requests`
    ADD CONSTRAINT `fk_custid_joinrequests` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_joinrequests` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
