<?php


class Service_Permissions
{
    public static function getAllPermissionsForUser($custid,$whitelabel_id)
    {
        return (new DB_Permissions())->getPermissionsByUser($custid, $whitelabel_id);
    }

    public static function checkPermission($permissionId){
        $permissions = self::getAllPermissionsForUser($_SESSION['custid'],$_SESSION['whitelabel_id']);
        foreach ($permissions as $id) {
            if($id['permission_id'] == $permissionId)
            {
                return true;
            }
        }
        return false;
    }

    public static function getAllPermissionOptions()
    {
        return (new DB_Permissions())->getAllPermissions();
    }

    public static function findUsersWithPermission($permissionId, $whitelabel_id)
    {
        return (new DB_Permissions())->findUsersWithPermission($permissionId, $whitelabel_id);
    }

    public static function findUsersWithPermissions($permissionIdArray, $whitelabel_id)
    {
        return (new DB_Permissions())->findUsersWithPermissions($permissionIdArray, $whitelabel_id);
    }

    public static function getAllPermissionsUsers($whitelabel_id)
    {
        return (new DB_Permissions())->getAllPermissionsWithUsers($whitelabel_id);
    }
}