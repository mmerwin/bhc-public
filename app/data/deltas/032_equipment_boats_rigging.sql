--
-- Table structure for table `equipment_boats_rigging`
--

CREATE TABLE IF NOT EXISTS `equipment_boats_rigging` (
                                                         `boat_id` int(11) NOT NULL,
                                                         `seat` int(11) NOT NULL,
                                                         `seat_side` varchar(10) NOT NULL DEFAULT 'unknown',
                                                         `total_spread` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_lateral_pitch_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_lateral_pitch_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_lateral_pitch_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_lateral_pitch_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_front_pitch_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_front_pitch_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_front_pitch_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_front_pitch_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_swivel_height_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `port_swivel_height_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_swivel_height_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `starboard_swivel_height_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `track_length` varchar(5) NOT NULL DEFAULT '0',
                                                         `foot_stretcher_angle_min` varchar(5) NOT NULL DEFAULT '0',
                                                         `foot_stretcher_angle_max` varchar(5) NOT NULL DEFAULT '0',
                                                         `foot_stretcher_placement` varchar(5) NOT NULL DEFAULT '0',
                                                         PRIMARY KEY (`boat_id`,`seat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_boats_rigging`
--
ALTER TABLE `equipment_boats_rigging`
    ADD CONSTRAINT `fk_boat_id_rigging` FOREIGN KEY (`boat_id`) REFERENCES `equipment_boats` (`boat_id`) ON DELETE CASCADE;
COMMIT;
