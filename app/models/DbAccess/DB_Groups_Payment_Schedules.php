<?php


class DB_Groups_Payment_Schedules extends DataModel
{
    protected $schema = "groups_payment_schedules";

    public function addToSchedule($whitelabel_id, $groupId, $amount, $dueDate)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, group_id, amount, due_date) VALUES (?, ?, ?, ?)", array($whitelabel_id, $groupId, $amount, $dueDate));
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND group_id = ? AND amount = ? AND due_date = ? ORDER BY schedule_id DESC LIMIT 1",
        array($whitelabel_id, $groupId, $amount, $dueDate))->fetchArray();
        return $result["schedule_id"];
    }

    public function removeFromSchedule($whitelabel_id, $scheduleId)
    {
        //remove from group schedules
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND schedule_id = ? LIMIT 1", array($whitelabel_id, $scheduleId));

        //remove from user payment schedules
        $this->DB->query("DELETE FROM user_payment_schedule WHERE whitelabel_id = ? AND charge_type = 'recurring_group' AND foreign_id = ?", array($whitelabel_id, $scheduleId));
    }

    public function getUpcomingSchedulesByGroup($whitelabel_id, $group_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND group_id = ? AND due_date > ? ORDER BY due_date ASC", array($whitelabel_id, $group_id, time()))->fetchAll();
    }

    public function getPastSchedulesByGroup($whitelabel_id, $group_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND group_id = ? AND due_date < ? ORDER BY due_date DESC", array($whitelabel_id, $group_id, time()))->fetchAll();
    }

    public function checkIfUserPaidJoinFee($whitelabel_id, $groupId, $custid)
    {
        $count = $this->DB->query("SELECT * FROM user_payment_schedule WHERE whitelabel_id = ? AND custid = ? AND charge_type = 'single_group' AND foreign_id = ?",
            array($whitelabel_id, $custid, $groupId))->numRows();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

}