<?php


class WhitelabelSettings
{
    public static function getSetting($whitelabel_id, $name, $default) {
        // check for whitelabel setting
        $result = (new DB_Whitelabel_Settings())->getSetting($whitelabel_id, $name);
        if (isset($result['value'])) {
            return $result['value'];
        } else {
            return $default;
        }
    }

    public static function updateValue($whitelabel_id, $name, $value) {
        (new DB_Whitelabel_Settings())->updateValue($whitelabel_id, $name, $value);
    }

    public static function getAllSettings($whitelabel_id)
    {
        return (new DB_Whitelabel_Settings())->getAllSettings($whitelabel_id);
    }

    public function uploadLogo($whitelabel_id, $image)
    {
        // typically use $_FILES["image"]["tmp_name"] as $image
        $newURL = (new Service_CDN())->uploadWhitelabelLogo($whitelabel_id);
        WhitelabelSettings::updateValue($whitelabel_id, "whitelabel.logo.url", $newURL);
    }

}