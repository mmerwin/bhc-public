<?php


class DB_Practice_Groups extends DataModel
{
    protected $schema = "practice_groups";

    public function getGroupsByPractice($whitelabel_id, $practiceId)
    {
        return $this->DB->query("SELECT pg.whitelabel_id, pg.practice_id, pg.group_id, g.title, g.descr, g.max as group_max, g.self_join as group_self_join, pg.attendance_start, pg.attendance_end
                                        FROM practice_groups pg
                                        LEFT JOIN groups g
                                        ON g.group_id = pg.group_id
                                        WHERE pg.whitelabel_id = ? AND pg.practice_id = ?", array($whitelabel_id, $practiceId))->fetchAll();
    }

    public function addGroupToPractice($whitelabel_id, $practiceId, $groupId, $start, $end)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, practice_id, group_id, attendance_start, attendance_end, created_at) VALUES (?, ?, ?, ?, ?, ?)", array($whitelabel_id, $practiceId, $groupId, $start, $end, time()));
    }

    public function removeGroup($whitelabel_id, $practiceId, $groupId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? AND group_id = ? LIMIT 1", array($whitelabel_id, $practiceId, $groupId));
    }

    public function updateGroup($whitelabel_id, $practiceId, $groupId, $newStart, $newEnd)
    {
        $this->DB->query("UPDATE {$this->schema} SET attendance_start = ?, attendance_end = ? WHERE practice_id = ? AND whitelabel_id = ? AND group_id = ? LIMIT 1", array($newStart, $newEnd, $practiceId, $whitelabel_id, $groupId));
    }

}