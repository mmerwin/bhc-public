<?php


class DB_Equipment_Manufacturers extends DataModel
{
    protected $schema = "equipment_manufacturers";

    public function getAllManufacturers()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

    public function getBoatManufacturers()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE type = 'Boat' ORDER BY name ASC")->fetchAll();
    }

    public function getManufacturerById($id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE manufacturer_id = ? LIMIT 1", array($id))->fetchArray();
    }

    public function getManufacturerByName($name)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE name = ? LIMIT 1", array($name))->fetchArray();
    }
}