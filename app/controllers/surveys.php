<?php


class surveys extends Controller
{
    public function newSurvey()
    {
        $title       = $this->param("title");
        $description = $this->param("description");
        $anonymous   = $this->param("anonymous");
        $public      = $this->param("public");

        $newSurvey  = (new Service_Survey())->createSurvey($_SESSION['whitelabel_id'], $_SESSION['custid'], $title, $description, $anonymous, $public);
        $result     = (new Service_Survey())->getSurvey($_SESSION['whitelabel_id'], $newSurvey);

        $this->view("json", $result);
    }

    public function getAllSurveys()
    {
        $this->view("json",(new Service_Survey())->getAllSurveys($_SESSION['whitelabel_id']));
    }

    public function getDraftSurveys()
    {
        $this->view("json",(new Service_Survey())->getDraftSurveys($_SESSION['whitelabel_id']));
    }

    public function getSurvey()
    {
        $surveyId = $this->param("survey_id");

        $this->view("json",(new Service_Survey())->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function updateSurveyAttribute()
    {
        $surveyId       = $this->param("survey_id");
        $title          = $this->param("title");
        $description    = $this->param("description");
        $anonymous      = $this->param("anonymous");
        $public         = $this->param("public");
        $openTime       = $this->param("open_time");
        $closeTime      = $this->param("close_time");

        $survey = new Service_Survey();

        if(!is_null($title)){ $survey->updateSurveyTitle($_SESSION['whitelabel_id'], $surveyId, $title); }
        if(!is_null($description)){ $survey->updateSurveyDescription($_SESSION['whitelabel_id'], $surveyId, $description); }
        if(!is_null($anonymous)){ $survey->updateSurveyAnonymous($_SESSION['whitelabel_id'], $surveyId, $anonymous); }
        if(!is_null($public)){ $survey->updateSurveyPublic($_SESSION['whitelabel_id'], $surveyId, $public); }
        if(!is_null($openTime)){ $survey->updateSurveyOpenTime($_SESSION['whitelabel_id'], $surveyId, $openTime); }
        if(!is_null($closeTime)){ $survey->updateSurveyCloseTime($_SESSION['whitelabel_id'], $surveyId, $closeTime); }

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function addDescriptionElementToSurvey()
    {
        $surveyId = $this->param("survey_id");
        $text     = $this->param("text","POST");

        $survey = new Service_Survey();
        $survey->addDescriptionElementToSurvey($_SESSION['whitelabel_id'], $surveyId, $text);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function addTextElementToSurvey()
    {
        $surveyId = $this->param("survey_id");
        $label = $this->param("label");

        $survey = new Service_Survey();
        $survey->addTextareaElementToSurvey($_SESSION['whitelabel_id'], $surveyId, $label);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function addDropdownElementToSurvey()
    {
        $surveyId = $this->param("survey_id");
        $label = $this->param("label");
        $options = array(
            "selection_1"   => $this->param("selection_1"),
            "selection_2"   => $this->param("selection_2"),
            "selection_3"   => $this->param("selection_3"),
            "selection_4"   => $this->param("selection_4"),
            "selection_5"   => $this->param("selection_5"),
            "selection_6"   => $this->param("selection_6"),
            "selection_7"   => $this->param("selection_7"),
            "selection_8"   => $this->param("selection_8"),
            "selection_9"   => $this->param("selection_9"),
            "selection_10"  => $this->param("selection_10"),
            "selection_11"  => $this->param("selection_11"),
            "selection_12"  => $this->param("selection_12"),
            "selection_13"  => $this->param("selection_13"),
            "selection_14"  => $this->param("selection_14"),
            "selection_15"  => $this->param("selection_15"),
            "selection_16"  => $this->param("selection_16"),
            "selection_17"  => $this->param("selection_17"),
            "selection_18"  => $this->param("selection_18"),
            "selection_19"  => $this->param("selection_19"),
            "selection_20"  => $this->param("selection_20")
        );

        $survey = new Service_Survey();
        $survey->addDropdownElementToSurvey($_SESSION['whitelabel_id'], $surveyId, $label,  $options);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function deleteSurveyElement()
    {
        $surveyId   = $this->param("survey_id");
        $questionId = $this->param("question_id");

        $survey = new Service_Survey();
        $survey->deleteSurveyElement($_SESSION['whitelabel_id'], $surveyId, $questionId);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function swapQuestionPositions()
    {
        $surveyId    = $this->param("survey_id");
        $questionId1 = $this->param("question_id_1");
        $questionId2 = $this->param("question_id_2");

        $survey = new Service_Survey();
        $survey->swapElementPositions($_SESSION['whitelabel_id'], $surveyId, $questionId1, $questionId2);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function addGroupToRecipients()
    {
        $surveyId       = $this->param("survey_id");
        $groupId        = $this->param("group_id");
        $stakeholders   = ucfirst(strtolower($this->param("stakeholders")));

        $groups = new Groups($_SESSION['whitelabel_id']);
        if($groups->checkIfGroupExists($groupId))
        {
            $survey = new Service_Survey();
            $survey->addGroupToSurvey($_SESSION['whitelabel_id'], $surveyId, $groupId, $stakeholders);
        }

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function removeRecipient()
    {
        $surveyId    = $this->param("survey_id");
        $recipientId = $this->param("recipient_id");

        $survey = new Service_Survey();
        $survey->removeRecipient($_SESSION['whitelabel_id'], $surveyId, $recipientId);

        $this->view("json", $survey->getSurvey($_SESSION['whitelabel_id'],$surveyId));
    }

    public function markReadyToSend()
    {
        $surveyId   = $this->param("survey_id");
        $survey     = new Service_Survey();

        $survey->setReadyToSend($_SESSION['whitelabel_id'], $surveyId);

        $this->view("json", $survey->getAllSurveys($_SESSION['whitelabel_id']));
    }

    public function getMySurveys()
    {
        $survey = new Service_Survey();

        $this->view("json", $survey->getMySurveys($_SESSION['whitelabel_id'], $_SESSION['custid']));
    }

    public function getPublicResults()
    {
        $survey = new Service_Survey();

        $this->view("json", $survey->getMySurveys($_SESSION['whitelabel_id'], $_SESSION['custid']));
    }

    public function deleteSurvey()
    {
        $surveyId   = $this->param("survey_id");
        $survey     = new Service_Survey();
        $result     = $survey->deleteSurvey($_SESSION['whitelabel_id'], $surveyId);

        $this->view("json", $result);
    }

}