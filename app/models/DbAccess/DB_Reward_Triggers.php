<?php


class DB_Reward_Triggers extends DataModel
{
    protected $schema = "reward_triggers";

    public function getActiveTriggers()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

    public function getTrigger($triggerId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE reward_trigger_id = ? LIMIT 1", array($triggerId))->fetchArray();
    }

    public function getUserTriggers()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE entity = 'user'")->fetchAll();
    }

    public function getWhitelabelTriggers()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE entity = 'whitelabel'")->fetchAll();
    }

}