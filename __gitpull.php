<?php

// this script is to be called after git-pull is successful during the deployment process
// If different scripts need to be pulled on test / staging / production, use the function getDomain to
// determine which environment is being used.

//verify script is only used on cli
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('cli only');
include("app/models/DB.php");
include("app/models/Environment.php");

error_log("ENV is ".Environment::getEnvCode());

function getDomain()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'] . '/';
    return $protocol . $domainName;
}

// run data data/deltas
$dir = dirname(__FILE__) . '/app/data/deltas';
$files1 = scandir($dir);
// print_r($files1);
$db = new DB();
for ($x = 2; $x < count($files1); $x++) {
    //$commands = file_get_contents($dir . '/' . $files1[$x]);
    $db->executeSqlFile($dir . '/' . $files1[$x]);
    //$db->query($commands);
    echo "executed {$files1[$x]} \n";
}

// run composer update -- note that composer must already be installed on the server
shell_exec("cd ".dirname(__FILE__) ."/app && composer install");

// setup cronjobs for the root user based on the script/crontab.php script
include(dirname(__FILE__)."/app/scripts/crontab.php");

// clear all sessions and app tokens if deploying to UAT or PROD. Both of these actions will cause all users to logout.
$env = Environment::getEnvCode();
if ($env == "UAT" || $env == "PROD") {
    //remove all sessions
    $db->query("TRUNCATE sessions");
    //remove all app API tokens
    $db->query("DELETE FROM api_tokens WHERE type = 'app'");
}

echo "end of execution";

?>