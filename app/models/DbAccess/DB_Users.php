<?php


class DB_Users extends DataModel
{
    protected $schema = "users";

    public function addNewUser($fname, $lname, $phone, $sex, $byear, $bmonth, $bday)
    {
        $timestamp = time();
        $this->DB->query("INSERT INTO {$this->schema} (fname, lname, birth_year, birth_month, birth_day, phone_number, created_at, sex) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", array($fname, $lname, $byear, $bmonth, $bday, $phone, $timestamp, $sex));

        //get custid of newly added user
        $custid = $this->DB->query("SELECT custid FROM {$this->schema} 
                                          WHERE fname = ? 
                                          AND lname = ? 
                                          AND birth_year = ? 
                                          AND birth_month = ? 
                                          AND birth_day = ? 
                                          AND phone_number = ?
                                          AND created_at = ? 
                                          AND sex = ?
                                          ORDER BY custid DESC LIMIT 1", array($fname, $lname, $byear, $bmonth, $bday, $phone, $timestamp, $sex))->fetchArray();

        return $custid['custid'];
    }

    public function getUser($custid)
    {
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ?", array($custid))->fetchArray();
        return $result;
    }

    public function releaseSigninLock($custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET initial_signin = 'no' WHERE custid = ?", $custid);
    }

    public function setDarkMode($custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET dark_mode = 'true' WHERE custid = ?", $custid);
    }

    public function setLightMode($custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET dark_mode = 'false' WHERE custid = ?", $custid);
    }

    public function updateFname($custid, $newFname)
    {
        $this->DB->query("UPDATE {$this->schema} SET fname = ? WHERE custid = ?", array($newFname, $custid));
    }

    public function updateLname($custid, $newLname)
    {
        $this->DB->query("UPDATE {$this->schema} SET lname = ? WHERE custid = ?", array($newLname, $custid));
    }

    public function updatePhone($custid, $phone)
    {
        $this->DB->query("UPDATE {$this->schema} SET phone_number = ? WHERE custid = ?", array($phone, $custid));
    }

    public function changeProfilePictureUrl($custid, $url)
    {
        $this->DB->query("UPDATE {$this->schema} SET profile_image = ? WHERE custid = ?",array($url, $custid));
    }

    public function dismissFeatureFlag($custid)
    {
        $this->DB->query("UPDATE {$this->schema} SET seen_feature = 'yes' WHERE custid = ? LIMIT 1", array($custid));
    }

    public function getAllUsers()
    {
        return $this->DB->query("SELECT u.custid, u.fname, u.lname, u.birth_year, u.birth_month, u.birth_day, 
        u.sex, u.phone_number, u.dark_mode, u.profile_image, u.created_at, u.initial_signin, u.seen_feature,
        (SELECT count(*) FROM user_affiliations ua WHERE ua.custid = u.custid) as whitelabel_count, l.email
        FROM users u
        LEFT JOIN logins l
        ON l.custid = u.custid")->fetchAll();
    }


}