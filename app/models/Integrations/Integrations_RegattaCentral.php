<?php


class Integrations_RegattaCentral extends Integrations
{
    public $token;
    private $baseUrl = "https://api.regattacentral.com/v4.0";

    public function __construct()
    {
        $this->refreshApiToken();
    }

    public function refreshApiToken()
    {
        $authenticate = $this->regattacentral();
        $this->token = $authenticate['access_token'];
    }

    public function getClubDetailsByName($search)
    {
        $url = "{$this->baseUrl}/organizations?name=".$search;
        return $this->m2($url);
    }

    public function getBulkRegattaDataById($regattaId)
    {
        $url = "{$this->baseUrl}/regattas/{$regattaId}/bulk";
        return $this->m2($url);
    }

    public function getParticipantIds($fname, $lname, $birthYear, $birthMonth, $birthDay)
    {
        $birthdayFormat = $birthYear."-".str_pad(strval($birthMonth), 2, "0", STR_PAD_LEFT)."-".str_pad(strval($birthDay), 2, "0",STR_PAD_LEFT);
        $lastname       = urlencode($lname);
        $url            = "{$this->baseUrl}/participants?lastname={$lastname}&birthdate={$birthdayFormat}";
        return $this->m2($url);
    }

    public function getResultsByEvent($regattaId, $eventId)
    {
        $url = "{$this->baseUrl}/regattas/{$regattaId}/events/{$eventId}/results";
        return $this->m2($url);
    }

    public function apiCall($url, $method)
    {
        // Create the context for the request
        $context = stream_context_create(array(
            "ssl" => array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
            'http' => array(
                'method' => $method,
                'header' => "Accept: application/json\r\n"."Content-Type: application/x-www-form-urlencoded\r\n Authorization: {$this->token}",
                'Authorization' => $this->token
            )
        ));
        // Send the request
        $response = file_get_contents($url, FALSE, $context);

        // Check for errors
        if($response === FALSE){
            die('Error');
        }
        // Decode the response
        return json_decode($response, TRUE);
    }

    public function getRequest($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: '.$this->token
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        echo $data;
        curl_close($ch);
    }

    public function m2($url)
    {
        // Create the context for the request
        $context = stream_context_create(array(
            "ssl" => array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
            'http' => array(
                'method' => 'GET',
                'header' => "Authorization: {$this->token}"
            )
        ));
        // Send the request
        return file_get_contents($url, FALSE, $context);


    }
}