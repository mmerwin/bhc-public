<?php


class DB_Usrowing_Raw extends DataModel
{
    protected $schema = "usrowing_raw";

    public function updateMember($memberId, $fname, $lname, $email, $gender, $ltwtStatus, $byear, $bmonth, $bday, $waiverSigned, $waiverExpires, $membershipExpires, $membershipTitle, $rc_orgid, $rc_participantId)
    {
        $this->DB->query("REPLACE INTO {$this->schema} (member_id, fname, lname, email, gender, ltwt_status, birth_year, birth_month, birth_day, waiver_signed, 
                   waiver_expires, membership_expires, membership_title, rc_orgid, rc_participant_id, last_updated)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($memberId, $fname, $lname, $email, $gender, $ltwtStatus, $byear, $bmonth, $bday, $waiverSigned, $waiverExpires, $membershipExpires, $membershipTitle, $rc_orgid,
                $rc_participantId, time()
            )
        );
    }

    public function getByRcId($rc_orgid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE rc_orgid = ? ORDER BY lname ASC", array($rc_orgid))->fetchAll();
    }

    public function searchForUser($fname, $lname, $gender, $birthYear, $birthMonth, $birthDay, $rcOrgid = null)
    {
        if ($rcOrgid) {
            $orgparam = "AND rc_orgid = ".intVal($rcOrgid);
        } else {
            $orgparam = "";
        }
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE fname = ? AND lname = ? AND gender = ? AND birth_year = ? AND birth_month = ? AND birth_day = ? {$orgparam} 
                                        ORDER BY last_updated DESC LIMIT 1", array($fname, $lname, $gender, $birthYear, $birthMonth, $birthDay))->fetchArray();
    }

}