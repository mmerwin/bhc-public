--
-- Table structure for table `regattacentral_participant_organizations`
--

CREATE TABLE IF NOT EXISTS `regattacentral_participant_organizations` (
                                                                          `participant_id` int(11) NOT NULL,
                                                                          `organization_id` int(11) NOT NULL,
                                                                          `custid` int(11) DEFAULT NULL,
                                                                          `fname` varchar(100) NOT NULL,
                                                                          `lname` varchar(100) NOT NULL,
                                                                          `gender` varchar(100) NOT NULL,
                                                                          `member_id` int(11) NOT NULL,
                                                                          PRIMARY KEY (`participant_id`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
