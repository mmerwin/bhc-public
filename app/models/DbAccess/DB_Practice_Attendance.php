<?php


class DB_Practice_Attendance extends DataModel
{
    protected $schema = "practice_attendance";

    public function declareAttendance($whitelabel_id, $practiceId, $custid, $status)
    {
        $this->DB->query("REPLACE INTO {$this->schema} (practice_id, whitelabel_id, custid, status, timestamp) VALUES (?, ?, ?, ?, ?)", array($practiceId, $whitelabel_id, $custid, $status, time()));
    }

    public function getFutureAttendanceByCustid($whitelabel_id, $custid)
    {

    }

    public function getPastAttendanceByCustid($whitelabel_id, $custid)
    {

    }

    public function getAttendanceByPracticeId($whitelabel_id, $practiceId)
    {
        return $this->DB->query("SELECT pa.whitelabel_id, pa.practice_id, @custid:=pa.custid as custid, u.fname, u.lname, u.profile_image, u.sex, u.birth_year, u.phone_number, l.email, 
                                        (SELECT height FROM biometrics WHERE custid = @custid ORDER BY added_at DESC LIMIT 1) as height,
                                        (SELECT weight FROM biometrics WHERE custid = @custid ORDER BY added_at DESC LIMIT 1) as weight,
                                        lp.port as preference_port, lp.starboard as preference_starboard, lp.sculling as preference_sculling, lp.coxwain as preference_coxswain,
                                        pa.timestamp as updated_attendance_on, pa.status as attendance_plan ,
                                        pl.boat_id as lineup_boat, pl.seat as lineup_seat, pl.lineup_sent, pl.survey as survey_status,
                                        (SELECT psm.start_time as last_coxed
                                        FROM practice_schedule_meta psm
                                        LEFT JOIN practice_lineups pl
                                        ON pl.practice_id = psm.practice_id AND pl.custid = @custid
                                        WHERE pl.seat='coxswain' AND start_time < UNIX_TIMESTAMP()
                                        ORDER BY start_time DESC LIMIT 1) as last_coxed
       
                                        FROM practice_attendance pa
                                        LEFT JOIN users u
                                        ON u.custid = pa.custid
                                        LEFT JOIN logins l
                                        on l.custid = pa.custid
                                        LEFT JOIN lineup_preferences lp
                                        ON lp.custid = pa.custid
                                        LEFT JOIN practice_lineups pl
                                        ON pl.whitelabel_id = pa.whitelabel_id AND pl.practice_id = pa.practice_id AND pl.custid = pa.custid
                                        WHERE pa.practice_id = ? AND pa.whitelabel_id = ?", array($practiceId, $whitelabel_id))->fetchAll();
    }

    public function getAllByCustid($whitelabel_id, $custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? ORDER BY timestamp ASC", array($whitelabel_id, $custid))->fetchAll();
    }

    public function getPossibleAttendees($whitelabel_id, $practiceId)
    {
        return $this->DB->query("SELECT distinct(gm.custid), u.fname, u.lname, u.profile_image, u.sex, u.birth_year, u.phone_number, l.email, pa.status
                                        FROM groups_members gm
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = gm.custid
                                        LEFT JOIN practice_attendance pa
                                        ON pa.custid = gm.custid AND pa.practice_id = ?
                                        WHERE gm.group_id IN (SELECT group_id FROM practice_groups pg WHERE whitelabel_id = ? AND practice_id = ?)", array($practiceId, $whitelabel_id, $practiceId))->fetchAll();
    }

    public function updateAttendance($whitelabel_id, $practiceId, $custid, $status)
    {
        $this->DB->query("INSERT INTO practice_attendance (practice_id, whitelabel_id, custid, status, timestamp) VALUES (?, ?, ?, ?, ?)
                                ON DUPLICATE KEY UPDATE status= ?, timestamp = ?", array($practiceId, $whitelabel_id, $custid, $status, time(), $status, time()));
    }

    public function getWhitelabelAttendeesByDateRange($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT distinct(custid) FROM (
                                        SELECT pa.whitelabel_id, pa.practice_id, pa.custid, pa.status, psa.name, psa.start_time, psa.end_time FROM practice_attendance pa
                                        LEFT JOIN practice_schedule_meta psa
                                        ON psa.practice_id = pa.practice_id
                                        ) tbl
                                     WHERE whitelabel_id = ? AND start_time >= ?  AND end_time <= ? 
                                    AND status = 'Attending'",
            array($whitelabel_id, $startTime, $endTime))->fetchAll();
    }

}