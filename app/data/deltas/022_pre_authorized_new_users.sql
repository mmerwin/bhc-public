--
-- Table structure for table `pre_authorized_new_users`
--

CREATE TABLE IF NOT EXISTS `pre_authorized_new_users` (
                                                          `authorized_id` int(11) NOT NULL AUTO_INCREMENT,
                                                          `email` varchar(320) NOT NULL,
                                                          `whitelabel_id` int(11) NOT NULL,
                                                          `created_at` int(11) NOT NULL,
                                                          `custid_creator` int(11) NOT NULL,
                                                          `claimed` int(11) NOT NULL DEFAULT 0,
                                                          PRIMARY KEY (`authorized_id`),
                                                          KEY `fk_whitelabel_preauth_user` (`whitelabel_id`),
                                                          KEY `fk_custid_preauth_user` (`custid_creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pre_authorized_new_users`
--
ALTER TABLE `pre_authorized_new_users`
    ADD CONSTRAINT `fk_custid_preauth_user` FOREIGN KEY (`custid_creator`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_preauth_user` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
