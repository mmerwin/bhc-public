<?php


class admin extends Controller
{
    public function validateAsAdmin()
    {
        $token = $this->param("token");
        if (substr($token, 0,9) != "bhcadmin-") {
            $status = ["Status" =>"Error", "Message" => "Tried to access Admin endpoint without admin token"];
        } else {
            // check that token exists
            $tokenStatus = Service_ApiTokenValidation::validateAdminToken($token, false);
            if ($tokenStatus["token_hash"] != $token) {
                $status = ["Status" =>"Error", "Message"=> "Admin token is not valid"];
            } else {
                // token is valid for Admin use
                $status = ["Status" =>"Success", "Message"=> "Admin token is valid"];
            }
        }
        if ($status["Status"] == "Error") {
            die(json_encode($status));
        }
    }
    public function index()
    {
        $result = array("endpoint is found, method is not found");
        $this->view('json',$result);
    }

    public function generateAdminToken()
    {
        $username = $this->param("username");
        $password = $this->param("password");
        $adminCredentials = Service_CredentialCheck::generateApiKey($username, $password, "admin");
        $this->view("json", $adminCredentials);
    }

    public function getAllUsers()
    {
        $this->validateAsAdmin();
        $allUsers = (new DB_Users())->getAllUsers();

        $this->view("json", $allUsers);
    }

    public function getAuditToken()
    {
        $this->validateAsAdmin();
        $custid = $this->param("custid");
        $newToken = (new DB_Api_Tokens())->createNewToken($custid, 'audit', 'Audit by Admin '.$_SESSION["adminid"]);
        $this->view("json", $newToken);
    }

    public function destroyAuditToken()
    {
        $this->validateAsAdmin();
        $token_hash = $this->param("token");
        (new DB_Api_Tokens())->deleteToken($token_hash);
    }

    public function destroyAdminToken()
    {
        $token = $this->param("token");
        (new DB_Admin_Api_Tokens())->deleteToken($token);
    }

}