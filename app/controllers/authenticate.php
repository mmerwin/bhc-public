<?php
/**
 *Authenticate Class
 * Used to generate api tokens, validate credentials, and check that tokens are still valid.
*/


class Authenticate extends Controller
{
    /**
     * Default endpoint landing method.
     *
     * return array error message stating endpoint is found but the method is not.
     */
    public function index()
    {
        $result = array("endpoint is found, method is not found");
          $this->view('json',$result);
    }
    /**
     * Generates a API token
     * @api
     * @param email login email address of account.
     * @param password login password of the account.
     * @return array array of status, email address, and the new token.
     */
    public function generateApiToken()
    {
        $email        =  $this->param('email');
        $password     = $this->param('password');
        $apptokenType = $this->param('apptokentype');

        if ($apptokenType != '') {
            $type = $apptokenType;
        } else {
            $type = "api";
        }
        //check for type of api key

        if(Service_IP::checkIfSystemIp($this->requestIP()) == true) {
          $type = 'app';
        }
        $token = Service_CredentialCheck::generateApiKey($email, $password, $type);
        if (is_null($token)) {
            $result = array("status"=> "Error","error"=>"email, or password incorrect", "email"=>$email);
        } else {
            $result = array("status"=>"Success", "email"=>$email, "token"=>$token['token']);
        }

        $this->view('json',$result);
    }

      /**
       * Checks that the token is valid.
       * @api
       * return array array of token information including custid, expiration (UNIX), type(app or api), and when the token was created.
       */
    public function checkApiKey()
    {
        $token  = $this->param("token");
        $result = Service_ApiTokenValidation::validateToken($token, false);
        $this->view('json',$result);
    }

    /**
     * Checks that email and password match an account.
     * Useful to make sure the account exists without generating a token
     * @api
     * @uses email login email address of account.
     * @uses password login password of the account.
     * return array array of whitelabels allowed to the user
     */
    public function validateCredentials()
    {
        $data['user']['verified'] = "false";
        if (!is_null($this->param('email')) && !is_null($this->param('password'))) {
          //check if credentials are valid
            $credentials = Service_CredentialCheck::checkLoginCredentials($this->param('email'), $this->param('password'));
            if ($credentials['verified']) {
                $user                    = new user($credentials['custid']);
                $whitelabel_affiliations = $user->getAffiliations();
                $data['user']            = $credentials;
                $data['whitelabels']     = $whitelabel_affiliations;
                $data['darkMode']        = $user->darkmode;

            }
        }
        $this->view('json',$data);
    }

    /**
     * Permanently deletes the api token
     * Useful when a user logged-out to ensure no more calls can be made on their behalf.
     * @api
     * @uses tokenDestroy token to destroy.
     * @return array status of the token
     */
    public function deleteToken()
    {
        $token = $this->param("tokenDestroy");
        Service_ApiTokenValidation::deleteKey($token);
        $data = array("Status"=>"Token destroyed", "OldToken"=>$token);
        $this->view('json',$data);
    }

    /**
     * add another 12-months to the life of the token
     * Should not be available in the public api
     *
     * @uses token token to renew for 12 months.
     * @return array details of the token
     */
    public function refreshTokenExpiration()
    {
        $token   = $this->param("token");
        $newData = Service_ApiTokenValidation::refreshExpirationDate($token);
        $this->view('json',$newData);
    }


    /**
     * Generates a API token using another token
     * Should not be part of public api
     * @param token an existing api token
     * @param type token type (api, zapier, etc).
     * @param descr user-provided description of the token
     * @return array array of new token data.
     */
    public function generateApiFromToken()
    {
        $token  =  $this->param('token');
        $type   = $this->param('type');
        $descr  = $this->param('descr');
        $custid = Service_ApiTokenValidation::getCustidFromToken($token);
        $token  = Service_ApiTokenValidation::newToken($custid, $type, $descr);
        if (is_null($token)) {
            $result = array("status"=> "Error","error"=>"Unknown error, no token generated");
        } else {
            $result = array("status"=>"Success", "token"=>$token['token'], "created_at"=>time());
        }

        $this->view('json',$result);
    }

    public function requestPasswordReset()
    {
        $email = $this->param("email");
        //generate reset token
        $loginData = Service_CredentialCheck::generatePasswordRecoveryToken($email);
        //send recovery email
        (new Service_Postmark())->passwordResetEmail($loginData['fname'], $loginData['lname'], $loginData['reset_token'],$loginData['reset_time'], $loginData['email'], $loginData['custid']);
    }

    public function changePasswordFromRecovery()
    {
        $resetToken  = $this->param('reset_token');
        $resetTime   = $this->param('reset_time');
        $newPassword = $this->param('password');

        if ($resetTime > (time()-86400)) { //reset token now lasts for 24 hours
            Service_CredentialCheck::resetPasswordFromToken($resetToken, $resetTime, $newPassword);
            $return = array("Status"=>"Success", "Message"=>"Password has been reset!");
        } else {
            $return = array("Status"=>"Error", "Message"=>"Reset token has expired. Password reset tokens expire after 24 hours");
        }
        $this->view('json', $return);
    }

}