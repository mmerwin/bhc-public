<?php


class DB_Whitelabels extends DataModel
{
    protected $schema = "whitelabels";

    public function addWhitelabel($name, $rc_orgid, $custid,$abbreviation)
    {
        $this->DB->query("INSERT INTO {$this->schema} (name, rc_orgid, created_at, custid_claimed, abbreviation) VALUES (?, ?, ?, ?, ?)", array($name, $rc_orgid, time(), $custid, $abbreviation));

        return $this->DB->query("SELECT * FROM {$this->schema} WHERE name = ? ORDER BY whitelabel_id DESC LIMIT 1", $name)->fetchArray();
    }

    public function findNameById($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? LIMIT 1",$whitelabel_id)->fetchAll();
    }

    public function getAllWhitelabels()
    {
        return $this->DB->query("select wl.whitelabel_id, wl.name, wl.rc_orgid, wl.created_at, wl.custid_claimed, rc.abbreviation
                                        FROM whitelabels wl
                                        LEFT JOIN regattacentral_orgs rc
                                        ON wl.rc_orgid = rc.organizationId")->fetchAll();
    }

    public function getNonAffiliatedWhitelabels($custid)
    {
        //this query will grab all whitelabels that already have at least one custid affiliated with it AND is not currently affiliated with $custid
        return $this->DB->query("select * FROM whitelabels where whitelabel_id NOT IN(SELECT whitelabel_id FROM user_affiliations WHERE custid = ?)", $custid)->fetchAll();
    }

    public function updateTimezone($whitelabel_id, $timezone)
    {
        $this->DB->query("UPDATE {$this->schema} SET timezone = ? WHERE whitelabel_id = ?", array($timezone, $whitelabel_id));
    }

    public function getTimezone($whitelabel_id)
    {
        $timezone = $this->DB->query("SELECT timezone FROM {$this->schema} WHERE whitelabel_id = ? LIMIT 1", array($whitelabel_id))->fetchArray();
        return $timezone['timezone'];
    }

}