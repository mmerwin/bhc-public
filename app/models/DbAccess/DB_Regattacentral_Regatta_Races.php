<?php


class DB_Regattacentral_Regatta_Races extends DataModel
{
    protected $schema = "regattacentral_regatta_races";

    public function addRace($raceId, $regattaId, $eventId, $uuid, $displayNumber, $displayType, $displayOrder, $status, $scheduledStart, $raceType)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (race_id, regatta_id, event_id, uuid, display_number, 
                          display_type, display_order, status, scheduled_start, race_type) 
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", array($raceId, $regattaId, $eventId, $uuid,
            $displayNumber, $displayType, $displayOrder, $status, $scheduledStart, $raceType));
    }

    public function getRacesByEvent($regattaId, $eventId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE regatta_id = ? AND event_id = ? 
                                        ORDER BY scheduled_start DESC",
            array($regattaId, $eventId))->fetchAll();
    }

}