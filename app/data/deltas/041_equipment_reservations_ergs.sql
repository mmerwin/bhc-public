--
-- Table structure for table `equipment_reservations_ergs`
--

CREATE TABLE IF NOT EXISTS `equipment_reservations_ergs` (
                                                             `reservation_id` int(11) NOT NULL,
                                                             `erg_id` int(11) NOT NULL,
                                                             `whitelabel_id` int(11) NOT NULL,
                                                             PRIMARY KEY (`reservation_id`,`erg_id`,`whitelabel_id`),
                                                             KEY `fk_whitelabel_id_reservations_ergs` (`whitelabel_id`),
                                                             KEY `fk_erg_id_reservations_ergs` (`erg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_reservations_ergs`
--
ALTER TABLE `equipment_reservations_ergs`
    ADD CONSTRAINT `fk_erg_id_reservations_ergs` FOREIGN KEY (`erg_id`) REFERENCES `equipment_ergs` (`erg_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_reservation_id_ergs` FOREIGN KEY (`reservation_id`) REFERENCES `equipment_reservations` (`reservation_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_reservations_ergs` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
