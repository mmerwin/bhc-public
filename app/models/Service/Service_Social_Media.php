<?php


class Service_Social_Media
{
    private $whitelabel_id;
    private $allData;
    private $urls = array();

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
        $this->allData       = (new DB_Socialmedia_Accounts())->getAllByWhitelabel($this->whitelabel_id);
        $this->setUrlsArray();
    }

    private function setUrlsArray()
    {
        // get whitelabel data
        foreach ($this->allData as $row) {
            switch ($row['type']){
                case "facebook--url":
                    $this->urls["facebook"] = $row['data'];
                    break;
                case "twitter--url":
                    $this->urls["twitter"] = $row['data'];
                    break;
                case "instagram--url":
                    $this->urls["instagram"] = $row['data'];
                    break;
                case "youtube--url":
                    $this->urls["youtube"] = $row['data'];
                    break;
                default:
                    break;
            }
        }
    }

    public function getSocialMediaUrls()
    {
        return $this->urls;
    }

    public function updateUrl($platform,$url)
    {
        // update db table
        (new DB_Socialmedia_Accounts())->updateUrl($this->whitelabel_id, $platform, $url);
        $this->allData = (new DB_Socialmedia_Accounts())->getAllByWhitelabel($this->whitelabel_id);
        $this->setUrlsArray();
    }

}