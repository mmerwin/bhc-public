<?php


class DB_Usrowing extends DataModel
{
    protected $schema = "usrowing";

    public function getUsrowingForCustid($custid)
    {
        return $this->DB->query("SELECT usrowing.custid, usrowing.usrowingID, usrowing_raw.waiver_signed, usrowing_raw.waiver_expires, usrowing_raw.membership_expires, usrowing_raw.membership_title, usrowing_raw.last_updated
                                        FROM usrowing
                                        LEFT JOIN usrowing_raw
                                        ON usrowing_raw.member_id = usrowing.usrowingID
                                        WHERE usrowing.custid = ? LIMIT 1", $custid)->fetchArray();
    }

    public function updateUsrowingId($custid, $usrowingId)
    {
        if(!$this->checkIfUsrowingIdExists($custid, $usrowingId))
        {
            //insert new usrowingId
            $this->addUsrowingId($custid, $usrowingId);
        } else{
            //update usrowingId
            $this->DB->query("UPDATE {$this->schema} SET usrowingID = ? WHERE custid = ?", array($usrowingId, $custid));
        }
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? LIMIT 1", $custid)->fetchArray();
    }

    public function checkIfUsrowingIdExists($custid, $usrowingId)
    {
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? AND usrowingID = ? LIMIT 1", array($custid, $usrowingId))->numRows();
        if($result > 0)
        {
            return true;
        } else {
            return false;
        }
    }

    public function addUsrowingId($custid, $usrowingId, $expires = 0)
    {
        $this->DB->query("INSERT INTO {$this->schema} (usrowingID, custid, expires, last_updated) VALUES (?, ?, ?, ?)", array($usrowingId, $custid, $expires, time()));
    }

    public function getByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ua.whitelabel_id, u.usrowingID, ua.custid, users.fname, users.lname, ur.gender, ur.birth_year, ur.birth_month, ur.birth_day, ur.waiver_signed, ur.waiver_expires, ur.membership_expires, ur.membership_title, ur.rc_orgid, ur.rc_participant_id, ur.last_updated
                                        FROM user_affiliations ua
                                        LEFT JOIN usrowing u
                                        ON u.custid = ua.custid
                                        LEFT JOIN usrowing_raw ur
                                        on ur.member_id = u.usrowingID
                                        LEFT JOIN users
                                        ON users.custid = ua.custid
                                        WHERE ua.whitelabel_id = ?", $whitelabel_id)->fetchAll();
    }

}