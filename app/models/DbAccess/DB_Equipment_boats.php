<?php


class DB_Equipment_boats extends DataModel
{
    protected $schema = "equipment_boats";

    public function addBoat($data, $whitelabel_id)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, manufacturer, manufacturer_id, model, year_built, year_acquired, purchase_price, insured, insured_value, hull_type, min_weight, max_weight, owner, owner_custid, boat_name, color, serial_number, mode, coxed, created_at)
             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($whitelabel_id, $data['manufacturer'], $data['manufacturer_id'], $data['model'], $data['year_built'], $data['year_acquired'], $data['purchase_price'], $data['insured'], $data['insured_value'], $data['hull_type'], $data['min_weight'], $data['max_weight'], 0, 0, $data['boat_name'], $data['color'], $data['serial_number'], "unknown", $data['coxed'], time()));
        //get boat_id
        $boatData = $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND boat_name = ? AND hull_type = ? ORDER BY boat_id DESC LIMIT 1", array($whitelabel_id, $data['boat_name'], $data['hull_type']))->fetchArray();

        //add rigging seats
        return (new DB_Equipment_Boats_Rigging())->addBoat($boatData['boat_id']);
    }

    public function getAllBoats($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? ORDER BY boat_name ASC", array($whitelabel_id))->fetchAll();
    }

    public function getBoatById($boatId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE boat_id = ? LIMIT 1", array($boatId))->fetchArray();
    }

    public function updateBoatAttributes($whitelabel_id, $boatId, $data)
    {
        $this->DB->query("UPDATE {$this->schema} SET boat_name = ?, serial_number = ?, color = ?, insured = ?, insured_value = ?, status = ? WHERE boat_id = ? AND whitelabel_id = ? LIMIT 1", array($data['boat_name'], $data['serial'], $data['color'], $data['insured'], $data['insured_value'], $data['status'], $boatId, $whitelabel_id));
    }

}