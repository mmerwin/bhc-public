<?php


class Service_Calendar
{
    private $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function addWhitelabelEvent($startTime, $endTime, $title, $descr, $linkColor, $public)
    {
        if ($startTime <= $endTime) {
            (new DB_Whitelabel_Calendar_Events())->addEvent($this->whitelabel_id, $startTime, $endTime, $public, $title, $descr, $linkColor);
        }
    }

    public function getWhitelabelEvents()
    {
        return (new DB_Whitelabel_Calendar_Events())->getEventsByWhitelabel($this->whitelabel_id);
    }

    public function getUpcomingEvents()
    {
        return (new DB_Whitelabel_Calendar_Events())->getUpcomingEvents($this->whitelabel_id);
    }

    public function removeEvent($eventId)
    {
        (new DB_Whitelabel_Calendar_Events())->removeEvent($this->whitelabel_id, $eventId);
    }

    public function getWhitelabelCalendar($public = true)
    {
        $AllEquipmentReservations = (new Service_Equipment_Reservations($this->whitelabel_id, 0))->getReservationsForWhitelabel();
        $PastPractices            = (new Service_Practices($this->whitelabel_id))->getPractices($this->whitelabel_id, 150, false, null, time(), 0, null);
        $FuturePractices          = (new Service_Practices($this->whitelabel_id))->getPractices($this->whitelabel_id, 150, true, null, time(), 0, null);
        $WlEvents                 = $this->getWhitelabelEvents();

        $finalArray = [];
        foreach ($AllEquipmentReservations as $event) {
            $boatsReserved = '<br><ul>';
            $boatCount = 0;
            foreach ($event["boats"] as $boat) {
                $boatsReserved = $boatsReserved."<li>{$boat['boat_name']}</li>";
                $boatCount++;
            }
            if ($boatCount > 0) {
                if ($public) {
                    $descr = ucfirst($event["type"])." equipment reservation on ".$event["time_human"]." <br><b>Boats Reserved:</b>".$boatsReserved;
                } else {
                    $descr = ucfirst($event["type"])." equipment reservation on ".$event["time_human"]." for ".$event["fname"]." ".$event["lname"]."<br><b>Boats Reserved:</b>".$boatsReserved;
                }
                $finalArray[] = array(
                    "title"     => "Equipment Reservation",
                    "start"     => date('c', $event["start_time"]),
                    "end"       => date('c', $event["end_time"]),
                    "descr"     => $descr,
                    "className" => ($event["type"] == "practice") ? "bg-warning":"bg-info"
                );
            }
        }

        foreach ($PastPractices as $event) {
            $finalArray[] = array(
                "title"     => "Practice - ".$event["name"],
                "start"     => date('c', $event["start_time"]),
                "end"       => date('c', $event["end_time"]),
                "descr"     => "Name: ".$event["name"]."<br>Location: ".$event["location"]["name"]."<br>Time: ".$event["formatted_times"]["time_range"],
                "className" => "bg-success"
            );
        }

        foreach ($FuturePractices as $event) {
            $finalArray[] = array(
                "title"     => "Practice - ".$event["name"],
                "start"     => date('c', $event["start_time"]),
                "end"       => date('c', $event["end_time"]),
                "descr"     => "Name: ".$event["name"]."<br>Location: ".$event["location"]["name"]."<br>Time: ".$event["formatted_times"]["time_range"],
                "className" => "bg-success"
            );
        }

        foreach ($WlEvents as $event) {
            if (($public && $event["public"] == "Yes") || !$public) {
                $finalArray[] = array(
                    "title"     => $event["title"],
                    "start"     => date('c', $event["start_time"]),
                    "end"       => date('c', $event["end_time"]),
                    "descr"     => "Start Time: ".date("D M j g:i a", $event["start_time"])."<br>End Time: ".date("D M j g:i a", $event["end_time"])."<br><br><br>".$event["descr"],
                    "className" => "bg-{$event["link_color"]}"
                );
            }
        }

        return $finalArray;
    }

}