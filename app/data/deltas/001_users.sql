CREATE TABLE IF NOT EXISTS `users` (
                                       `custid` int(11) NOT NULL AUTO_INCREMENT,
                                       `fname` varchar(100) NOT NULL,
                                       `lname` varchar(100) NOT NULL,
                                       `birth_year` int(11) NOT NULL,
                                       `birth_month` int(11) NOT NULL,
                                       `birth_day` int(11) NOT NULL,
                                       `sex` varchar(10) NOT NULL DEFAULT 'unknown',
                                       `phone_number` varchar(20) NOT NULL,
                                       `created_at` int(11) NOT NULL,
                                       `initial_signin` varchar(4) NOT NULL DEFAULT 'yes',
                                       PRIMARY KEY (`custid`)
) ENGINE=InnoDB AUTO_INCREMENT=549 DEFAULT CHARSET=utf8mb4;

--
-- Dumping default data for table `users`
--

INSERT Ignore INTO `users` (`custid`, `fname`, `lname`, `birth_year`, `birth_month`, `birth_day`, `sex`, `phone_number`, `created_at`, `initial_signin`) VALUES
(1, 'DEFAULT', 'DEFAULT', 0, 0, 0, 'unknown', '', 0, 'no') ;
COMMIT;
