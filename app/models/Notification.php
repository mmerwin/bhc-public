<?php


class Notification
{
    private $custid;
    private $whitelabel_id;

    public function __construct($custid, $whitelabel_id)
    {
        $this->custid = $custid;
        $this->whitelabel_id = $whitelabel_id;
    }

    public function newNotification($templateId, $mergeTags,$endpointParams)
    {
        (new DB_Notification_Receivers())->addNotification($templateId, $this->custid, $this->whitelabel_id, $endpointParams, $mergeTags);
    }

    public function readNotification($receipt_id)
    {
        (new DB_Notification_Receivers())->readNotification($receipt_id, $this->custid, $this->whitelabel_id);
    }

    public function getNotifications($since, $max, $read)
    {
        if ($max == 0 || ($max < 1 || $max > 30)) {
            $limit = 30;
        } else {
            $limit = $max;
        }
        if ($read == null || $read == "") {
            $read_status = "unread";
        } else {
            $read_status = $read;
        }
        if (is_null($since) || $since == "") {
            $since_id = 0;
        } else {
            $since_id = $since;
        }

        $notifications = (new DB_Notification_Receivers)->getNotifications($this->custid, $this->whitelabel_id, $since_id, $limit, $read_status);

        // format notifications merge tags
        $returnArray = [];
        if (count($notifications) != 0) {
            foreach ($notifications as $item) {
                $returnArray[] = $this->formatNotification($item);
            }
        }

        return $returnArray;
    }

    private function formatNotification($notification)
    {
        $mergeTags = json_decode($notification['merge_tags'], true);

        // replace merge tags with values
        $message = $notification['message'];
        if (!empty($mergeTags)) {
            foreach ($mergeTags as $tag=>$value) {
                $tagName = "[*{$tag}*]";
                $message = str_replace($tagName,$value,$message);
            }
        }


        // format url
        $url   = $notification['endpoint']."?";
        $first = true;
        $endpointParams = json_decode($notification['endpoint_params']);
        if (!empty($endpointParams)) {
            foreach($endpointParams as $key=>$value) {
                if ($first) {
                    $url = $url.$key."=".$value;
                    $first = false;
                } else {
                    $url = $url."&".$key."=".$value;
                }
            }
        }

        return [
            "notification_id"   => $notification['receipt_id'],
            "whitelabel_id"     => $notification['whitelabel_id'],
            "created_at"        => $notification['created_at'],
            "read_at"           => $notification['read_at'],
            "title"             => $notification['title'],
            "descr"             => $notification['descr'],
            "message"           => $message,
            "icon"              => $notification['icon'],
            "btn"               => $notification['btn'],
            "btn_link"          => $url,
            "btn_title"         => $notification['endpoint_title']
        ];
    }

    public function getNotificationById($receiptId, $custid, $whitelabel_id)
    {
        $notifications = (new DB_Notification_Receivers)->getNotificationById($custid, $whitelabel_id, $receiptId);

        // format notifications merge tags
        $returnArray = [];
        if (count($notifications) != 0) {
            foreach($notifications as $item) {
                $returnArray[] = $this->formatNotification($item);
            }
        }

        return $returnArray;
    }

}