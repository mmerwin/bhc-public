<?php
/**
 * The Integrations class.
 *
 * The base data class for dealing with external integrations. Extend this for each
 * created Integrations model.
 */
class Integrations
{

    protected $authorization;


    public function regattacentral()
    {
        //get integration info from database
        $postData = (new DB_Integration_Keys())->getData("RegattaCentral", "array");
        //print_r($postData);

        //print_r($postData);
        $responseData = $this->callHttps('https://api.regattacentral.com/oauth2/api/token',"POST", $postData);
        $this->authorization = $responseData['access_token'];
        return $responseData;
    }

    public function callHttps($url, $method, $params)
    {
        // Create the context for the request
        $context = stream_context_create(array(
            "ssl" => array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
            'http' => array(
                'method' => $method,
                'header' => "Accept: application/json\r\n"."Content-Type: application/x-www-form-urlencoded\r\n",
                'content' => http_build_query($params)
            )
        ));
        // Send the request
        $response = file_get_contents($url, FALSE, $context);

        // Check for errors
        if($response === FALSE){
            die('Error');
        }
        // Decode the response
        $responseData = json_decode($response, TRUE);

        return $responseData;
    }

}