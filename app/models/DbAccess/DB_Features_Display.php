<?php


class DB_Features_Display extends DataModel
{
    protected $schema = "features_display";

    public function getLiveFeature()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE start_time < ? AND end_time > ? LIMIT 1", array(time(), time()))->fetchArray();
    }

}