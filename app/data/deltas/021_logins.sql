--
-- Table structure for table `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
                                        `loginid` int(11) NOT NULL AUTO_INCREMENT,
                                        `custid` int(11) NOT NULL,
                                        `email` varchar(320) NOT NULL,
                                        `password` varchar(32) NOT NULL,
                                        PRIMARY KEY (`loginid`),
                                        KEY `fk_custid` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `logins`
--
ALTER TABLE `logins`
    ADD CONSTRAINT `fk_custid` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;
COMMIT;
