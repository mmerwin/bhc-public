-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
                                             `permission_id` int(11) NOT NULL AUTO_INCREMENT,
                                             `title` varchar(50) NOT NULL,
                                             `descr` text NOT NULL,
                                             PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `permissions`
--

--
-- Dumping data for table `permissions`
--

INSERT IGNORE INTO `permissions` (`permission_id`, `title`, `descr`) VALUES
(1, 'Basic Access', 'Anything every member of Boathouse Connect will need to access such as password reset, contact information, my profile information, notification center, etc.'),
(2, 'Add User to Organization', 'Allows a person to authorize other Boathouse Connect users to join this organization. Also allows a person to pre-authorize users to join this organization.'),
(3, 'Remove User from Organization', 'Allows a person to remove an existing user in the organization. Removing a user from the organization will cause data-loss for the removed user. Limit the number of users with this permission to Super Admins or to a membership committee. '),
(4, 'Manage Groups', 'Full group permission. Allowed to create groups, add/remove users from groups, and set permissions for groups.'),
(5, 'Create Practices', 'Allowed to add / remove / edit practices on the calendar.'),
(6, 'Practice Communications', 'Allowed to send communications about practices.'),
(7, 'Set Practice Lineups', 'Allowed to create/publish lineups for practices.'),
(8, 'Manage Equipment', 'Allowed to update information related to equipment.'),
(9, 'Add Equipment to Inventory', 'Allowed to add / remove equipment from organization inventory.'),
(10, 'Equipment Usage Hours & Group Permissions', 'Allowed to set hours equipment can be used and which groups are allowed to use them.'),
(11, 'Check out equipment', 'Allowed to check out/in equipment for private uses or for practices.'),
(12, 'Manage Communications', 'Allowed to change communications settings such as custom domain name settings, send hours, and other email settings.'),
(13, 'Send Communications to Allowed Groups', 'Allowed to send emails / text messages to groups the user is a member of.'),
(14, 'Approve Communications', 'Approve communications requested to be sent by users not authorized to send communications. Only allowed in groups you are a member of.'),
(15, 'Manage Communications Stakeholders', 'Allowed to create/manage stakeholder lists for groups you are a member of.'),
(17, 'Manage Organization Settings', 'Allowed to change basic organization-related settings, such as uploading logos, setting timezone, etc.'),
(18, 'USRowing Waiver Status', 'Allowed to setup USRowing waiver status integration and manually update waiver/membership status for other users.'),
(19, 'Manage Surveys', 'Allowed to create and send surveys to the entire organization.'),
(20, 'Manage Finances', 'Allowed to modify all finance settings related to collecting fees from users.'),
(21, 'Assess Fees', 'Allowed to assess fees to users.');
COMMIT;

-- update permissions to latest version
-- always change these values too!!!

UPDATE `permissions` SET `permission_id` = 1,`title` = 'Basic Access',`descr` = 'Anything every member of BoathouseConnect will need to access such as password reset, contact information, my profile information, notification center, etc.' WHERE `permissions`.`permission_id` = 1;
UPDATE `permissions` SET `permission_id` = 2,`title` = 'Add User to Organization',`descr` = 'Allows a person to authorize other Boathouse Connect users to join this organization. Also allows a person to pre-authorize users to join this organization.' WHERE `permissions`.`permission_id` = 2;
UPDATE `permissions` SET `permission_id` = 3,`title` = 'Remove User from Organization',`descr` = 'Allows a person to remove an existing user in the organization. Removing a user from the organization will cause data-loss for the removed user. Limit the number of users with this permission to Super Admins or to a membership committee. ' WHERE `permissions`.`permission_id` = 3;
UPDATE `permissions` SET `permission_id` = 4,`title` = 'Manage Groups',`descr` = 'Full group permission. Allowed to create groups, add/remove users from groups, and set permissions for groups.' WHERE `permissions`.`permission_id` = 4;
UPDATE `permissions` SET `permission_id` = 5,`title` = 'Create Practices',`descr` = 'Allowed to add / remove / edit practices on the calendar.' WHERE `permissions`.`permission_id` = 5;
UPDATE `permissions` SET `permission_id` = 6,`title` = 'Practice Communications',`descr` = 'Allowed to send communications about practices.' WHERE `permissions`.`permission_id` = 6;
UPDATE `permissions` SET `permission_id` = 7,`title` = 'Set Practice Lineups',`descr` = 'Allowed to create/publish lineups for practices.' WHERE `permissions`.`permission_id` = 7;
UPDATE `permissions` SET `permission_id` = 8,`title` = 'Manage Equipment',`descr` = 'Allowed to update information related to equipment.' WHERE `permissions`.`permission_id` = 8;
UPDATE `permissions` SET `permission_id` = 9,`title` = 'Add Equipment to Inventory',`descr` = 'Allowed to add / remove equipment from organization inventory.' WHERE `permissions`.`permission_id` = 9;
UPDATE `permissions` SET `permission_id` = 10,`title` = 'Equipment Usage Hours & Group Permissions',`descr` = 'Allowed to set hours equipment can be used and which groups are allowed to use them.' WHERE `permissions`.`permission_id` = 10;
UPDATE `permissions` SET `permission_id` = 11,`title` = 'Check out equipment',`descr` = 'Allowed to check out/in equipment for private uses or for practices.' WHERE `permissions`.`permission_id` = 11;
UPDATE `permissions` SET `permission_id` = 12,`title` = 'Manage Communications',`descr` = 'Allowed to change communications settings such as custom domain name settings, send hours, and other email settings.' WHERE `permissions`.`permission_id` = 12;
UPDATE `permissions` SET `permission_id` = 13,`title` = 'Send Communications to Allowed Groups',`descr` = 'Allowed to send emails / text messages to groups the user is a member of.' WHERE `permissions`.`permission_id` = 13;
UPDATE `permissions` SET `permission_id` = 14,`title` = 'Approve Communications',`descr` = 'Approve communications requested to be sent by users not authorized to send communications. Only allowed in groups you are a member of.' WHERE `permissions`.`permission_id` = 14;
UPDATE `permissions` SET `permission_id` = 15,`title` = 'Manage Communications Stakeholders',`descr` = 'Allowed to create/manage stakeholder lists for groups you are a member of.' WHERE `permissions`.`permission_id` = 15;
UPDATE `permissions` SET `permission_id` = 17,`title` = 'Manage Organization Settings',`descr` = 'Allowed to change basic organization-related settings, such as uploading logos, setting timezone, etc.' WHERE `permissions`.`permission_id` = 17;
UPDATE `permissions` SET `permission_id` = 18,`title` = 'Manage Documents and Waivers',`descr` = 'Allowed to setup USRowing waiver integration, and manage documents for users.' WHERE `permissions`.`permission_id` = 18;
UPDATE `permissions` SET `permission_id` = 19,`title` = 'Manage Surveys',`descr` = 'Allowed to create and send surveys to the entire organization. ' WHERE `permissions`.`permission_id` = 19;
UPDATE `permissions` SET `permission_id` = 20,`title` = 'Manage Finances',`descr` = 'Allowed to modify all finance settings related to collecting fees from users.' WHERE `permissions`.`permission_id` = 20;
UPDATE `permissions` SET `permission_id` = 21,`title` = 'Assess Fees',`descr` = 'Allowed to assess fees to users.' WHERE `permissions`.`permission_id` = 21;

