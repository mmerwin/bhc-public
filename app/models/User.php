<?php

class User
{

    private $custid;
    public $fname;
    public $lname;
    public $birthYear;
    public $birthMonth;
    public $birthDay;
    public $phoneNumber;
    public $createdAt;
    public $initialSignin;
    public $email;
    public $sex;
    public $darkmode;
    public $usrowing;
    public $profileImage;
    public $height = null;
    public $weight = null;
    public $dietary = null;
    public $allergies = null;
    public $address;
    public $lineupPreferences;
    public $stripeCustomerAccount;
    public $contacts;
    public $seenFeature;


    public function __construct($custid)
    {
        $this->custid = $custid;

        // information from users table
        $details                = (new DB_Users())->getUser($custid);
        $this->initialSignin    = $details['initial_signin'];
        $this->fname            = $details['fname'];
        $this->lname            = $details['lname'];
        $this->birthYear        = $details['birth_year'];
        $this->birthMonth       = $details['birth_month'];
        $this->birthDay         = $details['birth_day'];
        $this->phoneNumber      = $details['phone_number'];
        $this->createdAt        = $details['created_at'];
        $this->sex              = $details['sex'];
        $this->darkmode         = $details['dark_mode'];
        $this->profileImage     = $details['profile_image'];
        $this->seenFeature      = $details['seen_feature'];

        // email address
        $logins      = (new DB_Logins())->findLoginByCustid($custid);
        $this->email = $logins['email'];

        // get usrowing data
        $this->usrowing = (new DB_Usrowing())->getUsrowingForCustid($custid);

        // get biometric and health
        $this->getHeightWeight();
        $this->getDietaryAndAllergies();

        // get address
        $this->getAddress();

        // get additional contacts
        $this->getAdditionalContacts();

        // rowing preferences
        $this->lineupPreferences = (new Service_Skills())->getUserPreferences($custid);

        // stripe customer account
        $this->stripeCustomerAccount = (new Service_User_Payments())->getCustomerAccount($custid);
    }

    public function generalDetails()
    {
        return [
            "custid"                => $this->custid,
            "fname"                 => $this->fname,
            "lname"                 => $this->lname,
            "yearendAge"            => (date("Y") - $this->birthYear),
            "birthYear"             => $this->birthYear,
            "birthMonth"            => $this->birthMonth,
            "birthDay"              => $this->birthDay,
            "phoneNumber"           => $this->phoneNumber,
            "height"                => $this->height,
            "weight"                => $this->weight,
            "createdAt"             => $this->createdAt,
            "dietary_restrictions"  => $this->dietary,
            "allergies"             => $this->allergies,
            "initialSignin"         => $this->checkInitialSignin($this->initialSignin),
            "email"                 => $this->email,
            "additional_contacts"   => $this->contacts,
            "sex"                   => $this->sex,
            "darkMode"              => $this->darkmode,
            "affiliations"          => $this->getAffiliations(),
            "usrowing"              => $this->usrowing,
            "profile_image"         => $this->profileImage,
            "lineup_preferences"    => $this->lineupPreferences,
            "emergency_contacts"    => $this->getAllEmergencyContacts(),
            "address"               => $this->address,
            "payment_account"       => $this->stripeCustomerAccount,
            "feature_flag"          => $this->featureFlag(),
            "privacy"               => [
                "email_tracking_suppressed" => Service_Privacy::checkIfEmailTrackingSuppressed($this->custid)
            ]
        ];
    }

    public function getAffiliations()
    {
        return Service_Affiliations::findAllUserAffiliations($this->custid);
    }

    public function getHeightWeight()
    {
        $bio = (new DB_Biometrics())->getLastEntry($this->custid);
        if ($bio) {
            $this->height = $bio['height'];
            $this->weight = $bio['weight'];
        }
    }

    public function checkInitialSignin($initialSignin)
    {
        if (strtolower($initialSignin) == "yes") {
            // check for lineup preferences
            if (empty($this->lineupPreferences)) {
                return "yes";
            }
            // check for emergency contact
            if (empty($this->getAllEmergencyContacts())) {
                return "yes";
            }
            // check for biometric infomation
            if (is_null($this->height)) {
                return "yes";
            }
        }

        return "no";
    }

    public function featureFlag()
    {
        if ($this->seenFeature == "no") {
            $feature = (new DB_Features_Display())->getLiveFeature();
            if (isset($feature["content"])) {
                return $feature["content"];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function updateHeightWeight($newHeight, $newWeight)
    {
        (new DB_Biometrics())->addEntry($this->custid, $newHeight, $newWeight);
        (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(13, $this->custid);
        $this->getHeightWeight();
    }

    public function getDietaryAndAllergies()
    {
        $bio = (new DB_Biometrics_health())->getData($this->custid);
        if ($bio) {
            $this->dietary = $bio['dietary'];
            $this->allergies = $bio['allergies'];
        }
    }

    public function updateDietaryAndAllergies($newDietary, $newAllergies)
    {
        (new DB_Biometrics_health())->update($this->custid, $newDietary, $newAllergies);
        $this->getDietaryAndAllergies();
    }

    public function addAddress($street, $street2, $city, $state, $zip)
    {
        (new DB_User_Address())->addAddress($this->custid, $street, $street2, $city, $state, $zip);
        $this->getAddress();
    }

    public function updateAddress($street, $street2, $city, $state, $zip)
    {
        (new DB_User_Address())->updateFullAddress($this->custid, $street, $street2, $city, $state, $zip);
        $this->getAddress();
    }
    public function getAddress()
    {
       $address = (new DB_User_Address())->getAddress($this->custid);
        if ($address) {
            $this->address = $address;
        }
    }

    public function addUSRowingID($usrowingID, $expires)
    {
        if ($usrowingID != '') {
            (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(6, $this->custid);
        }
        (new DB_Usrowing())->addUsrowingId($this->custid, $usrowingID, $expires);
    }
    public function updateUSRowingID($usrowingID)
    {
        if ($usrowingID != '') {
            (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(6, $this->custid);
        }
        (new DB_Usrowing())->updateUsrowingId($this->custid, $usrowingID);
    }

    public function addEmergencyContact($fname, $lname, $phone, $relationship)
    {
        $emergencyContact = new DB_Emergency_Contact();
        $emergencyContact->addEmergencyContact($this->custid, $fname, $lname, $phone, $relationship);
        (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(5, $this->custid);
    }

    public function deleteEmergencyContact($emergencyContactID)
    {
        (new DB_Emergency_Contact())->deleteEmergencyContact($this->custid, $emergencyContactID);
        (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(5, $this->custid);
    }

    public function setLineupPreferences($port, $starboard, $sculling, $coxswain)
    {
        Service_Skills::createUserPreference($this->custid, $port, $starboard,$sculling,$coxswain);

    }

    public function releaseInitialSigninLock()
    {
        $DB_Users = new DB_Users();
        $DB_Users->releaseSigninLock($this->custid);
    }

    public function addToGroup($whitelabel_id, $groupId)
    {
        // check if affiliated to whitelabel
        $affiliated = Service_Affiliations::checkIfAffiliated($this->custid, $whitelabel_id);
        if ($affiliated) {
            // affiliation confirmed, add to group
            $group = new Groups($whitelabel_id);
            // check if group exists
            $groupExists = $group->checkIfGroupExists($groupId);
            if ($groupExists) {
                $group->addUserToGroup($groupId,$this->custid);
            } else {
                return array("status" => "Error", "message"=>"Group does not exist on this whitelabel");
            }

        } else {
            return array("status" => "Error", "message"=>"Cannot add to group, not affiliated with whitelabel");
        }
    }

    public function updateFname($newFname)
    {
        (new DB_Users())->updateFname($this->custid, $newFname);
        $this->fname = $newFname;
    }

    public function updateLname($newLname)
    {
        (new DB_Users())->updateLname($this->custid, $newLname);
        $this->lname = $newLname;
    }

    public function updateDarkMode($setting)
    {
        if ($setting == "true") {
            (new DB_Users())->setDarkMode($this->custid);
        } else {
            (new DB_Users())->setLightMode($this->custid);
        }
        $this->darkmode = $setting;
    }

    public function updatePhone($phone)
    {
        (new DB_Users())->updatePhone($this->custid, $phone);
        $this->phoneNumber = $phone;
    }

    public function updateEmail($email)
    {
        // check if email already exists
        $emailExists = (new DB_Logins())->checkIfEmailExists($email);
        if (isset($emailExists['custid']) && $emailExists['custid'] != $this->custid) {
            $existingEmail = (new DB_Logins())->getEmailByCustid($this->custid);
            return array("Error"=>"Email already exists","Email"=>$existingEmail['email']);
        } else {
            (new DB_Logins())->changeEmailByCustid($this->custid, $email);
            $this->email = $email;
            return array("Success"=>"Email updated");
        }
    }

    public function getAllEmergencyContacts()
    {
        return (new DB_Emergency_Contact())->getAllEmergencyContacts($this->custid);
    }

    public function setProfilePicture($image)
    {
        // typically use $_FILES["image"]["tmp_name"] as $image
        $newURL = (new Service_CDN())->uploadProfilePicture($this->custid);
        (new DB_Users())->changeProfilePictureUrl($this->custid, $newURL);
    }

    public function updateLineupPreferences($side, $skillLevel)
    {
        $DB_Lineup_Preferences = new DB_Lineup_Preferences();
        switch (strtolower($side)) {
            case "port":
                $DB_Lineup_Preferences->updatePort($this->custid, $skillLevel);
                $this->lineupPreferences['port'] = $skillLevel;
                break;
            case "starboard":
                $DB_Lineup_Preferences->updateStarboard($this->custid, $skillLevel);
                $this->lineupPreferences['starboard'] = $skillLevel;
                break;
            case "sculling":
                $DB_Lineup_Preferences->updateSculling($this->custid, $skillLevel);
                $this->lineupPreferences['sculling'] = $skillLevel;
                break;
            case "coxwain":
                $DB_Lineup_Preferences->updateCoxswain($this->custid, $skillLevel);
                $this->lineupPreferences['coxwain'] = $skillLevel;
                break;
        }
        (new Service_Rewards($_SESSION['whitelabel_id']))->addRewardPoints(4, $this->custid);
    }

    public function getAdditionalContacts()
    {
        $this->contacts = (new DB_User_Additional_Contacts())->getContactsByUser($this->custid);
    }

    public function addAdditionalContact($type, $contact)
    {
        switch (strtolower($type)) {
            case "phone":
                $contactType = "phone";
                break;
            case "email":
            default:
                $contactType = 'email';
                break;
        }
        (new DB_User_Additional_Contacts())->addContactToUser($this->custid, $contactType, $contact);
    }

    public function removeAdditionalContact($type, $contact)
    {
        switch (strtolower($type)) {
            case "phone":
                $contactType = "phone";
                break;
            case "email":
            default:
                $contactType = 'email';
                break;
        }
        (new DB_User_Additional_Contacts())->removeContactFromUser($this->custid, $contactType, $contact);
    }

}
?>