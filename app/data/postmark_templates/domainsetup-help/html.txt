Howdy,<br>
{{sender_name}} is setting up Boathouse Connect to send emails and needs your help adding DKIM and Return-Path DNS records to {{domain_name}}. This lets Boathouse Connect send emails on your behalf ensuring effective delivery and higher domain repuatation.
<br>
<br> <b>DKIM</b>

      <table class="attributes_content">

        <tr>
          <th style="white-space: nowrap;">Hostname: </th>
          <td>{{dkim_hostname}}</td>
        </tr>

        <tr>
          <th>Type: </th>
          <td>TXT</td>
        </tr>

        <tr>
          <th>Add this value: </th>
          <td>{{dkim_value}}</td>
        </tr>

      </table>

      <br> <b>Return-Path</b>

      <table class="attributes_content">

        <tr>
          <th style="white-space: nowrap;">Hostname: </th>
          <td>{{returnpath_hostname}}</td>
        </tr>

        <tr>
          <th>Type: </th>
          <td>CNAME</td>
        </tr>

        <tr>
          <th>Add this value: </th>
          <td>{{returnpath_value}}</td>
        </tr>


      </table>
      <br><br>
       <a href="https://BoathouseConnect.com/home/domainVerify/{{domain_id}}/{{whitelabel_id}}/{{domain_name}}" class="button button--green" target="_blank">Check Verifcation Status</a>

