<?php


class DB_External_Messages_Recipients extends DataModel
{
    protected $schema = "external_messages_recipients";

    public function populateEmailByMessageId($messageId)
    {
        $this->setGroupBySql();
        return $this->DB->query("INSERT IGNORE INTO external_messages_recipients (`name`,`contact`, `user_type`, `custid`, `message_id`) 
SELECT `name`, `contact`, `user_type`, `custid`, `message_id`
                                        FROM (SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, l.email as contact, 'User' as user_type, u.custid, '{$messageId}' as message_id
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = u.custid
                                        WHERE emg.message_id = {$messageId}
                                        GROUP BY name, contact
                                        HAVING name IS NOT NULL AND contact IS NOT NULL) as t1
                                        
                                        UNION ALL (SELECT gs.name, gs.email as contact, 'Stakeholder' as user_type, '0' as custid, '{$messageId}' as message_id
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_stakeholders gs
                                        ON gs.group_id = emg.group_id AND emg.stakeholders = 'Yes'
                                        WHERE emg.message_id = {$messageId}
                                        GROUP BY contact
                                        HAVING name IS NOT NULL AND contact IS NOT NULL)
                                        
                                        UNION ALL (SELECT CONCAT_WS(' ', u.fname, u.lname, '[alt]') AS name, uac.contact as contact, 'User' as user_type, u.custid, '{$messageId}' as message_id
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN user_additional_contacts uac
                                        ON uac.custid = u.custid
                                        WHERE emg.message_id = {$messageId} AND uac.type = 'email'
                                        GROUP BY name, contact
                                        HAVING name IS NOT NULL AND contact IS NOT NULL)");
    }

    public function populateTextByMessageId($messageId)
    {
        $this->setGroupBySql();
        return $this->DB->query("INSERT IGNORE INTO external_messages_recipients (`name`,`contact`, `user_type`, `custid`, `message_id`) 
SELECT `name`, `contact`, `user_type`, `custid`, `message_id`
                                        FROM (SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, u.phone_number as contact, 'User' as user_type, u.custid, '{$messageId}' as message_id
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        WHERE emg.message_id = {$messageId}
                                        GROUP BY name, contact
                                        HAVING name IS NOT NULL AND contact IS NOT NULL) as t1
                                        
                                        UNION ALL (SELECT CONCAT_WS(' ', u.fname, u.lname, '[alt]') AS name, uac.contact as contact, 'User' as user_type, u.custid, '{$messageId}' as message_id
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN user_additional_contacts uac
                                        ON uac.custid = u.custid
                                        WHERE emg.message_id = {$messageId} AND uac.type = 'phone'
                                        GROUP BY name, contact
                                        HAVING name IS NOT NULL AND contact IS NOT NULL)");
    }

    public function assembleList($messageId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE message_id = ? AND sent_time = 0", array($messageId))->fetchAll();
    }

    public function updateAsSent($message_id, $name, $contact, $external_id, $sent_time)
    {
        $this->DB->query("UPDATE {$this->schema} SET external_id = ?, status = 'Sent', sent_time = ? WHERE message_id = ? AND name = ? and contact = ? LIMIT 1", array($external_id, $sent_time, $message_id, $name, $contact));
    }

    public function getMessagesSentToUser($custid, $whitelabel_id)
    {
        return $this->DB->query("SELECT em.message_id, emr.name, emr.contact, emr.user_type, emr.custid, emr.sent_time, em.type, em.subject, em.body
                                        FROM external_messages_recipients emr
                                        LEFT JOIN external_messages em
                                        ON em.message_id = emr.message_id
                                        WHERE emr.custid = ? AND emr.status = 'Sent' AND em.whitelabel_id = ?
                                        ORDER BY emr.sent_time DESC", array($custid, $whitelabel_id))->fetchAll();
    }

    public function getSentRecipients($messageId, $contact)
    {
        return $this->DB->query("SELECT name, contact as {$contact}, user_type as type, custid, external_id FROM {$this->schema} WHERE message_id = ? AND status = 'Sent' ORDER BY name ASC", array($messageId))->fetchAll();
    }

    public function addSingleRecipient($messageID, $name, $contact, $custid)
    {
        $this->DB->query("external_messages_recipients (`name`,`contact`, `user_type`, `custid`, `message_id`) VALUES (?, ?, 'User', ?, ?)", array($name, $contact, $custid, $messageID));
    }

    public function populatePracticeEmailRecipients($whitelabel_id, $practiceId, $messageId)
    {
        $this->DB->query("INSERT IGNORE INTO external_messages_recipients (`name`,`contact`, `user_type`, `custid`, `message_id`) 
                                SELECT `name`, `contact`, `user_type`, `custid`, `message_id`
                                FROM (SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, l.email as contact, 'User' as user_type, pa.custid as custid, '{$messageId}' as message_id
                                FROM practice_attendance pa
                                LEFT JOIN users u
                                ON u.custid = pa.custid
                                LEFT JOIN logins l
                                on l.custid = pa.custid
                                WHERE pa.practice_id = ? AND pa.whitelabel_id = ? AND pa.status= 'Attending') tbl", array($practiceId, $whitelabel_id));
    }

    public function populatePracticeTextRecipients($whitelabel_id, $practiceId, $messageId)
    {
        $this->DB->query("INSERT IGNORE INTO external_messages_recipients (`name`,`contact`, `user_type`, `custid`, `message_id`) 
                                SELECT `name`, `contact`, `user_type`, `custid`, `message_id`
                                FROM (SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, u.phone_number as contact, 'User' as user_type, pa.custid as custid, '{$messageId}' as message_id
                                FROM practice_attendance pa
                                LEFT JOIN users u
                                ON u.custid = pa.custid
                                WHERE pa.practice_id = ? AND pa.whitelabel_id = ? AND pa.status= 'Attending') tbl", array($practiceId, $whitelabel_id));
    }

    public function getRecipientByExternalId($externalId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE external_id = ? LIMIT 1", array($externalId))->fetchArray();
    }

}