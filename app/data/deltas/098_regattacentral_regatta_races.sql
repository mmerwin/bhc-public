--
-- Table structure for table `regattacentral_regatta_races`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regatta_races` (
                                                              `race_id` int(11) NOT NULL,
                                                              `regatta_id` int(11) NOT NULL,
                                                              `event_id` int(11) NOT NULL,
                                                              `uuid` varchar(100) NOT NULL,
                                                              `display_number` varchar(100) NOT NULL,
                                                              `display_type` varchar(100) NOT NULL,
                                                              `display_order` int(11) NOT NULL,
                                                              `status` varchar(200) NOT NULL,
                                                              `scheduled_start` int(11) NOT NULL,
                                                              `race_type` varchar(20) NOT NULL COMMENT 'final,heat,semi,petite',
                                                              PRIMARY KEY (`race_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
