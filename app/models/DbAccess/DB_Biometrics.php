<?php


class DB_Biometrics extends DataModel
{
    protected $schema = "biometrics";

    public function addEntry($custid, $height, $weight)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (custid, added_at, height, weight) VALUES (?,?,?,?)", array($custid, time(), $height, $weight));
    }

    public function getLastEntry($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? ORDER BY added_at DESC LIMIT 1", array($custid))->fetchArray();
    }

    public function getAllByCustid($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? ORDER BY added_at DESC", array($custid))->fetchAll();
    }

}