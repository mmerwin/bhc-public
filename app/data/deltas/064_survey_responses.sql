--
-- Table structure for table `surveys_responses`
--

CREATE TABLE IF NOT EXISTS `surveys_responses` (
                                                   `survey_id` int(11) NOT NULL,
                                                   `unique_id` varchar(50) NOT NULL,
                                                   `question_id` int(11) NOT NULL,
                                                   `submission_time` int(11) NOT NULL,
                                                   `response` text NOT NULL,
                                                   PRIMARY KEY (`survey_id`,`unique_id`,`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;