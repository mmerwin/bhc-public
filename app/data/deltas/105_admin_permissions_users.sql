--
-- Table structure for table `admin_permissions_users`
--

CREATE TABLE IF NOT EXISTS `admin_permissions_users` (
                                                         `adminid` int(11) NOT NULL,
                                                         `permission_id` int(11) NOT NULL,
                                                         PRIMARY KEY (`adminid`,`permission_id`),
                                                         KEY `fk_permission_id_admin_permissions_users` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_permissions_users`
--
ALTER TABLE `admin_permissions_users`
    ADD CONSTRAINT `fk_adminid_admin_permissions_users` FOREIGN KEY (`adminid`) REFERENCES `admin_logins` (`adminid`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_permission_id_admin_permissions_users` FOREIGN KEY (`permission_id`) REFERENCES `admin_permissions` (`permission_id`) ON UPDATE CASCADE;
