<?php


class users extends Controller
{
    /**
     * Provides an array of all details affiliated with the authenticated user
     * @api
     * @return array array of information about the user including affiliated whitelabels
     */
    public function generalDetails()
    {
        if (!is_null($this->param("custid"))) {
            //verify user is affiliated with confirmed whitelabel
            if (Service_Affiliations::checkIfAffiliated($this->param("custid"), $_SESSION["whitelabel_id"])) {
                $user    = new user($this->param("custid"));
                $details = $user->generalDetails();
                $this->view('json',$details);
            } else {
                $this->view("json", array("Error" => "Not affiliated with whitelabel"));
            }
        } else {
            $user    = new user($_SESSION["custid"]);
            $details = $user->generalDetails();
            $this->view('json',$details);
        }

    }

    /**
     * Removes UI flag that requires information on first sign-in to the application.
     * These values should be updated at least once per year to ensure accuracy.
     * Options for the skill levels can be found in the appdata/getLineupSkills endpoint
     * @api
     * @uses port int skill level for rowing port-side (sweep)
     * @uses starboard int skill level for rowing starboard-side(sweep)
     * @uses sculling int skill level for sculling (all boat sizes)
     * @uses fname string first name of emergency contact
     * @uses lname string last name of emergency contact
     * @uses phone string phone number of emergency contact
     * @uses relationship string relationship of the user to the emergency contact
     * @uses height int height of the user in inches
     * @uses weight string weight of the user in pounds
     * @uses dietary string dietary restrictions of the user
     * @uses allergies string any allergies of the user
     * @uses street string street address of the user
     * @uses street_2 string (optional)street address line 2 of the user
     * @uses city string city address of the user
     * @uses state string state address of the user
     * @uses zip string zip-code of the user
     * @return array returns either success or error along with a message.
     */
    public function releaseInitialSigninLock()
    {
        if (
            null !== ($this->param("port")) &&
            null !== ($this->param("starboard")) &&
            null !== ($this->param("sculling")) &&
            null !== ($this->param("coxswain")) &&
            null !== ($this->param("fname")) &&
            null !== ($this->param("lname")) &&
            null !== ($this->param("phone")) &&
            null !== ($this->param("relationship")) &&
            null !== ($this->param("height")) &&
            null !== ($this->param("weight")) &&
            null !== ($this->param("street")) &&
            null !== ($this->param("city")) &&
            null !== ($this->param("state")) &&
            null !== ($this->param("zip"))
        ) {
            $user = new User($_SESSION['custid']);
            if ($user->initialSignin == "yes") {
                $user->setLineupPreferences($this->param("port"),$this->param("starboard"),$this->param("sculling"),$this->param("coxswain"));
                $user->addEmergencyContact($this->param("fname"), $this->param("lname"), $this->param("phone"), $this->param("relationship"));
                $user->updateHeightWeight($this->param("height"), $this->param("weight"));
                $user->updateDietaryAndAllergies($this->param("dietary"), $this->param("allergies"));
                $user->addAddress($this->param("street"), $this->param("street_2"),$this->param("city"),$this->param("state"), $this->param("zip"));
                $user->releaseInitialSigninLock();
                $message = array("Success"=> "Preferences added!");
            } else {
                $message = array("Error"=> "Signin lock already released");
            }

        } else {
            $message = array("Error" => "Not enough/correct parameters");
        }
        $this->view('json',$message);
    }

    /**
     * Provides an array of all whitelabel affiliations for the current user
     * @api
     * @return array array of affiliated whitelabels
     */
    public function getAllWhitelabels()
    {
        $data = (new user($_SESSION['custid']))->getAffiliations();
        $this->view('json',$data);

    }
    /**
     * Provides permissions based on the custid and whitelabel_id provided
     * The custid and whitelabel_id are not checked to an affiliation before use.
     * Best used for checking the permissions of other users, not of those performing actions.
     * @api
     * @uses whitelabel_id
     * @uses custid falls back to using the custid linked to the access token
     * @return array arrays of each permission.
     */
    public function getAllPermissionsForUser()
    {
        $postedCustid = $this->param("custid");
        if (is_null($postedCustid) || $postedCustid == '') {
            $custid = $_SESSION['custid'];
        } else {
            $custid = $postedCustid;
        }
        $whitelabel_id = $this->param("whitelabel_id");
        $permissions   = Service_Permissions::getAllPermissionsForUser($custid,$whitelabel_id);
        $this->view("json",$permissions);
    }

    /**
     * provides permissions that are verified by the token and provided whitelabel_id
     * @api
     * @uses whitelabel_id
     * @return array arrays of each permission.
     */
    public function verifiedPermissionsForUser()
    {
        $permissions = Service_Permissions::getAllPermissionsForUser($_SESSION['custid'],$_SESSION['whitelabel_id']);
        $this->view("json",$permissions);
    }

    /**
     * finds all of the whitelabel join requests and their status
     * @api
     * @return array of all whitelabel join requests.
     */
    public function whitelabelRequests()
    {
        $requests = Service_Whitelabel::getJoinRequestsByCustid($_SESSION['custid']);
        $this->view("json",$requests);
    }

    /**
     * @api
     */
    public function setDarkMode()
    {
        $dark = $this->param("dark");
        $user = new User($_SESSION['custid']);
        $user->updateDarkMode($dark);

        $this->view("json", $user->generalDetails());
    }

    /**
     * @api
     */
    public function updateGeneralData()
    {
        $fname      = $this->param("fname");
        $lname      = $this->param("lname");
        $usrowingId = $this->param("usrowingId");
        $phone      = $this->param("phone");
        $email      = $this->param("email");

        $street1    = $this->param("street1");
        $street2    = $this->param("street2");
        $city       = $this->param("city");
        $state      = $this->param("state");
        $zipcode    = $this->param("zipcode");

        $user = new User($_SESSION['custid']);

        if (isset($street1)) {
            $user->updateAddress($street1, $street2, $city, $state, $zipcode);
        }

        if (isset($fname) && $fname != '') {
            $user->updateFname($fname);
        }

        if (isset($lname) && $lname != '') {
            $user->updateLname($lname);
        }

        if (isset($usrowingId) && $usrowingId != '') {
            $user->updateUSRowingID($usrowingId);
        }

        if (isset($phone) && $phone != '') {
            $user->updatePhone($phone);
        }

        if (isset($email) && $email != '') {
            $emailStatus = $user->updateEmail($email);
        }

        if (isset($emailStatus['Error'])) {
            $this->view('json', $emailStatus);
        } else {
            $this->view("json", $user->generalDetails());
        }

    }

    /**
     * @api
     */
    public function getEmergencyContacts()
    {
        $custid        = $this->param("custid");
        $whitelabel_id = $_SESSION['whitelabel_id'];
        if (isset($custid) && !is_null($whitelabel_id)) {
            //check that custid also belongs to the user
            if (Service_Affiliations::checkIfAffiliated($custid,$whitelabel_id)) {
                $user              = new User($custid);
                $emergencyContacts = $user->getAllEmergencyContacts();
            } else {
                $emergencyContacts = array("Status"=>"Error", "Message"=>"You are not authorized to view this user's emergency contact information!");
            }

        }  else if (isset($custid) && (is_null($whitelabel_id) || isset($whitelabel_id)) && $custid != '') {
            $emergencyContacts = array("Status"=>"Error", "Message"=>"You are not authorized to view this user's emergency contact information!");
        }
        else if ($custid == '') {
            $user              = new User($_SESSION['custid']);
            $emergencyContacts = $user->getAllEmergencyContacts();
        }

        $this->view('json',$emergencyContacts);
    }

    /**
     * @api
     */
    public function deleteEmergencyContact()
    {
        $contactId = $this->param("emergency_contact_id");
        $user      = new User($_SESSION['custid']);
        $user->deleteEmergencyContact($contactId);
        $emergencyContacts = $user->getAllEmergencyContacts();
        $this->view('json',$emergencyContacts);
    }

    /**
     * @api
     */
    public function addEmergencyContact()
    {
        $fname        = $this->param("fname");
        $lname        = $this->param("lname");
        $phone        = $this->param("phone");
        $relationship = $this->param("relationship");

        if (is_null($fname) || is_null($lname) || is_null($phone) || is_null($relationship) || $fname == '' || $lname == '' || $phone == '' || $relationship == '') {
            $response = array("Status"=>"Error","Message"=>"Missing a required parameter");
        } else {
            $user = new User($_SESSION['custid']);
            $user->addEmergencyContact($fname, $lname, $phone, $relationship);
            $response = array("Status"=>"Success","Message"=>"Emergency contact added");
        }
        $this->view('json',$response);
    }

    /**
     * @method POST must use POST for this endpoint
     * @param image image file to be uploaded
     */
    public function uploadProfilePicture()
    {
        $user = new User($_SESSION['custid']);
        $user->setProfilePicture($_FILES["image"]["tmp_name"]);
    }

    public function updateHeightWeight()
    {
        $height = $this->param("height");
        $weight = $this->param("weight");
        $user   = new User($_SESSION['custid']);
        $user->updateHeightWeight($height, $weight);
        $this->view("json",$user->generalDetails());
    }

    public function updateHealth()
    {
        $dietary    = $this->param("dietary");
        $allergies  = $this->param("allergies");
        $user       = new User($_SESSION['custid']);
        $user->updateDietaryAndAllergies($dietary, $allergies);
        $this->view("json",$user->generalDetails());
    }

    public function updateRowingSkills()
    {
        $port       = $this->param("port");
        $starboard  = $this->param("starboard");
        $sculling   = $this->param("sculling");
        $coxwain    = $this->param("coxwain");
        $custid     = ($this->param("custid")) ? $this->param("custid") : $_SESSION['custid'];
        $user       = new User($custid);

        if ($port > 0) {
            $user->updateLineupPreferences("port", $port);
        }
        if ($starboard > 0) {
            $user->updateLineupPreferences("starboard", $starboard);
        }
        if ($sculling > 0) {
            $user->updateLineupPreferences("sculling", $sculling);
        }
        if($coxwain > 0) {
            $user->updateLineupPreferences("coxwain", $coxwain);
        }
        $this->view("json",$user->generalDetails());
    }

    public function addUSRowingId()
    {
        $custid     = $this->param("custid");
        $usrowingId = $this->param("usrowing_id");

        //verify that custid is affiliated with whitelabel
        if (Service_Affiliations::checkIfAffiliated($custid, $_SESSION['whitelabel_id'])) {
            $user = new User($custid);
            $user->addUSRowingID($usrowingId,0);
            $userUpdated = new User($custid);
            $this->view("json",$userUpdated->generalDetails());
        }
    }

    public function addAdditionalContact()
    {
        $type       = $this->param("type");
        $contact    = $this->param("contact");

        $user = new User($_SESSION["custid"]);
        $user->addAdditionalContact($type, $contact);
    }

    public function removeAdditionalContact()
    {
        $type       = $this->param("type");
        $contact    = $this->param("contact");

        $user = new User($_SESSION["custid"]);
        $user->removeAdditionalContact($type, $contact);
    }

    public function dismissFeatureDisplay()
    {
        (new DB_Users())->dismissFeatureFlag($_SESSION["custid"]);
    }

    public function updateEmailPrivacy()
    {
        $setting = $this->param("setting");
        if (strtolower($setting) == "no") {
            //disallow tracking
            Service_Privacy::addToEmailTrackingSuppression($_SESSION["custid"]);
        } else {
            //assume yes
            Service_Privacy::removeFromEmailTrackingSuppression($_SESSION["custid"]);
        }
    }
}