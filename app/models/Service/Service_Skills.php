<?php


class Service_Skills
{
    public static function getLineupSkills()
    {
        return (new DB_Lineup_Skills())->getSkills();
    }

    public function getUserPreferences($custid)
    {
        return (new DB_Lineup_Preferences())->retrievePreferences($custid);
    }

    public function createUserPreference($custid, $port, $starboard, $sculling, $coxswain)
    {
        (new DB_Lineup_Preferences())->setPreferences($custid, $port, $starboard, $sculling, $coxswain);
    }
}