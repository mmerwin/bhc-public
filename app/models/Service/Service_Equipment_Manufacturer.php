<?php


class Service_Equipment_Manufacturer
{
    public static function getAllManufacturers()
    {
        return (new DB_Equipment_Manufacturers())->getAllManufacturers();
    }

    public static function getBoatManufacturers()
    {
        return (new DB_Equipment_Manufacturers())->getBoatManufacturers();
    }

    public static function getManufacturerById($id)
    {
        return (new DB_Equipment_Manufacturers())->getManufacturerById($id);
    }

    public function getManufacturerByName($name)
    {
        return (new DB_Equipment_Manufacturers())->getManufacturerByName($name);
    }
}