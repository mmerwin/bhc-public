--
-- Table structure for table `practice_surveys`
--

CREATE TABLE IF NOT EXISTS `practice_surveys` (
                                                  `whitelabel_id` int(11) NOT NULL,
                                                  `practice_id` int(11) NOT NULL,
                                                  `custid` int(11) NOT NULL,
                                                  `lineups_satisfaction` varchar(5) NOT NULL,
                                                  `sessionplan_satisfaction` varchar(5) NOT NULL,
                                                  `equipment_issues` text NOT NULL,
                                                  `coach_comments` text NOT NULL,
                                                  `timestamp` int(11) NOT NULL,
                                                  PRIMARY KEY (`whitelabel_id`,`practice_id`,`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
