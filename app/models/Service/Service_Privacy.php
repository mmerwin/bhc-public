<?php


class Service_Privacy
{

    public static function addToEmailTrackingSuppression($custid)
    {
        if (isset($custid) && $custid != 0) {
            (new DB_Email_Tracking_Suppressions())->addSuppression($custid);
        }
    }

    public static function removeFromEmailTrackingSuppression($custid)
    {
        (new DB_Email_Tracking_Suppressions())->removeSuppression($custid);
    }

    public static function checkIfEmailTrackingSuppressed($custid)
    {
        //check db
        $suppressions = (new DB_Email_Tracking_Suppressions())->checkForSuppression($custid);
        return (count($suppressions) > 0) ? true : false;

    }

}