<?php


class DB_groups extends DataModel
{
    protected $schema = "groups";
    protected $subschemaA = "groups_default";
    protected $subschemaB = "groups_members";

    public function getDefaultGroups()
    {
        return $this->DB->query("SELECT * FROM {$this->subschemaA}")->fetchAll();
    }

    public function addNewGroup($whitelabel_id, $title, $descr, $max, $selfJoin)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, title, descr, max, self_join) VALUES (?, ?, ?, ?, ?)", array($whitelabel_id, $title, $descr, $max, $selfJoin));
        $newGroupId = $this->DB->query("SELECT group_id FROM {$this->schema} WHERE whitelabel_id = ? AND title = ? ORDER BY group_id DESC LIMIT 1", array($whitelabel_id, $title))->fetchArray();
        return $newGroupId['group_id'];
    }

    public function checkIfGroupExists($whitelabel_id, $title)
    {
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND title = ? LIMIT 1", array($whitelabel_id, $title))->numRows();
        if($result > 0)
        {
            return true;
        } else{
            return false;
        }
    }

    public function checkIfGroupExistsById($whitelabel_id, $group_id)
    {
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND group_id = ? LIMIT 1", array($whitelabel_id, $group_id))->numRows();
        if($result > 0)
        {
            return true;
        } else{
            return false;
        }
    }

    public function addUserToGroup($whitelabel_id, $groupId, $custid)
    {
        $this->DB->query("INSERT INTO {$this->subschemaB} (whitelabel_id, group_id, custid, created_at) VALUES (?, ?, ?, ?)", array($whitelabel_id, $groupId, $custid, time()));
    }

    public function getWhitelabelGroups($whitelabel_id, $custid = 0)
    {
        return $this->DB->query("SELECT g.group_id, g.whitelabel_id, g.title, g.descr, g.max, g.self_join, g.join_fee,
                                        if(
                                        (SELECT count(*) FROM {$this->subschemaB} gm WHERE gm.group_id = g.group_id  AND custid = ? LIMIT 1) > 0, \"yes\", \"no\"
                                        ) AS current_member,
                                        (SELECT count(*) FROM {$this->subschemaB} gm2 WHERE gm2.group_id = g.group_id) as members
                                        FROM {$this->schema} g
                                        WHERE whitelabel_id = ? ORDER BY current_member DESC, g.title ASC", array($custid, $whitelabel_id))->fetchAll();
    }

    public function getGroupMembers($groupId, $whitelabel_id)
    {
        return $this->DB->query("SELECT gm.group_id, u.custid, u.fname, u.lname, u.phone_number,l.email, u.phone_number, usr.usrowingID, usr.expires AS usrowing_waiver_expires_on, gm.created_at AS member_since
                                        FROM groups_members gm
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = u.custid
                                        LEFT JOIN usrowing usr
                                        ON usr.custid = u.custid
                                        WHERE gm.group_id = ? AND whitelabel_id=? ORDER BY u.lname ASC", array($groupId, $whitelabel_id))->fetchAll();
    }

    public function getGroupMetaData($groupId)
    {
        return $this->DB->query("SELECT group_id, whitelabel_id, title, descr, max, self_join, join_fee, (SELECT COUNT(*) FROM groups_members WHERE group_id = ?) AS member_count FROM groups WHERE group_id = ?", array($groupId, $groupId))->fetchArray();
    }

    public function updateGroupMetaData($groupId, $metadata)
    {
        $this->DB->query("UPDATE {$this->schema} SET title = ?, descr = ?, max = ?, self_join = ? WHERE group_id = ?", array($metadata['title'], $metadata['descr'], $metadata['max'], $metadata['self_join'], $groupId));
        return $this->getGroupMetaData($groupId);
    }

    public function deleteUserFromGroup($groupId, $custid, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->subschemaB} WHERE group_id = ? AND whitelabel_id = ? AND custid = ? LIMIT 1", array($groupId, $whitelabel_id, $custid));
    }

    public function deleteGroup($groupId,$whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE group_id = ? AND whitelabel_id = ? LIMIT 1", array($groupId, $whitelabel_id));
    }

    public function updateJoinFee($whitelabel_id, $groupId, $fee)
    {
        $this->DB->query("UPDATE {$this->schema} SET join_fee = ? WHERE whitelabel_id = ? AND group_id = ? LIMIT 1", array($fee, $whitelabel_id, $groupId));
    }


}