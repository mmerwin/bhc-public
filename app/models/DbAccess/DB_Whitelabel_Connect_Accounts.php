<?php


class DB_Whitelabel_Connect_Accounts extends DataModel
{
    protected $schema = "whitelabel_connect_accounts";

    public function getAccountNumber($whitelabel_id, $env)
    {
        $account =  $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND env = ? LIMIT 1", array($whitelabel_id, $env))->fetchArray();
        if (isset($account["account_id"])) {
            return $account["account_id"];
        } else {
            return null;
        }
    }

    public function getAccountByNumber($accountNumber)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE accound_id = ? LIMIT 1", array($accountNumber))->fetchArray();
    }

    public function getAccountByWhitelabel($whitelabel_id, $env)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND env = ? LIMIT 1", array($whitelabel_id, $env))->fetchArray();
    }

    public function setAccountId($whitelabel_id, $env, $accountId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, env, account_id) VALUES (?, ?, ?)", array($whitelabel_id, $env, $accountId));
    }

    public function deleteAccount($accountId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE account_id = ?", array($accountId));
    }

    public function updateStatus($accountId, $chargesEnabled, $detailsSubmitted, $payoutsEnabled)
    {
        $this->DB->query("UPDATE {$this->schema} SET charges_enabled = ?, details_submitted = ?, payouts_enabled = ? WHERE account_id = ?",
            array($chargesEnabled, $detailsSubmitted, $payoutsEnabled, $accountId));
    }
}