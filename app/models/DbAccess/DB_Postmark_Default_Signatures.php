<?php


class DB_Postmark_Default_Signatures extends DataModel
{
    protected $schema = "postmark_default_signatures";

    public function getDefaultSignaturesByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT pds.whitelabel_id, pds.type, pds.signature_id, pss.domain, pss.email_address, pss.reply_to_email_address, pss.email_name, pd.return_path_domain_verified, pd.dkim_verified
                                        FROM postmark_default_signatures pds
                                        LEFT JOIN postmark_sender_signatures pss
                                        ON pss.id = pds.signature_id
                                        LEFT JOIN postmark_domains pd
                                        ON pd.name = pss.domain
                                        WHERE pds.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function changeSignatureId($whitelabel_id, $type, $newSignatureId)
    {
        //delete existing default
        $this->deleteSignature($whitelabel_id, $type);

        //add replacement signature
        $this->addSignatureDefault($whitelabel_id, $type, $newSignatureId);
    }

    public function addSignatureDefault($whitelabel_id, $type, $newSignatureId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, type, signature_id) VALUES (?, ?, ?)", array($whitelabel_id, $type, $newSignatureId));
    }

    public function deleteSignature($whitelabel_id, $type)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND type = ? LIMIT 1", array($whitelabel_id, $type));
    }
}