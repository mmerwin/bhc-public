<?php


class DB_Regattacentral_Regatta_Events extends DataModel
{
    protected $schema = "regattacentral_regatta_events";

    public function addEvent($regattaId, $eventId, $sequence, $label, $code, $title, $minAthleteAge, $maxAthleteAge, $minAvgAge,
                             $maxAvgAge, $athleteClass, $athleteCountExcludingCox, $sweep, $coxed, $cost, $deadlineModel,
                             $defaultRaceFormat, $finalRaceTime, $defaultRaceUnits, $defaultHandicapAlgorithm,
                             $defaultHandicapMultiplier, $defaultDuration, $maxEntries, $maxEntriesPerClub,
                             $maxAlternates, $requireCoxswain, $status, $weight, $gender)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (regatta_id, event_id, sequence, label, code, title, 
                   min_athlete_age, max_athlete_age, min_avg_age, max_avg_age, athlete_class, athlete_count_excluding_cox, sweep, coxed, cost, 
                   deadline_model, default_race_format, final_race_time, default_race_units, default_handicap_algorithm, 
                   default_handicap_multiplier, default_duration, max_entries, max_entries_per_club, max_alternates, 
                   require_coxswain, status, weight, gender) 
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($regattaId, $eventId, $sequence, $label, $code, $title, $minAthleteAge, $maxAthleteAge, $minAvgAge,
            $maxAvgAge, $athleteClass, $athleteCountExcludingCox, $sweep, $coxed, $cost, $deadlineModel,
            $defaultRaceFormat, $finalRaceTime, $defaultRaceUnits, $defaultHandicapAlgorithm,
            $defaultHandicapMultiplier, $defaultDuration, $maxEntries, $maxEntriesPerClub,
            $maxAlternates, $requireCoxswain, $status, $weight, $gender));
    }

    public function getEventsByRegatta($regattaId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE regatta_id = ? ORDER BY sequence ASC", array($regattaId))->fetchAll();
    }

    public function getEntriesByCustid($regattaId, $custid)
    {
        return $this->DB->query("SELECT * FROM regattacentral_regatta_events WHERE regatta_id = ? AND event_id IN (
                                        SELECT distinct(event_id) FROM regattacentral_regatta_entries_participants 
                                        WHERE participant_id IN ( SELECT participant_id FROM regattacentral_participant_custid_map 
                                        WHERE custid = ? ) AND regatta_id = ?)",
            array($regattaId, $custid, $regattaId))->fetchAll();
    }

}