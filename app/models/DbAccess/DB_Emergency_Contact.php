<?php


class DB_Emergency_Contact extends DataModel
{
    protected $schema = "emergency_contact";

    public function addEmergencyContact($custid, $fname, $lname, $phone, $relationship)
    {
        $this->DB->query("INSERT INTO {$this->schema} (custid, fname, lname, phone, relationship, created_at, last_updated) VALUES (?, ?, ?, ?, ?, ?, ?)",
            array($custid, $fname, $lname, $phone, $relationship, time(), time()));
    }

    public function getAllEmergencyContacts($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ?", array($custid))->fetchAll();
    }

    public function deleteEmergencyContact($custid, $contactId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE emergency_contact_id = ? AND custid = ? LIMIT 1",array($contactId, $custid));
    }

}