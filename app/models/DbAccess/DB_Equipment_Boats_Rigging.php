<?php


class DB_Equipment_Boats_Rigging extends DataModel
{
    protected $schema = "equipment_boats_rigging";

    public function addBoat($boatId)
    {
        //get boat data
        $boatData = (new DB_Equipment_boats())->getBoatById($boatId);
        //build query
        $counter = 2;
        if($boatData['hull_type'] == 1){
            $values = "({$boatData['boat_id']}, 1, 'Sculling')";
        } else
        {
            $values = "({$boatData['boat_id']}, 1, 'unknown')";
        }
        while($counter <= $boatData['hull_type'])
        {
            $values = $values.",({$boatData['boat_id']}, {$counter} , 'unknown')";
            $counter++;
        }

        $this->DB->query("INSERT INTO {$this->schema} (boat_id, seat, seat_side) VALUES {$values}");
    }

    public function getRigging($boatId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE boat_id = ? ORDER BY seat ASC", array($boatId))->fetchAll();
    }

    public function updateSeatConfig($boatId, $seat, $config)
    {
        $this->DB->query("UPDATE {$this->schema} SET seat_side = ? WHERE boat_id = ? AND seat = ?", array($config, $boatId, $seat));
    }

    public function updateSpread($boatId, $seat, $spread)
    {
        $this->DB->query("UPDATE {$this->schema} SET total_spread = ? WHERE boat_id = ? AND seat = ?", array($spread, $boatId, $seat));
    }

    public function updatePitch($boatId, $seat, $field, $degrees)
    {
        $this->DB->query("UPDATE {$this->schema} SET {$field} = ? WHERE boat_id = ? AND seat = ?", array($degrees, $boatId, $seat));
    }

    public function updateTrackLength($boatId, $seat, $distance)
    {
        $this->DB->query("UPDATE {$this->schema} SET track_length = ? WHERE boat_id = ? AND seat = ?", array($distance, $boatId, $seat));
    }

    public function getAllRigging($whitelabel_id)
    {
      return $this->DB->query("SELECT eb.whitelabel_id, ebr.*
                                    FROM equipment_boats eb
                                    LEFT JOIN equipment_boats_rigging ebr
                                    ON ebr.boat_id = eb.boat_id
                                    WHERE eb.whitelabel_id = ? ORDER BY ebr.boat_id ASC, ebr.seat asc", array($whitelabel_id))->fetchAll();
    }
}