
-- Table structure for table `integration_keys`
--

CREATE TABLE IF NOT EXISTS `integration_keys` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `service` varchar(50) NOT NULL,
                                                  `data` text NOT NULL,
                                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;