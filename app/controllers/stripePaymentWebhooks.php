<?php

require_once (dirname(__FILE__).'/../vendor/autoload.php');
use Stripe\StripeClient;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
class stripePaymentWebhooks extends Controller
{
    public function createCustomer()
    {
        $whitelabel_id  = $this->param("whitelabel_id");
        $service        = new Service_Whitelabel_Subscriptions($whitelabel_id);
        $response       = $service->createWhitelabelCustomer();
        $this->view("json", $response);
    }

    public function addWhitelabelCreditCard()
    {
        $cardToken  = $this->param("cardToken");
        $service    = new Service_Whitelabel_Subscriptions($_SESSION["whitelabel_id"]);
        $service->addCreditCard($cardToken);
        WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.billingplan", "true");
    }

    public function getWhitelabelCardInfo()
    {
        $service = new Service_Whitelabel_Subscriptions($_SESSION["whitelabel_id"]);
        $cards   = $service->getWhitelabelCards();

        $this->view("json", $cards);
    }

    public function removeWhitelabelCard()
    {
        $cardId  = $this->param("cardToken");
        $service = new Service_Whitelabel_Subscriptions($_SESSION["whitelabel_id"]);
        $service->removeCard($cardId);
    }

    public function chargeOrgSubscriptions()
    {
        $stripeClient = new StripeClient(Service_External_Keys::getToken("stripe-secret"));
        $wlplans      = (new DB_Whitelabel_Plans());
        $duePayments  = $wlplans->getDuePayments();

        //postmark setting for declined payments
        $apiCredentials  = Environment::getExternalKeys("postmark-platform");
        $client          = new PostmarkClient($apiCredentials["token"]);
        $url             = (Environment::retrieveAppUrl(Environment::getEnvFromHostname()))."whitelabel/plan";

        foreach ($duePayments as $whitelabel) {
            try {
                $stripeClient->charges->create([
                    'amount'        => ($whitelabel["monthly_price"]*100),
                    'currency'      => 'usd',
                    'customer'      => $whitelabel["stripe_customer_id"],
                    'description'   => $whitelabel["plan_name"].' plan - Monthly Platform Fee',
                ]);
                $wlplans->updateNextPayment($whitelabel["whitelabel_id"], (time()+(86400*30)));
            } catch (exception $e){
                //add notification and send payment error email
                Service_Notification_Send::addNotificationToAllAllowedUsers(array(17), 13, $whitelabel["whitelabel_id"], [], $endpointParams = []);

                //send email & text message to all users with permission 17
                $allowedUsers = Service_Permissions::findUsersWithPermission(17, $whitelabel["whitelabel_id"]);

                $twilioCredentials = Environment::getExternalKeys("twilio");
                $twilioSid    = $twilioCredentials['client_id']; // Your Account SID from www.twilio.com/console
                $twilioToken  = $twilioCredentials['token']; // Your Auth Token from www.twilio.com/console
                $twilioClient = new Twilio\Rest\Client($twilioSid, $twilioToken);
                $senderPhone  = "+14695072121";

                foreach ($allowedUsers as $user) {
                    //send email
                    $client->sendEmailWithTemplate(
                        "Billing@BoathouseConnect.com",
                        $user["email"],
                        "platform-payment-declined",
                        [
                            "action_url" => $url
                        ]
                    );

                    //send text message
                    $twilioClient->messages->create(
                        $user["phone_number"], // Text this number
                        [
                            'from' => $senderPhone, // From a valid Twilio number
                            'body' => "bhconnect alert - Declined subscription payment. See notifications for details."
                        ]
                    );
                }
            }

        }

    }

    /**
     * Verifies that all whitelabels in the environment have a Stripe Connected account and that the state of the account
     * is up-to-date. Checking charges_enabled, details_submitted, and payouts_enabled. This should be run several times per day
     */
    public function updateAllWhitelabelConnectedAccounts()
    {
        (new Service_Stripe_Connected_Accounts())->updateAllWhitelabelConnectedAccounts();
    }

    public function processUserWhitelabelPayments()
    {
        $srp = new Service_User_Payments();
        //get all delinquent accounts
        $allAccounts = $srp->getAllOverdueAccounts();
        //attempt payment
        foreach ($allAccounts as $account) {
            $srp->processPayment($account["whitelabel_id"], $account["custid"], $account["email"], $account["outstanding"], $account["user_customer_id"], $account["preferred_payment_method"], $account["whitelabel_account"]);
        }

    }

}