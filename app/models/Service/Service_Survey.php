<?php


class Service_Survey
{

    public function createSurvey($whitelabel_id, $custid, $title, $description, $anonymous, $public)
    {
         $survey_id = (new DB_Surveys())->newSurvey($whitelabel_id, $custid, $title, $description);

         // set public and anonymous
        self::updateSurveyAnonymous($whitelabel_id, $survey_id, $anonymous);
        self::updateSurveyPublic($whitelabel_id,$survey_id, $public);

         return $survey_id;
    }

    public function setReadyToSend($whitelabel_id, $surveyId)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys())->setReadyToSend($whitelabel_id, $surveyId);
        }
    }

    private function getSurveyMeta($whitelabel_id, $surveyId)
    {
        return (new DB_Surveys())->getSurveyById($surveyId, $whitelabel_id);
    }

    public function getSurvey($whitelabel_id, $surveyId)
    {
        $meta = (new DB_Surveys())->getSurveyById($surveyId, $whitelabel_id);
        if (!$meta) {
            return array();
        }

        return [
            "survey_id"         => $meta['survey_id'],
            "whitelabel_id"     => $meta['whitelabel_id'],
            "custid_creator"    => $meta['custid_creator'],
            "title"             => $meta['title'],
            "description"       => $meta['description'],
            "open_time"         => $meta['open_time'],
            "close_time"        => $meta['close_time'],
            "ready_to_send"     => $meta['ready_to_send'],
            "anonymous"         => $meta['anonymous'],
            "sent_status"       => $meta['sent_status'],
            "public"            => $meta['public'],
            "timestamp_created" => $meta['timestamp_created'],
            "recipients"        => self::getRecipients($whitelabel_id, $surveyId),
            "questions"         => self::getSurveyQuestions($surveyId),
            "responses"         => self::getResponses($whitelabel_id, $surveyId, $meta['anonymous'], true)
        ];

    }

    private function getSurveyQuestions($surveyId)
    {
        $questions =  (new DB_Surveys_Questions())->getAllElements($surveyId);
        foreach ($questions as $key=>$field) {
            if ($field["element_type"] == "dropdown") {
                $questions[$key]["options"] = self::getDropdownElements($surveyId, $field["question_id"]);
            }
        }

        return $questions;
    }

    public function updateSurveyTitle($whitelabel_id, $surveyId, $title)
    {
        (new DB_Surveys())->updateTitle($whitelabel_id, $surveyId, $title);
    }

    public function updateSurveyDescription($whitelabel_id, $surveyId, $description)
    {
        (new DB_Surveys())->updateDescription($whitelabel_id, $surveyId, $description);
    }

    public function updateSurveyOpenTime($whitelabel_id, $surveyId, $newOpenTime)
    {
        (new DB_Surveys())->updateOpenTime($whitelabel_id, $surveyId, $newOpenTime);
    }

    public function updateSurveyCloseTime($whitelabel_id, $surveyId, $newCloseTime)
    {
        (new DB_Surveys())->updateCloseTime($whitelabel_id, $surveyId, $newCloseTime);
    }

    public function updateSurveyAnonymous($whitelabel_id, $surveyId, $anonymous)
    {
        if (ucfirst(strtolower($anonymous)) == "Yes" || ucfirst(strtolower($anonymous)) == 'No') {
            (new DB_Surveys())->updateAnonymous($whitelabel_id, $surveyId, ucfirst(strtolower($anonymous)));
        }
    }

    public function updateSurveyPublic($whitelabel_id, $surveyId, $public)
    {
        if (ucfirst(strtolower($public)) == "Yes" || ucfirst(strtolower($public)) == 'No') {
            (new DB_Surveys())->updatePublicStatus($whitelabel_id, $surveyId, ucfirst(strtolower($public)));
        }
    }

    public function getDraftSurveys($whitelabel_id)
    {
        // get all surveys by whitelabel
        $allWhitelabelSurveys = (new DB_Surveys())->getSurveysByWhitelabel($whitelabel_id);
        $draftSurveys         = [];
        foreach ($allWhitelabelSurveys as $surveys) {
            if ($surveys['ready_to_send'] == 'No') {
                $draftSurveys[] = self::getSurvey($whitelabel_id, $surveys['survey_id']);
            }
        }
        return $draftSurveys;
    }

    public function getAllSurveys($whitelabel_id)
    {
        // get all surveys by whitelabel
        $allWhitelabelSurveys = (new DB_Surveys())->getSurveysByWhitelabel($whitelabel_id);
        $draftSurveys         = [];
        foreach ($allWhitelabelSurveys as $surveys) {
            $draftSurveys[] = self::getSurvey($whitelabel_id, $surveys['survey_id']);
        }
        return $draftSurveys;
    }

    private function generateUniqueId($email, $survey_id)
    {
        return md5($survey_id."-".rand(0,9999999999)."-bhc112-".rand(0,9999999999)."-amtbbc123365-".rand(0,9999999999)."-email-".$email."-".time());
    }

    public function addDescriptionElementToSurvey($whitelabel_id, $surveyId, $html)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Questions())->addElement($surveyId,$html, "description");
        }
    }

    public function addTextareaElementToSurvey($whitelabel_id, $surveyId, $label)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Questions())->addElement($surveyId,$label, "textarea");
        }
    }

    public function addDropdownElementToSurvey($whitelabel_id, $surveyId, $label,  array $optionsArray)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            $questionNumber = (new DB_Surveys_Questions())->addElement($surveyId,$label, "dropdown");
            (new DB_Surveys_Dropdowns())->addDropdownData($surveyId, $questionNumber, $optionsArray);
        }
    }

    private function getDropdownElements($surveyId, $questionId)
    {
        $elements = (new DB_Surveys_Dropdowns())->getByQuestionId($surveyId, $questionId);
        foreach ($elements as $key=>$value) {
            if (is_null($value) || $value == '')
                unset($elements[$key]);
        }
        return $elements;
    }

    public function deleteSurveyElement($whitelabel_id, $surveyId, $questionNumber)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Questions())->deleteByQuestionId($surveyId, $questionNumber);
        }
    }

    public function swapElementPositions($whitelabel_id, $surveyId, $question1, $question2)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Questions())->swapPositions($surveyId, $question1, $question2);
        }
    }

    public function addGroupToSurvey($whitelabel_id, $surveyId, $groupId, $stakeholders)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            $DB_Surveys_Recipients = new DB_Surveys_Recipients();

            // get all current recipients
            $currentRecipients = (new DB_Surveys_Recipients())->getAllRecipients($whitelabel_id, $surveyId);

            // get all users in group
            $groupMembers = (new Groups($whitelabel_id))->getGroupMembers($groupId);

            // check if user is already a recipient (by custid)
            foreach ($groupMembers as $gmember) {
                $isRecipient = false;
                foreach ($currentRecipients as $cmember) {
                    if ($cmember["custid"] == $gmember['custid']) {
                        $isRecipient = true;
                        break;
                    }
                }
                if (!$isRecipient) {
                    $DB_Surveys_Recipients->addRecipient($whitelabel_id, $surveyId, $gmember['custid'], self::generateUniqueId($gmember['email'], $surveyId), $gmember['fname']." ".$gmember['lname'], $gmember['email']);
                }
            }

            // check if adding stakeholders
            if ($stakeholders == "Yes") {
                // get all stakeholders in the group
                $stakeholders = (new Groups($whitelabel_id))->getAllStakeholders($groupId);

                // add stakeholders to recipients list if they are not already
                foreach ($stakeholders as $smember) {
                    $isRecipient = false;
                    foreach ($currentRecipients as $cmember) {
                        if ($smember['email'] == $cmember['email']) {
                            $isRecipient = true;
                            break;
                        }
                    }
                    if (!$isRecipient) {
                        $DB_Surveys_Recipients->addRecipient($whitelabel_id, $surveyId, 0, self::generateUniqueId($smember['email'], $surveyId), $smember['name'], $smember['email']);
                    }
                }
            }
        }
    }

    public function getRecipients($whitelabel_id, $surveyId)
    {
        $allRecipients  = (new DB_Surveys_Recipients())->getAllRecipients($whitelabel_id, $surveyId);
        $returnable     = [];
        foreach ($allRecipients as $row) {
            $returnable[] = array(
                "recipient_id"  => $row['recipient_id'],
                "survey_id"     => $row['survey_id'],
                "whitelabel_id" => $row['whitelabel_id'],
                "custid"        => $row['custid'],
                "name"          => $row['name'],
                "email"         => $row['email']
            );
        }
        return $returnable;
    }

    public function removeRecipient($whitelabel_id, $surveyId, $recipientId)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Recipients())->removeRecipient($whitelabel_id, $surveyId, $recipientId);
        }
    }

    public function checkSurveyRecipient($whitelabel_id, $surveyId, $recipientId, $uniqueId)
    {

        $recipient = (new DB_Surveys_Recipients())->getRecipient($whitelabel_id, $surveyId, $recipientId, $uniqueId);
        if (isset($recipient['first_submission']) && $recipient["first_submission"] == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addResponse($whitelabel_id, $surveyId, $uniqueId, $questionId, $response)
    {
        // check that survey belongs to whitelabel and that the survey is still open
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id && $survey['close_time'] >= time()) {
            // add response to the table
            (new DB_Surveys_Responses())->saveResponse($surveyId, $uniqueId, $questionId, $response);
        }
    }

    public function markResponseDone($whitelabel_id, $surveyId, $uniqueId)
    {
        // check that survey belongs to whitelabel
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id) {
            (new DB_Surveys_Recipients())->markSubmitted($whitelabel_id, $surveyId, $uniqueId, $survey['anonymous']);
        }
    }

    public function getMySurveys($whitelabel_id, $custid)
    {
        return (new DB_Surveys_Recipients())->getSurveysByCustid($whitelabel_id, $custid);
    }

    public function deleteSurvey($whitelabel_id, $surveyId)
    {
        // check that survey belongs to whitelabel and that the survey is still open
        $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
        if ($survey['whitelabel_id'] == $whitelabel_id && (($survey['open_time'] >= time() || $survey['open_time'] == 0) || $survey["ready_to_send"] == 'No')) {
            // delete responses
            (new DB_Surveys_Responses())->deleteAllBySurvey($surveyId);
            // delete recipients
            (new DB_Surveys_Recipients())->deleteAllBySurvey($surveyId);
            // delete dropdowns
            (new DB_Surveys_Dropdowns())->deleteAllBySurvey($surveyId);
            // delete questions
            (new DB_Surveys_Questions())->deleteAllBySurvey($surveyId);
            // delete survey
            (new DB_Surveys())->deleteAllBySurvey($surveyId);
            return array("Success"=>"Survey, and all associated data, has been deleted");
        } else {
            return array("Error"=>"Survey has a open_time before now OR the survey has already started");
        }
    }

    public function getResponses($whitelabel_id, $surveyId, $anonymous, $verified = false)
    {
        if (!$verified) {
            $survey = self::getSurveyMeta($whitelabel_id, $surveyId);
            if ($survey['whitelabel_id'] != $whitelabel_id) {
                return [];
            }
        }

        // get all responses
        $allResponses                           = (new DB_Surveys_Responses())->getAllResponses($surveyId);
        $formattedResponses                     = array();
        $formattedResponses["by_recipient"]     = array();
        $formattedResponses["question_summary"] = array();

        // calculate responses by recipient
        foreach ($allResponses as $response) {
            if ($anonymous == "Yes") {
                $formattedResponses["by_recipient"][$response["unique_id"]][] = [
                    "question_id"   => $response["question_id"],
                    "response_time" => $response["submission_time"],
                    "response"      => $response["response"]
                ];
            } else {
                $formattedResponses["by_recipient"][$response["recipient_id"]][] = [
                    "question_id"   => $response["question_id"],
                    "response_time" => $response["submission_time"],
                    "custid"        => $response['custid'],
                    "name"          => $response["name"],
                    "email"         => $response["email"],
                    "response"      => $response["response"]
                ];
            }

        }

        // calculate responses by question
        foreach ($allResponses as $response) {
            if ($response['element_type'] == "textarea") {
                $formattedResponses["question_summary"][strval($response["question_id"])][] = [
                    "response"      => $response["response"]
                ];
            } else if ($response['element_type'] == "dropdown") {
                if (!isset($formattedResponses["question_summary"][strval($response["question_id"])])) {
                    $formattedResponses["question_summary"][strval($response["question_id"])] = [
                        "selection_1"  => 0,
                        "selection_2"  => 0,
                        "selection_3"  => 0,
                        "selection_4"  => 0,
                        "selection_5"  => 0,
                        "selection_6"  => 0,
                        "selection_7"  => 0,
                        "selection_8"  => 0,
                        "selection_9"  => 0,
                        "selection_10" => 0,
                        "selection_11" => 0,
                        "selection_12" => 0,
                        "selection_13" => 0,
                        "selection_14" => 0,
                        "selection_15" => 0,
                        "selection_16" => 0,
                        "selection_17" => 0,
                        "selection_18" => 0,
                        "selection_19" => 0,
                        "selection_20" => 0,
                    ];
                    $formattedResponses["question_summary"][strval($response["question_id"])][$response["response"]]++;
                } else {
                    $formattedResponses["question_summary"][strval($response["question_id"])][$response["response"]]++;
                }
            }

        }

        return $formattedResponses;
    }
}
