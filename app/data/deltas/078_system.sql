--
-- Table structure for table `__system`
--

CREATE TABLE IF NOT EXISTS `__system` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `sys_key` text NOT NULL,
                                          `sys_value` text NOT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `__system`
--

TRUNCATE TABLE `__system`;
--
-- Dumping data for table `__system`
--

INSERT INTO `__system` (`id`, `sys_key`, `sys_value`) VALUES
(1, 'bhcPrivateKey', 'fjw93u7vnhj38gbh398');
