<?php


class Service_External_Keys
{
    public static function getToken($service)
    {
        $env  = Environment::getEnvCode();
        $keys = (new DB_External_Keys())->getByServiceEnv($service, $env);
        return $keys["token"];
    }

    public static function getKeys($service)
    {
        $env = Environment::getEnvCode();
        return (new DB_External_Keys())->getByServiceEnv($service, $env);
    }

}