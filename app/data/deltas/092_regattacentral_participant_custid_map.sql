--
-- Table structure for table `regattacentral_participant_custid_map`
--

CREATE TABLE IF NOT EXISTS `regattacentral_participant_custid_map` (
                                                                       `participant_id` int(11) NOT NULL,
                                                                       `custid` int(11) NOT NULL,
                                                                       `timestamp_updated` int(11) NOT NULL,
                                                                       `uuid` varchar(100) NOT NULL,
                                                                       `fname` varchar(100) NOT NULL,
                                                                       `lname` varchar(100) NOT NULL,
                                                                       `byear` int(11) NOT NULL,
                                                                       `bmonth` int(11) NOT NULL,
                                                                       `bday` int(11) NOT NULL,
                                                                       PRIMARY KEY (`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
