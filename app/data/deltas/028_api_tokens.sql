--
-- Table structure for table `api_tokens`
--

CREATE TABLE IF NOT EXISTS `api_tokens` (
                                            `token_id` int(11) NOT NULL AUTO_INCREMENT,
                                            `custid` int(11) NOT NULL,
                                            `token_hash` varchar(32) NOT NULL,
                                            `expires` int(11) NOT NULL,
                                            `type` varchar(10) NOT NULL DEFAULT 'api',
                                            `created_at` int(11) NOT NULL,
                                            `last_used` int(11) NOT NULL,
                                            `descr` text NOT NULL,
                                            PRIMARY KEY (`token_id`),
                                            KEY `fk_custid_apitoken` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `api_tokens`
--
ALTER TABLE `api_tokens`
    ADD CONSTRAINT `fk_custid_apitoken` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE;

COMMIT;
