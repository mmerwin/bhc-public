<?php

// use this model to determine which environment we are located in and return necessary env variables
class Environment
{
    public static function retrieveApiUrl($currentEnv = null){
        if (!is_null($currentEnv)) {
            $env = $currentEnv;
        } else {
            $env = $_SERVER["HTTP_HOST"];
        }
        switch ($env) {
            case "localhost":
            case "rowerformsapi.localhost":
                $url = "http://localhost";
                break;
            case "test-api.boathouseconnect.com":
                $url = "http://test-api.boathouseconnect.com";
                break;
            case "dev-api.boathouseconnect.com":
                $url = "http://dev-api.boathouseconnect.com";
                break;
            case "uat-api.boathouseconnect.com":
                $url = "https:/uat-api.boathouseconnect.com";
                break;
            case "app.boathouseconnect.com":
            case "boathouseconnect.com":
            default:
                $url = "https://api.boathouseconnect.com";
                break;
        }
        return $url;
    }

    public static function getEnvCode($currentEnv = null)
    {
        if (!is_null($currentEnv)) {
            $env = $currentEnv;
        } else {
            if (!isset($_SERVER["HTTP_HOST"])) {
                return self::getEnvFromHostname();
            } else {
                $env = $_SERVER["HTTP_HOST"];
            }
        }
        switch($env) {
            case "test-api.boathouseconnect.com":
                return "TEST";
                break;
            case "uat-api.boathouseconnect.com":
            return "UAT";
                break;
            case "localhost":
            case "rowerformsapi.localhost":
            case "dev-api.boathouseconnect.com":
                return "DEV";
                break;
            case "api.boathouseconnect.com":
            case "boathouseconnect.com":
            default:
                return "PROD";
                break;
        }
    }

    public static function retrieveAppUrl($envCode)
    {
        if (!is_null($envCode)) {
            $env = strtoupper($envCode);
        } else{
            $env = self::getEnvFromHostname();
        }
        switch ($env) {
            case "DEV":
                return "http://rowerformsapp.localhost/";
                break;
            case "TEST":
                return "http://test-app.boathouseconnect.com/";
                break;
            case "UAT":
                return "http://uat.boathouseconnect.com/";
                break;
            case "PROD":
            default:
                return "https://boathouseconnect.com/";
                break;
        }
    }

    public static function getExternalKeys($service)
    {
        $env = self::getEnvFromHostname();
        return (new DB_External_Keys())->getByServiceEnv($service, $env);
    }

    public static function getEnvFromHostname()
    {
        // default must be set as DEV since it is unknown what the hostname could be in development
        switch (gethostname()) {
            case "Merwin-Test":
                return "TEST";
                break;
            case "BoathouseConnect":
            case "boathouseconnect.com":
                return "PROD";
                break;
            case "bhconnect-uat":
                return "UAT";
                break;
            default:
                return "DEV";
        }
    }
}