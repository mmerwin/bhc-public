<?php


class DB_Reward_History extends DataModel
{
    protected $schema = "reward_history";

    public function add($custid, $whitelabel_id, $triggerId, $points)
    {
        $this->DB->query("INSERT INTO {$this->schema} (custid, whitelabel_id, trigger_id, points_earned, timestamp) VALUES (?, ?, ?, ?, ?)", array($custid, $whitelabel_id, $triggerId, $points, time()));
    }

    public function getWhitelabelHistory($whitelabel_id)
    {
        return $this->DB->query("SELECT rh.reward_hist_id, rh.whitelabel_id, rh.custid, u.fname, u.lname, rh.trigger_id, rh.points_earned, rh.timestamp, rt.action, rt.descr
                                        FROM {$this->schema} rh
                                        LEFT JOIN reward_triggers rt
                                        ON rt.reward_trigger_id = rh.trigger_id
                                        LEFT JOIN users u
                                        ON u.custid = rh.custid
                                        WHERE rh.whitelabel_id = ? ORDER BY timestamp DESC", array($whitelabel_id))->fetchAll();
    }

    public function getCustidHistory($custid, $whitelabel_id)
    {
        return $this->DB->query("SELECT rh.reward_hist_id, rh.whitelabel_id, rh.custid, u.fname, u.lname, rh.trigger_id, rh.points_earned, rh.timestamp, rt.action, rt.descr
                                        FROM {$this->schema} rh
                                        LEFT JOIN reward_triggers rt
                                        ON rt.reward_trigger_id = rh.trigger_id
                                        LEFT JOIN users u
                                        ON u.custid = rh.custid
                                        WHERE rh.whitelabel_id = ? AND rh.custid = ? ORDER BY timestamp DESC", array($whitelabel_id, $custid))->fetchAll();
    }

    public function getLastTriggerTimeByWhitelabel($whitelabel_id, $triggerId)
    {
        return $this->DB->query("SELECT timestamp FROM {$this->schema} WHERE whitelabel_id = ? AND trigger_id = ? ORDER BY timestamp DESC LIMIT 1", array($whitelabel_id, $triggerId))->fetchArray();
    }

    public function getLastTriggerTimeByCustid($whitelabel_id, $custid, $triggerId)
    {
        return $this->DB->query("SELECT timestamp FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND trigger_id = ? ORDER BY timestamp DESC LIMIT 1", array($whitelabel_id, $custid, $triggerId))->fetchArray();
    }

    public function sumAllPointsInTable()
    {
        return $this->DB->query("SELECT sum(points_earned) as earned FROM {$this->schema}")->fetchArray();
    }

    public function getPointsEarnedBetweenDates($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND timestamp >= ? AND timestamp <= ?",
            array($whitelabel_id, $startTime, $endTime))->fetchAll();
    }
}