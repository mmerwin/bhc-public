<?php


class DB_Lineup_Skills extends DataModel
{
    protected $schema = "lineup_skills";

    public function getSkills()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} ORDER BY skill_id ASC")->fetchAll();
    }
}