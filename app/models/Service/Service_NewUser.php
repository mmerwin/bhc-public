<?php


class Service_NewUser
{
    public static function addNewUser($fname, $lname, $phone, $sex, $email, $password, $byear, $bmonth, $bday)
    {
        // insert user and retrieve custid
        $custid = (new DB_Users())->addNewUser($fname, $lname, $phone, $sex, $byear, $bmonth,$bday);

        // insert login info
        $loginid = (new DB_Logins())->addLogin($custid, $email, $password);

        // check if user has been pre-authorized to a whitelabel
        $preauth = (new DB_PreAuthorized_New_Users())->checkForPreauth($email);
        if (count($preauth) > 0) {
            // add whitelabel_affiliation
            foreach ($preauth as $auth) {
                Service_Affiliations::addAffiliation($custid, $auth['whitelabel_id'], $email);
            }
        }
        // create Stripe account
        (new Service_User_Payments())->getCustomerAccount($custid);

        // add welcome notification
        $mergeTags = array("fname"=>$fname);
        (new Notification($custid, 0))->newNotification(1, $mergeTags,[]);

        return array("Status"=>"Success", "custid"=>$custid, "loginid"=>$loginid);
    }
    
}