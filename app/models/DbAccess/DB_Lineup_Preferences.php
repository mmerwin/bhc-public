<?php


class DB_Lineup_Preferences extends DataModel
{
    protected $schema = "lineup_preferences";

    public function setPreferences($custid, $port, $starboard, $sculling, $coxswain)
    {
        $this->DB->query("INSERT INTO {$this->schema} (custid, port, starboard, sculling, coxwain, last_updated) VALUES (?, ?, ?, ?, ?, ?)", array($custid, $port, $starboard, $sculling, $coxswain, time()));
    }

    public function updatePort($custid, $skill)
    {
        $this->DB->query("UPDATE {$this->schema} SET port = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($skill,time(), $custid));
    }

    public function updateStarboard($custid, $skill)
    {
        $this->DB->query("UPDATE {$this->schema} SET starboard = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($skill,time(), $custid));
    }

    public function updateSculling($custid, $skill)
    {
        $this->DB->query("UPDATE {$this->schema} SET sculling = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($skill,time(), $custid));
    }

    public function updateCoxswain($custid, $skill)
    {
        $this->DB->query("UPDATE {$this->schema} SET coxwain = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($skill,time(), $custid));
    }

    public function retrievePreferences($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? LIMIT 1", array($custid))->fetchArray();
    }

}