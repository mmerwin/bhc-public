<?php

/**
 * Used specifically for communication between official BoathouseConnect Web App and API
 * Not to be used by individuals or whitelabels
 * Only used in server-to-server calls, not from javascript/ajax. Never from client-side!
 */
require_once (dirname(__FILE__).'/../vendor/autoload.php');
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
class bhcprivate extends Controller
{

    private function AppToken()
    {
        $token = $this->param("appTokenPrivate");
        if ($token != "fjw93u7vnhj38gbh398") //adjust this token in Controller method of APP framework
        {
            die("Error, Invalid request. BoathouseConnect private token required to access this endpoint. This method is not accessible to non-approved apps");
        }
    }

    public function getAllWhitelabels()
    {
        self::AppToken();

    }

    /**
     * Provides an array of possible permission levels and descriptions
     * @api
     * @return array of permissions.
     */
    public function permissions()
    {
        self::AppToken();
        $permissions = Service_Permissions::getAllPermissionOptions();
        $this->view('json',$permissions);
    }

    public function addNewWhitelabel()
    {
        self::AppToken();
        $rc_orgid  = $this->param("rc_orgid");
        $rcDetails = Service_Whitelabel::getRcOrgDetails($rc_orgid);
        //add the user before adding the whitelabel
        $fname      = $this->param("fname");
        $lname      = $this->param("lname");
        $phone      = $this->param("phone");
        $sex        = $this->param("sex");
        $email      = $this->param("email");
        $password   = $this->param("password");
        $bday       = $this->param("bday");
        $bmonth     = $this->param("bmonth");
        $byear      = $this->param("byear");
        $user       = Service_NewUser::addNewUser($fname, $lname, $phone, $sex, $email, $password, $byear, $bmonth, $bday);
        $custid     = $user['custid'];
        //add whitelabel
        $whitelabel_id = Service_Whitelabel::addWhitelabelFromRcOrg($rc_orgid, $custid);

        //add user to the whitelabel
        Service_Affiliations::addAffiliation($custid, $whitelabel_id);

        //add default groups to whitelabel
        $groups = new Groups($whitelabel_id);
        $groups->installDefaultGroups();

        //add creating user to Super Admin group
        $superAdminId = $groups->getSuperAdminId();
        $groups->addUserToGroup($superAdminId, $custid);

    }

    public function addNewUser()
    {
        self::AppToken();
        $fname      = $this->param("fname");
        $lname      = $this->param("lname");
        $phone      = $this->param("phone");
        $sex        = $this->param("sex");
        $email      = $this->param("email");
        $password   = $this->param("password");
        $bday       = $this->param("bday");
        $bmonth     = $this->param("bmonth");
        $byear      = $this->param("byear");

        Service_NewUser::addNewUser($fname, $lname, $phone, $sex, $email, $password, $byear, $bmonth, $bday);
    }

    public function checkIfUserExists()
    {
        self::AppToken();
        $email = $this->param("email");
        $result = Service_CredentialCheck::checkIfLoginExists($email);
        $this->view('json', $result);

    }

    public function findWhitelabelsForcustidPrivate()
    {
        self::AppToken();
        $custid       = $this->param('custid');
        $affiliations = (new User($custid))->getAffiliations();
        $this->view('json', $affiliations);
    }

    public function getUnclaimedUsOrgs()
    {
        self::AppToken();
        $orgs = Service_Whitelabel::getUnclaimedOrgs();
        $this->view('json', $orgs);
    }

    public function checkIfOrgClaimed()
    {
        self::AppToken();
        $rc_orgid = $this->param('rc_orgid');
        $result   = Service_Whitelabel::checkIfRcOrgIsClaimed($rc_orgid);
        $this->view('json', $result);
    }

    public function getRcOrgDetails()
    {
        self::AppToken();
        $rc_orgid = $this->param('rc_orgid');
        $result   = Service_Whitelabel::getRcOrgDetails($rc_orgid);
        $this->view('json', $result);
    }

    public function getAllPublicApiTokens()
    {
        self::AppToken();
        $custid = Service_ApiTokenValidation::getCustidFromToken($this->param("token"));
        $tokens = Service_ApiTokenValidation::getAllPublicTokens($custid);
        $this->view('json', $tokens);
    }

    public function collectiveRewardsPoints()
    {
        self::AppToken();
        $rewards = new Service_Rewards(0);
        $data    = $rewards->getPointsEarnedByAllWhitelabels();

        $this->view("json", $data);
    }

    public function getRewardsTriggers()
    {
        self::AppToken();
        $rewards = new Service_Rewards(0);
        $data    = $rewards->getTriggers();

        $this->view("json", $data);

    }

    public function getSurvey()
    {
        self::AppToken();
        $surveyId       = $this->param("survey_id");
        $whitelabel_id  = $this->param("whitelabel_id");
        $recipientId    = $this->param("recipient_id");
        $uniqueId       = $this->param("unique_id");

        //check that survey has not been completed yet
        $survey  = new Service_Survey();
        $allowed = $survey->checkSurveyRecipient($whitelabel_id, $surveyId, $recipientId, $uniqueId);
        if ($allowed) {
            $this->view("json", $survey->getSurvey($whitelabel_id,$surveyId));

        } else {
            $this->view("json", array("Error"=>"No User / Already Submitted"));
        }
    }

    public function submitSurveyResponse()
    {
        self::AppToken();
        $surveyId           = $this->param("survey_id");
        $whitelabel_id      = $this->param("whitelabel_id");
        $uniqueId           = $this->param("unique_id");
        $questionId         = $this->param("question_id");
        $questionResponse   = $this->param("response");
        $finalResponse      = $this->param("final_response");

        if (is_null($finalResponse)) {
            (new Service_Survey())->addResponse($whitelabel_id, $surveyId, $uniqueId, $questionId, $questionResponse);
        } else {
            (new Service_Survey())->markResponseDone($whitelabel_id, $surveyId, $uniqueId);
        }

    }

    public function getUSRowingWaivers()
    {
        self::AppToken();
        $whitelabel_id      = $this->param("whitelabel_id");
        $whitelabelWaivers  = (new Service_USRowing())->getWhitelabelWaivers($whitelabel_id);
        $rawWaivers         = (new Service_USRowing())->getRawByWhitelabel($whitelabel_id);

        $data = array(
            "whitelabel_waivers" =>$whitelabelWaivers,
            "usrowing_data"      =>$rawWaivers
        );
        $this->view("json", $data);
    }

    public function getPracticeInfo()
    {
        self::AppToken();
        $practiceId     = $this->param("practice_id");
        $whitelabel_id  = $this->param("whitelabel_id");

        $practices      = (new Service_Practices($whitelabel_id))->getPracticeById($whitelabel_id, $practiceId);

        $this->view("json", $practices);
    }

    public function savePracticeSurveyResults()
    {
        self::AppToken();
        $practiceId      = $this->param("practice_id");
        $whitelabel_id   = $this->param("whitelabel_id");
        $custid          = $this->param("custid");
        //survey responses
        $lineups         = $this->param("question_lineups");
        $sessionplan     = $this->param("question_sessionplan");
        $equipmentIssues = ($this->param("question_equipment") === "Yes") ? $this->param("equipment_text") : "";
        $generalFeedback = $this->param("question_generalfeedback");

        (new Service_Lineups())->submitPracticeSurvey($whitelabel_id, $practiceId, $custid, $lineups, $sessionplan, $equipmentIssues, $generalFeedback);
    }

    public function getWhitelabelSettings()
    {
        self::AppToken();
        $whitelabel_id = $this->param("whitelabel_id");
        $settings      = WhitelabelSettings::getAllSettings($whitelabel_id);

        $this->view("json", $settings);
    }

    public function sendContactFormToBoathouseConnect()
    {
        self::AppToken();
        $name            = $this->param("name");
        $email           = $this->param("email");
        $phone           = $this->param("phone");
        $message         = $this->param("message", "POST");

        $client          = new PostmarkClient(Service_External_Keys::getToken("postmark-platform"));

        $client->sendEmailWithTemplate(
            "Messaging@boathouseconnect.com",
            "Michael@BoathouseConnect.com",
            "landing-contact-form",
            [   "name"    => $name,
                "email"   => $email,
                "phone"   => $phone,
                "message" => $message
            ]
        );

    }

    public function getStripePublicKey()
    {
        self::AppToken();
        $key = Service_External_Keys::getToken("stripe-public");
        $result = array(
            "stripe-public" => $key,
            "environment"   => Environment::getEnvCode()
        );
        $this->view("json", $result);
    }

    public function getStripeAccountUrl()
    {
        self::AppToken();
        $whitelabel_id = $this->param("whitelabel_id");
        $urlType       = $this->param("url_type");
        $returnUrl     = $this->param("return_url");
        $refreshUrl    = $this->param("refresh_url");

        $url = (new Service_Stripe_Connected_Accounts())->getWhitelabelUrl($whitelabel_id, $refreshUrl, $returnUrl, $urlType);
        $this->view("json", $url);
    }

    /*
     * ONLY AVAILABLE IN LOWER ENVIRONMENTS
     * DISABLED IN PRODUCTION (PROD)
    */
    public function deleteConnectedAccount()
    {
        if (Environment::getEnvCode() != "PROD") {
            self::AppToken();
            $accountId = $this->param("account_id");
            (new Service_Stripe_Connected_Accounts())->deleteConnectedAccount($accountId);
        } else {
            die("DISABLED IN PRODUCTION, ONLY AVAILABLE IN LOWER ENVIRONMENTS");
        }

    }

    public function retrieveConnectedAccount()
    {
        self::AppToken();
        $whitelabel_id = $this->param("whitelabel_id");
        $data          = (new Service_Stripe_Connected_Accounts())->checkIfWhitelabelConnectedAccountSetup($whitelabel_id);
        $this->view("json", $data);

    }

    public function unsubscribeStakeholderFromWhitelabel()
    {
        self::AppToken();
        $contactEmail   = $this->param("contact_email");
        $whitelabel_id  = $this->param("whitelabel_id");

        $groups = new Groups($whitelabel_id);
        $groups->unsubscribeStakeholder($contactEmail);
    }

    public function getPublicCalendar()
    {
        self::AppToken();
        $whitelabel_id = $this->param("whitelabel_id");
        $securityHash  = $this->param("security_hash");
        if (md5("wlcalendar-".$whitelabel_id) == $securityHash) {
            $calendar = new Service_Calendar($whitelabel_id);
            $wlCalendar = $calendar->getWhitelabelCalendar(true);
        } else {
            $wlCalendar = [];
        }

        $this->view("json", $wlCalendar);
    }

    public function getPossibleParticipantIds($custidSet = null)
    {
        self::AppToken();
        if (is_null($custidSet)) {
            $custid         = $this->param("custid");
        } else {
            $custid = $custidSet;
        }
        $user           = new User($custid);

        // get participant ids from RegattaCentral API
        $regattaCentral = new Integrations_RegattaCentral();
        $result         = json_decode($regattaCentral->getParticipantIds($user->fname, $user->lname, $user->birthYear, $user->birthMonth, $user->birthDay), true);

        $rcIds = [];
        foreach ($result["data"] as $id) {
            foreach ($id["memberId"] as $orgRow) {
                $rcIds[] = [
                    "participant_id"    => $id["participantId"],
                    "member_id"         => $orgRow["memberId"],
                    "organization_id"   => $orgRow["organizationId"],
                    "organization_name" => $orgRow["organization"],
                    "first_initial"     => $id["firstName"],
                    "last_name"         => $id["lastName"],
                    "birthday"          => $user->birthYear."-".$user->birthMonth."-".$user->birthDay,
                    "uuid"              => $id["uuid"]
                ];
            }
        }

        // claim participant ids
        $table = new DB_Regattacentral_Participant_Custid_Map();
        foreach ($rcIds as $participantId) {
            $table->claimParticipantId($custid, $participantId["participant_id"], $participantId["uuid"],
                $participantId["first_initial"], $participantId["last_name"], $user->birthYear, $user->birthMonth,
                $user->birthDay);
        }

        // fetch all participant id's
        $allIds = $table->getParticipantIdsByCustid($custid);

        if (is_null($custidSet)) {
            $this->view("json", $allIds);
        }
    }

    public function getAttendedRegattasByCustid()
    {
        self::AppToken();
        $custid     = $this->param("custid");
        $this->getPossibleParticipantIds($custid);
        $regattas   = (new Service_Rowstats())->getRegattasByCustid($custid);
        $this->view("json", $regattas);
    }

}
