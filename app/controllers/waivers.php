<?php


class waivers extends Controller
{

    public function syncUSRowing()
    {
        $username = $this->param("username", "POST");
        $password = $this->param("password", "POST");
        $archived = $this->param("archived");
        if (!$archived) {
            $archived = "F";
        }

        $whitelabel = (new DB_Whitelabels())->findNameById($_SESSION["whitelabel_id"]);
        $rc_orgid = $whitelabel[0]["rc_orgid"];

        $credentials = array(
            "username"=>$username,
            "password"=>$password,
            "rc_orgid"=>$rc_orgid
        );

        $data = (new Service_USRowing())->fetchUpdates($_SESSION["whitelabel_id"], $_SESSION["custid"], $credentials, $archived);
        $this->view("json", $data);
    }

    public function getLastSync()
    {
        $data = (new Service_USRowing())->getLastSyncTime($_SESSION["whitelabel_id"]);
        $this->view("json", $data);
    }

    public function updateUsrowingWaivers()
    {
        $USRowingService = new Service_USRowing();
        $allConfirmedWhitelabels = $USRowingService->getAllConfirmedCredentials();

        foreach ($allConfirmedWhitelabels as $whitelabel) {
            //get unarchived member waivers
            $USRowingService->fetchUpdates($whitelabel["whitelabel_id"], null, null, "F", false);

            //get archived member waivers
            $USRowingService->fetchUpdates($whitelabel["whitelabel_id"], null, null, "T", false);
        }
    }

}