<?php


class Service_Whitelabel
{
    public static function findNameById($whitelabel_id)
    {
        return (new DB_Whitelabels())->findNameById($whitelabel_id);
    }
  
    public function getUnclaimedOrgs()
    {
        return (new DB_Regattacentral_Orgs())->getUnclaimedOrgs("US");
    }
  
    public function checkIfRcOrgIsClaimed($rc_orgid)
    {
        return (new DB_Regattacentral_Orgs())->checkIfOrgClaimed($rc_orgid);
    }
    
    public function getRcOrgDetails($rc_orgid)
    {
        return (new DB_Regattacentral_Orgs())->getRcOrgDetails($rc_orgid);
    }
  
    public function addWhitelabelFromRcOrg($rc_orgid, $custid, $planId = 3)
    {
        $rcDetails      = (new DB_Regattacentral_Orgs())->getRcOrgDetails($rc_orgid);
        $newWhitelabel  = (new DB_Whitelabels())->addWhitelabel($rcDetails['name'], $rc_orgid, $custid, $rcDetails['abbreviation']);
        // set rc_orgid as claimed
        (new DB_Regattacentral_Orgs())->claimOrg($rc_orgid);

        $whitelabel_id = $newWhitelabel["whitelabel_id"];

        // create whitelabel_plans entry
        (new DB_Whitelabel_Plans())->addWhitelabelToPlans($whitelabel_id, (time()+2592000), $planId, $stripeCustomerId = '');
        $plansService = (new Service_Whitelabel_Subscriptions($whitelabel_id));
        $plansService->createWhitelabelCustomer();

        // create Stripe Connected Account
        Service_Stripe_Connected_Accounts::checkForWhitelabelConnectedAccount($whitelabel_id);

        return $newWhitelabel['whitelabel_id'];
    }

    public static function getAllWhitelabels()
    {
        return (new DB_Whitelabels())->getAllWhitelabels();
    }

    public static function getNonAffiliatedWhitelabels($custid)
    {
        return (new DB_Whitelabels())->getNonAffiliatedWhitelabels($custid);
    }

    public static function getJoinRequestsByCustid($custid)
    {
        return (new DB_Whitelabel_Join_Requests())->getRequest($custid);
    }

    public function updateTimezone($whitelabel_id, $timezone)
    {
        WhitelabelSettings::updateValue($whitelabel_id, "wlchecklist.timezone", $timezone);
        (new DB_Whitelabels())->updateTimezone($whitelabel_id, $timezone);
    }
}