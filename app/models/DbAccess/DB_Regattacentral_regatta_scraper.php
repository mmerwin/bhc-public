<?php


class DB_Regattacentral_regatta_scraper extends DataModel
{
    protected $schema = "regattacentral_regatta_scraper";

    public function getUnscraped($limit)
    {
        $limitSecure = intval($limit);
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE scraped = 'no' ORDER BY regatta_id DESC LIMIT {$limitSecure}")->fetchAll();
    }

    public function updateAsScraped($regattaId)
    {
        $this->DB->query("UPDATE {$this->schema} SET scraped = 'yes' WHERE regatta_id = ? LIMIT 1", array($regattaId));
    }

    public function addRegattaId($regattaId)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (regatta_id, scraped) VALUES (?, 'no')", array($regattaId));
    }

}