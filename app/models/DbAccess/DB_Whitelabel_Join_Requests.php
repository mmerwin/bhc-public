<?php


class DB_Whitelabel_Join_Requests extends DataModel
{
    protected $schema = "whitelabel_join_requests";

    public function addRequest($custid, $whitelabel_id)
    {
        $this->DB->query("INSERT INTO {$this->schema} (custid, whitelabel_id, status, timestamp) VALUES (?, ?, ?, ?)", array($custid, $whitelabel_id, "Pending", time()));
    }

    public function getRequest($custid, $whitelabel_id=null)
    {
        if(is_null($whitelabel_id)){
            return $this->DB->query("SELECT tmp.custid, tmp.whitelabel_id, wl.name as whitelabel_name, tmp.status, tmp.timestamp FROM (SELECT custid, whitelabel_id, timestamp, status FROM `whitelabel_join_requests`  wjr
                                            UNION ALL (SELECT ua.custid, ua.whitelabel_id, ua.created_at as timestamp, 'Approved' as status FROM user_affiliations ua WHERE whitelabel_id NOT IN 
                                                       (SELECT whitelabel_id FROM whitelabel_join_requests WHERE custid = ?))) as tmp
                                                       LEFT JOIN whitelabels wl
                                                       ON wl.whitelabel_id = tmp.whitelabel_id
                                            WHERE tmp.custid = ? ORDER BY tmp.timestamp DESC", array($custid, $custid))->fetchAll();
        } else {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? AND whitelabel_id = ?", array($custid, $whitelabel_id))->fetchAll();
        }
    }

    public function updateRequest($custid,$whitelabel_id, $newStatus)
    {
        $this->DB->query("UPDATE {$this->schema} SET status = ? WHERE custid = ? AND whitelabel_id = ? LIMIT 1", array($newStatus, $custid, $whitelabel_id));
        $this->DB->query("UPDATE {$this->schema} SET timestamp = ? WHERE custid = ? AND whitelabel_id = ? LIMIT 1", array(time(), $custid, $whitelabel_id));
    }

    public function getRequests($whitelabel_id, $status)
    {
        return $this->DB->query("SELECT jr.custid, jr.status,jr.timestamp,u.fname,u.lname, jr.whitelabel_id
                                        FROM whitelabel_join_requests jr
                                        LEFT JOIN users u
                                        ON jr.custid = u.custid WHERE jr.whitelabel_id = ? AND jr.status = ?", array($whitelabel_id, $status))->fetchAll();
    }

    public function getRequestsZapier($whitelabel_id, $status)
    {
        return $this->DB->query("SELECT jr.custid, jr.status,jr.timestamp,u.fname,u.lname, jr.whitelabel_id, l.email
                                        FROM whitelabel_join_requests jr
                                        LEFT JOIN users u
                                        ON jr.custid = u.custid 
                                        LEFT JOIN logins l 
                                        ON l.custid = u.custid
                                        WHERE jr.whitelabel_id = ? AND jr.status = ?", array($whitelabel_id, $status))->fetchAll();
    }

}