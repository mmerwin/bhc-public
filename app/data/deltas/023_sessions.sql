--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
                                          `id` varchar(32) NOT NULL,
                                          `access` int(10) UNSIGNED DEFAULT NULL,
                                          `data` text DEFAULT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;
