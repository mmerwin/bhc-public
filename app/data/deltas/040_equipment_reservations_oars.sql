--
-- Table structure for table `equipment_reservations_oars`
--

CREATE TABLE IF NOT EXISTS `equipment_reservations_oars` (
                                                             `reservation_id` int(11) NOT NULL,
                                                             `oar_id` int(11) NOT NULL,
                                                             `whitelabel_id` int(11) NOT NULL,
                                                             PRIMARY KEY (`reservation_id`,`oar_id`,`whitelabel_id`),
                                                             KEY `fk_whitelabel_id_reservations_oars` (`whitelabel_id`),
                                                             KEY `fk_oar_id_reservations_oars` (`oar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_reservations_oars`
--
ALTER TABLE `equipment_reservations_oars`
    ADD CONSTRAINT `fk_oar_id_reservations_oars` FOREIGN KEY (`oar_id`) REFERENCES `equipment_oars` (`oar_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_reservation_id_oars` FOREIGN KEY (`reservation_id`) REFERENCES `equipment_reservations` (`reservation_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_reservations_oars` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
