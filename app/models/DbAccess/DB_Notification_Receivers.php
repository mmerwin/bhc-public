<?php


class DB_Notification_Receivers extends DataModel
{
    protected $schema = "notification_receivers";

    public function addNotification($templateId, $custid, $whitelabel_id, $endpointParams, $mergeTags)
    {
        $this->DB->query("INSERT INTO {$this->schema} (template_id, custid, whitelabel_id, created_at, read_at, endpoint_params, merge_tags) VALUES (?, ?, ?, ?,  0, ?, ?)",
            array($templateId, $custid, $whitelabel_id, time(), json_encode($endpointParams), json_encode($mergeTags)));
    }

    public function readNotification($receiptId, $custid, $whitelabel_id)
    {
        $this->DB->query("UPDATE {$this->schema} SET read_at = ? WHERE receipt_id = ? AND custid = ? AND whitelabel_id = ?",array(time(), $receiptId, $custid, $whitelabel_id));
    }

    public function getNotifications($custid, $whitelabel_id, $since, $max, $read)
    {
        if($max >= 30)
        {
            $limit = "LIMIT 30";
        } else {
            $limit = $this->verifyLimit($max);
        }

        if($read != "all")
        {
            return $this->DB->query("SELECT nr.receipt_id, nr.whitelabel_id, nr.created_at, nr.read_at, nr.endpoint_params, nr.merge_tags, nt.title, nt.descr, nt.message, nt.icon, nt.btn, nt.endpoint, nt.endpoint_title
                                        FROM notification_receivers nr
                                        LEFT JOIN notification_templates nt
                                        ON nt.template_id = nr.template_id
                                        WHERE nr.custid = ? AND (nr.whitelabel_id = ? OR nr.whitelabel_id = 0) AND nr.receipt_id > ? AND nr.read_at = ? ORDER BY nr.receipt_id DESC {$limit}",
                array($custid, $whitelabel_id, $since, $read))->fetchAll();
        } else
            {
                return $this->DB->query("SELECT nr.receipt_id, nr.whitelabel_id, nr.created_at, nr.read_at, nr.endpoint_params, nr.merge_tags, nt.title, nt.descr, nt.message, nt.icon, nt.btn, nt.endpoint, nt.endpoint_title
                                        FROM notification_receivers nr
                                        LEFT JOIN notification_templates nt
                                        ON nt.template_id = nr.template_id
                                        WHERE nr.custid = ? AND (nr.whitelabel_id = ? OR nr.whitelabel_id = 0) AND nr.receipt_id > ? ORDER BY nr.receipt_id DESC {$limit}",
                    array($custid, $whitelabel_id, $since))->fetchAll();
        }

    }

    public function getNotificationById($custid, $whitelabel_id, $receiptId)
    {
        return $this->DB->query("SELECT nr.receipt_id, nr.whitelabel_id, nr.created_at, nr.read_at, nr.endpoint_params, nr.merge_tags, nt.title, nt.descr, nt.message, nt.icon, nt.btn, nt.endpoint, nt.endpoint_title
                                        FROM notification_receivers nr
                                        LEFT JOIN notification_templates nt
                                        ON nt.template_id = nr.template_id
                                        WHERE nr.custid = ? AND (nr.whitelabel_id = ? OR nr.whitelabel_id = 0) AND nr.receipt_id = ? ORDER BY nr.receipt_id DESC LIMIT 1",
            array($custid, $whitelabel_id, $receiptId))->fetchAll();
    }

    private function verifyLimit($limit)
    {
        if($limit > 0 && $limit <= 30)
        {
            return "LIMIT {$limit}";
        } else {
            return "";
        }
    }
}