<?php


class DB_User_Affiliations extends DataModel
{
    protected $schema = "user_affiliations";

    public function findAllAffiliationsByCustid($custid)
    {
        return $this->DB->query("SELECT ua.custid, @whitelabel_id:=ua.whitelabel_id as whitelabel_id, wl.name as whitelabel_name, ua.created_at, ua.last_accessed, ua.skin, wl.timezone,
(SELECT count(*) FROM user_affiliations WHERE whitelabel_id = @whitelabel_id) as user_count
                                       FROM user_affiliations ua 
                                       LEFT JOIN whitelabels wl ON ua.whitelabel_id = wl.whitelabel_id
                                       WHERE custid = ? ORDER BY ua.last_accessed DESC", $custid)->fetchAll();
    }

    public function findAllAffiliationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ua.whitelabel_id, ua.custid, ua.created_at as joined_org_on, ua.last_accessed, u.fname, u.lname, u.birth_year, u.birth_month, u.birth_day, u.sex, u.created_at as joined_bc_on, l.email as email, u.phone_number, usr.usrowingID, usr.expires as usrowing_waiver_expires_on, u.profile_image  
                                        FROM user_affiliations as ua
                                        LEFT JOIN users u 
                                        ON ua.custid = u.custid
                                        LEFT JOIN logins l 
                                        ON u.custid = l.custid
                                        LEFT JOIN usrowing usr
                                        ON usr.custid = ua.custid
                                        WHERE ua.whitelabel_id = ? ORDER BY lname ASC", $whitelabel_id)->fetchAll();
    }

    public function addAffiliation($custid, $whitelabel_id)
    {
        //get existing skin colors
        $currentSkins = $this->DB->query("SELECT skin FROM {$this->schema} WHERE custid = ? ORDER BY skin ASC", $custid)->fetchAll();
        $nextSkin = $this->getNextSkin($currentSkins);
        $this->DB->query("INSERT INTO {$this->schema} (custid, whitelabel_id, created_at, skin, last_accessed) VALUES (?, ?, ?, ?, ?)", array($custid, $whitelabel_id, time(), $nextSkin, time()));
    }

    public function checkIfAffiliated($custid, $whitelabel_id, $timezone = false)
    {
        $affiliation = $this->DB->query("SELECT ua.* , wl.*
                                                FROM `user_affiliations` ua
                                                LEFT JOIN whitelabels wl
                                                ON wl.whitelabel_id = ua.whitelabel_id
                                                WHERE custid = ? AND ua.whitelabel_id=? LIMIT 1", array($custid, $whitelabel_id))->fetchArray();
        if(isset($affiliation['custid']) && $affiliation['whitelabel_id'] == $whitelabel_id)
        {
            if($timezone)
            {
                //set timezone for request
                date_default_timezone_set($affiliation['timezone']);
            }
            return true;
        }
        else
        {
            date_default_timezone_set('America/New_York');
            return false;
        }
    }

    public function updateLastAccessed($custid, $whitelabel_id)
    {
        $this->DB->query("UPDATE {$this->schema} SET last_accessed = ? WHERE custid = ? AND whitelabel_id = ? LIMIT 1", array(time(), $custid, $whitelabel_id));
    }

    private function getNextSkin($currentSkins)
    {
        if(count($currentSkins) == 0)
        {
            return 1;
        }
        $skinArray = array();
        $skinArray[0] = true;
        $skinArray[1] = false;
        $skinArray[2] = false;
        $skinArray[3] = false;
        $skinArray[4] = false;
        $skinArray[5] = false;
        $skinArray[6] = false;

        foreach ($currentSkins as $skin)
        {
            $skinArray[$skin['skin']] = true;
        }

        if($skinArray[1] == false){return 1;}
        if($skinArray[3] == false){return 3;}
        if($skinArray[4] == false){return 4;}
        if($skinArray[2] == false){return 2;}
        if($skinArray[5] == false){return 5;}
        if($skinArray[6] == false){return 6;}

    }

    public function updateTimeAccessed($whitelabel_id, $custid)
    {
       $this->DB->query("UPDATE {$this->schema} SET last_accessed = ? WHERE custid = ? AND whitelabel_id = ?", array(time(), $custid, $whitelabel_id));
    }

    public function countUsersInWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->numRows();
    }

    public function removeAffiliation($whitelabel_id, $custid)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE custid = ? AND whitelabel_id = ? LIMIT 1", array($custid, $whitelabel_id));
    }
}