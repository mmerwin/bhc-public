--
-- Table structure for table `system_ips`
--

CREATE TABLE IF NOT EXISTS `system_ips` (
                                            `ip` varchar(100) NOT NULL,
                                            `hostname` varchar(100) NOT NULL,
                                            `created_at` int(11) NOT NULL,
                                            `type` varchar(20) NOT NULL,
                                            PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `system_ips` (`ip`, `hostname`, `created_at`, `type`) VALUES
('206.189.183.184', 'boathouseconnect-singlenode prod', 1, 'prod'),
('206.189.184.238', 'merwin-test', 2, 'test'),
('::1', 'localhost', 0, 'internal');


COMMIT;
