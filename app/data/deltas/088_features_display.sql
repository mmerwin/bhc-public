--
-- Table structure for table `features_display`
--

CREATE TABLE IF NOT EXISTS `features_display` (
                                                  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `start_time` int(11) NOT NULL,
                                                  `end_time` int(11) NOT NULL,
                                                  `content` text NOT NULL,
                                                  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
