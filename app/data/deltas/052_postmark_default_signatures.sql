--
-- Table structure for table `postmark_default_signatures`
--

CREATE TABLE IF NOT EXISTS `postmark_default_signatures` (
                                                             `whitelabel_id` int(11) NOT NULL,
                                                             `type` varchar(50) NOT NULL,
                                                             `signature_id` int(11) NOT NULL,
                                                             PRIMARY KEY (`whitelabel_id`,`type`),
                                                             KEY `fk_signatureid_default_signatures` (`signature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `postmark_default_signatures`
--
ALTER TABLE `postmark_default_signatures`
    ADD CONSTRAINT `fk_signatureid_default_signatures` FOREIGN KEY (`signature_id`) REFERENCES `postmark_sender_signatures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_default_signatures` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
