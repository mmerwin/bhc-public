<?php


class DB_Admin_Logins extends DataModel
{
    protected $schema = "admin_logins";

    public function findAdminIdByCredentials($username, $password)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE username = ? AND password = ? LIMIT 1", array($username, $this->hashPassword($password)))->fetchArray();
    }

    private function hashPassword($password)
    {
        return md5($password);
    }

}