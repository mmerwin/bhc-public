<?php


class DB_Admin_Api_Tokens extends DataModel
{
    protected $schema = "admin_api_tokens";
    protected $tokenLifeMax = 31536000; // token will be valid for this many seconds after creation

    public function checkToken($token_hash)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE token_hash = ? AND expires > ?", array($token_hash, time()))->fetchArray();
    }

    private function generateToken($adminid, $type)
    {
        $currentTime = time();
        $randomNumber = rand(1,113284);
        return "bhcadmin-".md5($currentTime.$adminid.$type.$randomNumber);
    }

    private function checkTokenUniqueness($token)
    {
        $result = $this->DB->query("SELECT count(*) as count FROM {$this->schema} WHERE token_hash = ? ", array($token))->fetchArray();
        return $result['count'];
    }

    public function createNewToken($adminid, $type='api', $descr='')
    {
        //check that token is not taken
        $uniqueToken = false;
        while(!$uniqueToken){
            $token = $this->generateToken($adminid,$type);
            if($this->checkTokenUniqueness($token) == 0){
                $expiresOn = $this->addNewToken($adminid, $token, $type, $descr);
                return array("adminid"=>$adminid, "token"=>$token, "expires"=>$expiresOn, "type"=>$type);
            }
        }
    }

    private function addNewToken($adminid, $token, $type, $descr)
    {
        $expiresOn = (time() + $this->tokenLifeMax);
        $this->DB->query("INSERT INTO {$this->schema} (adminid, token_hash, expires, type, created_at, last_used, descr) VALUES (?, ?, ?, ?, ?, ?, ?)", $adminid, $token, $expiresOn, $type, time(), 0, $descr);
        return $expiresOn;
    }

    public function deleteToken($token_hash)
    {
        $this->deleteRowOnAttribute('token_hash', $token_hash);
    }

    public function setLastUsed($token_hash)
    {
        $this->DB->query("UPDATE {$this->schema} SET last_used = ? WHERE token_hash = ?",array(time(), $token_hash));
    }

    public function getAllPublicTokens($adminid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE adminid = ? AND type != 'app' AND type !='android' ", $adminid)->fetchAll();
    }

    public function refreshExpirationDate($token)
    {
        $expiresOn = (time() + $this->tokenLifeMax);
        $this->DB->query("UPDATE {$this->schema} SET expires = ? WHERE token_hash = ?", array($expiresOn, $token));
        return $this->checkToken($token);
    }

}