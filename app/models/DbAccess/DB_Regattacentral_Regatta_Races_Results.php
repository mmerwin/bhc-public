<?php


class DB_Regattacentral_Regatta_Races_Results extends DataModel
{
    protected $schema = "regattacentral_regatta_races_results";

    public function addResult($raceId, $regattaId, $eventId, $entryId, $lane, $displayNumber, $distance, $elapsedTime,
                              $splitTime, $marginToFirst, $marginToPrevious, $adjustedTime, $adjustedMarginToFirst,
                              $adjustedMarginToPrevious, $penaltyTime, $penaltyCode, $comment)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (race_id, regatta_id, event_id, entry_id, lane, 
                          display_number, distance, elapsed_time, split_time, margin_to_first, margin_to_previous, 
                          adjusted_time, adjusted_margin_to_first, adjusted_margin_to_previous, penalty_time, 
                          penalty_code, comment) 
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($raceId, $regattaId, $eventId, $entryId, $lane, $displayNumber, $distance, $elapsedTime,
            $splitTime, $marginToFirst, $marginToPrevious, $adjustedTime, $adjustedMarginToFirst,
            $adjustedMarginToPrevious, $penaltyTime, $penaltyCode, $comment));
    }

    public function getResultsByRace($regattaId, $eventId, $raceId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE regatta_id = ? AND event_id = ? 
                                        AND race_id = ? ORDER BY elapsed_time ASC",
            array($regattaId, $eventId, $raceId))->fetchAll();
    }

}