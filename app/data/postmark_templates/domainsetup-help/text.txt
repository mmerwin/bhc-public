Howdy,
{{sender_name}} is setting up Boathouse Connect to send emails and needs your help adding DKIM and Return-Path DNS records to {{domain_name}}. This lets Boathouse Connect send emails on your behalf ensuring effective delivery and higher domain repuatation.

******
DKIM
    Hostname: {{dkim_hostname}}
    Type: TXT
    Add this value: {{dkim_value}}


******
Return-Path
    Hostname: {{returnpath_hostname}}
    Type: CNAME
    Add this value: {{returnpath_value}}

******

To check the status or get more information, visit this link: https://BoathouseConnect.com/home/domainVerify/{{domain_id}}/{{whitelabel_id}}/{{domain_name}}
