<?php


class DB_Whitelabel_Settings extends DataModel
{
    protected $schema = "whitelabel_settings";

    public function getSetting($whitelabel_id, $name)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND name = ? LIMIT 1", array($whitelabel_id, $name))->fetchArray();
    }

    public function updateValue($whitelabel_id, $name, $value)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, name, value, last_updated) VALUES (?, ?, ?, ?)
                                ON DUPLICATE KEY UPDATE value = ?, last_updated = ?", array($whitelabel_id, $name, $value, time(), $value, time()));
    }

    public function getAllSettings($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

}