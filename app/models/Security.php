<?php


class Security
{
    // THE ENCRYPTION KEY SHOULD NEVER CHANGE. THINK CAREFULLY BEFORE TOUCHING THIS
    const ENCRYPTIONKEY = "[redacted]";

    protected function encrypt($string)
    {
        // Store the cipher method
                $ciphering = "AES-128-CTR";
                $options = 0;

        // Non-NULL Initialization Vector for encryption
                $encryption_iv = '[redacted]';

        // Use openssl_encrypt() function to encrypt the data
                return openssl_encrypt($string, $ciphering, self::ENCRYPTIONKEY, $options, $encryption_iv);
    }

    protected function decrypt($string)
    {
        // Store the cipher method
        $ciphering = "AES-128-CTR";
        $options = 0;

        // Non-NULL Initialization Vector for decryption
        $decryption_iv = '[redacted]';

        // Use openssl_decrypt() function to decrypt the data
        return openssl_decrypt ($string, $ciphering, self::ENCRYPTIONKEY, $options, $decryption_iv);
    }

}