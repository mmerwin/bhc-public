CREATE TABLE IF NOT EXISTS `notification_receivers` (
                                                        `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
                                                        `template_id` int(11) NOT NULL,
                                                        `custid` int(11) NOT NULL,
                                                        `whitelabel_id` int(11) NOT NULL,
                                                        `created_at` int(11) NOT NULL,
                                                        `read_at` int(11) NOT NULL,
                                                        `endpoint_params` text NOT NULL,
                                                        `merge_tags` text NOT NULL,
                                                        PRIMARY KEY (`receipt_id`),
                                                        KEY `fk_template_id_notification` (`template_id`),
                                                        KEY `fk_custid_notifications` (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `notification_receivers`
    ADD CONSTRAINT `fk_custid_notifications` FOREIGN KEY (`custid`) REFERENCES `users` (`custid`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_template_id_notification` FOREIGN KEY (`template_id`) REFERENCES `notification_templates` (`template_id`);
COMMIT;
