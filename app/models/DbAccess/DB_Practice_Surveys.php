<?php


class DB_Practice_Surveys extends DataModel
{
    protected $schema = "practice_surveys";

    public function addSurveyResponse($whitelabel_id, $practiceId, $custid, $lineup, $sessionplan, $equipment, $comments)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, practice_id, custid, lineups_satisfaction, sessionplan_satisfaction, equipment_issues, coach_comments, timestamp) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?)", array($whitelabel_id, $practiceId, $custid, $lineup, $sessionplan, $equipment, $comments, time()));
    }

    public function getSurveyResults($whitelabel_id, $practiceId=null)
    {
        $practiceId = intval($practiceId);
        if ($practiceId) {
            $param = " AND ps.practice_id = {$practiceId}";
        } else {
            $param = "";
        }
        return $this->DB->query("SELECT ps.whitelabel_id, ps.practice_id, ps.custid, ps.lineups_satisfaction, ps.sessionplan_satisfaction, ps.equipment_issues, ps.coach_comments, ps.timestamp,
                                        u.fname, u.lname, l.email, psm.name as practice_name, psm.start_time as practice_start_time, psm.end_time as practice_end_time
                                        FROM practice_surveys ps
                                        LEFT JOIN users u
                                        on u.custid = ps.custid
                                        LEFT JOIN logins l
                                        on l.custid = ps.custid
                                        LEFT JOIN practice_schedule_meta psm 
                                        ON psm.practice_id = ps.practice_id WHERE ps.whitelabel_id = ? {$param}", array($whitelabel_id))->fetchAll();
    }

}