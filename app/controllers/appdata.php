<?php


class appdata extends Controller
{
    /**
     * Provides an array of rowing/coxing skill level options
     * @api
     * @return array array of rowing/coxing skill levels.
     */
    public function getLineupSkills()
    {
        $skills = Service_Skills::getLineupSkills();
        $this->view('json',$skills);
    }

    /**
     * Provides an array of possible permission levels and descriptions
     * @api
     * @return array of permissions.
     */
    public function permissions()
    {
        $permissions = Service_Permissions::getAllPermissionOptions();
        $this->view('json',$permissions);
    }


    public function getAllRcOrgs()
    {
        $integrations   = new Integrations_RegattaCentral();
        $results        = json_decode($integrations->getClubDetailsByName(""), true);
        $DB             = new DB();
        $counter = 0;
        foreach ($results['data'] as $club) {
            $counter++;
            //check if orgid already exists:
            $check = $DB->query("SELECT organizationId FROM regattacentral_orgs WHERE organizationId = ? LIMIT 1", $club['organizationId'])->numRows();
            if ($check == 0) {
                $DB->query("INSERT INTO regattacentral_orgs (organizationId, links, officialName, name, abbreviation, city, region, country, IOC, type, url, logoUrl, blade, orgParticipants, orgContacts) 
                              values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    array($club['organizationId'], $club['links'], $club['officialName'], $club['name'], $club['abbreviation'], $club['city'], $club['region'], $club['country'], $club['ioc'], $club['type'], $club['url'], $club['logoUrl'], $club['blade'], $club['orgParticipants'], $club['orgContacts'], ));
                echo "New Entry for {$club['organizationId']} - {$club['name']}<br>";

            } else{
                //echo "Duplicate entry for {$club['organizationId']} <br>";
            }
        }
        echo "Done! returned {$counter} total clubs to attempt";
    }
}