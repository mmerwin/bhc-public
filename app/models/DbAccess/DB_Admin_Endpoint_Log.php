<?php


class DB_Admin_Endpoint_Log extends DataModel
{
    protected $schema = "admin_endpoint_log";

    public function addToLog($controller, $method, $ip, $adminid)
    {
        $this->DB->query("INSERT INTO {$this->schema} (controller, method, timestamp, ip, adminid) VALUES (?, ?, ?, ?, ?)", array($controller, $method, time(), $ip, $adminid));
    }

}