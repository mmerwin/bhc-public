<?php


class DB_PreAuthorized_New_Users extends DataModel
{
    protected $schema = "pre_authorized_new_users";
    public function addPreauth($email, $whitelabel_id, $custid_creator)
    {
        $this->DB->query("INSERT INTO {$this->schema} (email, whitelabel_id, created_at, custid_creator) VALUES (?, ?, ?, ?)", array($email, $whitelabel_id, time(), $custid_creator));
    }
    public function checkForPreauth($email, $whitelabel_id = null)
    {
        if(is_null($whitelabel_id))
        {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE email = ? AND claimed = 0", array($email))->fetchAll();
        } else
        {
            return $this->DB->query("SELECT * FROM {$this->schema} WHERE email = ? AND claimed = 0 AND whitelabel_id = ?", array($email, $whitelabel_id))->fetchAll();
        }

    }

    public function removePreauthByEmailWhitelabel($email, $whitelabel_id)
    {
        $this->DB->query("UPDATE {$this->schema} SET claimed = '1' WHERE email = ? AND whitelabel_id = ?", array($email, $whitelabel_id));
    }

    public function deletePreauth($whitelabel_id, $email)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND email = ?", array($whitelabel_id, $email));
    }

    public function getAllPreauths($whitelabel_id)
    {
        return $this->DB->query("SELECT pau.authorized_id as id, pau.email, pau.whitelabel_id, pau.created_at, CONCAT(u.fname, ' ',u.lname) as created_by, pau.custid_creator 
                                        FROM {$this->schema} pau
                                        LEFT JOIN users u
                                        ON u.custid = pau.custid_creator
                                        WHERE pau.whitelabel_id = ? AND pau.claimed = 0", array($whitelabel_id))->fetchAll();
    }

}