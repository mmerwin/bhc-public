<?php


class zapier extends Controller
{
    /**
     * Checks if the token is valid for a Zapier request
     * Only used by Zapier.com authentication
     * @uses token to check authentication
     * return array of token information OR 401 header
     */
    public function tokenCheck()
    {
        $token  = $this->param("token");
        $result = Service_ApiTokenValidation::validateToken($token, true);
        if (empty($result) || $result['type'] != 'zapier') {
            header("HTTP/1.1 401 Unauthorized");
            exit;
        } else {
            $user       = new User($result['custid']);
            $returnable = [];
                $returnable[] = [
                    "custid"    => $result['custid'],
                    "fname"     => $user->fname,
                    "lname"     => $user->lname,
                    "token"     => $result['token_hash'],
                    "expires"   => date("c",$result['expires'])
                ];
            $this->view('json',$returnable);
        }

    }

    /**
     * Provides an array of all whitelabel affiliations for the current user
     *
     * return array array of affiliated whitelabels
     */
    public function getAllWhitelabels()
    {
        $data   = (new user($_SESSION['custid']))->getAffiliations();
        $return = array();
        foreach ($data as $value) {
            $return[] = array(
                "id"                => $value['whitelabel_id'],
                "custid"            => $value['custid'],
                "whitelabel_name"   => $value['whitelabel_name'],
                "created_at"        => date("c",$value['created_at']),
                "last_accessed"     => date("c",$value['last_accessed']),
                "skin"              => $value['skin']
            );
        }
        $this->view('json',$return);
    }

    /**
     * Provides an array of all approved whitelabel users
     *
     * return array array of affiliated whitelabels
     */
    public function getWhitelabelUsers()
    {
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $result         = Service_Affiliations::getAllAffiliatedUsers($whitelabel_id);
        $return         = [];
        foreach ($result as $data) {
            $return[] =[
                "id"            => "{$data['whitelabel_id']}-{$data['custid']}",
                "custid"        => $data['custid'],
                "joined_org_on" => date("c",$data['joined_org_on']),
                "fname"         => $data['fname'],
                "lname"         => $data['lname'],
                "birthday"      => "{$data['birth_year']}-".str_pad($data['birth_month'], 2, '0', STR_PAD_LEFT)."-".str_pad($data['birth_day'], 2, '0', STR_PAD_LEFT),
                "sex"           => $data['sex'],
                "email"         => $data['email']
            ];
        }
        $this->view("json",$return);
    }

    /**
     * Provides an array all pending join requests
     *
     * return array array of affiliated whitelabels
     */
    public function getPendingJoinRequests()
    {
        $requests   = Service_Affiliations::getPendingRequestsZapier($_SESSION['whitelabel_id']);
        $return     = [];
        foreach ($requests as $data) {
            $return[] = [
                "id"            => "{$data['custid']}-{$data['whitelabel_id']}-{$data['timestamp']}",
                "status"        => $data['status'],
                "last_updated"  => date("c",$data['timestamp']),
                "fname"         => $data['fname'],
                "lname"         => $data['lname'],
                "email"         => $data['email'],
                "whitelabel_id" => $data['whitelabel_id']
            ];
        }
        $this->view('json',$return);
    }

    /**
     * Adds pre-authorization to join whitelabel
     *
     * return array success message
     */
    public function addPreauthorization()
    {
        $email          = $this->param("email");
        $custid_creator = $_SESSION['custid'];
        $whitelabel_id  = $_SESSION['whitelabel_id'];

        if (!isset($email) || !isset($whitelabel_id)) {
            header("HTTP/1.1 400 Bad Request");
            exit;
        }
        Service_Affiliations::addPreauthorization($email, $whitelabel_id, $custid_creator);
        $return = array("Status"=>"Success","Email"=>$email,"whitelabel_id"=>$whitelabel_id, "custid_creator"=>$custid_creator);
        $this->view("json",$return);
    }

    /**
     *  Returns all charges in the organization
     */
    public function postedCharges()
    {
        $allCharges      = (new DB_User_Payment_Schedule())->upcomingChargesByWhitelabel($_SESSION["whitelabel_id"]);
        $zapierFormatted = [];
        foreach ($allCharges as $charge) {
            $zapierFormatted[] = [
                "id"            => $charge["payment_schedule_id"],
                "whitelabel_id" => $charge["whitelabel_id"],
                "custid"        => $charge["custid"],
                "fname"         => $charge["fname"],
                "lname"         => $charge["lname"],
                "created_at"    => date("c", $charge['created_at']),
                "due_by"        => date("c", $charge["due_by"]),
                "amount"        => $charge["amount"],
                "amount_usd"    => round($charge["amount"]/100, 2),
                "description"   => $charge["description"],
                "charge_type"   => $charge["charge_type"]
            ];
        }
        $this->view("json", $zapierFormatted);
    }

    /**
     *  Returns the last 5000 credits posted to all accounts in the whitelabel
     */
    public function postedCredits()
    {
        $allCredits      = (new DB_User_Payment_Credits())->getCreditsByWhitelabel($_SESSION["whitelabel_id"], 5000);
        $zapierFormatted = [];

        foreach ($allCredits as $credit) {
            $zapierFormatted[] = [
                "id"                    => $credit["payment_credit_id"],
                "whitelabel_id"         => $credit["whitelabel_id"],
                "created_at"            => date("c", $credit["created_at"]),
                "custid"                => $credit["custid"],
                "fname"                 => $credit["fname"],
                "lname"                 => $credit["lname"],
                "email"                 => $credit["email"],
                "posted_by"             => $credit["posted_by"],
                "external_id"           => $credit["external_id"],
                "amount_charged"        => $credit["amount_charged"],
                "amount_charged_usd"    => round($credit["amount_charged"]/100, 2),
                "settled_amount"        => $credit["settled_amount"],
                "settled_amount_usd"    => round($credit["settled_amount"]/100, 2),
                "description"           => $credit["descr"],
                "receipt_url"           => $credit["receipt_url"]
            ];
        }

        $this->view("json", $zapierFormatted);
    }

    /**
     * returns all upcoming practices in the whitelabel
     */
    public function upcomingPractices()
    {
        $allPractices  = (new DB_Practice_Schedule_Meta())->getAllWhitelabelPractices($_SESSION["whitelabel_id"], "ASC", time(), true);
        $zapierFormatted = [];

        foreach ($allPractices as $practice) {
            if ($practice["public"] == "Yes") {
                $zapierFormatted[] = [
                    "id"            => $practice["practice_id"],
                    "whitelabel_id" => $practice["whitelabel_id"],
                    "series_id"     => $practice["series_id"],
                    "name"          => $practice["name"],
                    "location_id"   => $practice["location_id"],
                    "start_time"    => date("c", $practice["start_time"]),
                    "end_time"      => date("c", $practice["end_time"]),
                    "max_attendees" => $practice["max_attendees"],
                    "reservation_id"=> $practice["reservation_id"]
                ];
            }
        }

        $this->view("json", $zapierFormatted);
    }

    /**
     * returns all reported issues in the whitelabel
     */
    public function equipmentIssues()
    {
        $zapierFormatted = [];
        $practiceIssues  = (new Service_Lineups())->getReportedIssuesByWhitelabel($_SESSION["whitelabel_id"]);
        $selfReported    = (new EquipmentModel($_SESSION["whitelabel_id"]))->getEquipmentIssues();

        //format issues from post-practice surveys
        foreach ($practiceIssues as $issue) {
            $zapierFormatted[] = [
                "id"            => "postpractice-".$issue["whitelabel_id"]."-".$issue["practice_id"]."-".$issue["custid"],
                "custid"        => $issue["custid"],
                "whitelabel_id" => $issue["whitelabel_id"],
                "fname"         => $issue["fname"],
                "lname"         => $issue["lname"],
                "issue"         => $issue["issue"],
                "reported_at"   => date("c",$issue["reported_at"])
            ];
        }

        //format issues self-reported
        foreach ($selfReported as $issue) {
            $zapierFormatted[] = [
                "id"            => "selfreported-".$issue["whitelabel_id"]."-".$issue["id"]."-".$issue["custid"],
                "custid"        => $issue["custid"],
                "whitelabel_id" => $issue["whitelabel_id"],
                "fname"         => $issue["fname"],
                "lname"         => $issue["lname"],
                "issue"         => $issue["descr"],
                "reported_at"   => date("c",$issue["timestamp"])
            ];
        }

        $this->view("json", $zapierFormatted);
    }

    /**
     * retrieves all messages in the whitelabel that require review before being sent
     */
    public function getPendingMessages()
    {
        $externalMessageService = new Service_ExternalMessages();
        $rawMessages            = $externalMessageService->getPendingMessages($_SESSION['whitelabel_id'], "All", "Yes");
        $zapierFormatted        = [];

        foreach ($rawMessages as $messages) {
            $zapierFormatted[] = [
                "id"                    => $messages["message_id"],
                "whitelabel_id"         => $messages["whitelabel_id"],
                "custid"                => $messages["custid"],
                "sender_name"           => $messages["sender_name"],
                "message_type"          => $messages["type"],
                "subject"               => $messages["subject"],
                "message_body"          => $messages["body"],
                "created_at"            => date("c",$messages["created_at"]),
                "scheduled_send_time"   => date("c",$messages["scheduled_time"])
            ];
        }

        $this->view("json", $zapierFormatted);
    }

    /**
     * Returns all groups in the whitelabel
     */
    public function getAllGroups()
    {
        $groups          = new Groups($_SESSION['whitelabel_id']);
        $allGroups       = $groups->getWhitelabelGroups();
        $zapierFormatted = [];

        foreach ($allGroups as $group) {
            $zapierFormatted[] = [
                "id"                => $group["group_id"],
                "whitelabel_id"     => $group["whitelabel_id"],
                "title"             => $group["title"],
                "description"       => $group["descr"],
                "max_members"       => $group["max"],
                "self_join_allowed" => $group["self_join"]
            ];
        }

        $this->view('json',$zapierFormatted);
    }

    /**
     * Returns all members of a given group
     * @param group_id int group_id of the group to return members of. group_id's can be found with the /zapier/getAllGroups endpoint
     */
    public function getGroupMembers()
    {
        $groupId        = $this->param("group_id");
        $groups         = new Groups($_SESSION['whitelabel_id']);
        $groupMembers   = $groups->getGroupMembers($groupId);
        $zapierFormatted= [];

        foreach ($groupMembers as $member) {
            $zapierFormatted[] = [
                "id"            => $member["group_id"]."-".$member["custid"],
                "custid"        => $member["custid"],
                "fname"         => $member["fname"],
                "lname"         => $member["lname"],
                "phone_number"  => $member["phone_number"],
                "email"         => $member["email"]
            ];
        }

        $this->view("json", $zapierFormatted);
    }

    public function getGroupJoinRequests()
    {
        $groupId        = $this->param("group_id");
        $groups         = new Groups($_SESSION["whitelabel_id"]);
        $joinRequests   = $groups->getJoinRequests($groupId);
        $zapierFormatted= [];

        foreach ($joinRequests as $request) {
            $zapierFormatted[] = [
                "id"                => $request["group_id"]."-".$request["custid"]."-".$request["created_at"],
                "group_id"          => $request["group_id"],
                "custid"            => $request["custid"],
                "fname"             => $request["fname"],
                "lname"             => $request["lname"],
                "created_at"        => date("c", $request["created_at"]),
                "member_count"      => $request["group_member_count"],
                "group_capacity"    => $request["group_max_members"]
            ];
        }

        $this->view("json", $zapierFormatted);
    }

}