<?php


class DB_External_Messages_Webhooks extends DataModel
{
    protected $schema = "external_messages_webhooks";

    public function addEvent($externalId, $email, $tag, $activity)
    {
        $this->DB->query("INSERT INTO {$this->schema} (external_id, email, tag, activity, timestamp) VALUES (?, ?, ?, ?, ?)", array($externalId, $email, $tag, $activity, time()));
    }

    public function getOpenCount($messageId)
    {
        $data =  $this->DB->query("SELECT count(distinct(emw.external_id)) as opens
                                        FROM external_messages_webhooks emw
                                        WHERE emw.external_id IN
                                        (SELECT external_id FROM external_messages_recipients emr WHERE message_id = ?)
                                        AND emw.activity = 'Open'", array($messageId))->fetchArray();
        return $data['opens'];
    }

    public function getUniqueClickCount($messageId)
    {
        $data = $this->DB->query("SELECT count(distinct(emw.external_id)) as clicks
                                        FROM external_messages_webhooks emw
                                        WHERE emw.external_id IN
                                        (SELECT external_id FROM external_messages_recipients emr WHERE message_id = ?)
                                        AND emw.activity = 'Click'", array($messageId))->fetchArray();
        return $data['clicks'];
    }

    public function getFirstOpen($messageId)
    {
        return $this->DB->query("SELECT emr.external_id, 'Open' as activity, @open_time:=(SELECT timestamp FROM external_messages_webhooks WHERE external_id = emr.external_id AND activity = 'Open' ORDER BY timestamp ASC LIMIT 1) as open_time,
                                        emr.name, emr.custid, emr.sent_time, (@open_time - sent_time) as time_from_sent
                                        FROM external_messages_recipients emr 
                                        where message_id = ? HAVING open_time > 0", array($messageId))->fetchAll();
    }

    public function getFirstClick($messageId)
    {
        return $this->DB->query("SELECT emr.external_id, 'Click' as activity, @click_time:=(SELECT timestamp FROM external_messages_webhooks WHERE external_id = emr.external_id AND activity = 'Click' ORDER BY timestamp ASC LIMIT 1) as click_time,
                                        emr.name, emr.custid, emr.sent_time, (@click_time - sent_time) as time_from_sent
                                        FROM external_messages_recipients emr 
                                        where message_id = ?  HAVING click_time > 0", array($messageId))->fetchAll();
    }

    public function getAllActivityByCustid($custid)
    {
        //do not use for custid = 0 (i.e. stakeholders)
        return $this->DB->query("SELECT emr.message_id, emr.name, emr.contact, emr.custid, emr.user_type, emr.external_id, emr.status, emr.sent_time, emw.activity, (emw.timestamp - emr.sent_time) as time_from_sent
                                        FROM external_messages_webhooks emw
                                        LEFT JOIN external_messages_recipients emr
                                        ON emr.external_id = emw.external_id
                                        where emr.custid = ?", array($custid))->fetchAll();
    }

    public function getAllActivityByMessageId($messageId)
    {
        //do not use for custid = 0 (i.e. stakeholders)
        return $this->DB->query("SELECT emr.message_id, emr.name, emr.contact, emr.custid, emr.user_type, emr.external_id, emr.status, emr.sent_time, emw.activity, (emw.timestamp - emr.sent_time) as time_from_sent
                                        FROM external_messages_webhooks emw
                                        LEFT JOIN external_messages_recipients emr
                                        ON emr.external_id = emw.external_id
                                        where emr.message_id = ? ORDER BY time_from_sent ASC", array($messageId))->fetchAll();
    }

}