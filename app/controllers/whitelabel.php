<?php


class whitelabel extends Controller
{
    /**
     * List of all whitelabels.
     * @api

     * @return array array all whitelabels on BoathouseConnect.
     */
    public function getAllWhitelabels()
    {
        $whitelabels = Service_Whitelabel::getAllWhitelabels();
        $this->view('json',$whitelabels);
    }
    /**
     * Whitelabels that the user is not affiliated with.
     * Useful to show what whitelabels the user is eligible to join.
     * @api
     * @return array array of whitelabel information
     */
    public function getUnaffiliatedWhitelabels()
    {
        //get all whitelabels
        $whitelabels = Service_Whitelabel::getNonAffiliatedWhitelabels($_SESSION['custid']);
        $this->view('json',$whitelabels);
    }
    /**
     * Checks that email and password match an account.
     * @api
     * @uses requested_whitelabel Whitelabel the user wants to join.
     * @uses email email address of the user. Used to determine if they are already pre-authorized.
     * @return array array of status and message
     */
    public function requestInvitation()
    {
        $requested_whitelabel = $this->param("requested_whitelabel");
        $custid               = $_SESSION['custid'];
        $email                = $this->param("email");
        //check if already pre-authorized
            //if pre-authorized, add to whitelabel
        $preauth = (new DB_PreAuthorized_New_Users())->checkForPreauth($email);
        if (!empty($preauth)) {
            //add whitelabel_affiliation
            foreach ($preauth as $auth) {
                Service_Affiliations::addAffiliation($custid, $auth['whitelabel_id'], $email);
                if ($auth['whitelabel_id'] == $requested_whitelabel) {
                    $return = array("Status"=>"Success", "Message"=>"Added to whitelabel");
                    $added  = true;
                }
            }
            if (!isset($added)) {
                $return = array("Status"=>"Error", "Message"=>"Unknown error. Could not validate pre-authorization");
            }

        } else {
            //add to whitelabel_join_requests
            $return = Service_Affiliations::addJoinRequest($requested_whitelabel);
            Service_Notification_Send::newWhitelabelJoinRequest($custid, $requested_whitelabel);
        }
        $this->view('json',$return);
    }
    /**
     * Updates the last time the user logged-in to the whitelabel.
     * Useful to help sort most-used whitelabel for a user
     * @api
     * @uses whitelabel_id whitelabel_id the user is accessing
     * @return void
     */
    public function updateTimeaccessed()
    {
        $whitelabel_id = $this->param("whitelabel_id");
        Service_Affiliations::UpdateTimeAccessed($whitelabel_id, $_SESSION['custid']);
    }
    /**
     * Declines a new user join request.
     * Prevents the user from being able to access the whitelabel without re-sending a request
     * @api
     * @uses whitelabel_id whitelabel_id the user is being denied from.
     * @uses custid custid of user being declined.
     * @return array array of status, message, and new state of their request
     */
    public function declineJoinRequest()
    {
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $custid         = $this->param("custid");
        $result         = Service_Affiliations::declineJoinRequest($custid,$whitelabel_id);
        Service_Notification_Send::newdeclinedjoinrequest($custid, $_SESSION['custid'], $whitelabel_id);
        $this->view('json',$result);
    }
    /**
     * Approve a new user join request.
     * Adds the user to the whitelabel.
     * @api
     * @uses whitelabel_id whitelabel_id the user is being added to.
     * @uses custid custid of user being added.
     * @return array array of status, message, and new state of their request
     */
    public function approveJoinRequest()
    {
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $custid         = $this->param("custid");
        $result         = Service_Affiliations::approveJoinRequest($custid,$whitelabel_id);
        Service_Notification_Send::newapprovedjoinrequest($custid, $_SESSION['custid'], $whitelabel_id);
        $this->view('json',$result);
    }
    /**
     * List of all pending join requests for the whitelabel.
     * @api
     * @uses whitelabel_id whitelabel_id to retrieve join requests from
     * @return array array of status, message, and new state of their request
     */
    public function getPendingJoinRequests()
    {
        $requests = Service_Affiliations::getPendingRequests($_SESSION['whitelabel_id']);
        $this->view('json',$requests);
    }

    /**
     * List of all declined join requests for the whitelabel.
     * @uses whitelabel_id whitelabel_id to retrieve declined join requests from.
     * @return array array of status, message, and new state of their request
     */
    public function getDeclinedJoinRequests()
    {
        $requests = Service_Affiliations::getDeclinedRequests($_SESSION['whitelabel_id']);
        $this->view('json',$requests);
    }

    /**
     * Adds pre-authorization to join whitelabel
     * @param email email address to pre-authorize
     * @uses whitelabel_id whitelabel_id of the organization
     *
     * @return array success message
     */
    public function addPreauthorization()
    {
        $email          = $this->param("email");

        if(!isset($email) || !isset($_SESSION['whitelabel_id'])) {
            $return = array("Status"=>"Error","Message"=>"Missing valid whitelabel_id or email", "Email"=>$email,"whitelabel_id"=>$_SESSION['whitelabel_id']);
        } else {
            Service_Affiliations::addPreauthorization($email, $_SESSION['whitelabel_id'], $_SESSION['custid']);
            $return = array("Status"=>"Success","Email"=>$email,"whitelabel_id"=>$_SESSION['whitelabel_id'], "custid_creator"=>$_SESSION['custid']);
            WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.preauth", "true");
        }

        $this->view("json",$return);
    }

    public function getWhitelabelUsers()
    {
        $whitelabel_id  = $_SESSION['whitelabel_id'];
        $result         = Service_Affiliations::getAllAffiliatedUsers($whitelabel_id);
        $return         = array();
        foreach ($result as $data) {
            $return[] = array(
                "custid"                    => $data['custid'],
                "joined_whitelabel_on"      => $data['joined_org_on'],
                "fname"                     => $data['fname'],
                "lname"                     => $data['lname'],
                "birthYear"                 => $data['birth_year'],
                "birthMonth"                => $data['birth_month'],
                "birthDay"                  => $data['birth_day'],
                "yearendAge"                => (date("Y") - $data['birth_year']),
                "sex"                       => $data['sex'],
                "email"                     => $data['email'],
                "phone_number"              => $data['phone_number'],
                "usrowingID"                => $data['usrowingID'],
                "usrowing_waiver_expires_on"=> $data["usrowing_waiver_expires_on"],
                "profile_image"             => $data["profile_image"]
            );
        }
        $this->view("json",$return);
    }

    /**
     * List of all pre-authorized emails for the whitelabel.
     * @api
     * @uses whitelabel_id whitelabel_id to retrieve declined join requests from.
     * @return array email addresses pre-authorized
     */
    public function getPreauthorizedEmails()
    {
        $requests = Service_Affiliations::getpreauthorizedEmails($_SESSION['whitelabel_id']);
        $this->view('json',$requests);
    }

    public function removePreauthorizedEmail()
    {
        $email = $this->param("email");
        Service_Affiliations::removePreauthorizedEmail($_SESSION["whitelabel_id"], $email);
        $remainingPreauths = Service_Affiliations::getpreauthorizedEmails($_SESSION['whitelabel_id']);

        $this->view("json", $remainingPreauths);
    }

    /**
     * @api
     */
    public function getRewards()
    {
        $rewards = new Service_Rewards($_SESSION['whitelabel_id']);
        $data    = $rewards->getRewardData();

        $this->view("json", $data);
    }

    public function updateTimezone()
    {
        $timezone = $this->param("timezone");
        (new Service_Whitelabel())->updateTimezone($_SESSION['whitelabel_id'], $timezone);
    }

    /**
     * @method POST must use POST for this endpoint
     * @param image image file to be uploaded
     */
    public function uploadLogo()
    {
        (new WhitelabelSettings())->uploadLogo($_SESSION["whitelabel_id"], $_FILES["image"]["tmp_name"]);
    }

    public function changePlan()
    {
        $newPlan = $this->param("new_plan");
        (new Service_Whitelabel_Subscriptions($_SESSION["whitelabel_id"]))->changePlan($newPlan);
    }

    public function removeUser()
    {
        $custid = $this->param("custid");
        (new Service_Affiliations())->removeAffiliation($_SESSION["whitelabel_id"], $custid);
    }
    
    public function getCalendar()
    {
        $timeMultiplier = $this->param("time_multiplier");
        $calendar       = new Service_Calendar($_SESSION["whitelabel_id"]);
        $data           = $calendar->getWhitelabelCalendar(false);
        
        $this->view("json", $data);
        
    }

    public function addCalendarEvent()
    {
        $title      = $this->param("title");
        $startTime  = $this->param("start_time");
        $endTime    = $this->param("end_time");
        $descr      = $this->param("descr");
        $linkColor  = "primary";
        $public     = "Yes";

        if ($startTime <= $endTime) {
            $calendar = new Service_Calendar($_SESSION["whitelabel_id"]);
            $calendar->addWhitelabelEvent($startTime, $endTime, $title, $descr, $linkColor, $public);

            $data = ["Status" => "Success", "Message" => "event has been added to the calendar"];
        } else {
            $data = ["Status" => "Error", "Message" => "end_time must be sometime after start_time"];
        }

        $this->view("json", $data);
    }

    public function getEvents()
    {
        $upcoming = strtolower($this->param("upcoming")) == "yes";
        $calendar = new Service_Calendar($_SESSION["whitelabel_id"]);

        if ($upcoming) {
            $events = $calendar->getUpcomingEvents();
        } else {
            $events = $calendar->getWhitelabelEvents();
        }

        $this->view("json", $events);
    }

    public function removeEvent()
    {
        $eventId    = $this->param("event_id");
        $calendar   = new Service_Calendar($_SESSION["whitelabel_id"]);

        $calendar->removeEvent($eventId);
    }

}