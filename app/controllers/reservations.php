<?php


class reservations extends Controller
{
    public function getAllReservations()
    {
        $reservations       = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $allReservations    = $reservations->getReservationsForWhitelabel();

        $this->view("json", $allReservations);
    }

    public function searchAvailableEquipment()
    {
        $startTime      = $this->param("start_time");
        $endTime        = $this->param("end_time");
        $reservations   = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $equipment      = $reservations->getAvailableAssets($startTime, $endTime);

        $this->view("json", $equipment);
    }

    public function createReservation()
    {
        $type           = $this->param("type");
        $startTime      = $this->param("start_time");
        $endTime        = $this->param("end_time");
        $rawStartTime   = $this->param("raw_start_time");
        $rawEndTime     = $this->param("raw_end_time");

        $custid = (!is_null($this->param("custid"))) ? $this->param("custid") : $_SESSION['custid'];

        if ($rawStartTime != "") {
            $startTime = (new DateTime($rawStartTime))->getTimestamp();
        }
        if ($rawEndTime != "") {
            $endTime = (new DateTime($rawEndTime))->getTimestamp();
        }

        $reservation        = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $custid);
        $reservationData    = $reservation->newReservation($startTime, $endTime, $type);
        $this->view("json", $reservationData);
    }

    public function addBoatToReservation()
    {
        $reservationId   = $this->param("reservation_id");
        $boatId          = $this->param("boat_id");
        $custid          = (!is_null($this->param("custid"))) ? $this->param("custid") : $_SESSION['custid'];

        $reservation     = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $custid);
        $reservationData = $reservation->addBoatToReservation($reservationId, $boatId);
        $this->view("json", $reservationData);
    }

    public function addOarToReservation()
    {
        $reservationId = $this->param("reservation_id");
        $oarId         = $this->param("oar_id");

        $reservation        = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $reservationData    = $reservation->addOarToReservation($reservationId, $oarId);
        $this->view("json", $reservationData);
    }

    public function addErgToReservation()
    {
        $reservationId  = $this->param("reservation_id");
        $ergId          = $this->param("erg_id");

        $reservation        = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $reservationData    = $reservation->addErgToReservation($reservationId, $ergId);
        $this->view("json", $reservationData);
    }

    public function deleteReservation()
    {
        $reservationId = $this->param("reservation_id");

        $reservation        = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $reservationData    = $reservation->deleteReservation($reservationId);
        $this->view("json", $reservationData);
    }

    public function removeBoatFromReservation()
    {
        $boatId         = $this->param("boat_id");
        $reservationId  = $this->param("reservation_id");
        $custid         = (!is_null($this->param("custid"))) ? $this->param("custid") : $_SESSION['custid'];

        $reservation    = new Service_Equipment_Reservations($_SESSION["whitelabel_id"], $custid);
        $response       = $reservation->deleteBoatFromReservation($reservationId, $boatId);

        $this->view("json", $response);
    }

    public function getAllBoatReservations()
    {
        $reservation        = new Service_Equipment_Reservations($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $reservationData    = $reservation->getAllBoatReservations();
        $this->view("json", $reservationData);
    }

}