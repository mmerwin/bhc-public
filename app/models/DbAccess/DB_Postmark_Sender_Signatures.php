<?php


class DB_Postmark_Sender_Signatures extends DataModel
{
    protected $schema = "postmark_sender_signatures";

    public function addSignature($whitelabel_id, $domain, $id, $emailAddress, $name, $confirmed, $replyTo)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, id, domain, email_address, reply_to_email_address, email_name, confirmed) VALUES (?, ?, ?, ?, ?, ?, ?)", array($whitelabel_id, $id, $domain, $emailAddress, $replyTo, $name, $this->booleanToString($confirmed)));
    }

    public function getSignaturesByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getSignaturesByDomain($whitelabel_id, $domain)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND domain = ?", array($whitelabel_id, $domain))->fetchAll();
    }

    public function deleteSignature($whitelabel_id, $signatureId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND id = ? LIMIT 1", array($whitelabel_id, $signatureId));
    }

    public function getAllSignatures()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

    public function updateSignature($whitelabel_id, $domain, $id, $emailAddress, $name, $confirmed, $replyTo)
    {
        //delete existing signature by whitelabel, domain, and domain id
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND id = ? AND domain = ? LIMIT 1", array($whitelabel_id, $id, $domain));

        //reinsert signature
        $this->addSignature($whitelabel_id, $domain, $id, $emailAddress, $name, $confirmed, $replyTo);
    }
}