<?php


class DB_External_Messages_Groups extends DataModel
{
    protected $schema = "external_messages_groups";

    public function AddGroupWithoutStakeholder($messageId, $groupId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ? AND group_id = ? AND whitelabel_id = ?", array($messageId, $groupId, $whitelabel_id));
        $this->DB->query("INSERT INTO {$this->schema} (message_id, group_id, whitelabel_id, stakeholders) VALUES (?, ?, ?, 'No')", array($messageId, $groupId, $whitelabel_id));
    }

    public function AddGroupWithStakeholder($messageId, $groupId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ? AND group_id = ? AND whitelabel_id = ?", array($messageId, $groupId, $whitelabel_id));
        $this->DB->query("INSERT INTO {$this->schema} (message_id, group_id, whitelabel_id, stakeholders) VALUES (?, ?, ?, 'Yes')", array($messageId, $groupId, $whitelabel_id));
    }

    public function removeStakeholderGroup($messageId, $groupId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ? AND group_id = ? AND whitelabel_id = ?", array($messageId, $groupId, $whitelabel_id));
        $this->DB->query("INSERT INTO {$this->schema} (message_id, group_id, whitelabel_id, stakeholders) VALUES (?, ?, ?, 'No')", array($messageId, $groupId, $whitelabel_id));
    }

    public function removeGroup($messageId, $groupId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ? AND group_id = ? AND whitelabel_id = ?", array($messageId, $groupId, $whitelabel_id));
    }

    public function getGroupsById($messageId)
    {
        return $this->DB->query("SELECT emg.whitelabel_id, emg.message_id, emg.group_id, emg.stakeholders as send_to_stakeholders, g.title, g.descr, g.max, g.self_join, 
                                        (select count(*) as member_count FROM groups_members gm WHERE gm.group_id = emg.group_id) as member_count,
                                        (select count(*) as stakeholder_count FROM groups_stakeholders gs WHERE gs.group_id = emg.group_id) as stakeholder_count,
                                        (select 'Yes' as current_member FROM groups_members gm2 WHERE gm2.group_id = emg.group_id AND gm2.custid = em.custid) as sender_is_member
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups g
                                        ON g.group_id = emg.group_id
                                        LEFT JOIN external_messages em
                                        ON em.message_id = emg.message_id
                                        WHERE emg.message_id = ?", array($messageId))->fetchAll();
    }

    public function removeAllGroupsById($messageId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE message_id = ?", array($messageId));
    }

    public function getAllByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT emg.whitelabel_id, emg.message_id, emg.group_id, emg.stakeholders as send_to_stakeholders, g.title, g.descr, g.max, g.self_join,
                                        (select count(*) as member_count FROM groups_members gm WHERE gm.group_id = emg.group_id) as member_count,
                                        (select count(*) as stakeholder_count FROM groups_stakeholders gs WHERE gs.group_id = emg.group_id) as stakeholder_count,
                                        (select 'Yes' as current_member FROM groups_members gm2 WHERE gm2.group_id = emg.group_id AND gm2.custid = em.custid) as sender_is_member
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups g
                                        ON g.group_id = emg.group_id
                                        LEFT JOIN external_messages em
                                        ON em.message_id = emg.message_id
                                        WHERE emg.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getEmailList($messageId)
    {
        $this->setGroupBySql();
        return $this->DB->query("SELECT name, email, type, custid
                                        FROM (SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, l.email, 'User' as type, u.custid
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = u.custid
                                        WHERE emg.message_id = ?
                                        GROUP BY name, email
                                        HAVING name IS NOT NULL AND email IS NOT NULL) as t1
                                        
                                        UNION ALL (SELECT gs.name, gs.email, 'Stakeholder' as type, '0' as custid
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_stakeholders gs
                                        ON gs.group_id = emg.group_id AND emg.stakeholders = 'Yes'
                                        WHERE emg.message_id = ?
                                        GROUP BY email
                                        HAVING name IS NOT NULL AND email IS NOT NULL)", array($messageId, $messageId))->fetchAll();

    }

    public function getTextMessageList($messageId)
    {
        $this->setGroupBySql();
        return $this->DB->query("SELECT CONCAT_WS(' ', u.fname, u.lname) AS name, u.phone_number, 'User' as type, u.custid
                                        FROM external_messages_groups emg
                                        LEFT JOIN groups_members gm
                                        ON gm.group_id = emg.group_id
                                        LEFT JOIN users u
                                        ON u.custid = gm.custid
                                        LEFT JOIN logins l
                                        ON l.custid = u.custid
                                        WHERE emg.message_id = ?
                                        GROUP BY name, email
                                        HAVING name IS NOT NULL AND phone_number IS NOT NULL", array($messageId))->fetchAll();
    }
}