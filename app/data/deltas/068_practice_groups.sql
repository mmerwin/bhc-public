--
-- Table structure for table `practice_groups`
--

CREATE TABLE IF NOT EXISTS `practice_groups` (
                                                 `practice_id` int(11) NOT NULL,
                                                 `whitelabel_id` int(11) NOT NULL,
                                                 `group_id` int(11) NOT NULL,
                                                 `attendance_start` int(11) NOT NULL,
                                                 `attendance_end` int(11) NOT NULL,
                                                 `created_at` int(11) NOT NULL,
                                                 PRIMARY KEY (`practice_id`,`whitelabel_id`,`group_id`),
                                                 KEY `fk_whitelabel_id_practice_series_groups` (`whitelabel_id`),
                                                 KEY `fk_group_id_practice_series_groups` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
