--
-- Table structure for table `groups_stakeholders`
--

CREATE TABLE IF NOT EXISTS `groups_stakeholders` (
                                                     `group_stakeholder_id` int(11) NOT NULL AUTO_INCREMENT,
                                                     `whitelabel_id` int(11) NOT NULL,
                                                     `group_id` int(11) NOT NULL,
                                                     `name` varchar(100) NOT NULL,
                                                     `email` varchar(360) NOT NULL,
                                                     `created_at` int(11) NOT NULL,
                                                     PRIMARY KEY (`group_stakeholder_id`),
                                                     KEY `fk_whitelabel_group_stakeholders` (`whitelabel_id`),
                                                     KEY `fk_group_id_groups_stakeholders` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups_stakeholders`
--
ALTER TABLE `groups_stakeholders`
    ADD CONSTRAINT `fk_group_id_groups_stakeholders` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_group_stakeholders` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`) ON DELETE CASCADE;
COMMIT;
