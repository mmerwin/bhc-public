--
-- Table structure for table `equipment_reservations_boats`
--

CREATE TABLE IF NOT EXISTS `equipment_reservations_boats` (
                                                              `reservation_id` int(11) NOT NULL,
                                                              `whitelabel_id` int(11) NOT NULL,
                                                              `boat_id` int(11) NOT NULL,
                                                              PRIMARY KEY (`reservation_id`,`boat_id`,`whitelabel_id`),
                                                              KEY `fk_whitelabel_id_boats_reservations` (`whitelabel_id`),
                                                              KEY `fk_boat_id_reservations_boats_2` (`boat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment_reservations_boats`
--
ALTER TABLE `equipment_reservations_boats`
    ADD CONSTRAINT `fk_boat_id_reservations_boats_2` FOREIGN KEY (`boat_id`) REFERENCES `equipment_boats` (`boat_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_reservation_id_boats` FOREIGN KEY (`reservation_id`) REFERENCES `equipment_reservations` (`reservation_id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_whitelabel_id_boats_reservations` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
