<?php


class DB_Groups_Stakeholders extends DataModel
{
    protected $schema = "groups_stakeholders";

    public function getAllStakeholders($whitelabel_id, $groupId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND group_id = ? ORDER BY name ASC", array($whitelabel_id, $groupId))->fetchAll();
    }

    public function addStakeholder($whitelabel_id, $groupId, $name, $email)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, group_id, name, email, created_at) VALUES (?, ?, ?, ?, ?)", array($whitelabel_id, $groupId, $name, $email, time()));
    }

    public function deleteStakeholder($stakeholderId, $whitelabel_id, $groupId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE group_stakeholder_id = ? AND whitelabel_id = ? AND group_id = ? LIMIT 1", array($stakeholderId, $whitelabel_id, $groupId));
    }

    public function getStakeholderById($stakeholderId, $whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE group_stakeholder_id = ? AND whitelabel_id = ? LIMIT 1", array($stakeholderId, $whitelabel_id))->fetchArray();
    }

    public function removeStakeholderByWhitelabel($whitelabel_id, $stakeholderEmail)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND email = ?", array($whitelabel_id, $stakeholderEmail));
    }

}