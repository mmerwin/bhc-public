--
-- Table structure for table `practice_lineups`
--

CREATE TABLE IF NOT EXISTS `practice_lineups` (
                                                  `whitelabel_id` int(11) NOT NULL,
                                                  `practice_id` int(11) NOT NULL,
                                                  `custid` int(11) NOT NULL,
                                                  `boat_id` int(11) NOT NULL,
                                                  `seat` varchar(10) NOT NULL,
                                                  `last_updated` int(11) NOT NULL,
                                                  `lineup_sent` varchar(5) NOT NULL DEFAULT 'No',
                                                  `survey` varchar(10) NOT NULL DEFAULT 'Unsent',
                                                  PRIMARY KEY (`whitelabel_id`,`practice_id`,`boat_id`,`seat`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
