--
-- Table structure for table `stripe_cards`
--

CREATE TABLE IF NOT EXISTS `stripe_cards` (
                                              `id` varchar(100) NOT NULL,
                                              `object` varchar(100) DEFAULT NULL,
                                              `address_city` varchar(100) DEFAULT NULL,
                                              `address_country` varchar(100) DEFAULT NULL,
                                              `address_line1` varchar(100) DEFAULT NULL,
                                              `address_line1_check` varchar(100) DEFAULT NULL,
                                              `address_line2` varchar(100) DEFAULT NULL,
                                              `address_state` varchar(100) DEFAULT NULL,
                                              `address_zip` varchar(100) DEFAULT NULL,
                                              `address_zip_check` varchar(100) DEFAULT NULL,
                                              `brand` varchar(100) DEFAULT NULL,
                                              `country` varchar(100) DEFAULT NULL,
                                              `customer` varchar(100) DEFAULT NULL,
                                              `cvc_check` varchar(100) DEFAULT NULL,
                                              `dynamic_last4` varchar(100) DEFAULT NULL,
                                              `exp_month` varchar(100) DEFAULT NULL,
                                              `exp_year` varchar(100) DEFAULT NULL,
                                              `fingerprint` varchar(100) DEFAULT NULL,
                                              `funding` varchar(100) DEFAULT NULL,
                                              `last4` varchar(100) DEFAULT NULL,
                                              `name` varchar(100) DEFAULT NULL,
                                              `tokenization_method` varchar(100) DEFAULT NULL,
                                              `metadata` text DEFAULT NULL,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;