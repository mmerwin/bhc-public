<?php


class DB_Notification_Templates extends DataModel
{
    protected $schema = "notification_templates";

    public function getTemplate($templateId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE template_id = ? LIMIT 1", $templateId)->fetchArray();
    }
}