--
-- Table structure for table `whitelabel_settings`
--

CREATE TABLE IF NOT EXISTS `whitelabel_settings` (
                                                     `whitelabel_id` int(11) NOT NULL,
                                                     `name` varchar(100) NOT NULL,
                                                     `value` text NOT NULL,
                                                     `last_updated` int(11) NOT NULL,
                                                     PRIMARY KEY (`whitelabel_id`,`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
