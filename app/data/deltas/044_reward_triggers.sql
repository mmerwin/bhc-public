--
-- Table structure for table `reward_triggers`
--

CREATE TABLE IF NOT EXISTS `reward_triggers` (
                                                 `reward_trigger_id` int(11) NOT NULL AUTO_INCREMENT,
                                                 `action` text NOT NULL,
                                                 `descr` text NOT NULL,
                                                 `points` int(11) NOT NULL,
                                                 `entity` varchar(10) NOT NULL,
                                                 `frequency` int(11) NOT NULL,
                                                 `status` varchar(10) NOT NULL DEFAULT 'Active',
                                                 PRIMARY KEY (`reward_trigger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;


INSERT IGNORE INTO `reward_triggers` (`reward_trigger_id`, `action`, `descr`, `points`, `entity`, `frequency`, `status`) VALUES
(1, 'Verify email address', 'When a new user account is created, earn points by verifying the email address.', 1, 'user', 0, 'Active'),
(2, 'Verify cell phone number', 'When a user adds a cell phone number and opts in to receive text message notifications.', 1, 'user', 0, 'Active'),
(3, 'Create first group', 'When an organization creates their first group and adds at least one member to the group. ', 5, 'whitelabel', 0, 'Active'),
(4, 'Update rowing preferences', 'Update your personal rowing preferences at least once per year. Earns 1 point / user / year', 1, 'user', 31536000, 'Active'),
(5, 'Verify emergency contact', 'Verify your personal emergency contact at least once per year. Earns 1 point / user / year', 1, 'user', 31536000, 'Active'),
(6, 'Add USRowing number', 'Add your personal USRowing number to your profile. If USRowing numbers are bulk-imported, one point per account will be added.', 1, 'user', 0, 'Active'),
(7, 'Link USRowing account for waiver verification', 'Link the organizations USRowing account and automatically import waiver and membership status monthly. Points will also be earned for adding a USRowing waiver when new members join.', 10, 'whitelabel', 0, 'Active'),
(8, 'Add first stakeholder', 'Add your first stakeholder in a group. This can only be earned once, not for each group.', 5, 'whitelabel', 0, 'Active'),
(9, 'Add first boat', 'First time adding a boat to the organizations equipment inventory.', 5, 'whitelabel', 0, 'Active'),
(10, 'Add first set of oars', 'First time adding a set of oars to the organizations equipment inventory.', 5, 'whitelabel', 0, 'Active'),
(11, 'Create a practice series', 'automatically create multiple practices from a single series. This can only be redeemed once.', 10, 'whitelabel', 0, 'Active'),
(12, 'Attend one practice', '2 points earned for each different user that attends a practice every year. Points are only earned after the user is placed in a lineup at the start time of a practice', 2, 'user', 31536000, 'Active'),
(13, 'Update your weight', 'Weight of each rower varies throughout the year. Rowers that update their weight every 3 months will be rewarded with 2 points each time.', 2, 'user', 7884000, 'Active'),
(14, 'Create first Zapier Zap', 'We value saving time via automation. Earn 5 points for each user that creates a Zap with the Boathouse Connect Zapier integration. The Zap must be live for 30 days before points are earned.', 5, 'user', 0, 'Active'),
(15, 'Create first task', 'Everyone needs reminders once in a while. 1 point is rewarded When a user creates their first task (and completes it).', 1, 'user', 0, 'Active');

-- update existing rows. Make sure to adjust these every time!!!!

UPDATE `reward_triggers` SET `reward_trigger_id` = 1,`action` = 'Verify email address',`descr` = 'When a new user account is created, earn points by verifying the email address.',`points` = 1,`entity` = 'user',`frequency` = 0,`status` = 'Inactive' WHERE `reward_triggers`.`reward_trigger_id` = 1;
UPDATE `reward_triggers` SET `reward_trigger_id` = 2,`action` = 'Verify cell phone number',`descr` = 'When a user adds a cell phone number and opts in to receive text message notifications.',`points` = 1,`entity` = 'user',`frequency` = 0,`status` = 'Inactive' WHERE `reward_triggers`.`reward_trigger_id` = 2;
UPDATE `reward_triggers` SET `reward_trigger_id` = 3,`action` = 'Create first group',`descr` = 'When an organization creates their first group and adds at least one member to the group. ',`points` = 5,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 3;
UPDATE `reward_triggers` SET `reward_trigger_id` = 4,`action` = 'Update rowing preferences',`descr` = 'Update your personal rowing preferences at least once per year. Earns 1 point / user / year',`points` = 1,`entity` = 'user',`frequency` = 31536000,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 4;
UPDATE `reward_triggers` SET `reward_trigger_id` = 5,`action` = 'Verify emergency contact',`descr` = 'Verify your personal emergency contact at least once per year. Earns 1 point / user / year',`points` = 1,`entity` = 'user',`frequency` = 31536000,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 5;
UPDATE `reward_triggers` SET `reward_trigger_id` = 6,`action` = 'Add USRowing number',`descr` = 'Add your personal USRowing number to your profile. If USRowing numbers are bulk-imported, one point per account will be added.',`points` = 1,`entity` = 'user',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 6;
UPDATE `reward_triggers` SET `reward_trigger_id` = 7,`action` = 'Link USRowing account for waiver verification',`descr` = 'Link the organizations USRowing account and automatically import waiver and membership status monthly. Points will also be earned for adding a USRowing waiver when new members join.',`points` = 10,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 7;
UPDATE `reward_triggers` SET `reward_trigger_id` = 8,`action` = 'Add first stakeholder',`descr` = 'Add your first stakeholder in a group. This can only be earned once, not for each group.',`points` = 5,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 8;
UPDATE `reward_triggers` SET `reward_trigger_id` = 9,`action` = 'Add first boat',`descr` = 'First time adding a boat to the organizations equipment inventory.',`points` = 5,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 9;
UPDATE `reward_triggers` SET `reward_trigger_id` = 10,`action` = 'Add first set of oars',`descr` = 'First time adding a set of oars to the organizations equipment inventory.',`points` = 5,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 10;
UPDATE `reward_triggers` SET `reward_trigger_id` = 11,`action` = 'Create a practice series',`descr` = 'automatically create multiple practices from a single series. This can only be redeemed once.',`points` = 10,`entity` = 'whitelabel',`frequency` = 0,`status` = 'Inactive' WHERE `reward_triggers`.`reward_trigger_id` = 11;
UPDATE `reward_triggers` SET `reward_trigger_id` = 12,`action` = 'Attend one practice',`descr` = '2 points earned for each different user that attends a practice every year. Points are only earned after the user is placed in a lineup at the start time of a practice',`points` = 2,`entity` = 'user',`frequency` = 31536000,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 12;
UPDATE `reward_triggers` SET `reward_trigger_id` = 13,`action` = 'Update your weight',`descr` = 'Weight of each rower varies throughout the year. Rowers that update their weight every 3 months will be rewarded with 2 points each time.',`points` = 2,`entity` = 'user',`frequency` = 7884000,`status` = 'Active' WHERE `reward_triggers`.`reward_trigger_id` = 13;
UPDATE `reward_triggers` SET `reward_trigger_id` = 14,`action` = 'Create first Zapier Zap',`descr` = 'We value saving time via automation. Earn 5 points for each user that creates a Zap with the Boathouse Connect Zapier integration. The Zap must be live for 30 days before points are earned.',`points` = 5,`entity` = 'user',`frequency` = 0,`status` = 'Inactive' WHERE `reward_triggers`.`reward_trigger_id` = 14;
UPDATE `reward_triggers` SET `reward_trigger_id` = 15,`action` = 'Create first task',`descr` = 'Everyone needs reminders once in a while. 1 point is rewarded When a user creates their first task (and completes it).',`points` = 1,`entity` = 'user',`frequency` = 0,`status` = 'Inactive' WHERE `reward_triggers`.`reward_trigger_id` = 15;

