<?php


class Service_Rowstats
{
    public function getRegattasByCustid($custid)
    {
        $regattasRaw = (new DB_Regattacentral_Regattas())->getRegattasAttendedByCustid($custid);
        $regattaFormatted = [];
        foreach ($regattasRaw as $regatta) {
            // check that the regatta is not a test regatta
            if (strpos($regatta["name"], '[TEST]') === false) {
                $regattaFormatted[] = [
                    "regatta_id"        => $regatta["regatta_id"],
                    "parent_id"         => $regatta["parent_id"],
                    "name"              => $regatta["name"],
                    "abbreviation"      => $regatta["abbreviation"],
                    "start_time"        => $regatta["start_time"],
                    "timezone"          => $regatta["timezone"],
                    "check_payable_to"  => $regatta["check_payable_to"],
                    "billing_address"   => $regatta["billing_address"],
                    "contact_name"      => $regatta["contact_name"],
                    "contact_email"     => $regatta["contact_email"],
                    "primary_host"      => $regatta["primary_host"],
                    "regatta_url"       => $regatta["regatta_url"],
                    "host_org_id"       => $regatta["host_org_id"],
                    "race_type"         => $regatta["race_type"],
                    "venue_id"          => $regatta["venue_id"],
                    "results_url"       => $regatta["results_url"],
                    "entered_events"    => $this->getParticipantEventsByCustid($regatta["regatta_id"], $custid)
                ];
            }
        }
        return $regattaFormatted;
    }

    public function getEventsByRegatta($regattaId)
    {
        $events = (new DB_Regattacentral_Regatta_Events())->getEventsByRegatta($regattaId);

        return $events;
    }

    public function getEntryIdByEventAndCustid($regattaId, $eventId, $custid)
    {
        $entry = (new DB_Regattacentral_Regatta_Entries_Participants())->getEntryByEventAndCustid($regattaId, $eventId, $custid);
        return $entry["entry_id"];
    }

    public function getParticipantEventsByCustid($regattaId, $custid)
    {
        $entriesRaw = (new DB_Regattacentral_Regatta_Events())->getEntriesByCustid($regattaId, $custid);
        $entriesFormatted = [];
        foreach ($entriesRaw as $entry) {
            $entriesFormatted[] = [
                "regatta_id"                    => $entry["regatta_id"],
                "event_id"                      => $entry["event_id"],
                "sequence"                      => $entry["sequence"],
                "label"                         => $entry["label"],
                "code"                          => $entry["code"],
                "title"                         => $entry["title"],
                "min_athlete_age"               => $entry["min_athlete_age"],
                "max_athlete_age"               => $entry["max_athlete_age"],
                "min_avg_age"                   => $entry["min_avg_age"],
                "max_avg_age"                   => $entry["max_avg_age"],
                "athlete_class"                 => $entry["athlete_class"],
                "athlete_count"                 => $entry["athlete_count_excluding_cox"],
                "sweep"                         => $entry["sweep"],
                "coxed"                         => $entry["coxed"],
                "cost"                          => $entry["cost"],
                "deadline_model"                => $entry["deadline_model"],
                "default_race_format"           => $entry["default_race_format"],
                "final_race_time"               => $entry["final_race_time"],
                "default_race_units"            => $entry["default_race_units"],
                "default_handicap_algorithm"    => $entry["default_handicap_algorithm"],
                "default_handicap_multiplier"   => $entry["default_handicap_multiplier"],
                "default_duration"              => $entry["default_duration"],
                "max_entries"                   => $entry["max_entries"],
                "max_entries_per_club"          => $entry["max_entries_per_club"],
                "max_alternates"                => $entry["max_alternates"],
                "require_coxswain"              => $entry["require_coxswain"],
                "status"                        => $entry["status"],
                "weight"                        => $entry["weight"],
                "gender"                        => $entry["gender"],
                "lineup"                        => $this->getLineup($regattaId, $entry["event_id"], null, $custid),
                "races"                         => $this->getEventRaces($regattaId, $entry["event_id"])
            ];
        }

        return $entriesFormatted;
    }

    public function getLineup($regattaId, $eventId, $entryId = null, $custid = null)
    {
        if (is_null($entryId)) {
            $entryId = $this->getEntryIdByEventAndCustid($regattaId, $eventId, $custid);
        }
        $lineup          = (new DB_Regattacentral_Regatta_Entries_Participants())->getParticipantsInEntry($regattaId, $eventId, $entryId);
        $lineupFormatted = [];
        foreach ($lineup as $row) {
            $bhcUser = $this->getNameFromParticipantId($row["participant_id"]);
            if (isset($bhcUser["participant_id"])) {
                $fname  = $bhcUser["fname"];
                $lname  = $bhcUser["lname"];
                $custid = $bhcUser["custid"];
            } else {
                $fname  = $row["fname"];
                $lname  = $row["lname"];
                $custid = 0;
            }
            $lineupFormatted[] = [
                "regatta_id"        => $row["regatta_id"],
                "event_id"          => $row["event_id"],
                "entry_id"          => $row["entry_id"],
                "participant_id"    => $row["participant_id"],
                "fname"             => $fname,
                "lname"             => $lname,
                "custid"            => $custid,
                "organization_id"   => $row["organization_id"],
                "org_name"          => $row["org_name"],
                "org_abbreviation"  => $row["org_abbreviation"],
                "sequence"          => $row["sequence"],
                "type"              => $row["type"]
            ];
        }

        return $lineupFormatted;
    }

    public function getEventRaces($regattaId, $eventId)
    {
        $rawRaces = (new DB_Regattacentral_Regatta_Races())->getRacesByEvent($regattaId, $eventId);
        $formattedRaces = [];
        foreach ($rawRaces as $race) {
            $formattedRaces[] = [
                "regatta_id"        => $race["regatta_id"],
                "event_id"          => $race["event_id"],
                "race_id"           => $race["race_id"],
                "uuid"              => $race["uuid"],
                "scheduled_start"   => $race["scheduled_start"],
                "display_number"    => $race["display_number"],
                "display_type"      => $race["display_type"],
                "status"            => $race["status"],
                "results"           => $this->getRaceResults($race["regatta_id"], $race["event_id"], $race["race_id"])
            ];
        }
        return $formattedRaces;
    }

    public function getRaceResults($regattaId, $eventId, $raceId)
    {
        $resultsRaw = (new DB_Regattacentral_Regatta_Races_Results())->getResultsByRace($regattaId, $eventId, $raceId);
        return $resultsRaw;
    }

    private function getNameFromParticipantId($participantId)
    {
        return (new DB_Regattacentral_Participant_Custid_Map())->getBhcName($participantId);
    }

}