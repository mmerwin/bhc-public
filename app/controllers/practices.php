<?php


class practices extends Controller
{

    /**
     * Retrieves all practices
     * @api
     * @param practice_id int practice_id to be found. If this param is used, none of the other search parameters will be considered.
     * @param limit int max number of practices to return. Default is 10.
     * @param attending boolean restricts results to only practices that the custid parameter specified is attending. Default is false.
     * @param upcoming boolean restricts results to only practices that are in the future or currently in progress. Default is true.
     * @param eligible boolean restricts results to only practices that the custid parameter specified is eligible to attend. Default is false.
     * @param since int restricts results to practices ending before timestamp provided if the `upcoming` parameter is true, or after if the `upcoming` parameter is false. default is the current UNIX timestamp.
     * @param custid int custid of the user you want to impose search restrictions with the attending = true and/or eligible = true parameters.
     * @return array array of practices matching search criteria. Note that if practice_id is provided, the result is an array, not an object
     */
    public function getPractices()
    {
        $practiceId = $this->param("practice_id");
        $limit      = intval((is_null($this->param("limit"))) ? 10 : $this->param("limit"));
        $attending  = (strtolower($this->param("attending")) == "true") ? true : false;
        $upcoming   = (strtolower($this->param("upcoming")) == "false") ? false : true;
        $eligible   = (strtolower($this->param("eligible")) == "true") ? true : false;
        $since      = intval(($this->param("since") > 0) ? $this->param("since") : time());
        $custid     = ($this->param("custid") > 0) ? intval($this->param("custid")) : null;

        if (!is_null($practiceId)) {
            $practices = (new Service_Practices($_SESSION['whitelabel_id']))->getPracticeById($_SESSION["whitelabel_id"], $practiceId);
        } else {
            $practices  = (new Service_Practices($_SESSION["whitelabel_id"]))->getPractices($_SESSION["whitelabel_id"],$limit, $upcoming, $attending, $since, $custid, $eligible);
        }

        $this->view("json", $practices);
    }


    /**
     * Retrieves all practice series for a whitelabel
     * @api
     * @param series_id int series_id to be found. If this param is used, none of the other search parameters will be considered.
     * @return array array of practice series matching search criteria. Note that if series_id is provided, the result is an array, not an object
     */
    public function getSeries($seriesId = null)
    {
        if (is_null($seriesId)) {
            $seriesId = $this->param("series_id");
        }
        $seriesData = (new Service_Practice_Series())->getSeriesById($_SESSION["whitelabel_id"], $seriesId);

        $this->view("json", $seriesData);
    }

    public function createSeries()
    {
        $name           = $this->param("name");
        $locationId     = $this->param("location_id");
        $maxAttendees   = $this->param("max_attendees");
        $startDate      = $this->param("start_date");
        $endDate        = $this->param("end_date");
        $startTime      = $this->param("start_time");
        $endTime        = $this->param("end_time");
        $visitingRowers = $this->param("visiting_rowers_allowed");

        $days = array (
            "monday"    => $this->param("monday"),
            "tuesday"   => $this->param("tuesday"),
            "wednesday" => $this->param("wednesday"),
            "thursday"  => $this->param("thursday"),
            "friday"    => $this->param("friday"),
            "saturday"  => $this->param("saturday"),
            "sunday"    => $this->param("sunday")
        );
        $daysArray = $this->generateDaysFromArray($days);

        $newSeriesId = (new Service_Practice_Series())->createNewSeries($_SESSION["whitelabel_id"], $name, $locationId, $maxAttendees, $startDate, $endDate, $visitingRowers, $startTime, $endTime, $_SESSION["custid"], $daysArray);

        $this->getSeries($newSeriesId);
    }

    private function generateDaysFromArray($data)
    {
        $returnable = [];
        if (!is_null($data["monday"]))      { $returnable[] = "Monday"; }
        if (!is_null($data["tuesday"]))     { $returnable[] = "Tuesday"; }
        if (!is_null($data["wednesday"]))   { $returnable[] = "Wednesday"; }
        if (!is_null($data["thursday"]))    { $returnable[] = "Thursday"; }
        if (!is_null($data["friday"]))      { $returnable[] = "Friday"; }
        if (!is_null($data["saturday"]))    { $returnable[] = "Saturday"; }
        if (!is_null($data["sunday"]))      { $returnable[] = "Sunday"; }

        return $returnable;
    }

    public function createPractice()
    {
        $seriesId       = $this->param("series_id");
        $name           = $this->param("name");
        $locationId     = $this->param("location_id");
        $startTime      = $this->param("start_time");
        $endTime        = $this->param("end_time");
        $maxAttendees   = $this->param("max_attendees");
        $public         = $this->param("public");
        $visitingRowers = $this->param("visiting_rowers_allowed");

        $practiceService = new Service_Practices($_SESSION["whitelabel_id"]);

        //verify that the location_id belongs to the whitelabel

        $locationVerify = (new PracticeLocations($_SESSION["whitelabel_id"]))->getLocationById($locationId);
        if (is_null($locationId) && $seriesId > 0 || (isset($locationVerify["location_id"]) && $locationVerify["location_id"] == $locationId)) {
            $newPractice = $practiceService->createPractice($_SESSION["whitelabel_id"], $seriesId, $name, $locationId, $startTime, $endTime, $maxAttendees, $public, $visitingRowers, $_SESSION["custid"],true);
            $returnable = array(
                "Status"=>"Success",
                "practice_id"=> $newPractice
            );
        } else {
            $returnable = array("Status"=>"Error", "Message"=>"Bad location_id");
        }
        $this->view("json", $returnable);
    }

    public function getAllSeries()
    {
        $seriesService  = new Service_Practice_Series();
        $allSeries      = $seriesService->getAllSeries($_SESSION["whitelabel_id"]);
        $this->view("json",$allSeries);
    }

    public function updatePractice()
    {
        $practiceId     = $this->param("practice_id");
        $name           = $this->param("name");
        $locationId     = (new PracticeLocations($_SESSION["whitelabel_id"]))->checkIfAffiliated($this->param("location_id"));
        $startTime      = $this->param("start_time");
        $endTime        = $this->param("end_time");
        $maxAttendees   = $this->param("max_attendees");
        $lineupsSet     = $this->param("lineups_set");
        $visitingRowers = $this->param("visiting_rowers_allowed");

        (new Service_Practices($_SESSION["whitelabel_id"]))->updatePracticeMeta($practiceId, $name, $locationId, $startTime, $endTime, $maxAttendees, $lineupsSet, $visitingRowers);

        $updatedPractice = (new Service_Practices($_SESSION["whitelabel_id"]))->getPracticeById($_SESSION["whitelabel_id"], $practiceId);
        $this->view("json", $updatedPractice);
    }

    /**
     * Sets null values of past (or already started practices) to the value of their series
     * Should only run via crontab, but can run via web browser when needed.
     */
    public function normalizePastPracticesFromSeries()
    {
        //get past practices
        $pastPractices = (new DB_Practice_Schedule_Meta())->getPastPracticesFromAllWhitelabels();
        $DBpsm         = new DB_Practice_Series_Meta();

        foreach ($pastPractices as $practice) {
            //get series data
            $series = $DBpsm->getSeriesById($practice["whitelabel_id"], $practice["series_id"]);
            //prepare update
            if (is_null($practice["name"]))             { $name           = $series["name"];}            else { $name           = $practice["name"];}
            if (is_null($practice["location_id"]))      { $locationId     = $series["location_id"];}     else { $locationId     = $practice["location_id"];}
            if (is_null($practice["max_attendees"]))    { $maxAttendees   = $series["max_attendees"];}   else { $maxAttendees   = $practice["max_attendees"];}
            if (is_null($practice["visiting_rowers"]))  { $visitingRowers = $series["visiting_rowers"];} else { $visitingRowers = $practice["visiting_rowers"];}
            (new Service_Practices($_SESSION["whitelabel_id"]))->updatePracticeMeta($practice["practice_id"], $name, $locationId, $practice["start_time"], $practice["end_time"], $maxAttendees, $practice["lineups_set"], $visitingRowers);
        }
    }

    public function getLocations()
    {
        $locationId = $this->param("location_id");
        $practiceLocations = new PracticeLocations($_SESSION["whitelabel_id"]);

        if (!is_null($locationId)) {
            $locations = $practiceLocations->getLocationById($locationId);
        } else {
            $locations = $practiceLocations->getAllLocations();
        }

        $this->view("json", $locations);
    }

    public function addLocation()
    {
        $name    = $this->param("name");
        $address = $this->param("address");
        $city    = $this->param("city");
        $state   = $this->param("state");
        $zipcode = $this->param("zipcode");
        $fee     = (is_null($this->param("fee"))) ? "0" : $this->param("fee");
        $notes   = $this->param("notes");
        $active  = (is_null($this->param("active"))) ? "Yes" : $this->param("active");

        $practiceLocations = (new PracticeLocations($_SESSION["whitelabel_id"]));
        $practiceLocations->addLocation($name, $address, $city, $state, $zipcode, $fee, $notes, $active);
        $allLocations = $practiceLocations->getAllLocations();
        WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.practice.location", "true");

        $this->view("json", $allLocations);
    }

    public function UpdateLocationAddress()
    {
        $locationId = $this->param("location_id");
        $address    = $this->param("address");
        $city       = $this->param("city");
        $state      = $this->param("state");
        $zipcode    = $this->param("zipcode");

        $practiceLocations = (new PracticeLocations($_SESSION["whitelabel_id"]));
        $practiceLocations->updateAddress($locationId, $address, $city, $state, $zipcode);
        $allLocations = $practiceLocations->getAllLocations();

        $this->view("json", $allLocations);
    }

    public function UpdateLocationName()
    {
        $locationId = $this->param("location_id");
        $name       = $this->param("name");

        $practiceLocations = (new PracticeLocations($_SESSION["whitelabel_id"]));
        $practiceLocations->updateName($locationId, $name);
        $allLocations = $practiceLocations->getAllLocations();

        $this->view("json", $allLocations);
    }

    public function UpdateLocationNotes()
    {
        $locationId = $this->param("location_id");
        $notes      = $this->param("notes");

        $practiceLocations = (new PracticeLocations($_SESSION["whitelabel_id"]));
        $practiceLocations->updateNotes($locationId, $notes);
        $allLocations = $practiceLocations->getAllLocations();

        $this->view("json", $allLocations);
    }

    public function UpdateLocationFee()
    {
        $locationId = $this->param("location_id");
        $fee        = (is_null($this->param("fee"))) ? "0" : $this->param("fee");

        $practiceLocations = (new PracticeLocations($_SESSION["whitelabel_id"]));
        $practiceLocations->updateFee($locationId, $fee);
        $allLocations = $practiceLocations->getAllLocations();

        $this->view("json", $allLocations);
    }

    public function activateLocation()
    {
        $locationId = $this->param("location_id");
        $active     = $this->param("active");

        $location = new PracticeLocations($_SESSION["whitelabel_id"]);

        switch (strtolower($active)) {
            case "yes":
                $location->activateLocation($locationId);
                break;
            case "no":
                $location->deactivateLocation($locationId);
                break;
        }

        $allLocations = $location->getAllLocations();
        $this->view("json", $allLocations);
    }

    public function updateSessionplan()
    {
        $practiceId = $this->param("practice_id");
        $text = $this->param("sessionplan");

        $practice = new Service_Practices($_SESSION["whitelabel_id"]);
        $practice->updateSessionplan($practiceId, $text);
    }

    /**
     * Adds a group to a practice
     * @api
     * @param practice_id int practice_id of the practice.
     * @param group_id int group_id of the group being added to the practice.
     * @param attendance_start int number of seconds before practice starts that a user is allowed to declare their attendance.
     * @param attendance_end int number of seconds before practice starts that a user must have their attendance plans finalized.
     */
    public function addGroupToPractice()
    {
        $practiceId      = $this->param("practice_id");
        $groupId         = $this->param("group_id");
        $attendanceStart = $this->param("attendance_start");
        $attendanceEnd   = $this->param("attendance_end");
        $practice        = new Service_Practices($_SESSION["whitelabel_id"]);

        $practice->addGroup($practiceId, $groupId, $attendanceStart, $attendanceEnd);
    }

    public function removeGroup()
    {
        $practiceId      = $this->param("practice_id");
        $groupId         = $this->param("group_id");
        $practice        = new Service_Practices($_SESSION["whitelabel_id"]);

        $practice->removeGroup($practiceId, $groupId);
    }

    /**
     * Updates attendance allowances for a group in a practices
     * @param practice_id int practice_id of the practice.
     * @param group_id int group_id of the group being modified in the practice.
     * @param attendance_start int number of seconds before practice starts that a user is allowed to declare their attendance.
     * @param attendance_end int number of seconds before practice starts that a user must have their attendance plans finalized.
     */
    public function updateGroup()
    {
        $practiceId      = $this->param("practice_id");
        $groupId         = $this->param("group_id");
        $attendanceStart = $this->param("attendance_start");
        $attendanceEnd   = $this->param("attendance_end");
        $practice        = new Service_Practices($_SESSION["whitelabel_id"]);

        $practice->updateGroup($practiceId, $groupId, $attendanceStart, $attendanceEnd);
    }

    public function setAttendance()
    {
        $practiceId = $this->param("practice_id");
        $custid     = (is_null($this->param("custid"))) ? $_SESSION["custid"] : intval($this->param("custid"));
        $attendance = $this->param("attendance");
        $practices = new Service_Practices($_SESSION["whitelabel_id"]);
        $practices->setAttendance($practiceId, $custid, $attendance);

    }

    public function addAthleteToLineup()
    {
        $practiceId = $this->param("practice_id");
        $custid     = $this->param("custid");
        $boatId     = $this->param("boat_id");
        $position   = $this->param("position");

        (new Service_Lineups())->addToLineup($_SESSION["whitelabel_id"], $practiceId, $custid, $boatId, $position);
        $practices = (new Service_Practices($_SESSION['whitelabel_id']))->getPracticeById($_SESSION["whitelabel_id"], $practiceId);
        $this->view("json", $practices);
    }

    public function removeFromLineup()
    {
        $practiceId = $this->param("practice_id");
        $boatId     = $this->param("boat_id");
        $position   = $this->param("position");

        (new Service_Lineups())->removeFromLineup($_SESSION["whitelabel_id"], $practiceId, $boatId, $position);

        $practices = (new Service_Practices($_SESSION['whitelabel_id']))->getPracticeById($_SESSION["whitelabel_id"], $practiceId);
        $this->view("json", $practices);
    }

    public function updateLineupFocus()
    {
        $practiceId = $this->param("practice_id");
        $focusId    = $this->param("focus_id");

        $practiceService = (new Service_Practices($_SESSION['whitelabel_id']));
        $practiceService->updateLineupFocus($practiceId, $focusId);

        $allPractices = $practiceService->getPracticeById($_SESSION["whitelabel_id"], $practiceId);
        $this->view("json", $allPractices);
    }

    public function publishLineups()
    {
        $practiceId = $this->param("practice_id");
        (new Service_Lineups())->markPublished($_SESSION["whitelabel_id"], $practiceId);
    }

    public function sendMessageToAttendees()
    {
        $practiceId = $this->param("practice_id");
        $type       = $this->param("message_type");
        $subject    = $this->param("subject");
        $body       = $this->param("body");

        if (strtolower($type) == "email") {
            //create the message id
            $messageId = (new DB_External_Messages())->setPracticeMessage($_SESSION['whitelabel_id'], $_SESSION['custid'], "Email", $subject, $body);

            //populate external_message_recipients table
            (new DB_External_Messages_Recipients())->populatePracticeEmailRecipients($_SESSION["whitelabel_id"], $practiceId, $messageId);

            //set email as prepared and ready to send
            (new DB_External_Messages())->setPracticeMessageReady($messageId);

        } else if (strtolower($type) == "text") {
            //create the message id
            $messageId = (new DB_External_Messages())->setPracticeMessage($_SESSION['whitelabel_id'], $_SESSION['custid'], "Text", '', $body);

            //populate external_message_recipients table
            (new DB_External_Messages_Recipients())->populatePracticeTextRecipients($_SESSION["whitelabel_id"], $practiceId, $messageId);

            //set email as prepared and ready to send
            (new DB_External_Messages())->setPracticeMessageReady($messageId);
        }
    }

    public function getPracticeSurveyResults()
    {
        $practiceId = $this->param("practice_id");

        $data = (new Service_Lineups())->getPracticeSurveyResults($_SESSION["whitelabel_id"], $practiceId);
        $this->view("json", $data);
    }

    public function updatePrePracticeEmail()
    {
        $setting = $this->param("setting");

        switch (strtoupper($setting)) {
            case "NO":
                WhitelabelSettings::updateValue($_SESSION["whitelabel_id"], "sendPrepracticeEmails", "No");
                break;
            case "YES":
            default:
            WhitelabelSettings::updateValue($_SESSION["whitelabel_id"], "sendPrepracticeEmails", "Yes");
        }
    }

}
