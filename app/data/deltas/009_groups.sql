--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
                                        `group_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `whitelabel_id` int(11) NOT NULL,
                                        `title` varchar(50) NOT NULL,
                                        `descr` text NOT NULL,
                                        `max` int(11) NOT NULL,
                                        `self_join` varchar(5) DEFAULT 'No',
                                        `join_fee` int(11) DEFAULT NULL,
                                        PRIMARY KEY (`group_id`),
                                        KEY `fk_whitelabel_groups` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
    ADD CONSTRAINT `fk_whitelabel_groups` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
COMMIT;
