<?php


class DB_Biometrics_health extends DataModel
{
    protected $schema = "biometrics_health";

    public function update($custid, $dietary, $allergies)
    {
        //create entry if not exists
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (custid, dietary, allergies) VALUES (?, ?, ?)", array($custid, $dietary, $allergies));

        //update entry
        $this->DB->query("UPDATE {$this->schema} SET dietary = ?, allergies = ? WHERE custid = ?", array($dietary, $allergies,$custid));
    }

    public function getData($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? LIMIT 1", array($custid))->fetchArray();
    }

}