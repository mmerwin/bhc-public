<?php


class DB_Practice_Locations extends DataModel
{
    protected $schema = "practice_locations";

    public function getWhitelabelLocations($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getByLocationId($whitelabel_id, $locationId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND location_id = ?",array($whitelabel_id, $locationId))->fetchArray();
    }

    public function addLocation($whitelabel_id, $name, $address, $city, $state, $zip, $fee, $notes, $active)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, name, address, city, state, zipcode, fee, notes, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", array($whitelabel_id, $name, $address, $city, $state, $zip, $fee, $notes, $active));
    }

    public function updateAttribute($whitelabel_id,$locationId, $attribute, $newValue)
    {
        switch(strtolower($attribute))
        {
            case "name":
                $tag = "name";
                break;
            case "address":
                $tag = "address";
                break;
            case "city":
                $tag = "city";
                break;
            case "state":
                $tag = "state";
                break;
            case "zip":
            case "zipcode":
                $tag = "zipcode";
                break;
            case "fee":
                $tag = "fee";
                break;
            case "notes":
                $tag = "notes";
                break;
            default:
                return [];
        }
        $this->DB->query("UPDATE {$this->schema} SET {$tag} = ? WHERE whitelabel_id = ? AND location_id = ? LIMIT 1", array($newValue, $whitelabel_id, $locationId));
    }

    public function updateAddress($whitelabel_id, $locationId, $address, $city, $state, $zip)
    {
        $this->DB->query("UPDATE {$this->schema} SET address = ?, city = ?, state = ?, zipcode = ? WHERE whitelabel_id = ? AND location_id = ? LIMIT 1", array($address, $city, $state, $zip, $whitelabel_id, $locationId));
    }

    public function updateActive($whitelabel_id, $locationId, $status)
    {
        switch(strtolower($status))
        {
            case "yes":
                $tag = "Yes";
                break;
            case "no":
                $tag = "No";
                break;
            default:
                return [];
        }
        $this->DB->query("UPDATE {$this->schema} SET active = ? WHERE whitelabel_id = ? AND location_id = ? LIMIT 1", array($tag, $whitelabel_id, $locationId));
    }

}