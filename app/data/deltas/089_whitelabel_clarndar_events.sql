--
-- Table structure for table `whitelabel_calendar_events`
--

CREATE TABLE IF NOT EXISTS `whitelabel_calendar_events` (
                                                            `event_id` int(11) NOT NULL AUTO_INCREMENT,
                                                            `whitelabel_id` int(11) NOT NULL,
                                                            `start_time` int(11) NOT NULL,
                                                            `end_time` int(11) NOT NULL,
                                                            `public` varchar(10) NOT NULL DEFAULT 'Yes',
                                                            `title` varchar(200) NOT NULL,
                                                            `descr` text NOT NULL,
                                                            `link_color` varchar(50) NOT NULL,
                                                            PRIMARY KEY (`event_id`),
                                                            KEY `fk_whitelabel_id_whitelabel_calendar_events` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `whitelabel_calendar_events`
--
ALTER TABLE `whitelabel_calendar_events`
    ADD CONSTRAINT `fk_whitelabel_id_whitelabel_calendar_events` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
