--
-- Table structure for table `navbar_parents`
--

CREATE TABLE IF NOT EXISTS `navbar_parents` (
                                                `navbar_parent_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `text` varchar(15) NOT NULL,
                                                `icon` varchar(50) NOT NULL,
                                                `weight` int(11) NOT NULL DEFAULT 0,
                                                `header` varchar(20) NOT NULL,
                                                PRIMARY KEY (`navbar_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `navbar_parents`
--

INSERT IGNORE INTO `navbar_parents` (`navbar_parent_id`, `text`, `icon`, `weight`, `header`) VALUES
(1, 'My Profile', 'fas fa-user', 1, 'personal'),
(2, 'Groups', 'fas fa-users', 2, 'User Manager'),
(3, 'Communications', 'fas fa-envelope', 3, 'personal'),
(4, 'Practices', 'fas fa-bullhorn', 4, ''),
(5, 'Equipment', 'fas fa-warehouse', 5, ''),
(6, 'Regattas', 'fas fa-flag-checkered', 6, ''),
(7, 'Tasks', 'fas fa-list-ol', 7, ''),
(8, 'Org Settings', 'fas fa-cog', 8, ''),
(9, 'Waivers', 'fas fa-newspaper', 9, ''),
(10, 'Finances', 'fas fa-dollar-sign', 10, 'personal'),
(11, 'RowStats', 'mr-2 mdi mdi-chart-areaspline', 11, '');
COMMIT;

-- create update statements
UPDATE `navbar_parents` SET `navbar_parent_id` = 1,`text`  = 'My Profile',`icon` = 'fas fa-user',`weight` = 1,`header` = 'personal' WHERE `navbar_parents`.`navbar_parent_id` = 1;
UPDATE `navbar_parents` SET `navbar_parent_id` = 2,`text`  = 'Groups',`icon` = 'fas fa-users',`weight` = 2,`header` = 'User Manager' WHERE `navbar_parents`.`navbar_parent_id` = 2;
UPDATE `navbar_parents` SET `navbar_parent_id` = 3,`text`  = 'Communications',`icon` = 'fas fa-envelope',`weight` = 3,`header` = 'personal' WHERE `navbar_parents`.`navbar_parent_id` = 3;
UPDATE `navbar_parents` SET `navbar_parent_id` = 4,`text`  = 'Practices',`icon` = 'fas fa-bullhorn',`weight` = 4,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 4;
UPDATE `navbar_parents` SET `navbar_parent_id` = 5,`text`  = 'Equipment',`icon` = 'fas fa-warehouse',`weight` = 5,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 5;
UPDATE `navbar_parents` SET `navbar_parent_id` = 6,`text`  = 'Regattas',`icon` = 'fas fa-flag-checkered',`weight` = 6,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 6;
UPDATE `navbar_parents` SET `navbar_parent_id` = 7,`text`  = 'Tasks',`icon` = 'fas fa-list-ol',`weight` = 7,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 7;
UPDATE `navbar_parents` SET `navbar_parent_id` = 8,`text`  = 'Org Settings',`icon` = 'fas fa-cog',`weight` = 8,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 8;
UPDATE `navbar_parents` SET `navbar_parent_id` = 9,`text`  = 'Waivers',`icon` = 'fas fa-newspaper',`weight` = 9,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 9;
UPDATE `navbar_parents` SET `navbar_parent_id` = 10,`text` = 'Finances',`icon` = 'fas fa-dollar-sign',`weight` = 10,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 10;
UPDATE `navbar_parents` SET `navbar_parent_id` = 11,`text` = 'RowStats',`icon` = 'mr-2 mdi mdi-chart-areaspline',`weight` = 11,`header` = '' WHERE `navbar_parents`.`navbar_parent_id` = 11;
