<?php


class Service_CDN
{
    const ACCESSKEY = "IX6AIF6WU2GPO2BAS3A5";
    const SECRETKEY = "xIojOMfRu9vYFiA19mlb76U7Q2Hj4b4i8/WTmXIU7T4";

    public function uploadProfilePicture($custid)
    {
        // generate slug
        $slug = md5(time()."-ddsew3fbhconnect11535-".rand(1,9999999)."profilePicture").rand(1,9999999);

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) {
            $url      = Environment::getEnvCode()."/PROFILEPICTURES/".$custid."/".$slug.".".pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            $my_space = Spaces(self::ACCESSKEY, self::SECRETKEY)->space("bhconnect1", "nyc3");
            $my_space->uploadFile($_FILES["image"]["tmp_name"], $url, "public");

            return $url;

        } else {
            echo "File is not an image.";
            $uploadOk = 0;
            return null;
        }

    }


    public function uploadWhitelabelLogo($whitelabel_id)
    {
        // generate slug
        $slug = md5(time()."-ddsew3fbhconnect996583-".rand(1,9999999)."whitelabellogo").rand(1,9999999);

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if ($check !== false) {
            $url      = Environment::getEnvCode()."/WHITELABELLOGOS/".$whitelabel_id."/".$slug.".".pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            $my_space = Spaces(self::ACCESSKEY, self::SECRETKEY)->space("bhconnect1", "nyc3");
            $my_space->uploadFile($_FILES["image"]["tmp_name"], $url, "public");

            return $url;

        } else {
            echo "File is not an image.";
            $uploadOk = 0;
            return null;
        }

    }
    
    public function uploadExternalMessageAttachment($whitelabel_id, $messageId)
    {
        // generate slug
        $slug = md5(time()."-58gjh438gh47gh48-".rand(1,9999999)."whitelabellogo").rand(1,9999999);

        $url      = Environment::getEnvCode()."/EMAILATTACHMENTS/".$whitelabel_id."/".$messageId."/".$slug.".".pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
        $my_space = Spaces(self::ACCESSKEY, self::SECRETKEY)->space("bhconnect1", "nyc3");
        $my_space->uploadFile($_FILES["image"]["tmp_name"], $url, "public");
        
        // add url to the external_message metadata
        (new DB_External_Messages())->addAttachmentUrl($whitelabel_id, $messageId, $url);
        (new DB_External_Messages())->addAttachmentName($whitelabel_id, $messageId, $_FILES['image']['name']);
        return $url;

    }

}