<?php


class Groups
{
    private $whitelabel_id;
    private $initialInstall = false;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function installDefaultGroups()
    {
        $this->initialInstall = true;
        $groupsTable          = new DB_groups();
        $permissions          = new Permissions($this->whitelabel_id);
        $defaultGroups        = $groupsTable->getDefaultGroups();

        foreach ($defaultGroups as $group) {
            // check that group does not already exist
            $exists = $groupsTable->checkIfGroupExists($this->whitelabel_id, $group['title']);
            if (!$exists) {
                $newGroupId = $this->addNewGroup($group['title'], $group['descr'], $group['max'], $group['self_join']);
                // get default permissions for default group
                $defaultPermissions = $permissions->getDefaultPermissionByDefaultGroupId($group['default_group_id']);
                foreach ($defaultPermissions as $newPermission) {
                    $permissions->addPermissionToGroup($newPermission['permission_id'], $newGroupId);
                }
            }
        }
        return array("status"=>"success", "message" =>"Default groups and permissions installed");
    }

    public function addNewGroup($title, $descr, $max, $selfJoin)
    {
        // add rewards points if creating first group
        if (!$this->initialInstall) {
            (new Service_Rewards($this->whitelabel_id))->addRewardPoints(3, $_SESSION['custid']);
        }
        return (new DB_groups())->addNewGroup($this->whitelabel_id, $title, $descr, $max, $selfJoin);
    }

    public function addUserToGroup($groupId,$custid)
    {
        // verify there is space in the group
        $groupMetaData = $this->getGroupMetaData($groupId);
        if (($groupMetaData['member_count'] < $groupMetaData['max']) || ($groupMetaData['max'] == 0)) {
            (new DB_groups())->addUserToGroup($this->whitelabel_id,$groupId, $custid);

            // remove join request if it exists
            (new DB_Groups_Join_Requests())->removeRequest($groupId, $this->whitelabel_id, $custid);

            // if the member was added by another person (not a self-join)
            if($_SESSION['custid'] != $custid) {
                Service_Notification_Send::addedToGroup($custid, $groupId);
            }

            // add group join fee if it exists
            (new Service_User_Payments())->addChargeForUserGroupJoin($this->whitelabel_id, $groupId, $custid);
            // add all future charges associated with the group
            (new Service_User_Payments())->addUserToFeeGroup($this->whitelabel_id, $groupId, $custid);
        }
    }

    public function checkIfGroupExists($groupId)
    {
        return (new DB_groups())->checkIfGroupExistsById($this->whitelabel_id, $groupId);
    }

    public function getWhitelabelGroups($custid = 0)
    {
        $groups                 = (new DB_groups())->getWhitelabelGroups($this->whitelabel_id, $custid);
        $equipment              = (new EquipmentModel($this->whitelabel_id));
        $groupsWithCompleteData = [];
        foreach ($groups as $row) {
            $groupsWithCompleteData[] = [
                "group_id"                  => $row['group_id'],
                "whitelabel_id"             => $row['whitelabel_id'],
                "title"                     => $row['title'],
                "descr"                     => $row['descr'],
                "max"                       => $row['max'],
                "self_join"                 => $row['self_join'],
                "join_fee"                  => $row["join_fee"],
                "current_member"            => $row['current_member'],
                "member_count"              => $row['members'],
                "members"                   => self::getGroupMembers($row['group_id']),
                "join_requests"             => self::getJoinRequests($row['group_id']),
                "stakeholders"              => self::getAllStakeholders($row['group_id']),
                "permissions"               => self::getGroupPermissions($row['group_id']),
                "boat_checkout_permissions" => $equipment->getGroupPermissions($row['group_id'])
            ];
        }

        return $groupsWithCompleteData;
    }

    public function getSuperAdminId()
    {
        $allGroups = $this->getWhitelabelGroups();
        $returnId  = $this->loopToFindSuperAdmin($allGroups);
        if (is_null($returnId)) {
            $this->installDefaultGroups();
            return $this->loopToFindSuperAdmin($allGroups);
        } else {
            return $returnId;
        }

    }

    private function loopToFindSuperAdmin($allGroups)
    {
        foreach($allGroups as $group) {
            if ($group['title'] == "Super Admin") {
                return $group['group_id'];
            }
        }
        return null;
    }

    public function getGroupPermissions($groupId)
    {
        return (new DB_Permissions())->getGroupPermissions($groupId);
    }

    public function getGroupMembers($groupId)
    {
        return (new DB_groups())->getGroupMembers($groupId, $this->whitelabel_id);
    }

    public function getNonGroupMembers($groupId)
    {
        $whitelabelUsers = (new DB_User_Affiliations())->findAllAffiliationsByWhitelabel($this->whitelabel_id);
        $currentMembers = (new DB_groups())->getGroupMembers($groupId, $this->whitelabel_id);

        $returnable = [];
        foreach ($whitelabelUsers as $user) {
            // check if the user is already a current member
            $member = false;
            foreach ($currentMembers as $cu) {
                if($cu['custid'] == $user['custid']) {
                    $member = true;
                }
            }
            if (!$member) {
                // add to returnable list
                $returnable[] = array(
                    "custid"        => $user['custid'],
                    "fname"         => $user['fname'],
                    "lname"         => $user['lname'],
                    "birth_year"    => $user['birth_year'],
                    "birth_month"   => $user['birth_month'],
                    "birth_day"     => $user['birth_day'],
                    "sex"           => $user['sex'],
                    "joined_bc_on"  => $user['joined_bc_on'],
                    "email"         => $user['email']
                );
            }
        }
        return $returnable;
    }

    public function addPermissionToGroup($permissionId, $groupId)
    {
        // check that groupId belongs to session whitelabel
        $exists = (new DB_groups())->checkIfGroupExistsById($this->whitelabel_id, $groupId);
        if ($exists) {
            // add permission to group
            (new DB_Permissions())->addGroupPermission($groupId, $permissionId);
            return array("Success"=>"Permission added");
        }
        else {
            return array("Error"=>"group_id does not exist in whitelabel");
        }
    }

    public function removePermissionFromGroup($permissionId, $groupId)
    {
        // check that groupId belongs to session whitelabel
        $exists = (new DB_groups())->checkIfGroupExistsById($this->whitelabel_id, $groupId);
        if ($exists) {
            // add permission to group
            (new DB_Permissions())->removeGroupPermission($groupId, $permissionId);
            return array("Success"=>"Permission removed");
        }
        else {
            return array("Error"=>"group_id does not exist in whitelabel");
        }
    }

    public function getNonAssignedPermissions($groupId)
    {
        $exists = (new DB_groups())->checkIfGroupExistsById($this->whitelabel_id, $groupId);
        if ($exists) {
            return (new DB_Permissions())->getNonAssignedPermissions($groupId);
        }
        else {
            return array("Error"=>"group_id does not exist in whitelabel");
        }

    }

    public function getGroupMetaData($groupId)
    {
        return (new DB_groups())->getGroupMetaData($groupId);
    }

    public function updateGroupDetails($groupId, $metadata)
    {
        // make sure self_join is equal to "No" or "Yes"
        if ($metadata['self_join'] == 'no') {
            $metadata['self_join'] = "No";
        }
        if ($metadata['self_join'] == 'yes') {
            $metadata['self_join'] = "Yes";
        }
        if ($metadata['self_join'] != "No" && $metadata['self_join'] != "Yes") {
            $metadata['self_join'] = "No";
        }
        // make sure max is a number and greater than zero
        if ($metadata['max'] < 0 || !is_numeric($metadata['max'])) {
            $metadata['max'] = 0;
        }


        return (new DB_groups())->updateGroupMetaData($groupId, $metadata);
    }

    public function removeMemberFromGroup($groupId, $custid)
    {
        (new DB_groups())->deleteUserFromGroup($groupId, $custid, $this->whitelabel_id);
        // if the member was added by another person (not a self-join)
        if ($_SESSION['custid'] != $custid) {
            Service_Notification_Send::removeFromGroup($custid, $groupId);
        }
        // remove all future recurring charges for the group
        (new Service_User_Payments())->removeUserFromFeeGroup($this->whitelabel_id, $groupId, $custid);
    }

    public function deleteGroup($groupId)
    {
        // remove all members from the group
        foreach (self::getGroupMembers($groupId) as $member) {
            // by calling removeMemberFromGroup, the user will receive a notification that they have been removed from the group.
            // it should be very rare that a group is deleted, so the inefficiency of this method is tolerable
            self::removeMemberFromGroup($groupId, $member['custid']);
        }

        // remove group. Permissions inside of permissions_groups should automatically be deleted by foreign key constraint cascade.
        (new DB_groups())->deleteGroup($groupId,$this->whitelabel_id);

    }

    public function addStakeholder($groupId, $name, $email)
    {
        // rewards points for first-stakeholder
        (new Service_Rewards($this->whitelabel_id))->addRewardPoints(8, $_SESSION['custid']);

        (new DB_Groups_Stakeholders())->addStakeholder($this->whitelabel_id, $groupId, $name, $email);
    }

    public function getAllStakeholders($groupId)
    {
        return (new DB_Groups_Stakeholders())->getAllStakeholders($this->whitelabel_id, $groupId);
    }

    public function deleteStakeholder($stakeholderId, $groupId)
    {
        (new DB_Groups_Stakeholders())->deleteStakeholder($stakeholderId, $this->whitelabel_id, $groupId);
    }

    public function getJoinRequests($groupId)
    {
        $rawRequests  = (new DB_Groups_Join_Requests())->getAllRequestsByGroupId($groupId, $this->whitelabel_id);
        $joinRequests = [];
        foreach ($rawRequests as $user) {
            $joinRequests[] = [
              "group_id"                => $user['group_id'],
              "custid"                  => $user['custid'],
              "created_at"              => $user['created_at'],
              "fname"                   => $user['fname'],
              "lname"                   => $user['lname'],
              "group_member_count"      => $user['member_count'],
              "group_max_members"       => $user['max_members'],
              "user_current_permissions"=> Service_Permissions::getAllPermissionsForUser($user['custid'],$this->whitelabel_id)
            ];
        }

        return $joinRequests;
    }

    public function addJoinRequest($groupId, $custid)
    {
        (new DB_Groups_Join_Requests())->addRequest($groupId, $this->whitelabel_id, $custid);
        Service_Notification_Send::requestToJoinGroup($custid, $groupId, $this->whitelabel_id);
    }

    public function approveJoinRequest($groupId, $custid)
    {
        self::addUserToGroup($groupId,$custid);
    }

    public function declineJoinRequest($groupId, $custid)
    {
        (new DB_Groups_Join_Requests())->removeRequest($groupId, $this->whitelabel_id, $custid);
        Service_Notification_Send::declinedToJoinGroup($_SESSION['custid'], $custid, $groupId, $this->whitelabel_id);
    }

    public function unsubscribeStakeholder($contactEmail)
    {
        (new DB_Groups_Stakeholders())->removeStakeholderByWhitelabel($this->whitelabel_id, $contactEmail);
    }

}