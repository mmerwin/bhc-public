--
-- Table structure for table `usrowing_credentials`
--

CREATE TABLE IF NOT EXISTS `usrowing_credentials` (
                                                      `whitelabel_id` int(11) NOT NULL,
                                                      `username` text NOT NULL,
                                                      `password` text NOT NULL,
                                                      `confirmed` varchar(5) NOT NULL DEFAULT 'No',
                                                      `last_used` int(11) NOT NULL,
                                                      `rc_orgid` int(11) NOT NULL,
                                                      `custid` int(11) NOT NULL,
                                                      PRIMARY KEY (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;
