<?php


class DB_Whitelabel_Plans extends DataModel
{
    protected $schema = "whitelabel_plans";

    public function updateStripeCustomerId($whitelabel_id, $stripeCustomerId)
    {
        $this->DB->query("UPDATE {$this->schema} SET stripe_customer_id = ? WHERE whitelabel_id = ? LIMIT 1", array($stripeCustomerId, $whitelabel_id));
    }

    public function addWhitelabelToPlans($whitelabel_id, $nextPayment, $planId, $stripeCustomerId = '')
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, plan_id, next_payment, stripe_customer_id) VALUES (?, ?, ?, ?)", array($whitelabel_id, $planId, $nextPayment, $stripeCustomerId));
    }

    public function changePlan($whitelabel_id, $newPlan)
    {
        $this->DB->query("UPDATE {$this->schema} SET plan_id = ? WHERE whitelabel_id = ? LIMIT 1", array($newPlan, $whitelabel_id));
    }

    public function updateNextPayment($whitelabel_id, $nextPayment)
    {
        $this->DB->query("UPDATE {$this->schema} SET next_payment = ? WHERE whitelabel_id = ? LIMIT 1", array($nextPayment, $whitelabel_id));
    }

    public function getAllPlans()
    {
        return $this->DB->query("SELECT wp.whitelabel_id, wp.plan_id, p.name as plan_name, p.default_price as monthly_price, wp.next_payment, wp.stripe_customer_id
                                        FROM whitelabel_plans wp
                                        LEFT JOIN platform_plans p
                                        ON p.plan_id = wp.plan_id ")->fetchAll();
    }

    public function getWhitelabelPlan($whitelabel_id, $stripe = true)
    {
        if ($stripe) {
            $stripe = ", wp.stripe_customer_id";
        } else {
            $stripe = "";
        }
        return $this->DB->query("SELECT wp.whitelabel_id, wp.plan_id, p.name as plan_name, p.default_price as monthly_price, wp.next_payment{$stripe}
                                        FROM whitelabel_plans wp
                                        LEFT JOIN platform_plans p
                                        ON p.plan_id = wp.plan_id 
                                        WHERE wp.whitelabel_id = ? LIMIT 1", array($whitelabel_id))->fetchArray();
    }

    public function getAllByPlan($planId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE plan_id = ? LIMIT 1", array($planId))->fetchArray();
    }

    public function getDuePayments()
    {
        return $this->DB->query("SELECT wp.whitelabel_id, wp.plan_id, p.name as plan_name, p.default_price as monthly_price, wp.next_payment, wp.stripe_customer_id
                                        FROM whitelabel_plans wp
                                        LEFT JOIN platform_plans p
                                        ON p.plan_id = wp.plan_id  WHERE next_payment <= ?", array((time()+3600)))->fetchAll();
    }

}