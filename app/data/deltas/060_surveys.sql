--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
                                         `survey_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `whitelabel_id` int(11) NOT NULL,
                                         `custid_creator` int(11) NOT NULL,
                                         `title` varchar(100) NOT NULL,
                                         `description` text NOT NULL,
                                         `open_time` int(11) NOT NULL,
                                         `close_time` int(11) NOT NULL,
                                         `ready_to_send` varchar(10) NOT NULL DEFAULT 'No',
                                         `anonymous` varchar(10) NOT NULL DEFAULT 'Yes',
                                         `sent_status` varchar(10) NOT NULL DEFAULT 'Unsent',
                                         `public` varchar(10) NOT NULL DEFAULT 'No',
                                         `timestamp_created` int(11) NOT NULL,
                                         PRIMARY KEY (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
