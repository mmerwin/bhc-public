<?php


class DB_System_IPs extends DataModel
{
    protected $schema = "system_ips";

    public function getAllIps()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchArray();
    }

    public function checkIfExists($ip)
    {
        $result = $this->DB->query("SELECT * FROM {$this->schema} WHERE ip = ? LIMIT 1", $ip)->numRows();
        if($result != 0)
        {
            return true;
        } else{
            return false;
        }
    }
}