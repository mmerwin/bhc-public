<?php


class DB_External_Keys extends DataModel
{
    protected $schema = "external_keys";

    public function getByServiceEnv($service, $env)
    {
      return $this->DB->query("SELECT * FROM {$this->schema} WHERE service = ? AND (environment = ? OR environment = '') LIMIT 1", array($service, $env))->fetchArray();
    }

}