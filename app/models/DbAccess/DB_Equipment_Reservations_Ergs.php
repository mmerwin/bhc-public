<?php


class DB_Equipment_Reservations_Ergs extends DataModel
{
    protected $schema = "equipment_reservations_ergs";
    protected $subschemaA = "equipment_reservations";
    protected $subschemaB = "equipment_ergs";

    public function addReservation($reservationId, $whitelabel_id, $ergId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (reservation_id, whitelabel_id, erg_id) VALUES(?, ?, ?)", array($reservationId, $whitelabel_id, $ergId));
    }

    public function getReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT ee.erg_id, ere.reservation_id, ere.whitelabel_id, ee.identifier, ee.make, ee.model, ee.year_purchased, ee.location, ee.insured, ee.insured_value, ee.status, ee.created_at
                                        FROM equipment_reservations_ergs ere
                                        LEFT JOIN equipment_ergs ee
                                        ON ee.erg_id = ere.erg_id
                                        WHERE ere.whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getAvailableErgs($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT * FROM {$this->subschemaB} where erg_id NOT IN(SELECT ere.erg_id
                                FROM {$this->subschemaA} er
                                LEFT JOIN {$this->schema} ere
                                ON ere.reservation_id = er.reservation_id
                                WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ? AND erg_id IS NOT NULL)
                                AND whitelabel_id = ?",
            array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id, $whitelabel_id))->fetchAll();
    }

    public function getErgsReservedByTime($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT er.*, ere.erg_id
                                FROM {$this->subschemaA} er
                                LEFT JOIN {$this->schema} ere
                                ON ere.reservation_id = er.reservation_id
                                WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ?",
            array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id))->fetchAll();
    }
}