<?php


class wlgroups extends Controller
{
    /**
     * @api
     */
    public function getAllGroups()
    {
        $groups     = new Groups($_SESSION['whitelabel_id']);
        $allGroups  = $groups->getWhitelabelGroups($_SESSION['custid']);
        $this->view('json',$allGroups);
    }

    /**
     * @api
     */
    public function addPermissionToGroup()
    {
        $permissionId   = $this->param("permission_id");
        $groupId        = $this->param("group_id");
        $groups         = new Groups($_SESSION['whitelabel_id']);
        $status         = $groups->addPermissionToGroup($permissionId, $groupId);
        $this->view('json',$status);
    }

    /**
     * @api
     */
    public function removePermissionFromGroup()
    {
        $permissionId   = $this->param("permission_id");
        $groupId        = $this->param("group_id");
        $groups         = new Groups($_SESSION['whitelabel_id']);
        $status         = $groups->removePermissionFromGroup($permissionId, $groupId);
        $this->view('json',$status);
    }

    /**
     * @api
     */
    public function getNonAssignedPermissions()
    {
        $groupId    = $this->param("group_id");
        $groups     = new Groups($_SESSION['whitelabel_id']);
        $data       = $groups->getNonAssignedPermissions($groupId);
        $this->view('json',$data);
    }

    /**
     * @api
     */
    public function updateGroupDetails()
    {
        $groupId = $this->param("group_id");
        $groups  = new Groups($_SESSION['whitelabel_id']);
        //check if group is in whitelabel
        $exists  = $groups->checkIfGroupExists($groupId);
        if ($exists) {
            $newTitle   = $this->param("title");
            $newDescr   = $this->param("descr");
            $selfJoin   = $this->param("self_join");
            $maxMembers = $this->param("max");

            //get current data
            $currentGroupDetails = $groups->getGroupMetaData($groupId);
            $newGroupDetails     = array();

            if (!is_null($newTitle) && $newTitle != '') {
                $newGroupDetails['title'] = $newTitle;
            } else {
                $newGroupDetails['title'] = $currentGroupDetails['title'];
            }

            if (!is_null($newDescr) && $newDescr != '') {
                $newGroupDetails['descr'] = $newDescr;
            } else {
                $newGroupDetails['descr'] = $currentGroupDetails['descr'];
            }

            if (!is_null($selfJoin) && $selfJoin != '') {
                $newGroupDetails['self_join'] = $selfJoin;
            } else {
                $newGroupDetails['self_join'] = $currentGroupDetails['self_join'];
            }

            if (!is_null($maxMembers) && $maxMembers != '') {
                $newGroupDetails['max'] = $maxMembers;
            } else {
                $newGroupDetails['max'] = $currentGroupDetails['max'];
            }

            //make update
            $returnData = $groups->updateGroupDetails($groupId, $newGroupDetails);
        } else {
            $returnData = array("Error"=>"group_id is not in whitelabel");
        }

        $this->view('json', $returnData);

    }

    /**
     * @api
     */
    public function getNonMemberUsers()
    {
        $groupId     = $this->param("group_id");
        $groups      = new Groups($_SESSION['whitelabel_id']);
        $returnData  = $groups->getNonGroupMembers($groupId);
        $this->view('json',$returnData);
    }

    /**
     * @api
     */
    public function addUserToGroup()
    {
        $custid = $this->param("custid");
        if (is_null($custid) || $custid == "") {
            $custid = $_SESSION['custid'];
        }
        $groupId = $this->param("group_id");
        //check if authorized OR if group allows self-join
        $groups     = new Groups($_SESSION['whitelabel_id']);
        $groupMeta  = $groups->getGroupMetaData($groupId);
        if (Service_Permissions::checkPermission(4) || $groupMeta['self_join'] == "Yes" || $groupMeta['self_join'] == "yes" || $groupMeta['self_join'] == "YES") {
            $groups->addUserToGroup($groupId, $custid);
            $returnData = array("Success"=>"User added to group");
        } else {
            $returnData = array("Error"=>"Not authorized to join group");
        }
        $this->view("json", $returnData);

    }

    /**
     * @api
     */
    public function RemoveUserFromGroup()
    {
        $custid = $this->param("custid");
        if (is_null($custid) || $custid == "") {
            $custid = $_SESSION['custid'];
        }
        $groupId = $this->param("group_id");
        $groups  = new Groups($_SESSION['whitelabel_id']);
        $groups->removeMemberFromGroup($groupId, $custid);
        $this->view("json", array("Status"=>"Success"));
    }

    /**
     * @api
     */
    public function createGroup()
    {
        $name       = $this->param("name");
        $descr      = $this->param("descr");
        $max        = $this->param("max");
        $selfJoin   = $this->param("self_join");

        $groups = new Groups($_SESSION['whitelabel_id']);
        $groups->addNewGroup($name, $descr, $max, $selfJoin);
        WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.newGroup", "true");
        $this->view("json",array("Status"=>"Success"));
    }

    /**
     * @api
     */
    public function deleteGroup()
    {
        $groupId = $this->param("group_id");

        //verify that the group belongs to the whitelabel
        $groups  = new Groups($_SESSION['whitelabel_id']);
        if ($groups->checkIfGroupExists($groupId)) {
            $groups->deleteGroup($groupId);
            $returnable = array("Success"=>"Group has been deleted.");
        } else {
            $returnable = array("Error"=>"Group does not belong to whitelabel");
        }
        $this->view("json", $returnable);
    }

    /**
     * @api
     */
    public function deleteStakeholder()
    {
        $groupId        = $this->param("group_id");
        $stakeholderId  = $this->param("stakeholder_id");
        $groups         = new Groups($_SESSION['whitelabel_id']);
        $groups->deleteStakeholder($stakeholderId, $groupId);

        $this->view("json",array("Status"=>"Success"));
    }

    /**
     * @api
     */
    public function addStakeholder()
    {
        $groupId    = $this->param("group_id");
        $name       = $this->param("stakeholder_name");
        $email      = $this->param("stakeholder_email");
        $groups     = new Groups($_SESSION['whitelabel_id']);
        $groups->addStakeholder($groupId, $name, $email);

        $this->view("json",array("Status"=>"Success"));
    }

    /**
     * @api
     */
    public function addJoinRequest()
    {
        $groupId = $this->param("group_id");
        $groups  = new Groups($_SESSION['whitelabel_id']);
        $groups->addJoinRequest($groupId, $_SESSION['custid']);

        $this->view("json",array("Status"=>"Request Sent"));

        //add notification to all who can approve the request

    }

    /**
     * @api
     */
    public function approveJoinRequest()
    {
        $groupId = $this->param("group_id");
        $custid  = $this->param("custid");
        $groups  = new Groups($_SESSION['whitelabel_id']);
        $groups->approveJoinRequest($groupId,$custid);

        $this->view("json",array("Status"=>"Request Approved"));
    }

    /**
     * @api
     */
    public function declineJoinRequest()
    {
        $groupId = $this->param("group_id");
        $custid  = $this->param("custid");
        $groups  = new Groups($_SESSION['whitelabel_id']);
        $groups->declineJoinRequest($groupId,$custid);
    }

    /**
     * @api
     */
    public function getPermissionsUsers()
    {
        $result = Service_Permissions::getAllPermissionsUsers($_SESSION['whitelabel_id']);

        $this->view("json", $result);
    }

}



