--
-- Table structure for table `regattacentral_regatta_entries_participants`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regatta_entries_participants` (
                                                                             `regatta_id` int(11) NOT NULL,
                                                                             `event_id` int(11) NOT NULL,
                                                                             `entry_id` int(11) NOT NULL,
                                                                             `participant_id` int(11) NOT NULL,
                                                                             `organization_id` int(11) NOT NULL,
                                                                             `sequence` int(11) NOT NULL,
                                                                             `type` varchar(30) NOT NULL,
                                                                             PRIMARY KEY (`regatta_id`,`event_id`,`entry_id`,`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
