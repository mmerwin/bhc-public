<?php


class DB_Whitelabel_Digest_History extends DataModel
{
    protected $schema = "whitelabel_digest_history";

    public function addReport($whitelabel_id, $month, $totalUsers, $totalPaymentsReceived, $anticipatedRevenue, $delinquentUsers, $athletesAtPractice, $messagesSent, $rewardsPointsBalance)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (whitelabel_id, month, calc_timestamp, total_users, total_payments_received, anticipated_revenue, delinquent_users,
                   athletes_attended_practice, messages_sent, rewards_points_earned) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($whitelabel_id, $month, time(), $totalUsers, $totalPaymentsReceived, $anticipatedRevenue, $delinquentUsers, $athletesAtPractice, $messagesSent, $rewardsPointsBalance));
    }

    public function getLastReport($whitelabel_id, $currentReportMonth)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND month < ? LIMIT 1", array($whitelabel_id, $currentReportMonth))->fetchArray();
    }

    public function getAllReportsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

    public function getAllReportsByMonth($month)
    {
        return $this->DB->query("SELECT wdh.*, wl.name as whitelabel_name FROM whitelabel_digest_history wdh LEFT JOIN whitelabels wl ON wl.whitelabel_id = wdh.whitelabel_id
                                        WHERE month = ?", array($month))->fetchAll();
    }

}