SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE IF NOT EXISTS `regattacentral_orgs` (
                                                     `organizationId` int(11) NOT NULL,
                                                     `claimed` varchar(3) NOT NULL DEFAULT 'no',
                                                     `links` text DEFAULT NULL,
                                                     `officialName` text DEFAULT NULL,
                                                     `name` text DEFAULT NULL,
                                                     `abbreviation` varchar(10) DEFAULT NULL,
                                                     `city` text DEFAULT NULL,
                                                     `region` text DEFAULT NULL,
                                                     `country` text DEFAULT NULL,
                                                     `IOC` text DEFAULT NULL,
                                                     `type` text DEFAULT NULL,
                                                     `url` text DEFAULT NULL,
                                                     `logoUrl` text DEFAULT NULL,
                                                     `blade` text DEFAULT NULL,
                                                     `orgParticipants` text DEFAULT NULL,
                                                     `orgContacts` text DEFAULT NULL,
                                                     PRIMARY KEY (`organizationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `regattacentral_orgs` (`organizationId`, `claimed`, `links`, `officialName`, `name`, `abbreviation`, `city`, `region`, `country`, `IOC`, `type`, `url`, `logoUrl`, `blade`, `orgParticipants`, `orgContacts`) VALUES
(0, 'no', NULL, NULL, 'Undeclared', 'n/a', NULL, NULL, 'U', 'USA', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;
