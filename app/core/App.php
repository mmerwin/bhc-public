<?php
session_name("bhc-redacted-string"); // random unguessable string [redacted]
session_start();
header("Access-Control-Allow-Origin: *");
require_once (dirname(__FILE__).'/../vendor/autoload.php');
use \Rollbar\Rollbar;
class App
{

    protected $controller = 'authenticate';
    protected $requestecContoller = 'na';

    protected $method = 'index';
    protected $requestedMethod = 'na';

    protected $params = [];

    public function __construct()
    {
        $url = $this->parseUrl();

        if (file_exists('../app/controllers/'.$url[0].'.php')) {
            $this->controller = $url[0];
            $this->requestecContoller = $url[0];
            unset($url[0]);
        }

        require_once '../app/controllers/'.$this->controller.'.php';

        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                $this->requestedMethod = $url[1];
                unset($url[1]);
            } else {
                $error = array("Error" => "Invalid Endpoint");
                die (json_encode($error));
            }
        }

        $this->params = $url ? array_values($url) : [];

        $custidOfRequest = self::validateRequest();

        // rollbar error reporting
        Rollbar::init(
            array(
                'access_token'  => '[redacted]',
                'environment'   => Environment::getEnvCode(),
                'custom'        => array(
                    "custid" => $this->validateRequest()
                )
            )
        );
        if (isset($_SESSION["adminid"])) {
            Service_Logs::api_log($this->requestecContoller,$this->requestedMethod, $_SERVER['REMOTE_ADDR'], $_SESSION["adminid"], "admin");
        } else {
            Service_Logs::api_log($this->requestecContoller,$this->requestedMethod, $_SERVER['REMOTE_ADDR'], $custidOfRequest, "user");
        }


        call_user_func_array([$this->controller, $this->method], $this->params);
        session_destroy();

    }

    public function parseUrl()
    {
        if (isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        } else {
            return array("home","index");
        }
    }

    private function validateRequest()
    {
        $token = $this->param("token");
        $returnError = array("Error"=>"Token Unauthorized");
        if (substr($token, 0,9) == "bhcadmin-" && ($this->requestecContoller != 'authenticate' && $this->requestedMethod != 'validateCredentials')) {
            // validate as an admin token vs user token
            if ($this->requestecContoller == "admin" && $this->requestedMethod != "generateAdminToken") {
                // validate admin token
                $adminCredentials = Service_ApiTokenValidation::validateAdminToken($token, true);
                if (isset($adminCredentials["token_hash"])) {
                    // get admin details
                    $_SESSION["adminid"] = $adminCredentials["adminid"];
                } else {
                    die(json_encode($returnError));
                }
            }
        } else {
            if ($this->requestecContoller == 'authenticate' || $this->requestecContoller == 'bhcprivate' || $this->requestecContoller == 'webhooks' || ($this->requestecContoller == 'admin' && $this->requestedMethod == 'generateAdminToken')) {
                return "0";
            } else {
                $result = Service_ApiTokenValidation::validateToken($token, true);
                if (isset($result['custid'])) {
                    if (empty($result['custid'])) {
                        die(json_encode($returnError));
                    } else {
                        $_SESSION['custid'] = $result['custid'];
                        if (!is_null($this->param('whitelabel_id'))) {
                            // verify whitelabel_id affiliation
                            $affiliationCheck = Service_Affiliations::checkIfAffiliated($_SESSION['custid'], $this->param('whitelabel_id'), true);
                            if ($affiliationCheck) {
                                $_SESSION['whitelabel_id'] = $this->param('whitelabel_id');
                            } else {
                                $_SESSION['whitelabel_id'] = null;
                            }
                        } else {
                            $_SESSION['whitelabel_id'] = null;
                        }

                        return $result['custid'];
                    }
                } else {
                    die(json_encode($returnError));
                }
            }
        }
    }

    private function param($name)
    {
        if (isset($_POST[$name])) {
            return $_POST[$name];
        } else if (isset($_GET[$name])) {
            return $_GET[$name];
        }
    }

}

?>