<?php


class DB_Navbar extends DataModel
{
    protected $schema = "navbar";

    public function getAllNavbarItems()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} ORDER BY weight ASC")->fetchAll();
    }

    public function getAllNavbarParents($bravo=null)
    {
        return $this->DB->query("SELECT * FROM navbar_parents")->fetchAll();
    }

    public function getAllowedNavbarItems($custid, $whitelabel_id)
    {
        $results = $this->DB->query("SELECT distinct n.title, n.controller, n.method, n.parent_id, n.weight
                                        FROM groups_members gm
                                        LEFT JOIN permissions_groups pg
                                        ON gm.group_id = pg.group_id 
                                        LEFT JOIN navbar n
                                        ON (n.permission_id = pg.permission_id OR n.permission_id = 1) 
                                        AND (gm.custid = ? OR n.permission_id = 1) AND (gm.whitelabel_id = ? OR n.permission_id = 1)
                                        ORDER BY n.weight ASC", array($custid, $whitelabel_id))->fetchAll();
        if(count($results) == 0){
            //get all navbar items with level 1 permissions
            $results = $this->DB->query("SELECT * from navbar WHERE permission_id = 1 ORDER BY weight ASC")->fetchAll();
        }
        return $results;
    }

    public function getAllowedNavbarParents($navbarItems)
    {
        $searchString = "(";
        foreach ($navbarItems as $item){
            $searchString = $searchString.$item['parent_id'].",";
        }
        $searchString = substr($searchString, 0, -1).")";
        return $this->DB->query("SELECT * FROM navbar_parents WHERE navbar_parent_id IN {$searchString} ORDER BY weight ASC")->fetchAll();
    }

}