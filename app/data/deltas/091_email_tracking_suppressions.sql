--
-- Table structure for table `email_tracking_suppressions`
--

CREATE TABLE IF NOT EXISTS `email_tracking_suppressions` (
                                                             `custid` int(11) NOT NULL,
                                                             `last_updated` int(11) NOT NULL,
                                                             PRIMARY KEY (`custid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `email_tracking_suppressions`
--
ALTER TABLE `email_tracking_suppressions`
    ADD CONSTRAINT `fk_custid_email_tracking_suppressions` FOREIGN KEY (`custid`) REFERENCES `bhconnect`.`users` (`custid`) ON DELETE CASCADE;
