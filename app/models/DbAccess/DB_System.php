<?php

class DB_System extends DataModel
{
  protected $schema = "__system";
  
  public function readKey($key)
  {
    $value = $this->DB->query("SELECT * FROM {$this->schema} WHERE sys_key = ? LIMIT 1", $key)->fetchArray();
    return $value['sys_value'];
  }
  
  public function addKeyValue($key, $value)
  {
    $this->DB->query("INSERT INTO {$this->schema} (sys_key, sys_value) VALUES (?, ?)", array($key, $value));
  }
}

?>