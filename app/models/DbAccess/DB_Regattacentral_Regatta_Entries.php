<?php


class DB_Regattacentral_Regatta_Entries extends DataModel
{
    protected $schema = "regattacentral_regatta_entries";

    public function addEntry($regattaId, $eventId, $entryId, $organizationId, $contactId, $entryLabel, $alternateTitle,
                             $averageAge, $handicap, $compositeLabel, $seed, $ergScore, $division, $bow,
                             $waitlistPriority, $createdOn, $modifiedOn, $composite, $international)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (regatta_id, event_id, entry_id, organization_id, 
                   contact_id, entry_label, alternate_title, average_age, handicap, composite_label, seed, erg_score, 
                   division, bow, waitlist_priority, created_on, modified_on, composite, international) 
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($regattaId, $eventId, $entryId, $organizationId, $contactId, $entryLabel, $alternateTitle,
                $averageAge, $handicap, $compositeLabel, $seed, $ergScore, $division, $bow,
                $waitlistPriority, $createdOn, $modifiedOn, $composite, $international));
    }

}