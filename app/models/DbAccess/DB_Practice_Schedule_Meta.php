<?php


class DB_Practice_Schedule_Meta extends DataModel
{
    protected $schema = "practice_schedule_meta";

    public function getPracticeScheduleForUser($whitelabel_id, $custid, $upcoming, $since)
    {
        if ($upcoming) {
            $sub = "AND end_time > ?";
            $orderBy = " ORDER BY start_time ASC ";
        } else {
            $sub = "AND end_time < ?";
            $orderBy = " ORDER BY start_time DESC ";
        }

        return $this->DB->query("SELECT psm.* 
                                        FROM groups_members gm
                                        LEFT JOIN practice_groups pg
                                        ON pg.group_id = gm.group_id
                                        LEFT JOIN practice_schedule_meta psm
                                        ON psm.practice_id = pg.practice_id
                                        WHERE gm.custid = ? AND psm.whitelabel_id = ? {$sub} AND public = 'Yes'
                                        GROUP BY practice_id {$orderBy}", array($custid, $whitelabel_id, $since))->fetchAll();
    }

    public function getAllWhitelabelPractices($whitelabel_id, $order, $since, $upcoming)
    {
        if (strtoupper($order) == "ASC") {
            $order = "ASC";
        } else {
            $order = "DESC";
        }

        if ($upcoming) {
            $sub = "AND end_time > ?";
        } else {
            $sub = "AND end_time < ?";
        }

        return $this->DB->query("SELECT * FROM {$this->schema} WHERE public = 'Yes' {$sub} AND whitelabel_id = ? ORDER BY start_time {$order}", array($since, $whitelabel_id))->fetchAll();
    }

    public function getPracticeById($whitelabel_id, $practiceId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($whitelabel_id, $practiceId))->fetchArray();
    }

    public function getPracticesInSeries($whitelabel_id, $series_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND series_id = ? ORDER BY start_time ASC", array($whitelabel_id, $series_id))->fetchAll();
    }

    public function createPractice($whitelabel_id, $seriesId, $custid, $name, $locationId, $startTime, $endTime, $maxAttendees, $public, $visitingRowers, $reservationId, $returnId = false)
    {
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, series_id, name, location_id, start_time, end_time, max_attendees, lineups_set, public, visiting_rowers, custid, reservation_id) VALUES (?, ?, ?, ?, ?, ?, ?, 'No', ?, ?, ?, ?)", array($whitelabel_id, $seriesId, $name, $locationId, $startTime, $endTime, $maxAttendees, $public, $visitingRowers, $custid, $reservationId));
        if ($returnId) {
            $newPractice = $this->DB->query("SELECT practice_id FROM {$this->schema} WHERE whitelabel_id = ? AND series_id = ? AND start_time = ? AND end_time = ? AND public = ? AND reservation_id = ? ORDER BY practice_id DESC LIMIT 1",
                array($whitelabel_id, $seriesId, $startTime, $endTime, $public, $reservationId))->fetchArray();
            return $newPractice["practice_id"];
        }
    }

    public function updateName($whitelabel_id, $practiceId, $newName)
    {
        $this->DB->query("UPDATE {$this->schema} SET name = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($newName, $whitelabel_id, $practiceId));
    }

    public function updateLocation($whitelabel_id, $practiceId, $newLocation)
    {
        $this->DB->query("UPDATE {$this->schema} SET location_id = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($newLocation, $whitelabel_id, $practiceId));
    }

    public function updateStartTime($whitelabel_id, $practiceId, $newStartTime)
    {
        $this->DB->query("UPDATE {$this->schema} SET start_time = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($newStartTime, $whitelabel_id, $practiceId));
    }

    public function updateEndTime($whitelabel_id, $practiceId, $newEndTime)
    {
        $this->DB->query("UPDATE {$this->schema} SET end_time = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($newEndTime, $whitelabel_id, $practiceId));
    }

    public function updateMaxAttendees($whitelabel_id, $practiceId, $newAttendeeMax)
    {
        $this->DB->query("UPDATE {$this->schema} SET max_attendees = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($newAttendeeMax, $whitelabel_id, $practiceId));
    }

    public function markLineupsSet($whitelabel_id, $practiceId)
    {
        $this->DB->query("UPDATE {$this->schema} SET lineups_set = 'Yes' WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($whitelabel_id, $practiceId));
    }

    public function markSeriesPublic($whitelabel_id, $seriesId)
    {
        $this->DB->query("UPDATE {$this->schema} SET public = 'Yes' WHERE whitelabel_id = ? AND seris_id = ?", array($whitelabel_id, $seriesId));
    }

    public function updateVisitingRowers($whitelabel_id, $practiceId, $visitingRowers)
    {
        $this->DB->query("UPDATE {$this->schema} SET visiting_rowers = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($visitingRowers, $whitelabel_id, $practiceId));
    }

    public function getPastPracticesFromAllWhitelabels()
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE start_time < ? AND (name IS NULL OR location_id IS NULL OR max_attendees IS NULL OR visiting_rowers IS NULL)", array(time()))->fetchAll();
    }

    public function updateLineupFocus($whitelabel_id, $practiceId, $focusId)
    {
        $this->DB->query("UPDATE {$this->schema} SET lineup_focus = ? WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($focusId, $whitelabel_id, $practiceId));
    }

}