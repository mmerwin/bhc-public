-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2020 at 02:22 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `bhconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE IF NOT EXISTS `timezones` (
                                           `timezone` varchar(50) NOT NULL,
                                           `descr` varchar(50) NOT NULL,
                                           PRIMARY KEY (`timezone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `timezones`
--

INSERT IGNORE INTO `timezones` (`timezone`, `descr`) VALUES
('America/Adak', 'Hawaii'),
('America/Anchorage', 'Alaska'),
('America/Chicago', 'Central'),
('America/Denver', 'Mountain'),
('America/Los_Angeles', 'Pacific'),
('America/New_York', 'Eastern'),
('America/Phoenix', 'Mountain no DST'),
('Pacific/Honolulu', 'Hawaii no DST');
COMMIT;
