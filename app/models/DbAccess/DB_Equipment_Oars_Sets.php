<?php


class DB_Equipment_Oars_Sets extends DataModel
{
    protected $schema = "equipment_oars_sets";
    protected $subschemaA = "equipment_oars";

    public function addSet($whitelabel_id, $type, $name, $descr, $manufacturer, $model)
    {
        $created_at = time();
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, type, name, descr, manufacturer, model, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)", array($whitelabel_id, $type, $name, $descr, $manufacturer, $model, $created_at));

        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND created_at = ? AND name = ? ORDER BY oar_set_id DESC LIMIT 1", array($whitelabel_id, $created_at, $name))->fetchArray();
    }

    public function updateName($setId, $whitelabel_id, $newName)
    {
        $this->DB->query("UPDATE {$this->schema} SET name = ? WHERE oar_set_id = ? AND whitelabel_id = ? LIMIT 1", array($newName, $setId, $whitelabel_id));
    }

    public function updateDescr($setId, $whitelabel_id, $newDescr)
    {
        $this->DB->query("UPDATE {$this->schema} SET descr = ? WHERE oar_set_id = ? AND whitelabel_id = ? LIMIT 1", array($newDescr, $setId, $whitelabel_id));
    }

    public function deleteSet($setId, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE oar_set_id = ? AND whitelabel_id = ? LIMIT 1", array($setId, $whitelabel_id));
    }

    public function getAllSets($whitelabel_id)
    {
        return $this->DB->query("SELECT os.oar_set_id, os.whitelabel_id, os.type, os.name, os.descr, os.manufacturer, os.model, os.created_at, 
                                        (SELECT count(*) FROM {$this->subschemaA} WHERE oar_set_id = os.oar_set_id) as count, 
                                        (SELECT count(*) FROM {$this->subschemaA} WHERE oar_set_id = os.oar_set_id AND side='Port') as port_count, 
                                        (SELECT count(*) FROM {$this->subschemaA} WHERE oar_set_id = os.oar_set_id AND side='Starboard') as starboard_count 
                                        FROM {$this->schema} os WHERE whitelabel_id = ?", array($whitelabel_id))->fetchAll();
    }

}