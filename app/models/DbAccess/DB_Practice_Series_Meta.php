<?php


class DB_Practice_Series_Meta extends DataModel
{
    protected $schema = "practice_series_meta";

    public function getAllWhitelabelSeries($whitelabel_id)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ?  AND finalized = 'Yes' ORDER BY series_id ASC", array($whitelabel_id))->fetchAll();
    }

    public function getSeriesById($whitelabel_id, $seriesId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND series_id = ? LIMIT 1", array($whitelabel_id, $seriesId))->fetchArray();
    }

    public function createSeries($whitelabel_id, $name, $locationId, $maxAttendees, $startDate, $endDate, $visitingRowers)
    {
        $timestamp = time();
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, name, location_id, max_attendees, start_date, end_date, visiting_rowers, finalized, created_at) VALUES 
                                (?, ?, ?, ?, ?, ?, ?, 'No', ?)", array($whitelabel_id, $name, $locationId, $maxAttendees, $startDate, $endDate, $visitingRowers, $timestamp));

        //find new series_id
        $series = $this->DB->query("SELECT series_id FROM {$this->schema} WHERE whitelabel_id = ? AND name = ? AND created_at = ? ORDER BY series_id DESC LIMIT 1", array($whitelabel_id, $name, $timestamp))->fetchArray();
        return $series["series_id"];
    }

    public function setSeriesAsFinalized($whitelabel_id, $seriesId)
    {
        $this->DB->query("UPDATE {$this->schema} SET finalized = 'Yes' WHERE whitelabel_id = ? AND series_id = ? LIMIT 1", array($whitelabel_id, $seriesId));
    }

}