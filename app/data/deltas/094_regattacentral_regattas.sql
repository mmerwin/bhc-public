--
-- Table structure for table `regattacentral_regattas`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regattas` (
                                                         `regatta_id` int(11) NOT NULL,
                                                         `parent_id` int(11) NOT NULL,
                                                         `name` varchar(200) NOT NULL,
                                                         `abbreviation` varchar(50) NOT NULL,
                                                         `start_time` int(11) NOT NULL,
                                                         `timezone` varchar(50) NOT NULL,
                                                         `check_payable_to` varchar(500) NOT NULL,
                                                         `billing_address` varchar(500) NOT NULL,
                                                         `contact_name` varchar(200) NOT NULL,
                                                         `contact_email` varchar(200) NOT NULL,
                                                         `primary_host` text NOT NULL,
                                                         `regatta_url` text NOT NULL,
                                                         `host_org_id` int(11) NOT NULL,
                                                         `race_type` varchar(50) NOT NULL,
                                                         `venue_id` int(11) NOT NULL,
                                                         `results_url` text NOT NULL,
                                                         PRIMARY KEY (`regatta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
