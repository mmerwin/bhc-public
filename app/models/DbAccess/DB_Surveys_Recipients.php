<?php


class DB_Surveys_Recipients extends DataModel
{
    protected $schema = "surveys_recipients";

    public function getAllRecipients($whitelabel_id, $surveyId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND survey_id = ?", array($whitelabel_id, $surveyId))->fetchAll();
    }

    public function addRecipient($whitelabel_id, $surveyId, $custid, $unique_id, $name, $email)
    {
        $this->DB->query("INSERT INTO {$this->schema} (survey_id, whitelabel_id, custid, unique_id, name, email, first_submission) VALUES (?, ?, ?, ?, ?, ?, 0)",
            array($surveyId, $whitelabel_id, $custid, $unique_id, $name, $email));
    }

    public function removeRecipient($whitelabel_id, $surveyId, $recipientId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE whitelabel_id = ? AND survey_id = ? AND recipient_id = ? LIMIT 1", array($whitelabel_id, $surveyId, $recipientId));
    }

    public function getRecipient($whitelabel_id, $surveyId, $recipientId, $uniqueId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND survey_id = ? AND recipient_id = ? AND unique_id = ? ORDER BY recipient_id DESC LIMIT 1", array($whitelabel_id, $surveyId, $recipientId, $uniqueId))->fetchArray();
    }

    public function markSubmitted($whitelabel_id, $surveyId, $uniqueId, $anonymous)
    {
        if($anonymous == 'Yes')
        {
            $this->DB->query("UPDATE {$this->schema} SET first_submission = ?, unique_id='anonymous' WHERE whitelabel_id = ? AND survey_id = ? AND unique_id = ? LIMIT 1", array(time(), $whitelabel_id, $surveyId, $uniqueId));

        } else
        {
            $this->DB->query("UPDATE {$this->schema} SET first_submission = ? WHERE whitelabel_id = ? AND survey_id = ? AND unique_id = ? LIMIT 1", array(time(), $whitelabel_id, $surveyId, $uniqueId));
        }
    }

    public function getSurveysByCustid($whitelabel_id, $custid)
    {
        return $this->DB->query("SELECT s.whitelabel_id, s.survey_id, s.title, s.description, s.open_time, s.close_time, s.public, s.anonymous, sr.first_submission, sr.recipient_id, sr.unique_id
                                        FROM surveys s
                                        LEFT JOIN surveys_recipients sr
                                        ON s.survey_id = sr.survey_id
                                        WHERE sr.custid = ? AND s.whitelabel_id = ? AND s.sent_status = 'Sent'
                                        ORDER BY s.close_time DESC", array($custid, $whitelabel_id))->fetchAll();
    }

    public function deleteAllBySurvey($surveyId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE survey_id = ?", array($surveyId));
    }

}