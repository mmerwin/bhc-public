<?php


class navbar extends Controller
{
    /**
     * Retrieves the navigation bar items that the user is allowed to access.
     * Typically used for the official BoathouseConnect UI.
     * @api
     * return array array navbar parent and child elements.
     */
    public function getNavbar()
    {
        $custid         = $_SESSION['custid'];
        $navItems       = Service_Navbar::getAllNavbarItems($custid, $_SESSION['whitelabel_id']);
        $navHeaders     = Service_Navbar::getAllNavbarParents($navItems);
        $socialMedia    = new Service_Social_Media($_SESSION['whitelabel_id']);

        $formatted = [];
        foreach ($navHeaders as $parent) {
            $children = [];
            foreach ($navItems as $child) {
                if($child['parent_id'] == $parent['navbar_parent_id'] && self::planACL($child['controller'], $child['method'])){
                    $children[] = [
                        "controller"  => $child['controller'],
                        "method"      => $child['method'],
                        "title"     => $child['title']
                    ];
                }
            }
            if (count($children) > 0) {
                $formatted[] = [
                    "text"      => $parent['text'],
                    "icon"      => $parent['icon'],
                    "header"    => $parent['header'],
                    "children"  =>  $children
                ];
            }

        }
        $returnable = array(
            "navigation"    =>$formatted,
            "social_media"  =>$socialMedia->getSocialMediaUrls()
        );
        //$formatted["social_media"]=$socialMedia->getSocialMediaUrls();
        $this->view('json',$returnable);
    }

    private function planACL($controller, $method)
    {
        $controller = strtolower($controller);
        $method     = strtolower($method);

        //add to the endpoint array of plans NOT ALLOWED to access. Missing endpoints will be assumed to work for all whitelabel plans
        $disallowed =  array(
            "authenticate"=>array(
            ),
            "communications"=>array(
                "settings"=>array(1),
                "draft"=>array(1),
                "saved"=>array(1),
                "pending"=>array(1),
                "analytics"=>array(1),
                "mymessages"=>array(1),
            ),
            "equipment"=>array(
                "reportedissues"=>array(1)
            ),
            "groups"=>array(
                "stakeholders"=>array(1)
            ),
            "dashboard"=>array(
            ),
            "surveys"=>array(
                "index"=>array(1,2),
                "takesurvey"=>array(1,2),
                "surveysubmit"=>array(1,2),
                "edit"=>array(1,2),
                "createsurvey"=>array(1,2),
                "mysurveys"=>array(1,2),
                "markready"=>array(1,2),
                "results"=>array(1,2)
            ),
            "waivers"=>array(
                "usrowing"=>array(1,2)
            ),
            "practices"=>array(
                "allpractices"=>array(1),
                "lineups"=>array(1),
                "attendance"=>array(1),
                "coachingnotes"=>array(1),
                "manageupcoming"=>array(1),
                "managepast"=>array(1),
                "locations"=>array(1),
                "survey"=>array(1)
            ),
            "whitelabel"=>array(
                "rewards"=>array(1),
                "calendar"=>array(1)
            )
        );
        $plan = (new DB_Whitelabel_Plans())->getWhitelabelPlan($_SESSION["whitelabel_id"], false);
        if (isset($_SESSION["whitelabel_id"]) &&isset($disallowed[$controller][$method]) && in_array($plan["plan_id"], $disallowed[$controller][$method])) {
            return false;
        } else {
            return true;
        }
    }

}