<?php


class DB_Equipment_Reservations_Boats extends DataModel
{
    protected $schema = "equipment_reservations_boats";
    protected $subschemaA = "equipment_reservations";
    protected $subschemaB = "equipment_boats";

    public function addReservation($reservationId, $whitelabel_id, $boatId)
    {
        $this->DB->query("INSERT INTO {$this->schema} (reservation_id, whitelabel_id, boat_id) VALUES(?, ?, ?)", array($reservationId, $whitelabel_id, $boatId));
    }

    public function getReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT er.start_time as reservation_start, er.end_time as reservation_end, er.custid, u.fname, u.lname, erb.*, eb.boat_name, eb.manufacturer, eb.model, eb.year_built, eb.year_acquired, eb.purchase_price, eb.insured, eb.insured_value, eb.hull_type, eb.min_weight, eb.max_weight, eb.color, eb.serial_number, eb.coxed, eb.status, eb.created_at
                                        FROM {$this->schema} erb
                                        LEFT JOIN {$this->subschemaB} eb
                                        ON eb.boat_id = erb.boat_id
                                        LEFT JOIN {$this->subschemaA} er
                                        ON er.reservation_id = erb.reservation_id
                                        LEFT JOIN users u
                                        ON u.custid = er.custid
                                        WHERE erb.whitelabel_id = ? ORDER BY reservation_start ASC", array($whitelabel_id))->fetchAll();
    }

    public function getReservationsByBoatId($whitelabel_id, $boatId, $end=0)
    {
        return $this->DB->query("SELECT erb.boat_id, erb.reservation_id, erb.whitelabel_id, er.start_time, er.end_time, er.type, er.custid, u.fname, u.lname, eb.boat_name, eb.hull_type
                                        FROM {$this->schema} erb
                                        LEFT JOIN {$this->subschemaA} er
                                        ON er.reservation_id = erb.reservation_id
                                        LEFT JOIN users u
                                        ON u.custid = er.custid
                                        LEFT JOIN equipment_boats eb
                                        ON eb.boat_id = erb.boat_id
                                        WHERE erb.boat_id = ? AND erb.whitelabel_id = ? AND er.end_time >= ?", array($boatId, $whitelabel_id, $end))->fetchAll();
    }

    public function getAvailableBoats($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT * FROM {$this->subschemaB} where boat_id NOT IN(SELECT erb.boat_id
                                FROM {$this->subschemaA} er
                                LEFT JOIN {$this->schema} erb
                                ON erb.reservation_id = er.reservation_id
                                WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ?  AND boat_id IS NOT NULL)
                                AND whitelabel_id = ?",
            array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id, $whitelabel_id))->fetchAll();
    }

    public function getBoatsReservedByTime($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT er.*, erb.boat_id
                                FROM {$this->subschemaA} er
                                LEFT JOIN {$this->schema} erb
                                ON erb.reservation_id = er.reservation_id
                                WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ?",
            array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id))->fetchAll();
    }

    public function removeFromReservation($whitelabel_id, $reservationId, $boatId)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE reservation_id = ? AND whitelabel_id = ? AND boat_id = ? LIMIT 1", array($reservationId, $whitelabel_id, $boatId));
    }

}