<?php


class communications extends Controller
{
    public function getWhitelabelDomains()
    {
        $postmark   = new Service_Postmark();
        $allDomains = $postmark->getWhitelabelDomains($_SESSION['whitelabel_id']);
        $this->view("json", $allDomains);
    }

    public function addDomain()
    {
        $domain     = $this->param("domain");
        $postmark   = new Service_Postmark();
        $allDomains = $postmark->addDomain($_SESSION['whitelabel_id'], $domain);
        $this->view("json", $allDomains);
    }

    public function refreshDomain()
    {
        $domainId   = $this->param("domain_id");
        $postmark   = new Service_Postmark();
        $data       = $postmark->verifyRecords($domainId);
        $this->view("json", $data);
    }

    public function addSignature()
    {
        $domain         = $this->param("domain");
        $fromUsername   = $this->param("username");
        $name           = $this->param("name");
        $returnData     = array("Status"=>"Error","Message"=>"Domain not found");

        //check that domain belongs to whitelabel
        $postmark           = new Service_Postmark();
        $whitelabelDomains  = $postmark->getWhitelabelDomains($_SESSION['whitelabel_id']);
        foreach ($whitelabelDomains as $domains) {
            if($domains['domain']['name'] == $domain) {
                //if the domain belongs to the whitelabel, add the signature.
                $postmark->addSenderSignatures($_SESSION['whitelabel_id'], $domain,$fromUsername,$name);
                $returnData = $postmark->getWhitelabelDomains($_SESSION['whitelabel_id']);
            }
        }
        //if the request is successful, return new domain data, otherwise return error message
        $this->view("json",$returnData);
    }

    public function deleteSignature()
    {
        $signatureId            = $this->param("signature_id");
        $returnData             = array("Status"=>"Error", "Message"=>"Signature not found");
        $postmark               = new Service_Postmark();
        $whitelabelSignatures   = $postmark->getWhitelabelSignatures($_SESSION['whitelabel_id']);

        foreach ($whitelabelSignatures as $signature) {
            if ($signature['id'] == $signatureId && $signature['whitelabel_id'] == $_SESSION['whitelabel_id']) {
                $postmark->deleteSignature($_SESSION['whitelabel_id'], $signatureId);
                $returnData = $postmark->getWhitelabelDomains($_SESSION['whitelabel_id']);
            }
        }

        //if the request is successful, return new domain data, otherwise return error message
        $this->view("json",$returnData);
    }

    public function getSocialMediaLinks()
    {
        $socialMedia = new Service_Social_Media($_SESSION['whitelabel_id']);
        $links       = $socialMedia->getSocialMediaUrls();
        $this->view("json",$links);
    }

    public function updateSocialMediaLink()
    {
        $platform       = $this->param("platform");
        $url            = $this->param("newurl");
        $socialMedia    = new Service_Social_Media($_SESSION['whitelabel_id']);
        $socialMedia->updateUrl($platform,$url);
        WhitelabelSettings::updateValue($_SESSION['whitelabel_id'], "wlchecklist.socialmedia", "true");
        $this->view('json',$socialMedia->getSocialMediaUrls());
    }

    public function getSenderSignatures()
    {
        $postmark             = new Service_Postmark();
        $whitelabelSignatures = $postmark->getWhitelabelSignatures($_SESSION['whitelabel_id']);
        $this->view("json", $whitelabelSignatures);
    }

    public function getDefaultSignatures()
    {
        $postmark           = new Service_Postmark();
        $defaultSignatures  = $postmark->getDefaultSignatures($_SESSION['whitelabel_id']);
        $this->view("json", $defaultSignatures);
    }

    public function updateDefaultSenderSignature()
    {
        $slug        = $this->param("slug");
        $signatureId = $this->param("signature_id");
        $postmark    = new Service_Postmark();
        $postmark->swapDefaultSignature($_SESSION['whitelabel_id'], $slug, $signatureId);

        $defaultSignatures = $postmark->getDefaultSignatures($_SESSION['whitelabel_id']);
        $this->view("json", $defaultSignatures);
    }

    public function deleteDefaultSenderSignature()
    {
        $slug       = $this->param("slug");
        $postmark   = new Service_Postmark();

        $postmark->deleteDefaultSignature($_SESSION['whitelabel_id'],$slug);

        $defaultSignatures = $postmark->getDefaultSignatures($_SESSION['whitelabel_id']);
        $this->view("json", $defaultSignatures);
    }

    public function newMessage()
    {
        $externalMessageService = new Service_ExternalMessages();
        $messageId              = $externalMessageService->getNewMessageId($_SESSION['whitelabel_id'], $_SESSION['custid']);
        $details                = $externalMessageService->getMessageById($messageId, $_SESSION['whitelabel_id']);

        $this->view('json', $details);
    }

    public function addGroupToMessage()
    {
        $messageId  = $this->param("message_id");
        $groupId    = $this->param("group_id");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->addGroupToMessage($messageId, $_SESSION['whitelabel_id'], $groupId);

    }

    public function addStakeholderToMessage()
    {
        $messageId  = $this->param("message_id");
        $groupId    = $this->param("group_id");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->addStakeholdersToMessage($messageId, $_SESSION['whitelabel_id'], $groupId);
    }
    public function removeGroupFromMessage()
    {
        $messageId  = $this->param("message_id");
        $groupId    = $this->param("group_id");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->removeGroupFromMessage($messageId, $_SESSION['whitelabel_id'], $groupId);

    }

    public function removeStakeholderFromMessage()
    {
        $messageId  = $this->param("message_id");
        $groupId    = $this->param("group_id");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->removeStakeholderGroupFromMessage($messageId, $_SESSION['whitelabel_id'], $groupId);
    }

    public function changeMessageType()
    {
        $messageId  = $this->param("message_id");
        $type       = $this->param("type");

        $externalMessageService = new Service_ExternalMessages();
        if ($type == 'Email' || $type == 'email') {
            $externalMessageService->changeMessageToEmail($messageId, $_SESSION['whitelabel_id']);
        }
        if ($type == 'Text' || $type == 'text') {
            $externalMessageService->changeMessageToText($messageId, $_SESSION['whitelabel_id']);
        }
    }

    public function updateMessageScheduledTime()
    {
        $messageId  = $this->param("message_id");
        $sendTime   = $this->param("scheduled_time");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->updateScheduledSendTime($messageId, $_SESSION['whitelabel_id'], $sendTime);
    }

    public function updateMessageSubject()
    {
        $messageId  = $this->param("message_id");
        $subject    = $this->param("subject");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->updateSubjectLine($messageId, $_SESSION['whitelabel_id'], $subject);
    }

    public function updateMessageBody()
    {
        $messageId  = $this->param("message_id");
        $body       = $this->param("body", "POST");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->updateMessageBody($messageId, $_SESSION['whitelabel_id'], $body);
    }

    public function markMessageReadyToSend()
    {
        $messageId = $this->param("message_id");

        $externalMessageService = new Service_ExternalMessages();
        $externalMessageService->setReadyToSend($messageId, $_SESSION['whitelabel_id']);
    }
    
    
    /**
     * @method POST must use POST for this endpoint
     */
    public function addAttachmentToMessage()
    {
        $messageId = $this->param("message_id");
        
        //typically use $_FILES["image"]["tmp_name"] as the file being uploaded
        $newURL = (new Service_CDN())->uploadExternalMessageAttachment($_SESSION["whitelabel_id"], $messageId);
    }


    /**
     * List of external email and text messages that have not been sent yet.
     *
     * @param  string approval_required (optional) options are 'Yes','No', or 'All'.
     * @param  string type (optional) options are 'Email','Text', or 'All'.
     */
    public function getUnsentMessages()
    {
        $approvalNeeded = $this->param("approval_required");
        $type = $this->param("type");

        //clean optional variables
        if (!is_null($type)) {
            $type = ucfirst(strtolower($type));
        }
        if (!is_null($approvalNeeded)) {
            $approvalNeeded = ucfirst(strtolower($approvalNeeded));
        }
        if ($type != 'Email' && $type != 'Text' && $type != 'All') {
            $type = "All";
        }

        if ($approvalNeeded != 'Yes' && $approvalNeeded != 'No' && $approvalNeeded != 'All') {
            $approvalNeeded = "All";
        }


        $externalMessageService = new Service_ExternalMessages();
        $messages               = $externalMessageService->getPendingMessages($_SESSION['whitelabel_id'], $type, $approvalNeeded);

        $this->view("json",$messages);

    }

    public function getMessageById()
    {
        $messageId              = $this->param("message_id");
        $externalMessageService = new Service_ExternalMessages();
        $messages               = $externalMessageService->getMessageById($messageId, $_SESSION['whitelabel_id']);

        $this->view("json",$messages);
    }
    public function approveMessage()
    {
        $messageId              = $this->param("message_id");
        $approval               = ucfirst(strtolower($this->param("approval")));
        $externalMessageService = new Service_ExternalMessages();

        $externalMessageService->setApprovals($messageId, $_SESSION['whitelabel_id'], $approval, $_SESSION['custid']);

        $messages = $externalMessageService->getPendingMessages($_SESSION['whitelabel_id'], "All", "Yes");

        $this->view("json",$messages);
    }

    public function sentMessages()
    {
        $type   = ucfirst(strtolower($this->param("type")));
        $limit  = $this->param("limit");
        $since  = $this->param("since");
        $sort   = strtoupper($this->param("sort"));

        //set defaults
        if (!$limit) {
            $limit = 10;
        }
        if (!$since) {
            $since = 0;
        }
        if ($type != "Email" && $type != 'Text' && $type != "All") {
            $type = "All";
        }

        if ($sort != "ASC" && $sort != 'DESC') {
            $sort = "ASC";
        }

        //retrieve data
        $data = (new Service_ExternalMessages())->getSentMessages($_SESSION['whitelabel_id'], $type, $limit, $since, $sort);
        $this->view("json", $data);
    }

    public function getFirstOpenAndClicks()
    {
        $messageId = $this->param("message_id");

        $opens  = (new Service_ExternalMessages())->getFirstOpens($messageId, $_SESSION['whitelabel_id']);
        $clicks = (new Service_ExternalMessages())->getFirstClicks($messageId, $_SESSION['whitelabel_id']);

        $data = array(
            "opens"=> $opens,
            "clicks"=>$clicks
        );
        $this->view("json", $data);
    }

    public function getMyMessages()
    {
        //returns messages sent to this user and those sent/scheduled by this user
        $messages = (new Service_ExternalMessages())->getMyMessages($_SESSION['custid'], $_SESSION['whitelabel_id']);

        $this->view("json", $messages);
    }

    public function deleteMessage()
    {
        $messageId              = $this->param("message_id");
        $externalMessageService = new Service_ExternalMessages();
        $messages               = $externalMessageService->getMessageById($messageId, $_SESSION['whitelabel_id']);

        if ($messages['whitelabel_id'] == $_SESSION['whitelabel_id'] && $messages['sent_time'] == 0) {
            //delete message
            (new Service_ExternalMessages())->deleteMessage($messageId, $_SESSION['whitelabel_id']);
        }

        $this->view("json",[]);
    }
}