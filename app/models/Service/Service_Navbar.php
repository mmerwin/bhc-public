<?php


class Service_Navbar
{
    public static function getAllNavbarItems($custid, $whitelabel_id)
    {
        return (new DB_Navbar())->getAllowedNavbarItems($custid, $whitelabel_id);
    }
    public static function getAllNavbarParents($navbarItems)
    {
        return (new DB_Navbar())->getAllowedNavbarParents($navbarItems);
    }

}