<?php


class DB_Platform_Plans extends DataModel
{
    protected $schema = "platform_plans";

    public function getPlanById($planId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE plan_id = ? LIMIT 1", array($planId))->fetchArray();
    }

    public function getAllPlans()
    {
        return $this->DB->query("SELECT * FROM {$this->schema}")->fetchAll();
    }

}