<?php


class DB_Practice_Sessionplans extends DataModel
{
    protected $schema = "practice_sessionplans";

    public function updateSessionplan($whitelabel_id, $practiceId, $text)
    {
        $this->DB->query("REPLACE INTO {$this->schema} (practice_id, whitelabel_id, session_plan, last_updated) VALUES (?, ?, ?, ?)", array($practiceId, $whitelabel_id, $text, time()));
    }

    public function getSessionplan($whitelabel_id, $practiceId)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND practice_id = ? LIMIT 1", array($whitelabel_id, $practiceId))->fetchArray();
    }

}