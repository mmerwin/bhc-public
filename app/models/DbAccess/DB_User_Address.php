<?php


class DB_User_Address extends DataModel
{
    protected $schema = "user_address";

    public function addAddress($custid, $street, $street2, $city, $state, $zip)
    {
        $time = time();
        $this->DB->query("INSERT INTO {$this->schema} (custid, street, street_2, city, state, zip, created_at, last_updated) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", array($custid, $street, $street2, $city, $state, $zip, $time,$time));
    }

    public function getAddress($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ? LIMIT 1", array($custid))->fetchArray();
    }

    public function updateStreet($custid, $newStreet)
    {
        $this->DB->query("UPDATE {$this->schema} SET street = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($newStreet, time(), $custid));
    }

    public function updateStreet2($custid, $newStreet)
    {
        $this->DB->query("UPDATE {$this->schema} SET street_2 = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($newStreet, time(), $custid));
    }

    public function updateCity($custid, $newCity)
    {
        $this->DB->query("UPDATE {$this->schema} SET city = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($newCity, time(), $custid));
    }

    public function updateState($custid, $newState)
    {
        $this->DB->query("UPDATE {$this->schema} SET state = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($newState, time(), $custid));
    }

    public function updateZip($custid, $newZip)
    {
        $this->DB->query("UPDATE {$this->schema} SET zip = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($newZip, time(), $custid));
    }

    public function updateFullAddress($custid, $street, $street2, $city, $state, $zip)
    {
        $this->DB->query("UPDATE {$this->schema} SET street = ?, street_2 = ?, city = ?, state = ?, zip = ?, last_updated = ? WHERE custid = ? LIMIT 1", array($street, $street2, $city, $state, $zip, time(), $custid));
    }



}