-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2021 at 11:10 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `bhconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups_payment_schedules`
--

CREATE TABLE IF NOT EXISTS `groups_payment_schedules` (
                                                         `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
                                                         `whitelabel_id` int(11) NOT NULL,
                                                         `group_id` int(11) NOT NULL,
                                                         `amount` int(11) NOT NULL,
                                                         `due_date` int(11) NOT NULL,
                                                         PRIMARY KEY (`schedule_id`),
                                                         KEY `whitelabel_id` (`whitelabel_id`),
                                                         KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
