<?php


class DB_Regattacentral_Regattas extends DataModel
{
    protected $schema = "regattacentral_regattas";

    public function addRegatta($regattaId, $parentId, $name, $abbreviation, $startTime, $timezone, $checkPayableTo,
                               $billingAddress, $contactName, $contactEmail, $primaryHost, $regattaUrl, $hostOrdId,
                               $raceType, $venueId, $resultsUrl)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (regatta_id, parent_id, name, abbreviation, start_time, 
                          timezone, check_payable_to, billing_address, contact_name, contact_email, primary_host, regatta_url, 
                          host_org_id, race_type, venue_id, results_url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            array($regattaId, $parentId, $name, $abbreviation, $startTime, $timezone, $checkPayableTo, $billingAddress,
                $contactName, $contactEmail, $primaryHost, $regattaUrl, $hostOrdId, $raceType, $venueId, $resultsUrl));
    }

    public function getRegattasAttendedByCustid($custid)
    {
        return $this->DB->query("SELECT * FROM regattacentral_regattas WHERE regatta_id IN (
                                        SELECT distinct(regatta_id) FROM regattacentral_regatta_entries_participants 
                                        WHERE participant_id IN ( SELECT participant_id FROM regattacentral_participant_custid_map 
                                        WHERE custid = ? ) ) ORDER BY start_time DESC", array($custid))->fetchAll();
    }

}