--
-- Table structure for table `platform_plans`
--

CREATE TABLE `platform_plans` (
                                  `plan_id` int(11) NOT NULL,
                                  `name` varchar(50) NOT NULL,
                                  `default_price` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `platform_plans`
--

INSERT IGNORE INTO `platform_plans` (`plan_id`, `name`, `default_price`) VALUES
(1, 'Small Team', '15'),
(2, 'Standard', '35'),
(3, 'Professional', '45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `platform_plans`
--
ALTER TABLE `platform_plans`
    ADD PRIMARY KEY (`plan_id`);
