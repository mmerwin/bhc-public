<?php


class Permissions
{
    protected $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function addDefaultPermissions()
    {

    }

    public function getDefaultPermissionByDefaultGroupId($defaultGroupId)
    {
        return (new DB_Permissions())->getDefaultPermissionByDefaultGroupId($defaultGroupId);
    }

    public function addPermissionToGroup($permissionId, $groupId)
    {
        return (new DB_Permissions())->addGroupPermission($groupId, $permissionId);
    }

    public function getPermissionById($id)
    {
        return (new DB_Permissions())->getPermissionById($id);
    }
}