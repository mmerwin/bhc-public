<?php


class PracticeLocations
{
    private $whitelabel_id;

    public function __construct($whitelabel_id)
    {
        $this->whitelabel_id = $whitelabel_id;
    }

    public function getAllLocations()
    {
        $rawLocations       = (new DB_Practice_Locations())->getWhitelabelLocations($this->whitelabel_id);
        $formattedLocations = [];
        foreach ($rawLocations as $rawLocation) {
            $formattedLocations[] = [
                "location_id"   => $rawLocation["location_id"],
                "name"          => $rawLocation["name"],
                "address"       => $rawLocation["address"],
                "city"          => $rawLocation["city"],
                "state"         => $rawLocation["state"],
                "zipcode"       => $rawLocation["zipcode"],
                "fee"           => "$".$rawLocation["fee"],
                "notes"         => $rawLocation["notes"],
                "active"        => $rawLocation["active"]
            ];
        }
        return $formattedLocations;
    }

    public function getLocationById($locationId)
    {
        $rawLocation = (new DB_Practice_Locations())->getByLocationId($this->whitelabel_id, $locationId);
        if (!isset($rawLocation["location_id"])) {
            return [];
        } else {
            return [
                "location_id"   => $rawLocation["location_id"],
                "name"          => $rawLocation["name"],
                "address"       => $rawLocation["address"],
                "city"          => $rawLocation["city"],
                "state"         => $rawLocation["state"],
                "zipcode"       => $rawLocation["zipcode"],
                "fee"           => "$".$rawLocation["fee"],
                "notes"         => $rawLocation["notes"],
                "active"        => $rawLocation["active"]
            ];
        }
    }

    public function checkIfAffiliated($locationId)
    {
        $rawLocation = $this->getLocationById($locationId);
        if (is_null($locationId) || $rawLocation["location_id"] == $locationId) {
            return $locationId;
        } else {
            return null;
        }
    }

    public function addLocation($name, $address, $city, $state, $zip, $fee, $notes, $active)
    {
        (new DB_Practice_Locations())->addLocation($this->whitelabel_id, $name, $address, $city, $state, $zip, $fee, $notes, $active);
    }

    public function deactivateLocation($locationId)
    {
        (new DB_Practice_Locations())->updateActive($this->whitelabel_id, $locationId, "No");
    }

    public function activateLocation($locationId)
    {
        (new DB_Practice_Locations())->updateActive($this->whitelabel_id, $locationId, "Yes");
    }

    public function updateAddress($locationId, $address, $city, $state, $zip)
    {
        (new DB_Practice_Locations())->updateAddress($this->whitelabel_id, $locationId, $address, $city, $state, $zip);
    }

    public function updateName($locationId, $newName)
    {
        (new DB_Practice_Locations())->updateAttribute($this->whitelabel_id, $locationId, "name", $newName);
    }

    public function updateFee($locationId, $newFee)
    {
        (new DB_Practice_Locations())->updateAttribute($this->whitelabel_id, $locationId, "fee", $newFee);
    }

    public function updateNotes($locationId, $newNotes)
    {
        (new DB_Practice_Locations())->updateAttribute($this->whitelabel_id, $locationId, "notes", $newNotes);
    }

}