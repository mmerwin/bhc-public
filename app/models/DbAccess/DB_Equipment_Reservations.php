<?php


class DB_Equipment_Reservations extends DataModel
{
    protected $schema = "equipment_reservations";

    public function newReservation($whitelabel_id, $custid, $start_time, $end_time, $type)
    {
        $createdAt = time();
        $this->DB->query("INSERT INTO {$this->schema} (whitelabel_id, custid, start_time, end_time, type, created_at) VALUES (?, ?, ?, ?, ?, ?)", array($whitelabel_id, $custid, $start_time, $end_time, $type, $createdAt));
        //find reservation_id
        $reservation = $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? and created_at = ? ORDER BY reservation_id DESC LIMIT 1", array($whitelabel_id, $custid, $createdAt))->fetchArray();
        return $reservation['reservation_id'];
    }

    public function deleteReservation($reservationId, $custid, $whitelabel_id)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE reservation_id = ? AND custid = ? AND whitelabel_id = ? LIMIT 1", array($reservationId, $custid, $whitelabel_id));
    }

    public function getAllReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT er.reservation_id, er.whitelabel_id, er.start_time, er.end_time, er.type, er.created_at, er.custid, u.fname, u.lname, u.phone_number, l.email
                                                FROM {$this->schema} er
                                                LEFT JOIN users u
                                                ON u.custid = er.custid
                                                LEFT JOIN logins l
                                                ON l.custid = er.custid
                                                WHERE er.whitelabel_id = ? ORDER BY start_time ASC", array($whitelabel_id))->fetchAll();
    }

    public function getReservationById($whitelabel_id, $reservationId)
    {
        return $this->DB->query("SELECT er.reservation_id, er.whitelabel_id, er.start_time, er.end_time, er.type, er.created_at, er.custid, u.fname, u.lname, u.phone_number, l.email
                                                FROM {$this->schema} er
                                                LEFT JOIN users u
                                                ON u.custid = er.custid
                                                LEFT JOIN logins l
                                                ON l.custid = er.custid
                                                WHERE er.whitelabel_id = ? AND er.reservation_id = ? ORDER BY start_time ASC", array($whitelabel_id, $reservationId))->fetchArray();
    }

    public function getAllFutureReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT er.reservation_id, er.whitelabel_id, er.start_time, er.end_time, er.type, er.created_at, er.custid, u.fname, u.lname, u.phone_number, l.email
                                                FROM {$this->schema} er
                                                LEFT JOIN users u
                                                ON u.custid = er.custid
                                                LEFT JOIN logins l
                                                ON l.custid = er.custid
                                                WHERE er.whitelabel_id = ? AND start_time >= ?", array($whitelabel_id, time()))->fetchAll();
    }

    public function getAllPastReservationsByWhitelabel($whitelabel_id)
    {
        return $this->DB->query("SELECT er.reservation_id, er.whitelabel_id, er.start_time, er.end_time, er.type, er.created_at, er.custid, u.fname, u.lname, u.phone_number, l.email
                                                FROM {$this->schema} er
                                                LEFT JOIN users u
                                                ON u.custid = er.custid
                                                LEFT JOIN logins l
                                                ON l.custid = er.custid
                                                WHERE er.whitelabel_id = ? AND start_time < ?", array($whitelabel_id, time()))->fetchAll();
    }

    public function reservationsByCustid($whitelabel_id, $custid, $type = "private")
    {
        switch (strtolower($type)) {
            case "practice":
                return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND type = 'practice'", array($whitelabel_id, $custid))->fetchAll();
                break;
            case "private":
                return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND type = 'private'", array($whitelabel_id, $custid))->fetchAll();
                break;
            default:
                return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? ", array($whitelabel_id, $custid))->fetchAll();
                break;
        }

    }

    public function getReservationsByTime($whitelabel_id, $startTime, $endTime)
    {
        return $this->DB->query("SELECT er.*, erb.boat_id
                                FROM {$this->schema} er
                                LEFT JOIN equipment_reservations_boats erb
                                ON erb.reservation_id = er.reservation_id
                                WHERE ((start_time BETWEEN ? AND ?) OR (end_time BETWEEN ? AND ?) OR (start_time <= ? AND end_time >= ?)) AND er.whitelabel_id = ?",
                                array($startTime, $endTime, $startTime, $endTime, $startTime, $startTime, $whitelabel_id))->fetchAll();
    }

    public function sevenDayCount($whitelabel_id, $custid)
    {
        $minTime = time() - (86400 * 3.5);
        $maxTime = time() + (86400 * 3.5);
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND type='private' AND (end_time BETWEEN ? AND ?)", array($whitelabel_id, $custid, $minTime, $maxTime))->numRows();
    }

    public function fourteenDayCount($whitelabel_id, $custid)
    {
        $minTime = time() - (86400 * 7);
        $maxTime = time() + (86400 * 7);
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE whitelabel_id = ? AND custid = ? AND type='private' AND (end_time BETWEEN ? AND ?)", array($whitelabel_id, $custid, $minTime, $maxTime))->numRows();
    }
}