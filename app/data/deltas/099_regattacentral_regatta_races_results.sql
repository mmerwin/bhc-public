--
-- Table structure for table `regattacentral_regatta_races_results`
--

CREATE TABLE IF NOT EXISTS `regattacentral_regatta_races_results` (
                                                                      `race_id` int(11) NOT NULL,
                                                                      `regatta_id` int(11) NOT NULL,
                                                                      `event_id` int(11) NOT NULL,
                                                                      `entry_id` int(11) NOT NULL,
                                                                      `lane` int(11) NOT NULL,
                                                                      `display_number` varchar(10) NOT NULL,
                                                                      `distance` int(11) NOT NULL,
                                                                      `elapsed_time` int(11) NOT NULL,
                                                                      `split_time` int(11) NOT NULL,
                                                                      `margin_to_first` int(11) NOT NULL,
                                                                      `margin_to_previous` int(11) NOT NULL,
                                                                      `adjusted_time` int(11) NOT NULL,
                                                                      `adjusted_margin_to_first` int(11) NOT NULL,
                                                                      `adjusted_margin_to_previous` int(11) NOT NULL,
                                                                      `penalty_time` int(11) NOT NULL,
                                                                      `penalty_code` varchar(100) NOT NULL,
                                                                      `comment` text NOT NULL,
                                                                      PRIMARY KEY (`race_id`,`regatta_id`,`event_id`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;
