--
-- Table structure for table `user_payment_credits`
--

CREATE TABLE IF NOT EXISTS `user_payment_credits` (
                                                      `payment_credit_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                                                      `external_id` varchar(150) NOT NULL,
                                                      `custid` int(11) NOT NULL,
                                                      `whitelabel_id` int(11) NOT NULL,
                                                      `descr` varchar(200) NOT NULL,
                                                      `created_at` int(11) NOT NULL,
                                                      `posted_by` varchar(150) NOT NULL,
                                                      `amount_charged` int(11) NOT NULL,
                                                      `settled_amount` int(11) NOT NULL,
                                                      `receipt_url` text NOT NULL,
                                                      PRIMARY KEY (`payment_credit_id`),
                                                      KEY `custid` (`custid`),
                                                      KEY `whitelabel_id` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
