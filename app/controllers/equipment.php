<?php


class equipment extends Controller
{
    /**
     * @api
     */
    public function getManufacturers()
    {
        $manufacturers = Service_Equipment_Manufacturer::getAllManufacturers();
        $this->view("json", $manufacturers);
    }

    /**
     * @api
     */
    public function getBoatManufacturers()
    {
        $manufacturers = Service_Equipment_Manufacturer::getBoatManufacturers();
        $this->view("json", $manufacturers);
    }

    /**
     * @api
     */
    public function addBoat()
    {
        $status = array("Status"=>"Success");
        //check that required values are present and accurate
        if(is_null($this->param("boat_type")) || empty($this->param("boat_type"))){$status = array("Status"=>"Error", "Message"=>"Missing boat_type");}
        if(is_null($this->param("boat_name")) || empty($this->param("boat_name"))){$status = array("Status"=>"Error", "Message"=>"Missing boat_name");}
        if((is_null($this->param("manufacturer_id")) || empty($this->param("manufacturer_id"))) && $this->param("manufacturer_id") < 0){$status = array("Status"=>"Error", "Message"=>"Missing manufacturer_id");}
        if(is_null($this->param("model")) || empty($this->param("model"))){$status = array("Status"=>"Error", "Message"=>"Missing model");}
        if(is_null($this->param("serial")) || empty($this->param("serial"))){$status = array("Status"=>"Error", "Message"=>"Missing serial");}
        if(is_null($this->param("year_built")) || empty($this->param("year_built"))){$status = array("Status"=>"Error", "Message"=>"Missing year_built");}
        if(is_null($this->param("year_purchased")) || empty($this->param("year_purchased"))){$status = array("Status"=>"Error", "Message"=>"Missing year_purchased");}
        if(is_null($this->param("weight_min")) || empty($this->param("weight_min"))){$status = array("Status"=>"Error", "Message"=>"Missing weight_min");}
        if(is_null($this->param("weight_max")) || empty($this->param("weight_max"))){$status = array("Status"=>"Error", "Message"=>"Missing weight_max");}
        if(is_null($this->param("color")) || empty($this->param("color"))){$status = array("Status"=>"Error", "Message"=>"Missing color");}
        if(is_null($this->param("insured")) || empty($this->param("insured"))){$status = array("Status"=>"Error", "Message"=>"Missing insured");}
        $data = array(
            "hull_type"         => $this->param("boat_type"),
            "boat_name"         => $this->param("boat_name"),
            "manufacturer_id"   => $this->param("manufacturer_id"),
            "manufacturer"      => $this->param("manufacturer_other"),
            "model"             => $this->param("model"),
            "serial_number"     => $this->param("serial"),
            "year_built"        => $this->param("year_built"),
            "year_acquired"     => $this->param("year_purchased"),
            "min_weight"        => $this->param("weight_min"),
            "max_weight"        => $this->param("weight_max"),
            "color"             => $this->param("color"),
            "coxed"             => $this->param("coxed"),
            "purchase_price"    => $this->param("purchase_price"),
            "insured"           => $this->param("insured"),
            "insured_value"     => $this->param("insured_value")
        );
        if($status["Status"] == "Success") {
            $equipment = (new EquipmentModel($_SESSION['whitelabel_id']))->addBoat($data);
        }


        $this->view("json", $status);

    }

    /**
     * @api
     */
    public function getAllBoats()
    {
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $boats      = $equipment->getAllBoats();
        $this->view("json", $boats);
    }

    /**
     * @api
     */
    public function updateBoatDetails()
    {
        $boatId     = $this->param("boat_id");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $boatData   = $equipment->getBoatById($boatId);

        //set new attributes
        $boatName       = $this->param("boat_name");
        $serialNumber   = $this->param("serial");
        $color          = $this->param("color");
        $insured        = $this->param("insured");
        $insuredValue   = $this->param("insured_value");
        $status         = $this->param("status");

        //adding new values (if they exist) to an update array
        if (isset($boatName) && $boatName != "") {
            $data['boat_name'] = $boatName;
        }
        else {
            $data['boat_name'] = $boatData['boat_name'];
        }
        if (isset($serialNumber) && $serialNumber != "") {
            $data['serial'] = $serialNumber;} else{$data['serial'] = $boatData['serial'];
        }
        if (isset($color) && $color != "") {
            $data['color'] = $color;
        } else {
            $data['color'] = $boatData['color'];
        }
        if (isset($insured) && $insured != "" && ($insured == 'Yes' || $insured == 'No')) {
            $data['insured'] = $insured;
        } else {
            $data['insured'] = $boatData['insured'];
        }
        if (isset($insuredValue) && $insuredValue != "") {
            $data['insured_value'] = $insuredValue;
        } else {
            $data['insured_value'] = $boatData['insured_value'];
        }
        if (isset($status) && $status != "" && ($status == 'Available' || $status == 'Out Of Service')) {
            $data['status'] = $status;
        } else {
            $data['status'] = $boatData['status'];
        }

        $allBoats = $equipment->updateBoatData($boatId, $data);
        $this->view("json", $allBoats);

    }

    /**
     * @api
     */
    public function updateSeatConfiguration()
    {
        $boatId     = $this->param("boat_id");
        $seat       = $this->param("seat");
        $seatSide   = $this->param("seat_side");
        if ($seatSide == "Port" || $seatSide == "Starboard" || $seatSide == "Sculling" || $seatSide == "unknown") {
            if ($seat >=1 && $seat <=8) {
                $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
                $equipment->updateSeatConfig($boatId, $seat, $seatSide);
                $status = array("Success"=>"Seat Updated");
            }
            else {
                $status = array("Error"=>"Invalid Seat");
            }
        }
        else {
            $status = array("Error"=>"Invalid Configuration");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updateSpread()
    {
        $boatId = $this->param("boat_id");
        $seat   = $this->param("seat");
        $spread = $this->param("spread");

        if ($seat >=1 && $seat <=8 && $spread >= 0) {
            $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
            $equipment->updateSpread($boatId, $seat, $spread);
            $status = array("Success"=>"Seat Updated");
        }
        else {
            $status = array("Error"=>"Invalid Seat / Invalid Spread");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updatePitch()
    {
        $boatId     = $this->param("boat_id");
        $seat       = $this->param("seat");
        $field      = $this->param("field");
        $degrees    = $this->param("degrees");
        if ($field == "port_lateral_pitch_min" || $field == 'port_lateral_pitch_max'
            || $field == "starboard_lateral_pitch_min" || $field == "starboard_lateral_pitch_max"
            || $field == "port_front_pitch_min" || $field == "port_front_pitch_max"
            || $field == "starboard_front_pitch_min" || $field == "starboard_front_pitch_max")
        {
            $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
            $equipment->updatePitch($boatId, $seat, $field, $degrees);
            $status = array("Success"=>"Pitch updated!");

        } else{
            $status = array("Error"=>"Invalid field. See documentation for options");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updateSwivel()
    {
        $boatId = $this->param("boat_id");
        $seat   = $this->param("seat");
        $field  = $this->param("field");
        $inches = $this->param("inches");
        if ($field == "port_swivel_height_min" || $field == 'port_swivel_height_max' || $field == "starboard_swivel_height_min" || $field == "starboard_swivel_height_max") {
            $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
            $equipment->updatePitch($boatId, $seat, $field, $inches);
            $status = array("Success"=>"Swivel height updated!");

        } else {
            $status = array("Error"=>"Invalid field. See documentation for options");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updateFootstretcherPlacement()
    {
        $boatId     = $this->param("boat_id");
        $seat       = $this->param("seat");
        $placement  = $this->param("placement");

        if ($seat >=1 && $seat <=8 && $placement >= 0) {
            $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
            $equipment->updateFootPlacement($boatId, $seat, $placement);
            $status = array("Success"=>"Foot Stretcher Placement Updated");
        }
        else {
            $status = array("Error"=>"Invalid Seat / Invalid Distance");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updateFootAngle()
    {
        $boatId     = $this->param("boat_id");
        $seat       = $this->param("seat");
        $field      = $this->param("field");
        $degrees    = $this->param("degrees");
        if ($field == "foot_stretcher_angle_min" || $field == 'foot_stretcher_angle_max' || $field == "foot_stretcher_placement") {
            $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
            $equipment->updatePitch($boatId, $seat, $field, $degrees);
            $status = array("Success"=>"Angle updated!");

        } else {
            $status = array("Error"=>"Invalid field. See documentation for options");
        }
        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function updateTrackLength()
    {
        $boatId     = $this->param("boat_id");
        $seat       = $this->param("seat");
        $distance   = $this->param("distance");

        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateTrackLength($boatId, $seat, $distance);
        $status = array("Success"=>"Distance from pin updated!");

        $this->view("json", $status);
    }

    /**
     * @api
     */
    public function getAllEquipment()
    {
        $equipment      = new EquipmentModel($_SESSION['whitelabel_id']);
        $allEquipment   = [
            "boats"             => $equipment->getAllBoats(),
            "oars"              => $equipment->getAllOars(),
            "launches"          => [],
            "trailers"          => [],
            "coxing"            => [],
            "boathouse_misc"    => [],
            "ergs"              => $equipment->getAllErgs(),
            "spare_parts"       => [],
            "misc"              => []
        ];
        $this->view("json", $allEquipment);
    }

    /**
     * @api
     */
    public function addBoatCheckoutPermissionToGroup()
    {
        $boatId     = $this->param("boat_id");
        $groupId    = $this->param("group_id");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->addBoatCheckoutPermissions($boatId, $groupId);

        $boats = $equipment->getAllBoats();
        $this->view("json", $boats);
    }

    /**
     * @api
     */
    public function removeBoatCheckoutPermissionToGroup()
    {
        $boatId     = $this->param("boat_id");
        $groupId    = $this->param("group_id");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->removeBoatCheckoutPermissions($boatId, $groupId);

        $boats = $equipment->getAllBoats();
        $this->view("json", $boats);
    }

    /**
     * @api
     */
    public function getAllOars()
    {
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $oars       = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function addOarSet()
    {
        $name           = $this->param("name");
        $type           = $this->param("type");
        $descr          = $this->param("descr");
        $manufacturer   = $this->param("manufacturer");
        $model          = $this->param("model");
        $equipment      = new EquipmentModel($_SESSION['whitelabel_id']);

        $equipment->addOarSet($name, $type, $descr, $manufacturer, $model);

        $allOars = $equipment->getAllOars();
        $this->view('json', $allOars);
    }

    /**
     * @api
     */
    public function deleteOarSet()
    {
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $setId      = $this->param("oar_set_id");
        $equipment->deleteOarSet($setId);

        $allOars = $equipment->getAllOars();
        $this->view('json', $allOars);
    }

    /**
     * @api
     */
    public function addOar()
    {
        $setId      = $this->param("oar_set_id");
        $side       = $this->param("side");
        $descriptor = $this->param("descriptor");

        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->addOar($setId, $side, $descriptor);
        $oars       = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function deleteOar()
    {
        $setId = $this->param("oar_set_id");
        $oarId = $this->param("oar_id");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->deleteOar($setId, $oarId);

        $oars = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function updateOarStatus()
    {
        $oarId  = $this->param("oar_id");
        $status = $this->param("status");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateOarStatus($oarId, $status);

        $oars = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function updateOarDesc()
    {
        $oarId      = $this->param("oar_id");
        $descriptor = $this->param("descr");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateOarDescriptor($oarId, $descriptor);

        $oars = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function updateOarSetName()
    {
        $setId      = $this->param("oar_set_id");
        $newName    = $this->param("name");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateOarSetName($setId, $newName);

        $oars = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function updateOarSetDescr()
    {
        $setId      = $this->param("oar_set_id");
        $newDescr   = $this->param("descr");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateOarSetDescr($setId, $newDescr);

        $oars = $equipment->getAllOars();
        $this->view("json", $oars);
    }

    /**
     * @api
     */
    public function getAllErgs()
    {
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $ergs       = $equipment->getAllErgs();

        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function addErg()
    {
        $identifier     = $this->param("identifier");
        $make           = $this->param("make");
        $model          = $this->param("model");
        $yearPurchased  = $this->param("year_purchased");
        $location       = $this->param("location");
        $insured        = $this->param("insured");
        $insured_value  = $this->param("insured_value");

        $equipment = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->addErg($identifier, $make, $model, $yearPurchased, $location, $insured, $insured_value);

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function deleteErg()
    {
        $ergId      = $this->param("erg_id");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->deleteErg($ergId);

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function updateErgIdentifier()
    {
        $ergId          = $this->param("erg_id");
        $newIdentifier  = $this->param("identifier");
        $equipment      = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateIdentifier($ergId, $newIdentifier);

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function updateErgLocation()
    {
        $ergId      = $this->param("erg_id");
        $location   = $this->param("location");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateLocation($ergId, $location);

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function updateErgInsuredStatus()
    {
        $ergId          = $this->param("erg_id");
        $insured        = $this->param("insured");
        $insuredValue   = $this->param("insured_value");
        $equipment      = new EquipmentModel($_SESSION['whitelabel_id']);
        if ($insured != '') {
            $equipment->updateInsured($ergId, $insured);
        }

        if ($insuredValue != '') {
            $equipment->updateInsuredValue($ergId, $insuredValue);
        }

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    /**
     * @api
     */
    public function updateErgStatus()
    {
        $ergId      = $this->param("erg_id");
        $status     = $this->param("status");
        $equipment  = new EquipmentModel($_SESSION['whitelabel_id']);
        $equipment->updateStatus($ergId, $status);

        $ergs = $equipment->getAllErgs();
        $this->view("json", $ergs);
    }

    public function getIssues()
    {
        $equipmentIssuesFromPracticeSurveys = (new Service_Lineups())->getReportedIssuesByWhitelabel($_SESSION["whitelabel_id"]);
        $reportedIssues                     = (new EquipmentModel($_SESSION["whitelabel_id"]))->getEquipmentIssues();
        $data = [
            "practices_surveys" => $equipmentIssuesFromPracticeSurveys,
            "reported"          => $reportedIssues
        ];

        $this->view("json", $data);
    }

    public function addEquipmentIssue()
    {
        $descr = $this->param("description");
        (new EquipmentModel($_SESSION["whitelabel_id"]))->addIssue($_SESSION["custid"], $descr);
    }
}