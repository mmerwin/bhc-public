<?php


class DB_User_Additional_Contacts extends DataModel
{
    protected $schema = "user_additional_contacts";

    public function getContactsByUser($custid)
    {
        return $this->DB->query("SELECT * FROM {$this->schema} WHERE custid = ?", array($custid))->fetchAll();
    }

    public function addContactToUser($custid, $type, $contact)
    {
        $this->DB->query("INSERT IGNORE INTO {$this->schema} (custid, type, contact) VALUES (?, ?, ?)", array($custid, $type, $contact));
    }

    public function removeContactFromUser($custid, $type, $contact)
    {
        $this->DB->query("DELETE FROM {$this->schema} WHERE custid = ? AND type = ? AND contact = ? LIMIT 1", array($custid, $type, $contact));
    }

    public function getContactsByGroup($whitelabel_id, $groupId, $type = null)
    {
        if (!is_null($type)) {
            $this->DB->query("SELECT uac.contact
                                    FROM groups_members gm
                                    LEFT JOIN user_additional_contacts uac
                                    ON uac.custid = gm.custid
                                    WHERE gm.group_id = ? AND contact IS NOT NULL AND uac.type = ? AND gm.whitelabel_id = ?", array($groupId, $type, $whitelabel_id))->fetchAll();
        } else {
            $this->DB->query("SELECT uac.contact
                                    FROM groups_members gm
                                    LEFT JOIN user_additional_contacts uac
                                    ON uac.custid = gm.custid
                                    WHERE gm.group_id = ? AND contact IS NOT NULL AND gm.whitelabel_id = ?", array($groupId, $whitelabel_id))->fetchAll();
        }
    }

}