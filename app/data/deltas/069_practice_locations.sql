--
-- Table structure for table `practice_locations`
--

CREATE TABLE IF NOT EXISTS `practice_locations` (
                                                    `location_id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `whitelabel_id` int(11) NOT NULL,
                                                    `name` varchar(100) NOT NULL,
                                                    `address` varchar(100) NOT NULL,
                                                    `city` varchar(100) NOT NULL,
                                                    `state` varchar(2) NOT NULL,
                                                    `zipcode` int(11) NOT NULL,
                                                    `fee` varchar(20) NOT NULL,
                                                    `notes` text NOT NULL,
                                                    `active` varchar(5) NOT NULL DEFAULT 'Yes',
                                                    PRIMARY KEY (`location_id`),
                                                    KEY `fk_whitelabel_id_practice_locations` (`whitelabel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `practice_locations`
--
ALTER TABLE `practice_locations`
    ADD CONSTRAINT `fk_whitelabel_id_practice_locations` FOREIGN KEY (`whitelabel_id`) REFERENCES `whitelabels` (`whitelabel_id`);
