<?php


class Service_CredentialCheck
{
    public static function checkLoginCredentials($email, $password)
    {
        // find matching loginid
        $loginidRow = (new DB_Logins())->findLoginByEmailPassword($email, $password);
        if (!isset($loginidRow['loginid'])) {
            return array("loginid"=> null, "custid"=>null,"verified"=>false);
        }
        $loginid = $loginidRow['loginid'];
        if (!empty($loginid)) {
            return array("loginid"=> $loginid, "custid"=>$loginidRow['custid'],"verified"=>true);
        }
        else {
            return array("loginid"=> null, "custid"=>null,"verified"=>false);
        }
    }

    public static function checkAdminLoginCredentials($username, $password)
    {
        $adminidRow = (new DB_Admin_Logins())->findAdminIdByCredentials($username, $password);
        if (!isset($adminidRow['adminid'])) {
            return array("adminid"=> null, "username"=>null,"verified"=>false);
        }
        $adminid = $adminidRow['adminid'];
        if (!empty($adminid)) {
            return array("adminid"=> $adminid, "username"=>$adminidRow['username'],"verified"=>true);
        }
        else {
            return array("adminid"=> null, "username"=>null,"verified"=>false);
        }
    }

    public static function generateApiKey($username, $password, $type = "api")
    {
        if (strtolower($type) == "admin") {
            $loginArray = self::checkAdminLoginCredentials($username, $password);
            if ($loginArray["verified"]) {
                return Service_ApiTokenValidation::newToken($loginArray['adminid'], $type);
            }
        }
        // verify credentials match
        $loginArray = self::checkLoginCredentials($username, $password);
        if ($loginArray['verified']) {
            return Service_ApiTokenValidation::newToken($loginArray['custid'],  $type);
        }
    }

    public static function checkIfLoginExists($email)
    {
        return (new DB_Logins())->checkIfEmailExists($email);
    }

    public static function generatePasswordRecoveryToken($email)
    {
        return (new DB_Logins())->addResetToken($email);
    }

    public static function resetPasswordFromToken($resetToken, $resetTime, $newPassword)
    {
        (new DB_Logins())->resetPasswordFromToken($resetToken, $resetTime, $newPassword);
    }

}